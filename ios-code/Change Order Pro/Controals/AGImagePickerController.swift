//
//  AGPHPhotoLibrary.swift
//  BaseProject
//
//  Created by Ashvin Gudaliya on 11/15/17.
//  Copyright © 2017 AshvinGudaliya. All rights reserved.
//

import UIKit
import Photos
import PhotosUI

/*
 <!-- Photo Library -->
 <key>NSPhotoLibraryUsageDescription</key>
 <string>$(PRODUCT_NAME) photo use</string>
 
 <!-- Camera -->
 <key>NSCameraUsageDescription</key>
 <string>$(PRODUCT_NAME) camera use</string>
 */

open class AGImagePickerController: NSObject {
    
    var pickerController = UIImagePickerController()
    var isAllowsEditing: Bool = false
    
    var iPadSetup: UIView
    private var sourceType: UIImagePickerControllerSourceType?
    
    fileprivate var rootViewController: UIViewController & UIImagePickerControllerDelegate & UINavigationControllerDelegate
    var completionHandler: ((UIImage?, Bool?) -> Void)?
    
    @discardableResult
    init(with controller: UIViewController & UIImagePickerControllerDelegate & UINavigationControllerDelegate, type: UIImagePickerControllerSourceType? = nil, allowsEditing: Bool, iPadSetup: UIView){
        
        rootViewController = controller
        self.iPadSetup = iPadSetup
        self.sourceType = type
        super.init()
        
        checkPermission()
        
        isAllowsEditing = allowsEditing
        rootViewController = controller
    }
    
    func checkPermission() {
        switch sourceType {
        case .some(let type):
            switch type {
            case .photoLibrary:
                if PHPhotoLibrary.authorizationStatus() == .authorized {
                    self.presentPicker(with: type)
                }
                else{
                    self.photosAccessPermission()
                }
                
            default:
                if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == .authorized {
                    self.presentPicker(with: type)
                }
                else{
                    self.cameraAccessPermission()
                }
            }
            break
            
        default:
            if PHPhotoLibrary.authorizationStatus() == .notDetermined {
                self.photosAccessPermission()
            }
            else if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == .notDetermined {
                self.cameraAccessPermission()
            }
            else{
                let isPhotosPermision = PHPhotoLibrary.authorizationStatus() == .authorized
                let isCameraPermision = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == .authorized
                
                if isPhotosPermision && isCameraPermision {
                    setupAlertController()
                }
                else if isPhotosPermision {
                    self.presentPicker(with: .photoLibrary)
                }
                else if isCameraPermision {
                    self.presentPicker(with: .camera)
                }
            }
        }
    }
    
    func photosAccessPermission() {

        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized:
            checkPermission()
            break
            
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == .authorized{
                    self.checkPermission()
                }
            })
            break
            
        default:
            AGAlertBuilder(withAlert: "App Permission Denied", message: "To re-enable, please go to Settings and turn on Photo Library Service for this app.")
                .addDefaultAction(title: "Setting", handler: { _ in
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
                    } else {
                        UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
                    }
                })
                .show()
            break
        }
    }
    
    func cameraAccessPermission() {
        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        
        switch authStatus {
        case .authorized:
            checkPermission()
            break
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted) in
                if granted {
                    self.checkPermission()
                }
            })
            break
            
        default:
            AGAlertBuilder(withAlert: "App Permission Denied", message: "To re-enable, please go to Settings and turn on Photo Library Service for this app.")
                .addDefaultAction(title: "Setting", handler: { _ in
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
                    } else {
                        UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
                    }
                })
                .show()
            break
        }
    }
    
    func presentPicker(with sourceType: UIImagePickerControllerSourceType){
        
        guard UIImagePickerController.isSourceTypeAvailable(sourceType) else {
            return
        }
        
        pickerController.delegate = rootViewController
        pickerController.allowsEditing = isAllowsEditing
        pickerController.sourceType = sourceType
        
        DispatchQueue.main.async {
            self.rootViewController.present(self.pickerController, animated: true, completion: nil)
        }
    }
    
    func setupAlertController() {
        let alert = AGAlertBuilder(withActionSheet: "Choose Option", message: "Select an option to pick an image", iPadOpen: .sourceView(iPadSetup))
        
        if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
            alert.addAction(title: "Take Photo", style: .default, handler: { (alert) in
                self.presentPicker(with: .camera)
            })
        }
        
        alert.addAction(title: "Photo Library", style: .default, handler: { (alert) in
            self.presentPicker(with: .photoLibrary)
        })

        alert.addCancelAction()
        alert.show()
    }
    
    var isCameraSupports: Bool {
        if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
            return true
        }
        
        //no camera found -- alert the user.
        
        AGAlertBuilder(withAlert: "No Camera", message: "Sorry, this device has no camera")
            .addAction(title: "OK", style: .default)
            .show()
        return false
    }
}

