//
//  AGAlertController.swift
//  KZ
//
//  Created by KZ on 14/02/18.
//  Copyright © 2018 KZ. All rights reserved.
//

import UIKit

class AGAlertBuilder: UIAlertController {
    
    typealias AGAlertActionBlock = ((UIAlertAction) -> Swift.Void)
    
    var isVisible : Bool {
        return self.view.superview != nil
    }
    
    enum TapOnType {
        case outSide
        case none
        case autoHidden(time: TimeInterval)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience init(withAlert title: String?, message: String?) {
        self.init(title: title, message: message, preferredStyle: .alert)
    }
    
    convenience init(withActionSheet title: String?, message: String?, iPadOpen: ActionSheetOpen, directions: UIPopoverArrowDirection = .any) {
        self.init(title: title, message: message, preferredStyle: .actionSheet)
        self.setPopoverPresentationProperties(iPadOpen, directions: directions)
    }
    
    convenience init(withActionSheet title: String, message: String?) {
        self.init(title: title, message: message, preferredStyle: .actionSheet)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: UIAlertAction Methods
    @discardableResult
    func addAction(title: String?, style: UIAlertActionStyle, handler: AGAlertActionBlock? = nil) -> Self{
        let action = UIAlertAction(title: title, style: style, handler: handler)
        addAction(action)
        return self
    }
    
    @discardableResult
    func addDefaultAction(title: String, handler: AGAlertActionBlock? = nil) -> Self{
        let action = UIAlertAction(title: title, style: .default, handler: handler)
        addAction(action)
        return self
    }
    
    @discardableResult
    func addCancelAction(title: String = "Cancel", handler: AGAlertActionBlock? = nil) -> Self{
        let action = UIAlertAction(title: title, style: .cancel, handler: handler)
        addAction(action)
        return self
    }
    
    @discardableResult
    func addDestructiveAction(title: String, handler: AGAlertActionBlock? = nil) -> Self{
        let action = UIAlertAction(title: title, style: .destructive, handler: handler)
        addAction(action)
        return self
    }
    
    @discardableResult
    public func show(delayTime: TimeInterval? = nil, animated: Bool = true, completion: (() -> Void)? = nil) -> Self{
        
        //If a delay time has been set, delay the presentation of the alert by the delayTime
        if let time = delayTime {
            let dispatchTime = DispatchTime.now() + time
            DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                AppDelegate.topViewController()?.present(self, animated: animated, completion: completion)
            }
        }
        else{
            DispatchQueue.main.async {
                AppDelegate.topViewController()?.present(self, animated: animated, completion: completion)
            }
        }
        
        return self
    }
    
    func dissmiss(withType withHandler: @escaping (() -> Void), dismissType: TapOnType = .none) {
        switch dismissType {
            
        case .outSide:
            self.view.superview?.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(alertControllerBackgroundTapped))
            tap.numberOfTapsRequired = 1
            self.view.superview?.addGestureRecognizer(tap)
            break
            
        case .none: break
            
        case .autoHidden(time: let time):
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + time) {
                if self.isVisible{
                    self.dismiss(animated: true, completion: nil)
                    withHandler()
                }
            }
            break
        }
    }
    
    enum ActionSheetOpen{
        case sourceView(UIView)
        case sourceRect(CGRect)
        case barButtonItem(UIBarButtonItem)
        case none
    }
    
    @discardableResult
    public func setPopoverPresentationProperties( _ iPadOpen: ActionSheetOpen, directions: UIPopoverArrowDirection = .any) -> Self {
        
        if let poc = self.popoverPresentationController {
            switch iPadOpen {
            case .sourceView(let view):
                poc.sourceView = view
                poc.sourceRect = view.frame
                
            case .sourceRect(let rect):
                poc.sourceRect = rect
                
            case .barButtonItem(let item):
                poc.barButtonItem = item
                
            case .none:
                self.modalPresentationStyle = .none
                break
            }
            
            poc.permittedArrowDirections = directions
        }
        
        return self
    }
    
    @objc func alertControllerBackgroundTapped(){
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: Image added in alert and Action Sheet
extension AGAlertBuilder {
    func addImage(with image: UIImage, width: CGFloat, height: CGFloat){
        
        let displayImage = UIImageView(image: image)
        self.view.addSubview(displayImage)
        displayImage.translatesAutoresizingMaskIntoConstraints = false
        self.view.addConstraint(NSLayoutConstraint(item: displayImage, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: displayImage, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: displayImage, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: width))
        self.view.addConstraint(NSLayoutConstraint(item: displayImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: height))
    }
}

extension AppDelegate {
    public class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}
