//
//  AGProgress.swift
//  ShowBuddy
//
//  Created by Ashvin Gudaliya on 16/03/18.
//  Copyright © 2018 AshvinGudaliya. All rights reserved.
//

import UIKit

class AGProgress: NSObject {
    
    static var shared: AGProgress = AGProgress()
    
    var hub: MBProgressHUD!
    
    func showProgress(with title: String = "Loading..."){
        DispatchQueue.main.async {
            self.hideProgress()
            self.hub = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!, animated: true)
            self.hub.labelText = title
        }
    }
    
    func hideProgress(){
        if hub != nil {
            self.hub.hide(true)
            self.hub = nil
        }
    }
}

