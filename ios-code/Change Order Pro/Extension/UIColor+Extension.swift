//
//  UIColor+Ex.swift
//  KZ
//
//  Created by KZ on 12/6/17.
//  Copyright © 2017 KZ. All rights reserved.
//

import UIKit

extension UIFont {
    enum SFProTextType: String {
        case Bold
        case BoldItalic
        case Heavy
        case HeavyItalic
        case Light
        case LightItalic
        case Medium
        case MediumItalic
        case Regular
        case RegularItalic
        case SemiBold
        case SemiBoldItalic
    }
    
    enum SFProDisplayType: String {
        case Black
        case BlackItalic
        case Bold
        case BoldItalic
        case Heavy
        case HeavyItalic
        case Light
        case LightItalic
        case Medium
        case MediumItalic
        case Regular
        case RegularItalic
        case SemiBold
        case SemiBoldItalic
        case Thin
        case ThinItalic
        case Ultralight
        case UltralightItalic
    }
    
    class func sfProText(size: CGFloat, weight: SFProTextType) -> UIFont {
        if let font = UIFont(name: "SFProText-\(weight)", size: size) {
            return font
        }
        else{
            fatalError("Font not found SFProText-\(weight)")
        }
    }
    
    class func sfProDisplay(size: CGFloat, weight: SFProDisplayType) -> UIFont {
        if let font = UIFont(name: "SFProDisplay-\(weight)", size: size) {
            return font
        }
        else{
            fatalError("Font not found SFProDisplay-\(weight)")
        }
    }
}

extension UIColor {
    class gradient {
        static let landing: [UIColor] = [
            UIColor(hex: 0x00ffcc),
            UIColor(hex: 0x3399ff)
        ]
        
        static let contact: [UIColor] = [
            UIColor(hex: 0xA6DCFF),
            UIColor.white
        ]
    }
    
    static let buttonGreen = UIColor(hex: 0x00ffcc)
    static let buttonPink = UIColor(hex: 0xFE4052)
    static let buttonBlue = UIColor(hex: 0x3399FC)
    
    static let searchBarTextFiled = UIColor(hex: 0xF6FAFC)
    static let blueLiteText = UIColor(hex: 0xF6FAFC)
    
    static let grayBackColor = UIColor(hex: 0xEAEAEA)
    static let borderColor = UIColor(hex: 0xd6D6D6)
    static let borderLightColor = UIColor(hex: 0xF2F2F2)
    
    static let sepratorColor = UIColor(hex: 0xF2F2F2)
    static let lightPlaceHolder = UIColor(hex: 0xd6D6D6)
    
    static let blueLiteColor = UIColor(hex: 0x3399FC)
    static let searchBarTextFieldColor = UIColor(hex: 0xF2F2F2)
    static let adminColor = UIColor(hex: 0xf4ce00)
    
    static let searchbarBorderColor = UIColor(hex: 0xd6d6d6)
    static let searchBackgroundColor = UIColor(hex: 0xf4f4f4)
    static let searchPlaceholderTextColor = UIColor(hex: 0x5e5e5e)

}

// MARK: -

extension UIColor {

    /// Compare two colors
    ///
    /// - Parameters:
    ///   - color: color to be compared
    ///   - tolerance: tolerance (0.0 ~ 1.0)
    /// - Returns: result
    func isEqual(to color: UIColor, withTolerance tolerance: CGFloat = 0.0) -> Bool {

        var r1 : CGFloat = 0
        var g1 : CGFloat = 0
        var b1 : CGFloat = 0
        var a1 : CGFloat = 0
        var r2 : CGFloat = 0
        var g2 : CGFloat = 0
        var b2 : CGFloat = 0
        var a2 : CGFloat = 0

        getRed(&r1, green: &g1, blue: &b1, alpha: &a1)

        color.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)

        return fabs(r1 - r2) <= tolerance
            && fabs(g1 - g2) <= tolerance
            && fabs(b1 - b2) <= tolerance
            && fabs(a1 - a2) <= tolerance
    }
    
    convenience init(hex:Int, alpha:CGFloat = 1.0) {
        self.init(
            red:   CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8)  / 255.0,
            blue:  CGFloat((hex & 0x0000FF) >> 0)  / 255.0,
            alpha: alpha
        )
    }
    
    convenience init(withR red: CGFloat, g green: CGFloat, b blue: CGFloat, a alpha: CGFloat = 1.0) {
        self.init(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
}


// MARK: -

extension UIColor {

    class var random: UIColor {
        let randomRed = CGFloat(drand48())
        let randomGreen = CGFloat(drand48())
        let randomBlue = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }

    func darker(_ scale: CGFloat) -> UIColor {
        var h = CGFloat(0)
        var s = CGFloat(0)
        var b = CGFloat(0)
        var a = CGFloat(0)
        self.getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return UIColor(hue: h, saturation: s, brightness: b * scale, alpha: a)
    }

    func lighter(_ scale: CGFloat) -> UIColor {
        var h = CGFloat(0)
        var s = CGFloat(0)
        var b = CGFloat(0)
        var a = CGFloat(0)
        self.getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return UIColor(hue: h, saturation: s * scale, brightness: b, alpha: a)
    }

    func alpha(_ scale: CGFloat) -> UIColor {
        return withAlphaComponent(scale)
    }

    var alpha: CGFloat {
        var h = CGFloat(0)
        var s = CGFloat(0)
        var b = CGFloat(0)
        var a = CGFloat(0)
        self.getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return a
    }

}

extension UIColor {

    var components: UnsafePointer<CGFloat> { get { return cgColor.__unsafeComponents! } }

    var cRed: CGFloat { get { return components[0] } }

    var cGreen: CGFloat { get { return components[1] } }

    var cBlue: CGFloat { get { return components[2] } }

    func whiter(_ scale: CGFloat) -> UIColor {
        return UIColor(
            red: cRed * scale,
            green: cGreen * scale,
            blue: cBlue,
            alpha: 1.0
        )
    }
}

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

struct GradientPoint {
    var location: CGFloat
    var color: UIColor
}

extension UIImage {
    convenience init?(size: CGSize = CGSize(width: 1, height: 1) , gradientPoints: [GradientPoint]) {
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        
        guard let context = UIGraphicsGetCurrentContext() else { return nil }       // If the size is zero, the context will be nil.
        guard let gradient = CGGradient(colorSpace: CGColorSpaceCreateDeviceRGB(), colorComponents: gradientPoints.compactMap { $0.color.cgColor.components }.flatMap { $0 }, locations: gradientPoints.map { $0.location }, count: gradientPoints.count) else {
            return nil
        }
        
        context.drawLinearGradient(gradient, start: CGPoint.zero, end: CGPoint(x: 0, y: size.height), options: CGGradientDrawingOptions())
        guard let image = UIGraphicsGetImageFromCurrentImageContext()?.cgImage else { return nil }
        self.init(cgImage: image)
        defer { UIGraphicsEndImageContext() }
    }
}
