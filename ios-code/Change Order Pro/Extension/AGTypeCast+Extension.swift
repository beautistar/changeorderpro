//
//  AGTypeCast+Extension.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 03/07/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit

extension NSArray {
    public func obj(_ atIndex: Int) -> Element? {
        guard atIndex >= 0 else { return nil }
        guard atIndex < count else { return nil }
        return self[atIndex]
    }
}

extension NSMutableArray {
    public override func obj(_ atIndex: Int) -> Element? {
        guard atIndex >= 0 else { return nil }
        guard atIndex < count else { return nil }
        return self[atIndex]
    }
}

extension NSMutableArray {
    @discardableResult
    public func replaceObject(atIndex: Int, with: Any) -> Element? {
        guard atIndex >= 0 else { return nil }
        guard atIndex < count else { return nil }
        
        self.replaceObject(at: atIndex, with: with)
        return [atIndex]
    }
}

extension Array {
    public func obj(_ atIndex: Int) -> Element? {
        guard atIndex >= 0 else { return nil }
        guard atIndex < count else { return nil }
        return self[atIndex]
    }
}

extension String {
    var isDummy: Bool {
        return self == "Dummy"
    }
    
    public var isNull: Bool {
        return self.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty
    }

    public var isNotNull: Bool { return !self.isNull }
}
