//
//  Environment.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 07/08/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit

struct Environment {
    private static let production: Bool = {
        #if DEBUG
        return false
        #else
        return true
        #endif
    }()
    
    static let isDebug: Bool = {
        return !Environment.production
    }()
    
    static let isSimulator: Bool = {
        
        var isSim = false
        #if targetEnvironment(simulator)
        isSim = true
        #endif
        return isSim
    }()
    
    static let isDeveloperMode: Bool = {
        if Environment.isSimulator {
            return false
        }
        else if Environment.isDebug {
            return false
        }
        return false
    }()
}
