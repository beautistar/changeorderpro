//
//  AlmofireHelper.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 06/08/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire

class AlamofireHelper: NSObject {
    static var simple: SessionManager {
        let dataSession = Alamofire.SessionManager.default
        dataSession.session.configuration.timeoutIntervalForRequest = 10
        return dataSession
    }
    
    static var multipart: SessionManager{
        let dataSession = Alamofire.SessionManager.default
        dataSession.session.configuration.timeoutIntervalForRequest = 200
        return dataSession
    }
}

extension DataResponse {
    func logShow() {
        guard Environment.isDebug else {
            return
        }
        
        switch result {
        case .success: break
            
        case .failure(let error):
            if error._code == 401 {
                UserDetails.logoutApiCalled()
            }
            else{
                AgLog.debug("===========Error===========")
                AgLog.debug("Error Code: \(response?.url?.absoluteString ?? "")")
                AgLog.debug("Error Code: \(error._code)")
                AgLog.debug("Error Messsage: \(error.localizedDescription)")
                if error._code == 4 {
                    if let data = data, let str = String(data: data, encoding: String.Encoding.utf8){
                        if let url = response?.url, Environment.isDeveloperMode {
                            let messageBody = "\(self.request?.string ?? ""))"
                            + "\n\n Error string:\(error.localizedDescription)"
                            + "\n\n Error :\(error)"
                            
                            logw(str, subject: url.lastPathComponent, url: messageBody)
                        }
                    }
                }
                AgLog.debug(error)
                AgLog.debug("===========================")
            }
        }
    }
    
    var isSuccess: Bool {
        if let dicServer = self.result.value as? NSDictionary {
            let status = dicServer.string(forKey: "status")
            if status == "success" {
                return true
            }
        }
        return false
    }
    
    var message: String {
        if let dicServer = self.result.value as? NSDictionary {
            if let m = dicServer.object(forKey: "message") as? String, m.isNotNull {
                return m
            }
            else if let mArr = dicServer.object(forKey: "message") as? [AnyObject] {
                if let m = mArr.first as? String {
                    return m
                }
            }
            
            return dicServer.string(forKey: "message")
        }
        return ""
    }
}
