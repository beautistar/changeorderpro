//
//  Constants.swift
//  ChartHopper
//
//  Created by MobiSharnam on 15/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import Foundation
import Alamofire


//OLD
//let kBaseURL =  "http://changeorder.centralschoolofphotography.com/"

//NEW
let kBaseURL =  "https://changeorder.pro/"
//let kBaseURL = "http://192.168.1.6:8888/ChangeOrderPRO/"
//let kBaseURL = "http://192.168.43.128:8888/ChangeOrderPRO/"

var preview_Url = ""

var arry_siteContactList_globle = NSMutableArray()
var array_myOwnProjects = NSMutableArray()
var array_myProjectsID = NSMutableArray()

var current_lineItemNumber = 0
var total_ItemsAdded = 0

var projName = ""

var chng_ordID = ""

//var arry_ChangeItems = NSMutableArray()
var arry_allOrderID = NSMutableArray()
var prev_Pdf = ""
var selected_projID = ""

var isCostSet = true

//Edit order Itemarry_allOrderID

var arry_EditChangeItems = NSMutableArray()


class Constants {
    
    // MARK: List of Constants
    static let STATUS_APPROVED = ChangeOrderStatus.approved.rawValue
    static let STATUS_ASSIGNED = ChangeOrderStatus.assigned.rawValue
    static let STATUS_SAVED = ChangeOrderStatus.saved.rawValue
    static let STATUS_NEW = ChangeOrderStatus.new.rawValue
    static let STATUS_DENIED = ChangeOrderStatus.denied.rawValue
    static let STATUS_SUBMITTED = ChangeOrderStatus.submitted.rawValue
    static let STATUS_DELETED = ChangeOrderStatus.deleted.rawValue
    static let STATUS_COMPLETED = ChangeOrderStatus.completed.rawValue
    static let STATUS_REBILLED = ChangeOrderStatus.rebilled.rawValue
    static let REMOTEAPPROVER = "RemoteApprover"
    static let CUSTOMER = "Customer"
    static let ASSIGNER = "AssignOther"
}

enum ChangeOrderStatus: String {
    case approved
    case assigned
    case saved
    case denied
    case new
    case submitted
    case deleted
    case completed
    case rebilled
}

func resetAllDeclaredValue(){
    
    AgLog.debug("reseteddd all uni value")
//    arry_ChangeItems = NSMutableArray()
    arry_allOrderID = NSMutableArray()
    prev_Pdf = ""
    chng_ordID = ""
    isCostSet = true
}

struct TableRowActionType {
    static var edit = "\u{270D}"
    static var arrow = "\u{2191}"
    static var check = "\u{2713}"
    static var delete = "\u{1F5d1}"
    
    static var approve = TableRowActionType.delete//"\u{f4fc}"
    static var deny = TableRowActionType.check//"\u{f235}"
}








