//
//  TableViewCell.swift
//  BaseProject
//
//  Created by Ashvin Gudaliya on 12/6/17.
//  Copyright © 2017 AshvinGudaliya. All rights reserved.
//

import UIKit

extension NSDictionary{
    
    func string(forKey: String) -> String{
        if let string = self.value(forKey: forKey) as? String{
            return string
        }
        else if let string = self.value(forKey: forKey) as? NSString{
            return string as String
        }
        else if let obj = self.object(forKey: forKey), obj is NSNull {
            return ""
        }
        else if let string = self.value(forKey: forKey) {
            return String(describing: string).replacingOccurrences(of: "<null>", with: "")
        }
        AgLog.debug("[AGTypeCast] String \(forKey) is null Value")
        return ""
    }
    
    func number(forKey: String) -> NSNumber {
        
        if let integer = self.value(forKey: forKey) as? NSNumber {
            return integer
        }
        else if let integer = self.value(forKey: forKey)  as? String {
            if let number = NumberFormatter().number(from: integer) {
                return number
            }
        }
        
        return 0
    }
    
    func integer(forKey: String) -> Int {
        if let integer = (self.value(forKey: forKey) as AnyObject).integerValue{
            return integer
        }
        else if let integer = (self.value(forKey: forKey) as? NSString)?.intValue{
            return Int(integer)
        }
        else if let integer = (self.value(forKey: forKey) as AnyObject).int32Value{
            return Int(integer)
        }
        
        AgLog.debug("[AGTypeCast] Integer \(forKey) is null Value")
        return self.number(forKey: forKey).intValue
    }
    
    func obj(forKey aKey: Any, defaultValue v: Any = "") -> Any {
        if let obj = self.object(forKey: aKey) {
            return obj
        }
        
        return v
    }
    
    func float(forKey: String) -> Float{
        
        if let float = self.value(forKey: forKey) as? Float{
            return float
        }
        else if let float = self.value(forKey: forKey) as? CGFloat{
            return Float(float)
        }
        
        AgLog.debug("[AGTypeCast] Float \(forKey) is null Value")
        return self.number(forKey: forKey).floatValue
    }
    
    func bool(forKey: String, defaultValue: Bool = false) -> Bool{
        
        if let bool = self.value(forKey: forKey) as? Bool {
            return bool
        }
        else if let bool = self.value(forKey: forKey) as? NSString {
            if bool == "1" ||  bool.lowercased == "true" {
                return true
            }
            return false
        }
        else if let bool = self.value(forKey: forKey) as? NSNumber{
            if bool == 1{
                return true
            }
            return false
        }

        AgLog.debug("[AGTypeCast] Bool \(forKey) is null Value")
        return defaultValue
    }
    
    func dictionary(forKey: String) -> NSDictionary{
    
        if let dictionary = self.value(forKey: forKey) as? NSDictionary{
            return dictionary
        }
        else if let dictionary = self.value(forKey: forKey) as? Dictionary<AnyHashable, Any> {
            return dictionary as NSDictionary
        }
        else if let dictionary = (self.value(forKey: forKey) as AnyObject) as? NSDictionary {
            return dictionary
        }
        
        AgLog.debug("[AGTypeCast] Dictionary \(forKey) is null Value")
        return NSDictionary()
    }
    
    func array(forKey: String) -> NSArray {
        if let array = self.value(forKey: forKey) as? NSArray{
            return array
        }
        else if let array = self.value(forKey: forKey) as? [Any]{
            return array as NSArray
        }
        
        AgLog.debug("[AGTypeCast] Array \(forKey) is null Value")
        return NSArray()
    }
    
    func isHas(forKey: String) -> Bool{
        guard ((self.value(forKey: forKey) as? NSNull) == nil) else {
            return false
        }
        return self.value(forKey: forKey) != nil
    }
}

extension NSArray{
    func dictionary(at: NSInteger) -> NSDictionary {
        if at < self.count {
            if let dict = self.object(at: at) as? NSDictionary{
                return dict
            }
            else{
                AgLog.debug("[AGTypeCast] Dictionary \(at) is null Value")
                return NSDictionary()
            }
        }
        else{
            fatalError("Array out of index bound :- \(at)")
        }
    }
    
    var mutable: NSMutableArray {
        return NSMutableArray(array: self)
    }
}


extension String {
    var dimension: String {
        return Double(self)?.dimension ?? "0.00"
    }
}

extension Int {
    var dimension: String {
        return Float(self).dimension
    }
}

extension Float {
    var dimension: String {
        return String(format: "%.2f", self)
    }
}

extension Double {
    var dimension: String {
        return NSNumber(floatLiteral: self).floatValue.dimension
    }
}

