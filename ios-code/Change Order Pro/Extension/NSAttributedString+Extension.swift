//
//  NSAttributedString.swift
//  BaseProject
//
//  Created by Ashvin Gudaliya on 12/6/17.
//  Copyright © 2017 AshvinGudaliya. All rights reserved.
//

import UIKit

extension String {
    var attribute: NSAttributedString {
        return NSAttributedString(string: self)
    }
}

extension NSAttributedString {

    var range: NSRange {
        get {
            return (self.string as NSString).range(of: self.string)
        }
    }
    
    public func font(_ font: UIFont?) -> NSAttributedString {
        guard let copy = self.mutableCopy() as? NSMutableAttributedString else { return self }

        copy.addAttributes([NSFontAttributeName: font ?? UIFont.boldSystemFont(ofSize: UIFont.systemFontSize)], range: range)
        return copy
    }
    
    public func underline(_ strikeThroughStyle: NSUnderlineStyle) -> NSAttributedString {
        guard let copy = self.mutableCopy() as? NSMutableAttributedString else { return self }
        
        copy.addAttributes([NSUnderlineStyleAttributeName: strikeThroughStyle.rawValue], range: range)
        return copy
    }

    
    public func foreground(_ color: UIColor) -> NSAttributedString {
        guard let copy = self.mutableCopy() as? NSMutableAttributedString else { return self }
        
        copy.addAttributes([NSForegroundColorAttributeName: color], range: range)
        return copy
    }
}

public func += (left: inout NSAttributedString, right: NSAttributedString) {
    let ns = NSMutableAttributedString(attributedString: left)
    ns.append(right)
    left = ns
}

public func + (left: NSAttributedString, right: NSAttributedString) -> NSAttributedString {
    let ns = NSMutableAttributedString(attributedString: left)
    ns.append(right)
    return ns
}
