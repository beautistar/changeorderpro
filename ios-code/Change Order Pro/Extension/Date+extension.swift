//
//  Date+extension.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 24/08/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit

extension Date {
    
    static let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter
    }()
    
    static func getCurrentDate(date: Date = Date()) -> String {
        return dateFormatter.string(from: date)
    }
}

extension String {
    func toDate() -> Date? {
        return Date.dateFormatter.date(from: self)
    }
}

extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
    
    static var genrateOfflineId: String {
        return "\(String(Date().ticks))_offline"
    }
    
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
