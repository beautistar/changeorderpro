//
//  UITableView+RefreshControl.swift
//  BaseProject
//
//  Created by AshvinGudaliya on 12/02/18.
//  Copyright © 2018 AshvinGudaliya. All rights reserved.
//

import UIKit

extension UITableView {
    private struct AssociatedKeys {
        static var ActionKey = "UIRefreshControlActionKey"
    }
    
    private class ActionWrapper {
        let action: RefreshControlAction
        init(action: @escaping RefreshControlAction) {
            self.action = action
        }
    }
    
    typealias RefreshControlAction = ((UIRefreshControl) -> Void)
    
    var pullToRefresh: (RefreshControlAction)? {
        set(newValue) {
            agRefreshControl.removeTarget(self, action: #selector(refreshAction(_:)), for: .valueChanged)
            var wrapper: ActionWrapper? = nil
            if let newValue = newValue {
                wrapper = ActionWrapper(action: newValue)
                agRefreshControl.addTarget(self, action: #selector(refreshAction(_:)), for: .valueChanged)
            }
            
            objc_setAssociatedObject(self, &AssociatedKeys.ActionKey, wrapper, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        get {
            guard let wrapper = objc_getAssociatedObject(self, &AssociatedKeys.ActionKey) as? ActionWrapper else {
                return nil
            }
            
            return wrapper.action
        }
    }
    
    var agRefreshControl: UIRefreshControl {
        if #available(iOS 10.0, *) {
            if let refreshView = self.refreshControl {
                return refreshView
            }
            else{
                self.refreshControl = UIRefreshControl()
                return self.refreshControl!
            }
        }
        else{
            if let refreshView = backgroundView as? UIRefreshControl {
                return refreshView
            }
            else{
                backgroundView = UIRefreshControl()
                return UIRefreshControl()
            }
        }
    }
    
    override func endRefreshing() {
        self.agRefreshControl.endRefreshing()
    }
    
    override func beginRefreshing() {
        self.agRefreshControl.beginRefreshing()
    }
    
    @objc private func refreshAction(_ refreshControl: UIRefreshControl) {
        if let action = pullToRefresh {
            action(refreshControl)
        }
    }
}

extension UIScrollView {
    private struct AssociatedKeys {
        static var ActionKey = "UIRefreshControlActionKey"
    }
    
    private class ActionWrapper {
        let action: RefreshControlAction
        init(action: @escaping RefreshControlAction) {
            self.action = action
        }
    }
    
    typealias RefreshControlAction = ((UIRefreshControl) -> Void)
    
    var pullToRefreshScroll: (RefreshControlAction)? {
        set(newValue) {
            agRefreshControl.removeTarget(self, action: #selector(refreshScrollViewAction(_:)), for: .valueChanged)
            var wrapper: ActionWrapper? = nil
            if let newValue = newValue {
                wrapper = ActionWrapper(action: newValue)
                agRefreshControl.addTarget(self, action: #selector(refreshScrollViewAction(_:)), for: .valueChanged)
            }
            
            objc_setAssociatedObject(self, &AssociatedKeys.ActionKey, wrapper, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        get {
            guard let wrapper = objc_getAssociatedObject(self, &AssociatedKeys.ActionKey) as? ActionWrapper else {
                return nil
            }
            
            return wrapper.action
        }
    }
    
    func beginRefreshing() {
        self.agRefreshControl.beginRefreshing()
    }
    
    func endRefreshing(){
        self.agRefreshControl.endRefreshing()
    }
    
    private var agRefreshControl: UIRefreshControl {
        if #available(iOS 10.0, *) {
            if let refreshView = self.refreshControl {
                return refreshView
            }
            else{
                self.refreshControl = UIRefreshControl()
                return self.refreshControl!
            }
        }
        else{
            
            for view in self.subviews {
                if let refreshView = view as? UIRefreshControl {
                    return refreshView
                }
            }
            
            let refreshControl = UIRefreshControl()
            self.addSubview(refreshControl)
            return refreshControl
        }
    }
    
    @objc private func refreshScrollViewAction(_ refreshControl: UIRefreshControl) {
        if let action = pullToRefreshScroll {
            action(refreshControl)
        }
    }
}
