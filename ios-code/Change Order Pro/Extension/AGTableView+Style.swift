//
//  AGTableView.swift
//  BaseProject
//
//  Created by Ashvin Gudaliya on 12/22/17.
//  Copyright © 2017 AshvinGudaliya. All rights reserved.
//

import UIKit

class AGTableView: UITableView {
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        defaultInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        defaultInit()
    }
    
    func defaultInit(){
        self.keyboardDismissMode = .onDrag
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        self.tableFooterView = UIView(frame: .zero)
        self.tableHeaderView = UIView(frame: .zero)
        self.sectionFooterHeight = 0
        self.sectionHeaderHeight = 0
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        if self.heightConstraint != nil {
            self.heightConstraint?.constant = self.contentSize.height
        }
        else if self.widthConstraint != nil {
            self.widthConstraint?.constant = self.contentSize.width
        }
        else{
            sizeToFit()
            AgLog.debug("Set a heightConstraint to set cocontentSize with same")
        }
    }
}

extension UITableView {
    
    public func estimatedRowHeight(_ height: CGFloat) {
        self.rowHeight = UITableViewAutomaticDimension
        self.estimatedRowHeight = height
    }
    
    @discardableResult
    func registerNib(_ cellClass: UITableViewCell.Type) -> Self{
        let id = String(describing: cellClass.self)
        let nib = UINib(nibName: id, bundle: nil)
        register(nib, forCellReuseIdentifier: id)
        return self
    }
    
    @discardableResult
    func register(_ cellClass: Swift.AnyClass) -> Self{
        register(cellClass, forCellReuseIdentifier: String(describing: cellClass.self))
        return self
    }
}

extension UITableView{
    func dequeueReusableCell<T: UITableViewCell>(withClassIdentifier cell: T.Type, for indexPath: IndexPath) -> T {
        if let Cell = self.dequeueReusableCell(withIdentifier: String(describing: cell.self), for: indexPath) as? T{
            return Cell
        }
        fatalError( String(describing: cell.self))
    }
    
    func dequeueReusableCell<T : UITableViewCell>(withClassIdentifier cell: T.Type) -> T {
        if let Cell = dequeueReusableCell(withIdentifier: String(describing: cell.self)) as? T {
            return Cell
        }
        fatalError( String(describing: cell.self))
    }
}
