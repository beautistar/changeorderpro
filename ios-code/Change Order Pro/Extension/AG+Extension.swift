//
//  AG+Extension.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 03/07/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire

extension UITextField {
    
    public var isNull: Bool    {
        if let t = self.text {
            return t.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty
        }
        return false
    }
    
    public var isValidEmail: Bool {
        if (text == nil || (text?.isEmpty)!) { return false }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text)
    }
}

extension UITextView {

    public var isNull: Bool    {
        if let t = self.text {
            return t.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty
        }
        return false
    }
}

extension UIViewController{
    
    func barButton(with image: UIImage, selector: Selector) -> UIBarButtonItem{
        let homeBtn = UIButton(frame: CGRect.zero)
        homeBtn.addTarget(self, action: selector, for: .touchUpInside)
        homeBtn.setImage(image, for: .normal)
        
        homeBtn.translatesAutoresizingMaskIntoConstraints = false
        homeBtn.widthAnchor.constraint(equalToConstant: 25).isActive = true
        homeBtn.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        let homeBarBtn = UIBarButtonItem(customView: homeBtn)
        homeBarBtn.action = selector
        return homeBarBtn
    }
}

extension NSArray {
    var dict: [NSDictionary] {
        return self as? [NSDictionary] ?? []
    }
}

extension NSMutableArray {
    override var dict: [NSDictionary] {
        return self as? [NSDictionary] ?? []
    }
    
    public func removeDuplicates() -> NSMutableArray{
        return NSMutableArray(array: NSSet(array: self as? [Any] ?? []).allObjects)
    }
}

extension Data {
    var toJson: String {
        return String(data: self, encoding: String.Encoding.utf8) ?? ""
    }
}

extension UIImage {
    func resizeImage(newWidth: CGFloat) -> UIImage {
        
//        let scale = newWidth / self.size.width
        //   let newHeight = image.size.height * scale
        
        let newHeight: CGFloat =  426.66
        
        let newSize: CGSize = CGSize(width: newWidth, height: newHeight)
        
        AgLog.debug("new size of image is \(newSize)")
        
        UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
        self.draw(in: CGRect(origin: CGPoint.zero, size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension MultipartFormData {
    public func append(_ image: UIImage?, withName name: String, status: String) {
        
        if let img = image, let i = UIImageJPEGRepresentation(img, 1), status.lowercased() == ImageFlag.selected.lowercased() {
            
            self.append(i, withName: name, fileName: "\(name).jpg", mimeType: "image/jpeg")
        }
        else{
            if let str = "".data(using: .utf8) {
                self.append(str, withName: "image1")
            }
        }
    }
}

public extension Array where Element: NSDictionary {
    
    // remove duplicate element
    public mutating func removeDuplicates() {
        
        self = self.reduce([]) {
            $0.contains($1) ? $0 : $0 + [$1]
        }
    }
}
