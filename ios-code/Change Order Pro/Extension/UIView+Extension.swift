//
//  UIView+Extension.swift
//  BaseProject
//
//  Created by Ashvin Gudaliya on 24/06/18.
//  Copyright © 2018 AshvinGudaliya. All rights reserved.
//

import UIKit

extension UIView {
    
    /// Load view from nib. Note: Nib name must be equal to the class name.
    ///
    /// - parameter view:    The name of the nib file, which need not include the .nib extension
    /// - parameter owner:   The object to assign as the nib’s File's Owner object
    /// - parameter options: options
    ///
    /// - returns: view
    class func loadFromNibAndClass<T : UIView>(_ view: T.Type, owner: Any? = nil, options: [AnyHashable : Any]? = nil) -> T? {
        
        let name = String(describing: view.self)
        
        guard let nib = Bundle.main.loadNibNamed(name, owner: owner, options: options) else { return nil }
        
        return nib.first as? T
    }
    
    class func loadFromNib(named name: String, bundle : Bundle? = nil) -> UIView? {
        return UINib(nibName: name, bundle: bundle).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
}

extension UIView {
    
    var heightConstraint: NSLayoutConstraint? {
        get {
            return constraints.filter {
                if $0.firstAttribute == .height, $0.relation == .equal {
                    return true
                }
                return false
                }.first
        }
        set{ setNeedsLayout() }
    }
    
    var widthConstraint: NSLayoutConstraint? {
        get {
            return constraints.filter {
                if $0.firstAttribute == .width, $0.relation == .equal {
                    return true
                }
                return false
                }.first
        }
        set{ setNeedsLayout() }
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    public var screenshot: UIImage? {
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, 0.0);
        defer {
            UIGraphicsEndImageContext()
        }
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        layer.render(in: context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

//MARK : My Custom
extension UIView{
    
    func renderImage() -> UIImage {
        
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(size: (self.bounds.size))
            return  renderer.image { ctx in
                self.layer.render(in: ctx.cgContext)
            }
        } else {
            return UIImage()
        }
    }
    
    public func addSubviews(_ subviews: [UIView]) {
        subviews.forEach({self.addSubview($0)})
    }
}
