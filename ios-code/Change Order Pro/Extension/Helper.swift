//
//  Helper.swift
//  BaseProject
//
//  Created by Ashvin Gudaliya on 12/7/17.
//  Copyright © 2017 AshvinGudaliya. All rights reserved.
//

import UIKit
import MessageUI
import Alamofire

class Helper: NSObject {
    
    class func size<T>(iPhone: T, iPad: T) -> T {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return iPhone
        }
        return iPad
    }
    
    class func openAppSetting(){
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
        } else {
            UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
        }
    }
    
    class func delay(_ duration: TimeInterval, action: @escaping ()->Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
            action()
        }
    }
    
    class func AgAlertView(title: String = "", _ alertmessage: String, okayHandler: (() -> ())? = nil)  {
        if alertmessage.isNull { return }
        
        let alertCon = UIAlertController(title: title, message: alertmessage, preferredStyle: .alert)
        let okay = UIAlertAction(title: "OK_TEXT".localized, style: .default) { _ in
            if let b = okayHandler {
                b()
            }
        }
        alertCon.addAction(okay)
        AppDelegate.topViewController()?.present(alertCon, animated: true, completion: nil)
    }
    
    class func agAlertSubscription(_ message: String) {

        AGAlertBuilder(withAlert: "", message: message)
            .addAction(title: "Cancel", style: .cancel, handler: nil)
            .addAction(title: "View Plan", style: .default) { _ in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let payment = storyboard.instantiateViewController(withIdentifier: "ViewAllPlansViewController") as! ViewAllPlansViewController
                let navi = UINavigationController(rootViewController: payment)
                AppDelegate.topViewController()?.present(navi, animated: true, completion: nil)
            }
        .show()
    }
    
    func getStatusAndImage(dict: [String: Any]) -> (status: String, image: UIImage?){
        if let i = dict["Image1"] as? UIImage {
            return (ImageFlag.selected, i)
        }
        return (ImageFlag.notSelected, nil)
    }
    
    static func phoneNumberFormat(with textFiled: UITextField, string: String?, str: String?) -> Bool{
        
        if string == "" { //BackSpace
            return true
        }
            
        else if str!.count < 3 {
            if str!.count == 1 {
                textFiled.text = "("
            }
        }
        else if str!.count == 5 {
            textFiled.text = textFiled.text! + ") "
        }
        else if str!.count == 10 {
            textFiled.text = textFiled.text! + "-"
        }
        else if str!.count > 14 {
            return false
        }
        return true
    }
}

extension Dictionary {
    static func += <K, V> (left: inout [K: V], right: [K: V]) {
        for (k, v) in right {
            left[k] = v
        }
    }
}

extension NSMutableDictionary {
    func replaceValue(with dict: NSDictionary, ignor: [String] = []) {
        
        for (key, _ ) in self {
            if let k = key as? String {
                if !ignor.contains(k) {
                    if let dValue = dict.object(forKey: k){
                        self.setObject(dValue, forKey: k as NSCopying)
                    }
                }
            }
        }
        
        for (key, _) in dict {
            if let k = key as? String, !self.isHas(forKey: k) {
                if let dValue = dict.object(forKey: k){
                    self.setObject(dValue, forKey: k as NSCopying)
                }
            }
        }
    }
    
    func copy(with dict: NSDictionary, keys: [String] = []) {
        for key in keys {
            if let dValue = dict.object(forKey: key){
                self.setObject(dValue, forKey: key as NSCopying)
            }
            else{
                AgLog.debug("Key Not Copy in Dict \(key)")
            }
        }
    }
}

extension NSArray {
    var toJson: String? {
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}

enum DataFormate: String {
    case MMddyyyy = "MM/dd/yyyy"
}

extension Date {
    func toFormate(format f: DataFormate) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = f.rawValue
        return dateFormatter.string(from: self)
    }
}

