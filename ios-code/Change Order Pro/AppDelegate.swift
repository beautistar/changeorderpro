//
//  AppDelegate.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import IQKeyboardManager
import SwiftMessages
import UserNotifications
import UserNotificationsUI
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import Fabric
import Crashlytics
import AlamofireNetworkActivityIndicator
import WebKit
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,UNUserNotificationCenterDelegate ,MessagingDelegate{
    
    var window: UIWindow?
    var orientationLock = UIInterfaceOrientationMask.portrait
    
    static var selectedTabBar: Int = 0
    var badgeCnt = 0
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
        
    
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {(granted, error) in
                    if (granted) {
                        DispatchQueue.main.async(execute: {
                            AgLog.debug("registerForRemoteNotifications")
                            UIApplication.shared.registerForRemoteNotifications()
                        })
                    }
            })
    
            application.registerForRemoteNotifications()
            Messaging.messaging().delegate = self
        }
        else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        let logger = RequestLogger(leval: Level.verbose)
        logger.logHTMLResponse = true
        logger.startLogging()
        
        Fabric.with([Crashlytics.self])
        FirebaseApp.configure()
        
        NetworkActivityIndicatorManager.shared.isEnabled = true
        NetworkActivityIndicatorManager.shared.startDelay = 0.0
        
        if UserDetails.isUserLogin {
            CheckUserDetails.checkUser { _ in }
        }
        
        let token = Messaging.messaging().fcmToken
        AgLog.debug("This the Tocken Value of FIrebase \(token ?? "not got")")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification),
                                               name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        if let userInfo = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [String: AnyObject] {
            UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: userInfo), forKey: "NOTI_DETAIL")
        }
        
        UserDefaults.standard.setValue(badgeCnt, forKey: "BADGE_CNT")
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        badgeCnt = 0
        UserDefaults.standard.setValue(badgeCnt, forKey: "BADGE_CNT")
        UIApplication.shared.applicationIconBadgeNumber = badgeCnt
//        application.applicationIconBadgeNumber = 0
//        if #available(iOS 10.0, *) {
//            let center = UNUserNotificationCenter.current()
//            center.removeAllDeliveredNotifications() // To remove all delivered notifications
//            center.removeAllPendingNotificationRequests()
//        } else {
//            application.cancelAllLocalNotifications()
//        }
    }
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        AgLog.debug(remoteMessage.appData)
    }
    
    func tokenRefreshNotification(notification: NSNotification) {
        // NOTE: It can be nil here
        InstanceID.instanceID().instanceID { (result, error) in
            if let e = error {
                AgLog.debug(e)
            }
            else{
                if let refreshedToken = result?.instanceID {
                    AgLog.debug("InstanceID token: \(refreshedToken)")
                }
            }
        }
        connectToFcm()
    }
    
    func connectToFcm() {
        
        if Messaging.messaging().shouldEstablishDirectChannel {
            let token = Messaging.messaging().fcmToken
            AgLog.debug("FCM token 1 : \(token ?? "")")
        }
        Messaging.messaging().connect { (error) in
            if let e = error {
                AgLog.debug("Unable to connect with FCM. \(e)")
            } else {
                AgLog.debug("Connected to FCM.")
            }
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        AgLog.debug("tocken Delegate 1\(fcmToken)")
        
        let token = Messaging.messaging().fcmToken
        AgLog.debug("FCM token:2 \(token ?? "")")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
    
        let notiInfo  = userInfo as NSDictionary
        AgLog.debug("The Notification Information is \(notiInfo)")
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: notiInfo), forKey: "NOTI_DETAIL")
        
        
        let state : UIApplicationState = application.applicationState
        if (state == .inactive || state == .background) {
            
        }
        else {
            AgLog.debug("The Notification Information in active state \(notiInfo)")
        }
    }
    
    /*
     
     func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
   
     UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: userInfo), forKey: "NOTI_DETAIL_BACK")
     // Capture payload here something like:
     let appState = userInfo["appState"] as? String
     print(appState!)
     
     // Do something for every state
     if (application.applicationState == UIApplicationState.active){
     print("Active")
     
     }else if (application.applicationState == UIApplicationState.background){
     print("Background")
     
     
     }else if (application.applicationState == UIApplicationState.inactive){
     print("Inactive")
     
     }
     
     completionHandler(.noData)
     
     }
     */
    
    func application( _ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        
        let aps = userInfo["aps"] as! [String: AnyObject]
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: aps), forKey: "NOTI_DETAIL_FF1")
        // 1
        if aps["content-available"] as? Int == 1 {
            UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: aps), forKey: "NOTI_DETAIL_FF")
        } else  {
            // News
            // 4
            
            completionHandler(.newData)
        }
    }
    
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void){

//        badgeCnt = badgeCnt + 1
//        UserDefaults.standard.setValue(badgeCnt, forKey: "BADGE_CNT")
//        UIApplication.shared.applicationIconBadgeNumber = UserDefaults.standard.integer(forKey: "BADGE_CNT")
        
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        
        let userInfo = notification.request.content.userInfo as NSDictionary
        AgLog.debug(userInfo)
        
        if notification.request.content.title == "logout" {
            CheckUserDetails.checkUser { }
            return
        }
        
        if let value = userInfo["gcm.notification.data"] as? String,
            let data = value.data(using: String.Encoding.utf8) {
            
            do {
                if let data = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                    let statusV = data["status"] as? String {
                    
                    if let status = ChangeOrderStatus(rawValue: statusV),
                        let vc = window?.rootViewController as? TabsViewController{
                        
                        switch status {
                        case .assigned, .new, .denied, .approved, .completed:
                            
                            if status == .new || status == .assigned {
                                if let tab = window?.rootViewController as? TabsViewController,
                                    let v = (tab.selectedViewController as? UINavigationController)?.viewControllers.first {
                                    
                                    if let vc = v as? MYProjectsController, vc.isViewLoaded {
                                        vc.calltogetMyallProjects()
                                    }
                                }
                            }
                            
                            if let v = (vc.viewControllers?.obj(1) as? UINavigationController)?.viewControllers.first as? MyAllOrdersController, v.isViewLoaded{
                                v.calltogetallMyChangeOrders()
                            }
                            break
                            
                        case .submitted:
                            if let v = (vc.viewControllers?.obj(2) as? UINavigationController)?.viewControllers.first as? CheckListController, v.isViewLoaded{
                                if let dict = data["ChangeOrder"] as? NSArray {
                                    v.calltogetMyassignedProject(withDict: dict)
                                }
                                else{
                                    v.calltogetMyassignedProject()
                                }
                            }
                            break
                            
                        default: break
                        }
                    }
                    else{
                        if statusV == "logout" {
                            CheckUserDetails.checkUser { }
                            return
                        }
                        else{
                            self.reloadTabViewControllers()
                        }
                    }
                }
            }
            catch {
                AgLog.debug(error)
            }
        }
    
        completionHandler([.alert,.badge,.sound])
    }
    
    func reloadTabViewControllers() {
        if let tab = window?.rootViewController as? TabsViewController,
            let v = (tab.selectedViewController as? UINavigationController)?.viewControllers.first {
            
            switch tab.selectedIndex{
            case 0:
                if let vc = v as? MYProjectsController, vc.isViewLoaded {
                    vc.calltogetMyallProjects()
                }
                
            case 1:
                if let vc = v as? MyAllOrdersController, vc.isViewLoaded{
                    vc.calltogetallMyChangeOrders()
                }
                
            case 2:
                if let vc = v as? CheckListController, vc.isViewLoaded{
                    vc.calltogetMyassignedProject()
                }
                
            default: break
            }
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
//        badgeCnt = badgeCnt + 1
//        UserDefaults.standard.setValue(badgeCnt, forKey: "BADGE_CNT")
//        UIApplication.shared.applicationIconBadgeNumber = UserDefaults.standard.integer(forKey: "BADGE_CNT")
        
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        
        let userInfo = response.notification.request.content.userInfo as NSDictionary
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: userInfo), forKey: "NOTI_DETAIL_RESPONCE")
        
        AgLog.debug(userInfo)
        
        if let value = userInfo["gcm.notification.data"] as? String,
            let data = value.data(using: String.Encoding.utf8) {
            
            do {
                if let data = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                    let statusV = data["status"] as? String {
                    

                    if let status = ChangeOrderStatus(rawValue: statusV) {
                        switch status {
                        case .assigned, .new, .denied, .approved, .completed:
                            if let vc = window?.rootViewController as? TabsViewController {
                                vc.selectedIndex = 1
                            }
                            AppDelegate.selectedTabBar = 1
                            break
                            
                        case .submitted:
                            if let vc = window?.rootViewController as? TabsViewController {
                                vc.selectedIndex = 2
                            }
                            AppDelegate.selectedTabBar = 2
                            break
                            
                        default: break
                        }
                        self.reloadTabViewControllers()
                    }
                    else{
                        if statusV == "logout" {
                            CheckUserDetails.checkUser { }
                            return
                        }
                    }
                }
            }
            catch {
                AgLog.debug(error)
            }
        }
        
        completionHandler()
        
        AgLog.debug("Did Receive Call and data is  \(response)")
        AgLog.debug("The Whole Info is \(response.notification.request.content.userInfo)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Messaging.messaging().apnsToken = deviceToken
        let characterSet = CharacterSet(charactersIn: "<>")
        let deviceTokenString = deviceToken.description.trimmingCharacters(in: characterSet).replacingOccurrences(of: " ", with: "");
        
        AgLog.debug("Device Tocketn \(deviceTokenString)")
        
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        AgLog.debug("didFailToRegisterForRemoteNotificationsWithError")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        if UserDetails.isUserLogin {
            CheckUserDetails.checkUser { (userCheckModel) in
                
            }
        }
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
    func switchViewControllers() {
        
        var is_setup_company_info = "0";
        if let detailUsr = UserDefaults.standard.object(forKey: "is_setup_company_info") {
            is_setup_company_info = detailUsr as? String ?? "0"
        }
        else {
            is_setup_company_info = "1";
        }
        
        if (is_setup_company_info == "0") {
            
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let nav = storyboard.instantiateViewController(withIdentifier: "FirstTimeCompanyInfoController")
            self.window?.rootViewController = UINavigationController(rootViewController: nav)

        }
        else{
            // switch root view controllers
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let nav = storyboard.instantiateViewController(withIdentifier: "tBBar") as? TabsViewController
            nav?.selectedIndex = AppDelegate.selectedTabBar
            self.window?.rootViewController = nav
        }
    }
    
    func switchBack() {
        SwiftMessages.hideAll()
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let nav = storyboard.instantiateViewController(withIdentifier: "logIn")
        self.window?.rootViewController = nav
    }
}

extension UIViewController{
    func updateUserData(complitionhandler: ((Bool) -> Void)) {
        
        if let detailUsr = UserDefaults.standard.data(forKey: "USERDETAIL") {
            if let result = NSKeyedUnarchiver.unarchiveObject(with: detailUsr) as? NSDictionary {
                
                AgLog.debug("User info ", result)
                
                AgLog.debug("user is logged in  setup info")
                
                let userID = result.integer(forKey: "id")
                let userName = result.object(forKey: "username") as? String ?? ""
                UserDetails.loginUserId = "\(userID)"
                AgLog.debug("User ID is \(UserDetails.loginUserId)")

                // Create Project Plist to documentDirectory if not available
                uni_MyProject_PlistName = "MyProjects_\(UserDetails.loginUserId)_\(userName).plist"
                uni_RemovedProjects_PlistName = "DeletedProjects_\(UserDetails.loginUserId)_\(userName).plist"
                
                DataController.sharedInstance.copyProjectesPlistfromBundletoDevice()
                RemovedController.shared.copyDeletedPlistfromBundletoDevice()
                
                //FOR All Orders setting plist and Image Folder
                
                approve_user_FirstName = result.object(forKey: "first_name") as? String ?? ""
                approve_user_Lastname = result.object(forKey: "last_name") as? String ?? ""
                
                uni_MyChangeOrders_PlistName = "MyChageOrders_\(UserDetails.loginUserId)_\(userName).plist"
                uni_OrdersRemoved_PlistName = "DeletedOrders_\(UserDetails.loginUserId)_\(userName).plist"
                
                DataController.sharedInstance.copyChangeOrdersPlistfromBundletoDevice()
                RemovedController.shared.copyDeletedOrdersItemPlistToDevice()
                
                // Create Folder also to save Images
                
                uni_MyImageFolderName = "\(UserDetails.loginUserId)_\(userName)_Images"
                DataController.sharedInstance.createFoldertoSaveIamges()
                
                uni_Array_allProjects = NSMutableArray(array:DataController.sharedInstance.getMyProjectData())
                
                uni_array_allChangeOrders = NSMutableArray(array:DataController.sharedInstance.getMyChangeOrderData())
                
                complitionhandler(true)
                return
            }
        }
        complitionhandler(false)
    }
}
