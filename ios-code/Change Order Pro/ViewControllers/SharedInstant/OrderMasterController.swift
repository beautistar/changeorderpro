//
//  OrderMasterController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 07/02/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire

class OrderMasterController: NSObject {
    
    static let shared = OrderMasterController()
    
    var off_ser_img1 = UIImage()
    var off_ser_img2 = UIImage()
    var off_ser_img3 = UIImage()
    var off_ser_img4 = UIImage()
    
    var offser_img1Status = ""
    var offser_img2Status = ""
    var offser_img3Status = ""
    var offser_img4Status = ""
    
    //MARK: First Time Upload Order Item Functions
    
    func callOffline_uploadOrderTextPart(project_id: String, offlineID:String, finalItemsarray : NSMutableArray , completion: @escaping (NSDictionary) -> ()){
        
        if Reachability.isConnectedToNetwork() == true {
            
            let strURL = kBaseURL + "addchangeorder"
            
            let dictParams:  [String: Any] = [
                "user_id" : UserDetails.loginUserId,
                "project_id" : project_id ,
                "offline_id" : offlineID,
                "change_order_items" : finalItemsarray.toJson ?? "",
                ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default)
                .responseJSON { response in
                    
                    response.logShow()
                    
                    if let dicServer = response.result.value as? NSDictionary{
                        
                        if response.isSuccess {
                            completion(dicServer)
                        }
                        else{
                          //  completion(dicServer,false)
                        }
                    }
            }
        }
    }
    
    func callOffline_uploadItemsALLImagesPart(changeID: String, projectId: String, orderItem: NSDictionary ,isLastIndex : String, completion: @escaping (Bool) -> ()){
        
        var arrayImgs = orderItem["all_images"] as! [Any]
        var dicimage = arrayImgs[0] as! [String:Any]
        
        if let stImg1 = dicimage["Image1"] as? String{
            
            if stImg1 == ImageFlag.notSelected{
                offser_img1Status = ImageFlag.notSelected
            }else{
                off_ser_img1 = DataController.sharedInstance.getImageFromDirFolder(withName: stImg1 )
                offser_img1Status = ImageFlag.selected
            }
        }
        
        if let stImg2 = dicimage["Image2"] as? String{
            
            if stImg2 == ImageFlag.notSelected{
                offser_img2Status = ImageFlag.notSelected
            }else{
                off_ser_img2 = DataController.sharedInstance.getImageFromDirFolder(withName: stImg2 )
                offser_img2Status = ImageFlag.selected
            }
        }
        
        if let stImg3 = dicimage["Image3"] as? String{
            
            if stImg3 == ImageFlag.notSelected{
                offser_img3Status = ImageFlag.notSelected
            }else{
                off_ser_img3 = DataController.sharedInstance.getImageFromDirFolder(withName: stImg3 )
                offser_img3Status = ImageFlag.selected
            }
        }
        
        if let stImg4 = dicimage["Image4"] as? String{
            
            if stImg4 == ImageFlag.notSelected{
                offser_img4Status = ImageFlag.notSelected
            }else{
                off_ser_img4 = DataController.sharedInstance.getImageFromDirFolder(withName: stImg4 )
                offser_img4Status = ImageFlag.selected
            }
            
        }
        
        if Reachability.isConnectedToNetwork() == true {
            
            var parameters:  [String: String] = [
                "change_order_id" : changeID,
                "project_id" : projectId,
                "order_item_id" : "\(orderItem.integer(forKey: "order_item_id"))",
                 "is_update" : "no",
                "user_id" : UserDetails.loginUserId
            ]
            
            if isLastIndex == "YES"{
                parameters["is_last"] = "true"
            }
            
            let strURL = kBaseURL + "uploadimages"
            let headers1 = ["Authorization": "123456"]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                multipartFormData.append(self.off_ser_img1, withName: "image1", status: self.offser_img1Status)
                multipartFormData.append(self.off_ser_img2, withName: "image2", status: self.offser_img2Status)
                multipartFormData.append(self.off_ser_img3, withName: "image3", status: self.offser_img3Status)
                multipartFormData.append(self.off_ser_img4, withName: "image4", status: self.offser_img4Status)
                
                for (key, value) in parameters {
                    multipartFormData.append((value.data(using: .utf8))!, withName: key)
                }}, to: strURL, method: .post, headers: headers1,
                    
                    encodingCompletion: { encodingResult in
                        
                        switch encodingResult {
                            
                        case .success(let upload, _, _):
                            
                            upload.responseJSON { response in
                                
                                response.logShow()
                                
                                switch response.result{
                                case .success(_ ):
                                    
                                    if response.isSuccess {
                                        completion(true)
                                        return
                                    }
                                    break
                                    
                                case .failure(let error):
                                    AgLog.debug(error)
                                    completion(false)
                                    break
                                }
                            }
                            
                        case .failure(let encodingError):
                            AgLog.debug("error:\(encodingError)")
                            completion(false)
                        }
            })
        }
    }
    
    //MARK: Edited Order Item Functions
    func callOffline_editedOrderItemsTextPart(changOrderId: String, finalEditedItemsarray : NSMutableArray , completion: @escaping (NSDictionary) -> ()){
        
        if Reachability.isConnectedToNetwork() == true {
            
            let strURL = kBaseURL + "editchangeorder"
            let dictParams:  [String: Any] = [
                "change_order_id" : changOrderId,
                "change_order_items" : finalEditedItemsarray.toJson ?? "",
                ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                
                if let dicServer = response.result.value as? NSDictionary {
                    
                    if response.isSuccess {
                        completion(dicServer)
                    }
                }
            }
        }
    }
    
    func callOffline_eitedItemsUploadImages(changeID: String, projectId: String, orderItem : NSDictionary ,isLastIndex : String, completion: @escaping (Bool) -> ()){
        
        var arrayImgs = orderItem["all_images"] as! [Any]
        var dicimage = arrayImgs[0] as! [String:Any]
        
        if let stImg1 = dicimage["Image1"] as? String{
            
            if stImg1 == ImageFlag.notSelected{
                offser_img1Status = ImageFlag.notSelected
            }
            else{
                off_ser_img1 = DataController.sharedInstance.getImageFromDirFolder(withName: stImg1 )
                offser_img1Status = ImageFlag.selected
            }
        }
        
        if let stImg2 = dicimage["Image2"] as? String{
            
            if stImg2 == ImageFlag.notSelected{
                offser_img2Status = ImageFlag.notSelected
            }else{
                off_ser_img2 = DataController.sharedInstance.getImageFromDirFolder(withName: stImg2 )
                offser_img2Status = ImageFlag.selected
            }
        }
        
        if let stImg3 = dicimage["Image3"] as? String{
            
            if stImg3 == ImageFlag.notSelected{
                offser_img3Status = ImageFlag.notSelected
            }else{
                off_ser_img3 = DataController.sharedInstance.getImageFromDirFolder(withName: stImg3 )
                offser_img3Status = ImageFlag.selected
            }
        }
        
        if let stImg4 = dicimage["Image4"] as? String{
            
            if stImg4 == ImageFlag.notSelected{
                offser_img4Status = ImageFlag.notSelected
            }else{
                off_ser_img4 = DataController.sharedInstance.getImageFromDirFolder(withName: stImg4 )
                offser_img4Status = ImageFlag.selected
            }
        }
        
        if Reachability.isConnectedToNetwork() == true {
            
            var parameters:  [String: String] = [
                "change_order_id" : changeID,
                "project_id" : projectId,
                "order_item_id" : "\(orderItem.integer(forKey: "order_item_id"))",
                "is_update" : "yes",
                "user_id" : UserDetails.loginUserId]
            
            if isLastIndex == "YES"{
                parameters["is_last"] = "true"
            }
            
            AgLog.debug("upload image with parameters \(parameters)")
            let strURL = kBaseURL + "uploadimages"
            let headers1 = ["Authorization": "123456"]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                multipartFormData.append(self.off_ser_img1, withName: "image1", status: self.offser_img1Status)
                multipartFormData.append(self.off_ser_img2, withName: "image2", status: self.offser_img2Status)
                multipartFormData.append(self.off_ser_img3, withName: "image3", status: self.offser_img3Status)
                multipartFormData.append(self.off_ser_img4, withName: "image4", status: self.offser_img4Status)
                
                for (key, value) in parameters {
                    multipartFormData.append((value.data(using: .utf8))!, withName: key)
                }}, to: strURL, method: .post, headers: headers1,
                    
                    encodingCompletion: { encodingResult in
                        
                        switch encodingResult {
                        case .success(let upload, _, _):
                            
                            upload.responseJSON { response in
                                
                                response.logShow()
                                
                                switch response.result{
                                case .success(_ ):
                                    
                                    if response.isSuccess {
                                        completion(true)
                                    }
                                    break
                                    
                                case .failure(let error):
                                    AgLog.debug(error)
                                    break
                                }
                                
                            }
                        case .failure(let encodingError):
                            AgLog.debug("error:\(encodingError)")
                        }
            })
        }
    }
    
    func callOffline_deleteOrderFromServer(orderIDToDelete : String, completion: @escaping (Bool) -> ()){
        
        if Reachability.isConnectedToNetwork() == true {
            
            let strURL = kBaseURL + "deletechangeorder"
            let dictParams:  [String: String] = [
                "change_order_id" :  orderIDToDelete,
                "user_id": UserDetails.loginUserId
            ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default)
                .responseJSON { response in
                    
                    response.logShow()
                    
                    if response.isSuccess {
                        completion(true)
                    }
            }
        }
    }
    
    func callOffline_markOrderAsCompleted(orderIDToComplete : String, completion: @escaping (Bool) -> ()){
        
        if Reachability.isConnectedToNetwork() == true {
            
            let strURL = kBaseURL + "completechangeorder"
            let dictParams:  [String: String] = [
                "top_change_order_id" :  orderIDToComplete,
                "user_id": UserDetails.loginUserId
            ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default)
                .responseJSON { response in
                    
                    response.logShow()
                    
                    if response.isSuccess {
                        completion(true)
                    }
            }
        }
    }
}
