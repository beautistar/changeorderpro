//
//  ApproverMasterController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 10/02/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire

var approve_user_FirstName = ""
var approve_user_Lastname = ""
var approve_compny_Name = "no compnay name"
var approve_projectId = ""

class ApproverMasterController: NSObject {
    
    static let shared = ApproverMasterController()
    
    func offline_customerApprovealAPI(dicCustApprove : NSDictionary , ChangeOrderId: String ,completion: @escaping (Bool) -> ()){
        
        let cSign = dicCustApprove.string(forKey: "customer_signature")
        let signImage = DataController.sharedInstance.getImageFromDirFolder(withName: cSign )
        
        let ch_Id = "\(ChangeOrderId)"
        let c_name = "\(dicCustApprove.object(forKey: "customer_name") ?? "")"
        var p_Id = "\(dicCustApprove.object(forKey: "project_id") ?? "")"
        
        if p_Id.contains("offline") {
            p_Id = approve_projectId
        }
        
        let us_Id = "\(dicCustApprove.object(forKey: "user_id") ?? "")"
        let logoU = "\(dicCustApprove.object(forKey: "logo_url") ?? "")"
        
        var temId = "1"
        if let temp = UserDefaults.standard.object(forKey: "TEMPLATE"){
            temId = temp as? String ?? ""
        }
        
        var p_title = ""
        if let titl = dicCustApprove.object(forKey: "customer_Title") as? String{
            p_title = titl
        }
        
        if Reachability.isConnectedToNetwork() == true {
            
            let parameters:  [String: String] = [
                "change_order_id" : ch_Id,
                "customer_name" : c_name,
                "project_id" : p_Id,
                "user_id" : us_Id,
                "logo_url" : logoU,
                "template_id" : temId,
                "title": p_title
            ]
            
            let strURL = kBaseURL + "customerapproval"
            let headers1 = ["Authorization": "123456"]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if let imageData1 = UIImageJPEGRepresentation(signImage, 1) {
                    multipartFormData.append(imageData1, withName: "customer_signature", fileName: "custsign.jpg", mimeType: "image/jpeg")
                }
                
                for (key, value) in parameters {
                    multipartFormData.append((value.data(using: .utf8))!, withName: key)
                }
                
            }, to: strURL, method: .post, headers: headers1,
               encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        response.logShow()
                        
                        if response.isSuccess {
                            completion(true)
                        }
                    }
                    
                case .failure(let encodingError):
                    AgLog.debug("error:\(encodingError)")
                    
                }
            })
        }
    }
    
    // MARK: Remote Approver API
    func offline_remoteApproverAPI(dicRemoteApprove: NSDictionary, ChangeOrderId: String, completion: @escaping (Bool) -> ()){
        
        let remoteApprovrId = dicRemoteApprove.string(forKey: "approver_id")
        let arrAllApproverID = dicRemoteApprove.array(forKey: "site_contact_ids")
        
        if Reachability.isConnectedToNetwork() == true {
            
            let strURL = kBaseURL + "remoteapproval"
            
            let dictParams:  [String: String] = [
                
                "change_order_id" : ChangeOrderId,
                "approver_id" : remoteApprovrId,
                "site_contact_ids" : arrAllApproverID.toJson ?? "",
                "first_name" : approve_user_FirstName,
                "last_name" : approve_user_Lastname,
                "company_name" : approve_compny_Name,
                ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                
                if response.isSuccess {
                    completion(true)
                }
                //                    else{
                //                        completion(true)
                //                    }
                
            }
        }
    }
    
    // MARK: Remote Approver API
    func offline_sendDataAssignedToOthersAPi(dicAssignedto : NSDictionary, ChangeOrderId: String, completion: @escaping (Bool) -> ()){
        
        let emailAssign = dicAssignedto.string(forKey: "email")
        let photneAssifn = dicAssignedto.string(forKey: "phone")
        
        if Reachability.isConnectedToNetwork() == true {
            
            let strURL = kBaseURL + "assignchangeorder"
            
            let dictParams:  [String: String] = [
                "email" : emailAssign,
                "phone" : photneAssifn,
                "change_order_id" : ChangeOrderId,
                "first_name" : approve_user_FirstName,
                "last_name" : approve_user_Lastname,
                "company_name" : approve_compny_Name,
                "user_id": UserDetails.loginUserId
                ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default)
                .responseJSON { response in
                    
                    response.logShow()
                    
                    if response.isSuccess {
                        completion(true)
                    }
                    //                    else{
                    //                        completion(true)
                    //                    }
            }
        }
    }
}


