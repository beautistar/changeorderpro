//
//  DataController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 20/01/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit

var uni_MyProject_PlistName = ""
var uni_MyChangeOrders_PlistName = ""
var uni_MyImageFolderName = ""

var uni_isWorking_Offline = true
var uni_Array_allProjects = NSMutableArray()
var uni_array_allChangeOrders = NSMutableArray()

var uni_selectedindextoEdit = 0
var uni_fromEdit = "NO"

class DataController: UIViewController {

    static let sharedInstance = DataController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkUseLoginOrNot() -> NSDictionary {
        
        let dic = NSDictionary()
        
        if let detailUsr = UserDefaults.standard.data(forKey: "USERDETAIL") {
            if let result = NSKeyedUnarchiver.unarchiveObject(with: detailUsr) as? NSDictionary {
                return result
            }
        }
        return dic
        
    }
    
    //MARK: **********   All Projects Document Directory Plist Operations   ***********   ////
    
    func copyProjectesPlistfromBundletoDevice() {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentDirectory = paths[0] as? String ?? ""
        let path = documentDirectory.appending("/\(uni_MyProject_PlistName)")
        AgLog.debug("Plist Path is: ", path)
        let fileManager = FileManager.default
        if(!fileManager.fileExists(atPath: path)){
            if let bundlePath = Bundle.main.path(forResource: "MyProjects", ofType: "plist"){
//                let result = NSMutableDictionary(contentsOfFile: bundlePath)
                
                do
                {
                    try fileManager.copyItem(atPath: bundlePath, toPath: path)
                }
                catch{
                    AgLog.debug("copy failure.")
                }
            }
            else{
                AgLog.debug("file  not found.")
            }
        }
        else{
            AgLog.debug("file already exits at path.")
        }
    }
    
    func getMyProjectData() -> NSArray {
        let empAr = NSArray()
        let path = self.dataProjectsFilePath()
        let defaultManager = FileManager()
        if defaultManager.fileExists(atPath: path) {
            let url = URL(fileURLWithPath: path)
            do {
                let data = try Data(contentsOf: url)
                let unarchiver = NSKeyedUnarchiver(forReadingWith: data)
                if let savedProj = unarchiver.decodeObject(forKey: "RemainProjects") as? NSArray {
                    unarchiver.finishDecoding()
                    return savedProj
                }
            }
            catch {
               AgLog.debug(error)
            }
        }
        
        return empAr
    }
    
    
    func saveMyProjectData(allProject:NSMutableArray) {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.encode(allProject, forKey: "RemainProjects")
        archiver.finishEncoding()
        data.write(toFile: dataProjectsFilePath(), atomically: true)

    }
    
    func documentsDirectoryPath()->String {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                        .userDomainMask, true)
        let documentsDirectory = paths.first!
        return documentsDirectory
        
    }
    
    func dataProjectsFilePath ()->String{
        return self.documentsDirectoryPath().appendingFormat("/\(uni_MyProject_PlistName)")
    }
    
    
    func deleteProjectPlistFile(){
        
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentDirectory = paths[0] as? String ?? ""
        let path = documentDirectory.appending("/\(uni_MyProject_PlistName)")
        AgLog.debug("Plist Path is: ", path)
        let fileManager = FileManager.default
        if(!fileManager.fileExists(atPath: path)){
            
        }else{
            AgLog.debug("remove project Plist .")
            do {
                
                try fileManager.removeItem(atPath: path)
            }
                
            catch let error as NSError {
                
                AgLog.debug("FimeManager Remove Item error: \(error.debugDescription)")
            }
        }
    }

    //MARK: **********   Change Orders Document Directory Plist Operations   ***********   ////
    func copyChangeOrdersPlistfromBundletoDevice() {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentDirectory = paths[0] as? String ?? ""
        let path = documentDirectory.appending("/\(uni_MyChangeOrders_PlistName)")
        
        let fileManager = FileManager.default
        if(!fileManager.fileExists(atPath: path)){
            if let bundlePath = Bundle.main.path(forResource: "MyChangeOrders", ofType: "plist"){
//                let result = NSMutableDictionary(contentsOfFile: bundlePath)
                
                do{
                    try fileManager.copyItem(atPath: bundlePath, toPath: path)
                }catch{
                    AgLog.debug("copy failure. change Orders")
                }
            }else{
                AgLog.debug("file  not found.change Orders")
            }
        }else{
            AgLog.debug("file already exits at path.change Orders ")
        }
    }
    
    func getMyChangeOrderData()->NSArray {
        let empAr = NSArray()
        let path = self.dataChangeOrderFilePath()
        let defaultManager = FileManager()
        if defaultManager.fileExists(atPath: path) {
            let url = URL(fileURLWithPath: path)
            let data = try! Data(contentsOf: url)
            
            let unarchiver = NSKeyedUnarchiver(forReadingWith: data)
            let savedProj = unarchiver.decodeObject(forKey: "RemainChagneOrders") as? NSArray
            
            if savedProj != nil{
                unarchiver.finishDecoding()
                return savedProj!
            }else{
                return empAr
            }
        }
        return empAr
    }
    func dataChangeOrderFilePath ()->String{
        return self.documentsDirectoryPath().appendingFormat("/\(uni_MyChangeOrders_PlistName)")
    }
    
    func deleteChagneOrderPlistFile(){
    
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentDirectory = paths[0] as? String ?? ""
        let path = documentDirectory.appending("/\(uni_MyChangeOrders_PlistName)")
        
        let fileManager = FileManager.default
        if(!fileManager.fileExists(atPath: path)){
            
        }else{
            AgLog.debug("remove Cahgne Order Plist .")
            do {
                try fileManager.removeItem(atPath: path)
            }
            catch let error as NSError {
                
                AgLog.debug("FimeManager Remove Item error: \(error.debugDescription)")
            }
        }
    }
  
    //MARK: **********   Change Orders Item Images  Operations   ***********   ////
    
    func createFoldertoSaveIamges(){
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectoryURL = urls[urls.count - 1] as URL
        let dbDirectoryURL = documentDirectoryURL.appendingPathComponent("\(uni_MyImageFolderName)")
        
        if FileManager.default.fileExists(atPath: dbDirectoryURL.path) == false{
            AgLog.debug("no folder available by the name \(uni_MyImageFolderName) create new one")
            do{
                try FileManager.default.createDirectory(at: dbDirectoryURL, withIntermediateDirectories: false, attributes: nil)
            }catch{
            }
        }else{
            AgLog.debug("folder already created")
        }
    }
    
    func saveMyChangeOrderData(allChangeOrders: NSMutableArray) {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.encode(allChangeOrders, forKey: "RemainChagneOrders")
        archiver.finishEncoding()
        data.write(toFile: dataChangeOrderFilePath(), atomically: true)
    }
    
    func listFilesFromDocumentsFolder() -> [String]?
    {
        let fileMngr = FileManager.default;
        
        // Full path to documents directory
        let docs = fileMngr.urls(for: .documentDirectory, in: .userDomainMask)[0].path
        
        // List all contents of directory and return as [String] OR nil if failed
        return try? fileMngr.contentsOfDirectory(atPath:docs)
    }
    
    func saveImageToDocumentDirFolder(withImage:UIImage,imageName:String){
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectoryURL = urls[urls.count - 1] as URL
        let dbDirectoryURL = documentDirectoryURL.appendingPathComponent("\(uni_MyImageFolderName)")
        
        let fileURL = dbDirectoryURL.appendingPathComponent("\(imageName)")
        
        AgLog.debug("path of saved image \(fileURL.path)")
        
        if !FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try UIImageJPEGRepresentation(withImage, 0.8)!.write(to: fileURL)
                AgLog.debug("Image Added Successfully")
            } catch {
                AgLog.debug(error)
            }
        } else {
            do
            {
                try FileManager.default.removeItem(atPath: fileURL.path)
                
                do {
                    try UIImageJPEGRepresentation(withImage, 0.8)!.write(to: fileURL)
                    AgLog.debug("Image replaced")
                } catch {
                    AgLog.debug(error)
                }

            }catch{
                AgLog.debug(error)
            }
        }
    }
    
    func isImageDownloadded(withName: String) -> Bool {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectoryURL = urls[urls.count - 1] as URL
        let dbDirectoryURL = documentDirectoryURL.appendingPathComponent("\(uni_MyImageFolderName)")
        
        let fileURL = dbDirectoryURL.appendingPathComponent("\(withName)")
        
        return FileManager.default.fileExists(atPath: fileURL.path)
    }
    
    
    func getImageFromDirFolder(withName:String) -> UIImage{
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectoryURL = urls[urls.count - 1] as URL
        let dbDirectoryURL = documentDirectoryURL.appendingPathComponent("\(uni_MyImageFolderName)")
        
        let fileURL = dbDirectoryURL.appendingPathComponent("\(withName)")
        
        if FileManager.default.fileExists(atPath: fileURL.path) {
            
            let image  = UIImage(contentsOfFile: fileURL.path)
            return image ?? UIImage()
        }else{
            
            let nilImage = UIImage()
            return nilImage
        }
    }
    
    //MARK: Compnay Detail save in ,get for offline use
    func createCompnayLogoFolder(){
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectoryURL = urls[urls.count - 1] as URL
        let dbDirectoryURL = documentDirectoryURL.appendingPathComponent("CompnayLogos")
        
        if FileManager.default.fileExists(atPath: dbDirectoryURL.path) == false{
            AgLog.debug("no folder available by the name CompnayLogos create new one")
            do{
                try FileManager.default.createDirectory(at: dbDirectoryURL, withIntermediateDirectories: false, attributes: nil)
            }catch{
            }
        }else{
            AgLog.debug("folder already created")
        }
    }
    
    
    func saveCompnayLogoImageToDocumentDirFolder(withImage:UIImage,imageName:String){
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectoryURL = urls[urls.count - 1] as URL
        let dbDirectoryURL = documentDirectoryURL.appendingPathComponent("CompnayLogos")
        
        let fileURL = dbDirectoryURL.appendingPathComponent("\(imageName)")
        AgLog.debug("path of saved logo \(fileURL.path)")
        
        do {
            try UIImagePNGRepresentation(withImage)!.write(to: fileURL)
            AgLog.debug("Logo downloaded Successfully")
        } catch {
            AgLog.debug(error)
        }
    }
    
    func getCompnayLogoImageFromDirFolder(withName:String)->UIImage{
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectoryURL = urls[urls.count - 1] as URL
        let dbDirectoryURL = documentDirectoryURL.appendingPathComponent("CompnayLogos")
        
        let fileURL = dbDirectoryURL.appendingPathComponent("\(withName)")
        if FileManager.default.fileExists(atPath: fileURL.path) {
            
            let image    = UIImage(contentsOfFile: fileURL.path)
            return image!
        }else{
            
            let nilImage = UIImage(named: "emptyClogo.png")
            return nilImage!
        }
    }
}
