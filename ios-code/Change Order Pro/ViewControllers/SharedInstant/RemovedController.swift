//
//  RemovedController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 09/02/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit

var uni_RemovedProjects_PlistName = ""
var uni_OrdersRemoved_PlistName = ""


class RemovedController: NSObject {
    
    static let shared = RemovedController()
    
    //MARK: FOR Projects
    func copyDeletedPlistfromBundletoDevice() {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentDirectory = paths[0] as? String ?? ""
        let path = documentDirectory.appending("/\(uni_RemovedProjects_PlistName)")
        AgLog.debug("Plist Path is: \(path)")
        let fileManager = FileManager.default
        
        if(!fileManager.fileExists(atPath: path)){
            if let bundlePath = Bundle.main.path(forResource: "DeletedProjects", ofType: "plist") {
//                let result = NSMutableDictionary(contentsOfFile: bundlePath)
                
                do{
                    try fileManager.copyItem(atPath: bundlePath, toPath: path)
                }
                catch{
                    AgLog.debug(error.localizedDescription)
                }
            }
            else{
                AgLog.debug("file  not found.")
            }
        }
        else{
            AgLog.debug("file already exits at path.")
        }
    }
    
    func getMyDeletedProjectData() -> NSArray {
        let empAr = NSArray()
        let path = self.dataProjectsFilePath()
        let defaultManager = FileManager()
        if defaultManager.fileExists(atPath: path) {
            let url = URL(fileURLWithPath: path)
            do {
                let data = try Data(contentsOf: url)
                let unarchiver = NSKeyedUnarchiver(forReadingWith: data)
                if let savedProj = unarchiver.decodeObject(forKey: "DeletedProjects") as? NSArray {
                    unarchiver.finishDecoding()
                    return savedProj
                }
            }
            catch{
                AgLog.debug(error)
            }
        }
        
        return empAr
    }
    
    func saveMyDeletedProjectData(allDeletedProjects:NSMutableArray) {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        
        archiver.encode(allDeletedProjects, forKey: "DeletedProjects")
        archiver.finishEncoding()
        data.write(toFile: dataProjectsFilePath(), atomically: true)
        
    }
    
    func documentsDirectoryPath()->String {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                        .userDomainMask, true)
        let documentsDirectory = paths.first!
        return documentsDirectory
        
    }
    
    func dataProjectsFilePath ()->String{
        return self.documentsDirectoryPath().appendingFormat("/\(uni_RemovedProjects_PlistName)")
    }
    
    //to delete Plist of Removed Projects
    func deleteRemovedProjectsPlist(){
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentDirectory = paths[0] as? String ?? ""
        let path = documentDirectory.appending("/\(uni_RemovedProjects_PlistName)")
        
        let fileManager = FileManager.default
        if(!fileManager.fileExists(atPath: path)){
            
        }
        else{
            AgLog.debug("remove Cahgne Order Plist .")
            do {
                try fileManager.removeItem(atPath: path)
            }
                
            catch let error as NSError {
                AgLog.debug("FimeManager Remove Item error: \(error.debugDescription)")
            }
        }
    }
    
    //MARK: FOR ALL ORDERS DELETE
    func copyDeletedOrdersItemPlistToDevice() {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentDirectory = paths[0] as? String ?? ""
        let path = documentDirectory.appending("/\(uni_OrdersRemoved_PlistName)")
        AgLog.debug("Plist Path is: ", path)
        let fileManager = FileManager.default
        if(!fileManager.fileExists(atPath: path)){
            if let bundlePath = Bundle.main.path(forResource: "MyDeletedOrders", ofType: "plist"){
//                let result = NSMutableDictionary(contentsOfFile: bundlePath)
                
                do{
                    try fileManager.copyItem(atPath: bundlePath, toPath: path)
                }
                catch{
                    AgLog.debug(error.localizedDescription)
                }
            }
            else{
                AgLog.debug("file  not found.")
            }
        }
        else{
            AgLog.debug("file already exits at path.")
        }
    }
    
    func saveMyDeletedOrdersItems(allDeletedOrders:NSMutableArray) {
        
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.encode(allDeletedOrders, forKey: "DeletedChagneOrders")
        archiver.finishEncoding()
        data.write(toFile: dataDeletedChangeOrderFilePath(), atomically: true)
    }
    
    func getMyDeletedOrderItemsData()->NSArray {
        let empAr = NSArray()
        let path = self.dataDeletedChangeOrderFilePath()
        let defaultManager = FileManager()
        if defaultManager.fileExists(atPath: path) {
            let url = URL(fileURLWithPath: path)
            let data = try! Data(contentsOf: url)
            
            let unarchiver = NSKeyedUnarchiver(forReadingWith: data)
            let savedProj = unarchiver.decodeObject(forKey: "DeletedChagneOrders") as? NSArray
            
            if savedProj != nil{
                unarchiver.finishDecoding()
                return savedProj!
            }else{
                return empAr
            }
        }
        return empAr
    }
    
    func dataDeletedChangeOrderFilePath ()->String{
        return self.documentsDirectoryPath().appendingFormat("/\(uni_OrdersRemoved_PlistName)")
    }
    
    func deleteRemovedChagneOrderPlist(){
        
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentDirectory = paths[0] as? String ?? ""
        let path = documentDirectory.appending("/\(uni_OrdersRemoved_PlistName)")
        
        let fileManager = FileManager.default
        if(!fileManager.fileExists(atPath: path)){
            
        }
        else{
            AgLog.debug("remove Cahgne Order Plist .")
            do {
                try fileManager.removeItem(atPath: path)
            }
                
            catch let error as NSError {
                AgLog.debug("FimeManager Remove Item error: \(error.debugDescription)")
            }
        }
    }
}

