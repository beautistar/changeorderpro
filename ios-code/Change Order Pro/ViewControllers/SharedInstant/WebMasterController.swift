//
//  WebMasterController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 22/01/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire

class WebMasterController: NSObject {

    static let shared = WebMasterController()

   //MARK: call #EDIT_SITE_CONTACT
    func calloffline_SiteContactchangeApi(isNew: Bool, dicSiteData : NSDictionary , completion: @escaping (NSDictionary?) -> ()){
        
        if Reachability.isConnectedToNetwork() == true {
            
            let strURL = kBaseURL + (isNew ? "addcontact" : "editcontact")
            
            let s_name = dicSiteData.string(forKey: "name")
            let s_mobile = dicSiteData.string(forKey: "mobile")
            let s_email = dicSiteData.string(forKey: "email")
            let s_title = dicSiteData.string(forKey: "title")
            let s_id = dicSiteData.string(forKey: "id")
           
            var dictParams:  [String: Any] = [
                "user_id": UserDetails.loginUserId,
                "name": s_name,
                "mobile": s_mobile,
                "email": s_email,
                "title": s_title
            ]
            
            if !isNew {
                dictParams["site_contact_id"] = s_id
            }
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default)
                .responseJSON { response in
                    
                response.logShow()
                    
                    if let dicServer = response.result.value as? NSDictionary {
                        
                        let dicSCont = NSMutableDictionary()
                        
                        let siteId = dicServer.string(forKey: "site_contact_id")
                        dicSCont.setValue(siteId, forKey: "id")

                        dicSCont.setValue(s_name , forKey: "name")
                        dicSCont.setValue(s_mobile, forKey: "mobile")
                        dicSCont.setValue(s_title, forKey: "title")
                        dicSCont.setValue(s_email, forKey: "email")
                        dicSCont.setValue("true", forKey: "is_uploaded")
                        
                        if let isApp = dicSiteData.object(forKey: "is_approver") as? String{
                            dicSCont.setValue(isApp == "0" ? "0" : "1", forKey: "is_approver")
                            
                        }
                        else {
                            let isApp = dicSiteData.object(forKey: "is_approver") as? Bool ?? true
                            dicSCont.setValue(isApp, forKey: "is_approver")
                        }
                        
                        completion(dicSCont)
                    }
                    else{
                        completion(nil)
                    }
            }
        }
    }
    
    //MARK: Upload # NEW PROJECT DATA
    func calloffline_ProjectApi(isNew: Bool,  dicProjectData : NSDictionary , completion: @escaping (NSDictionary?) -> ()){
        
        let siteContactArray = NSMutableArray()
        let offlineSiteContact = dicProjectData.array(forKey: "site_contact_ids").mutable
        
        for dict in offlineSiteContact.dict {
            let serDic: [String : Any] = [
                "site_contact_id": dict.object(forKey: "id") ?? "",
                "is_approver": dict.object(forKey: "is_approver") ?? ""
            ]
            siteContactArray.add(serDic)
        }
        
        let p_jobname = dicProjectData.object(forKey: "job_name") ?? ""
        let p_address = dicProjectData.object(forKey: "address") ?? ""
        let p_address1 = dicProjectData.object(forKey: "address1") ?? ""
        let p_city = dicProjectData.object(forKey: "city") ?? ""
        let p_cname = dicProjectData.object(forKey: "client_name") ?? ""
        let p_cno = dicProjectData.object(forKey: "contract_no") ?? ""
        let p_jobnumber = dicProjectData.object(forKey: "job_number") ?? ""
        let p_state = dicProjectData.object(forKey: "state") ?? ""
        let p_zip = dicProjectData.object(forKey: "zip") ?? ""
        
        let p_offlineID = dicProjectData.object(forKey: "offline_id") ?? ""
        let p_ID = dicProjectData.object(forKey: "id") ?? ""
        
        var dictParams:  [String: Any] = [
            "user_id" : UserDetails.loginUserId,
            "client_name" : p_cname,
            "contract_no" : p_cno,
            "address1": p_address1,
            "job_name" : p_jobname,
            "job_number" : p_jobnumber ,
            "address" : p_address ,
            "state" : p_state ,
            "zip" : p_zip ,
            "city" : p_city,
            "site_contact_ids" : siteContactArray.toJson ?? "",
            "offline_id" : p_offlineID
        ]
        
        if isNew {
            dictParams["offline_id"] = p_ID
        }
        else{
            dictParams["project_id"] = p_ID
            dictParams["offline_id"] = p_offlineID
        }
        
        if Reachability.isConnectedToNetwork() == true {

            let strURL = kBaseURL + (isNew ? "createproject" : "editproject")
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default)
                .responseJSON { response in
                
                response.logShow()
                
                if let dicServer = response.result.value as? NSDictionary{
        
                    if response.isSuccess {
                        
                        let dicProj = NSMutableDictionary()
                        if isNew {
                            guard let projId = dicServer.object(forKey: "project_id") as? Int else {
                                fatalError()
                            }
                            
                            dicProj.setValue("\(projId)", forKey: "id")
                            dicProj.setValue(p_ID, forKey: "offline_id")
                        }
                        else{
                            dicProj.setValue(p_ID, forKey: "id")
                            dicProj.setValue(p_offlineID, forKey: "offline_id")
                        }
                        
                        dicProj.setValue(p_jobname, forKey: "job_name")
                        dicProj.setValue(p_address , forKey: "address")
                        dicProj.setValue(p_address1 , forKey: "address1")
                        dicProj.setValue(p_city, forKey: "city")
                        dicProj.setValue(p_cname, forKey: "client_name")
                        dicProj.setValue(p_cno, forKey: "contract_no")
                        dicProj.setValue(dicServer.string(forKey: "parent_id"), forKey: "parent_id")
                        dicProj.setValue(p_jobnumber, forKey: "job_number")
                        dicProj.setValue(p_state, forKey: "state")
                        dicProj.setValue(p_zip, forKey: "zip")
                        dicProj.setValue(offlineSiteContact, forKey: "site_contact_ids")
                        dicProj.setValue("true", forKey: "is_uploaded")
                        
                        completion(dicProj)
                        return
                    }
                    else{
                        completion(nil)
                    }
                }
            }
        }
    }

    //MARK: call Dlete Project APi for offline delete
     func callOffline_deleteProjectFromServer(projectIDToDelete : String, completion: @escaping (Bool) -> ()){
        
        if Reachability.isConnectedToNetwork() == true {
            
            let strURL = kBaseURL + "deleteproject"
            let dictParams:  [String: String] = ["project_id" : projectIDToDelete]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                response.logShow()
                
                completion(response.isSuccess)
            }
        }
    }
}
