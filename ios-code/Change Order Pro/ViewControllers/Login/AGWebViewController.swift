//
//  AGWebViewController.swift
//  BaseProject
//
//  Created by AshvinGudaliya on 14/02/18.
//  Copyright © 2018 AshvinGudaliya. All rights reserved.
//

import UIKit
import WebKit

class AGWebViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {
    
    var webView: WKWebView!
    var url: URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard url != nil else {
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        webView = WKWebView(frame: self.view.frame)
        webView.navigationDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false

        webView.load(URLRequest(url: url))
        webView.uiDelegate = self
        webView.allowsBackForwardNavigationGestures = true
        
        self.view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
        NSLayoutConstraint.activate([
            webView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0),
            webView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0),
            webView.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor, constant: 0),
            webView.bottomAnchor.constraint(equalTo: self.bottomLayoutGuide.topAnchor, constant: 0),
            ])
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "backWhiteAer"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(dismissViewController))
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0x303030)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.all)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    @objc func dismissViewController() {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func reloadWebView(){
        if webView.url == nil {
            webView.load(URLRequest(url: url))
        }
        else{
            webView.reload()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if webView.isLoading { return }
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}


class AGWebNavigationController: UINavigationController {
    
    private override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    @discardableResult
    required init(url: URL, title: String) {
        let web = AGWebViewController()
        web.url = url
        web.title = title
        super.init(rootViewController: web)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
