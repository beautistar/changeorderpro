//
//  SignUpViewController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 02/10/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire
import FirebaseMessaging

extension UIView {
    func fadeIn() {
        // Move our fade out code from earlier
        UIView.animate(withDuration: 1.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0 // Instead of a specific instance of, say, birdTypeLabel, we simply set [thisInstance] (ie, self)'s alpha
        }, completion: nil)
    }
    
    func fadeOut() {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.alpha = 0.0
        }, completion: nil)
    }
}

class SignUpViewController: UIViewController ,UITextFieldDelegate{
    
    @IBOutlet var txtfirstName: UITextField!
    @IBOutlet var txtlastName: UITextField!
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtInviewCode: UITextField!
    
    @IBOutlet var btnBetaCheckbox: UIButton!
    @IBOutlet var lblBetaTesting: UILabel!
    
    var deviceTokenFireBase = ""
    var arryTextFields = [UITextField]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtfirstName.attributedPlaceholder = "First Name".attribute.foreground(.white)
        txtlastName.attributedPlaceholder = "Last Name".attribute.foreground(.white)
        txtUserName.attributedPlaceholder = "Username".attribute.foreground(.white)
        txtEmail.attributedPlaceholder = "Email".attribute.foreground(.white)
        // txtContactNo.attributedPlaceholder = "Telephone #".attribute.foreground(.white)
        txtPassword.attributedPlaceholder = "Enter Password".attribute.foreground(.white)
        txtInviewCode.attributedPlaceholder = "Re enter Password".attribute.foreground(.white)
        
        for txt in [txtfirstName, txtlastName, txtUserName, txtEmail, txtPassword, txtInviewCode] {
            txt?.text = ""
        }
        
        let token = Messaging.messaging().fcmToken
        deviceTokenFireBase = token ?? ""
        AgLog.debug("This the Tocken Value of FIrebase \(deviceTokenFireBase)")
        
        self.adjustforTextFieldReturn()
        
        btnBetaCheckbox.layer.borderColor = UIColor.white.cgColor
        btnBetaCheckbox.layer.borderWidth = 1
        btnBetaCheckbox.setImage(UIImage(named: "4TabCheckMarkSelected"), for: .selected)
        btnBetaCheckbox.setImage(nil, for: .normal)
        
        lblBetaTesting.numberOfLines = 0
        lblBetaTesting.attributedText = "By checking this box I certify that I have read and AGREE to the ".attribute.foreground(UIColor.white).font(UIFont.systemFont(ofSize: 13)) + "Beta Testing Agreement.".attribute.foreground(UIColor(hex: 0x8DD825)).font(UIFont.boldSystemFont(ofSize: 14))
        
        lblBetaTesting.tapped { _ in
            let vc = AGWebNavigationController(url: URL(string: "http://changeorder.pro/public/CHANGE_ORDER_APP_BETA_TEST_AGREEMENT.html")!, title: "Beta Test Agreement")
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func  adjustforTextFieldReturn(){
        
        arryTextFields.append(txtfirstName)
        arryTextFields.append(txtlastName)
        arryTextFields.append(txtUserName)
        arryTextFields.append(txtEmail)
        //arryTextFields.append(txtContactNo)
        arryTextFields.append(txtPassword)
        arryTextFields.append(txtInviewCode)
        
        txtfirstName.delegate = self
        txtlastName.delegate = self
        txtUserName.delegate = self
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtInviewCode.delegate = self
        
        arryTextFields.sort { $0.frame.origin.y < $1.frame.origin.y }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let currentIndex = arryTextFields.index(of: textField), currentIndex < arryTextFields.count-1 {
            arryTextFields[currentIndex+1].becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func setSignUpdummydata(){
        
        self.txtfirstName.text = "Rowena"
        self.txtlastName.text = "Odhner"
        self.txtUserName.text = "test2"
        self.txtEmail.text = "test2@gmail.com"
        // self.txtContactNo.text = ""
        self.txtPassword.text = "123456"
    }
    
    //    @IBAction func txtTelStartEditing(_ sender: Any) {
    //        self.viewInfoNumber.isHidden = false
    //        self.viewInfoNumber.alpha = 0
    //        self.viewInfoNumber.fadeIn()
    //
    //        Helper.delay(10.0) {
    //            self.viewInfoNumber.isHidden = true
    //        }
    //    }
    //
    //    @IBAction func txtEmailStart(_ sender: Any) {
    //        self.viewInfoNumber.isHidden = true
    //    }
    
    @IBAction func btnBetaCheckBox(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnsignUpClicked(_ sender: Any) {
        self.chekTextFields()
    }
    
    func chekTextFields(){
        
        if txtfirstName.isNull {
            Helper.AgAlertView("NAME".localized)
        }
        else if txtlastName.isNull {
            Helper.AgAlertView("Enter last name")
        }
        else if !self.isValidEmail(testStr: self.txtEmail.text!){
            Helper.AgAlertView("EMAIL".localized)
        }
        else if txtUserName.isNull {
            Helper.AgAlertView("USERNAME".localized)
        }
        else if (self.txtPassword.text?.count)! < 6 {
            Helper.AgAlertView("PASSWORDLENGTH".localized)
        }
        else if (self.txtInviewCode.text?.count)! < 6 {
            Helper.AgAlertView("PASSWORDLENGTH".localized)
        }
        else if self.txtPassword.text! != self.txtInviewCode.text! {
            Helper.AgAlertView("PASSWORDNOTMATCH".localized)
        }
        else if !btnBetaCheckbox.isSelected {
            Helper.AgAlertView("You must agree to the Beta Testing Agreement to create an account.")
        }
        else{
            self.callSignUpApi()
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        //        if textField == self.txtContactNo{
        //            return Helper.phoneNumberFormat(with: self.txtContactNo, string: string, str: str)
        //
        //        }else{
        //
        //            return true
        //        }
        return true
    }
    
    func callSignUpApi() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "createuser"
            let dictParams: [String: String] = [
                "first_name": self.txtfirstName.text!,
                //   "phone": self.txtContactNo.text!,
                "email": self.txtEmail.text!,
                "last_name": self.txtlastName.text!,
                "username": self.txtUserName.text!,
                "password": self.txtPassword.text!,
                "device_token": deviceTokenFireBase
            ]
            
            AgLog.debug("\(dictParams)")
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                MBProgressHUD.hide(for: self.view, animated: true)
                if let dicServer = response.result.value as? NSDictionary{
                    
                    if response.isSuccess {
                        
                        Helper.AgAlertView("SUCCESS_REGISTER".localized) {
                            UserDefaults.standard.setValue("YES" ,forKey:"INFOPROJECTTOSHOW")
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    else{
                        
                        if let message = dicServer.array(forKey: "message").firstObject as? String{
                            Helper.AgAlertView(message)
                        }
                        else{
                            let mess = dicServer.object(forKey: "message") as? String ?? ""
                            Helper.AgAlertView(mess)
                        }
                    }
                }
            }
        }
    }
}

