//
//  PasswordController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire

class PasswordController: UIViewController ,UITextFieldDelegate{
   
    @IBOutlet var txtUname: UITextField!
    @IBOutlet var btnSend: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtUname.attributedPlaceholder = "Enter Email".attribute.foreground(.white)
        txtUname.text = ""
        txtUname.keyboardType = .emailAddress
        txtUname.returnKeyType = .done
        
        btnSend.setTitle("Send", for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSendMeClicked(_ sender: Any) {

        if self.txtUname.isNull{
            Helper.AgAlertView("Enter email")
        }
        else if !self.txtUname.isValidEmail{
            Helper.AgAlertView("EMAIL".localized)
        }
        else{
            self.callforgotPasswotdApi()
        }
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
//
//        if textField == self.txtUname{
//            return Helper.phoneNumberFormat(with: self.txtUname, string: string, str: str)
//
//        }
//        else{
//            return true
//        }
//    }
//
    func callforgotPasswotdApi() {
        
        if Reachability.isConnectedToNetwork() == true {

            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "forgotpassword"
            let dictParams:  [String: String] = ["phone" : self.txtUname.text!]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
            
                if response.isSuccess{
                    Helper.AgAlertView(response.message, okayHandler: {
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                }
                else{
                    Helper.AgAlertView(response.message)
                }
                
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
}
