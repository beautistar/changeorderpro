//
//  LoginController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire
import FirebaseMessaging

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}

class LoginController: UIViewController,UITextFieldDelegate {
    
    var arryTextFields = [UITextField]()
    
    @IBOutlet var btnViewCheck: UIButton!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var btnCheckMark: UIButton!
    var deviceTokenFireBase = ""
    
    @IBOutlet var txtUserId: UITextField!
    var statChkBx = ""
    var statChkVPass = ""
    
    @IBOutlet var btnPrivacyPolicy: UIButton!
    @IBOutlet var btnTreamsAndCondition: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtUserId.attributedPlaceholder = "Username or Email".attribute.foreground(.white)
        txtUserId.text = ""
        
        txtPassword.attributedPlaceholder = "Enter Password".attribute.foreground(.white)
        txtPassword.text = ""
        
        if let classA = UserDefaults.standard.data(forKey: "USERDETAIL") {
            if let result = NSKeyedUnarchiver.unarchiveObject(with: classA) as? NSDictionary {
                
                AgLog.debug("\(result.string(forKey: "username")) is logged in go to Home view")
                UserDetails.loginUserId = result.string(forKey: "id")
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.switchViewControllers()
            }
            
        }
        else{
            AgLog.debug("user is not logged in")
        }
        
        let token = Messaging.messaging().fcmToken
        deviceTokenFireBase = token ?? ""
        AgLog.debug("FireBase Login Token \(deviceTokenFireBase)")
        
        txtUserId.delegate = self
        txtPassword.delegate = self
        
        self.btnViewCheck.setBackgroundImage(UIImage(named:"CheckNotApprover.png"), for: .normal)
        self.btnViewCheck.setBackgroundImage(UIImage(named:"checkRight.png"), for: .selected)
    }
    
    @IBAction func btnViewPassClicked(_ sender: UIButton)
    {
        if sender.isSelected {
            txtPassword.isSecureTextEntry = true
            sender.isSelected = false
        }
        else{
            txtPassword.isSecureTextEntry = false
            sender.isSelected = true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtUserId {
            txtPassword.becomeFirstResponder()
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    
    override func viewDidLayoutSubviews() {
        
        let token = Messaging.messaging().fcmToken
        deviceTokenFireBase = token ?? ""
        AgLog.debug("FireBase Login did layout Token \(deviceTokenFireBase)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    @IBAction func btnContinueClicked(_ sender: Any) {
        self.chekTextFields()
    }
    
    @IBAction func btnPrivacyPolicy(_ sender: Any) {
        let vc = AGWebNavigationController(url: URL(string: "http://changeorder.pro/public/Change_Order_Pro_Privacy_Policy.html")!, title: "Privacy Policy")
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnTreamsAndCondition(_ sender: Any) {
        let vc = AGWebNavigationController(url: URL(string: "http://changeorder.pro/public/CHANGE_ORDER_PRO_INC_T26C_APP_VERSION.html")!, title: "Terms and Conditions")
        self.present(vc, animated: true, completion: nil)
    }
    
    func chekTextFields(){
        
        if txtUserId.isNull {
            Helper.AgAlertView("USERNAME_MESSAGE".localized)
        }
        else if txtPassword.isNull {
            Helper.AgAlertView("PASSWORD_MESSAGE".localized)
        }
        else{
            self.callSigninApi()
        }
    }
    
    func callSigninApi() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "login"
            
            let dictParams: [String: String] = [
                "username": self.txtUserId.text!,
                "password": self.txtPassword.text!,
                "device_token": deviceTokenFireBase,
                "device_name": UIDevice.current.name,
                "device_type":"iOS",
                "udid": UserDetails.loginTimeUDID
            ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                
                if let dicServer = response.result.value as? NSDictionary {
                    
                    if response.isSuccess {
                        
                        if let is_login = dicServer["is_login"] as? Bool, is_login {
                            AGAlertBuilder(withAlert: "", message: response.message)
                                .addDefaultAction(title: "OK", handler: { _ in
                                    UserDetails.loginUserId = dicServer.dictionary(forKey: "user").string(forKey:"id")
                                    CheckUserDetails.logoutOtherDevices(message: response.message)
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    //self.loginRemainProcess(withDict: dicServer)
                                })
                                .addCancelAction(title: "Cancel", handler: { _ in
                                    UserDetails.clearUserData()
                                })
                                .show()
                            return
                        }
                        else{
                            self.loginRemainProcess(withDict: dicServer)
                            CheckUserDetails.checkUser { }
                        }
                        
                    }
                    else{
                        Helper.AgAlertView(response.message)
                    }
                }
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    func loginRemainProcess(withDict dicServer: NSDictionary) {
        UIApplication.shared.registerForRemoteNotifications()
        
        MBProgressHUD.hide(for: self.view, animated: true)
        
        UserDetails.loginUserId = dicServer.dictionary(forKey: "user").string(forKey:"id")
        
        DataController.sharedInstance.createCompnayLogoFolder()
        if let arraycIn =  dicServer.object(forKey: "company_info") as? NSArray{
            if arraycIn.count > 0 {
                let dic = arraycIn[0] as? NSDictionary ?? NSDictionary()
                if let cName = dic.object(forKey: "company_name") as? String{
                    let logoUrl = dic.object(forKey: "logo_url") as? String ?? ""
                    if logoUrl.count > 0 {
                        let path1 = kBaseURL + "public/" + logoUrl
                        Alamofire.request(path1).responseImage { response in
                            
                            if let image = response.result.value {
                                AgLog.debug("logo downloaded: 1\(image)")
                                let cId = dic.integer(forKey: "id")
                                let cLogoName = "off_logo_\(cId).jpg"
                                DataController.sharedInstance.saveCompnayLogoImageToDocumentDirFolder(withImage: image, imageName: cLogoName)
                            }
                        }
                    }
                    UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: dic), forKey: "COMPNAYDETAIL")
                    AgLog.debug("compnay detail saved ",cName)
                }
            }
        }
        let userDetail = dicServer["user"] as? NSDictionary ?? NSDictionary()
        let tempID = userDetail.string(forKey: "template_id")
        UserDefaults.standard.setValue(tempID, forKey: "TEMPLATE")
        UserDefaults.standard.setValue(dicServer["is_setup_company_info"] as? String ?? "", forKey: "is_setup_company_info")
        
        if(dicServer.string(forKey: "is_first_login") == "1") {
            UserDefaults.standard.removeObject(forKey: "InfoProjectShow")
            UserDefaults.standard.removeObject(forKey: "newOrder")
            UserDefaults.standard.removeObject(forKey: "eyeInfo")
            UserDefaults.standard.removeObject(forKey: "sortInfo")
            UserDefaults.standard.removeObject(forKey: "dropdownInfo")
            UserDefaults.standard.removeObject(forKey: "swapInfo")
            UserDefaults.standard.removeObject(forKey: "approved")
        }
        else
        {
            UserDefaults.standard.set("YES", forKey: "InfoProjectShow")
            UserDefaults.standard.set("YES", forKey: "newOrder")
            UserDefaults.standard.set("YES", forKey: "eyeInfo")
            UserDefaults.standard.set("YES", forKey: "sortInfo")
            UserDefaults.standard.set("YES", forKey: "dropdownInfo")
            UserDefaults.standard.set("YES", forKey: "swapInfo")
            UserDefaults.standard.set("YES", forKey: "approved")
        }
        
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: userDetail), forKey: "USERDETAIL")
        
        let userPass = self.txtPassword.text!
        UserDefaults.standard.setValue(userPass, forKey: "PASSWORD")
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.switchViewControllers()
    }
    
    @IBAction func btnCheckmarkclicked(_ sender: Any) {
        // checkRightImage.png
        //  CheckEmpty.png
        
        if statChkBx == ImageFlag.selected{
            
            statChkBx = "unselected"
            self.btnCheckMark.setBackgroundImage(UIImage(named:"CheckNotApprover.png"), for: .normal)
            
        }else{
            // checkRight.png
            statChkBx = ImageFlag.selected
            self.btnCheckMark.setBackgroundImage(UIImage(named:"checkRight.png"), for: .normal)
        }
    }
    
    @IBAction func btnForgotPassClicked(_ sender: Any) {
        let pass = self.storyboard?.instantiateViewController(withIdentifier: "password") as! PasswordController
        self.navigationController?.pushViewController(pass, animated: true)
    }
    
    @IBAction func btnSIgnUpClicked(_ sender: Any) {
        let signUP = self.storyboard?.instantiateViewController(withIdentifier: "signUp") as! SignUpViewController
        self.navigationController?.pushViewController(signUP, animated: true)
    }
}

