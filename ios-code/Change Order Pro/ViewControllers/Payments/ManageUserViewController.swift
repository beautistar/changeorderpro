//
//  ManageUserViewController.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 19/07/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire

class ManageUserViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var usersModel: [ManageSubscriptionUser] = []
    
    enum RejectionType: String {
        case Approved = "1"
        case Deny = "2"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.estimatedRowHeight(70)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.pullToRefresh = { ref in
            ref.beginRefreshing()
            self.getAllManageSubscriptionMembers()
        }
        
        let nib = UINib(nibName: "MYProjCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "projCell")

        self.navigationController?.navigationBar.tintColor = UIColor.white;
        self.navigationItem.title = "Manage Users"
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0x303030)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(hex: 0x8DD825)]
        self.getAllManageSubscriptionMembers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "projCell") as! MYProjCell
        cell.lblProjTitle.text = "\(usersModel[indexPath.row].first_name) \(usersModel[indexPath.row].last_name)"
        cell.lblProjAddress.text = usersModel[indexPath.row].email
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        switch self.usersModel[editActionsForRowAt.item].status {
        case .ApprovedDeny:
            let approved = UITableViewRowAction(style: .normal, title: "\(TableRowActionType.approve)\n Approved") { action, index in
                self.removeSubscriptionUser(with: editActionsForRowAt, type: .Approved)
            }
            approved.backgroundColor = UIColor(red: 49/255, green: 167/255, blue: 106/255, alpha: 1.0)
            
            let delete = UITableViewRowAction(style: .normal, title:"\(TableRowActionType.deny)\n Delete ") { action, index in
                self.removeSubscriptionUser(with: editActionsForRowAt, type: .Deny)
            }
            delete.backgroundColor = .red
            
            return [delete, approved]
            
        default:
            let delete = UITableViewRowAction(style: .normal, title:"\(TableRowActionType.deny)\n Delete ") { action, index in
                self.removeSubscriptionUser(with: editActionsForRowAt, type: .Deny)
            }
            delete.backgroundColor = .red
            
            return [delete]
        }
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        switch self.usersModel[indexPath.item].status {
        case .ApprovedDeny:
            let approved = UIContextualAction(style: .normal, title: "\(TableRowActionType.approve)\n Approved") { action, view, completionHandler in
            
                self.removeSubscriptionUser(with: indexPath, type: .Approved)
                completionHandler(true)
            }
            approved.backgroundColor = UIColor(red: 49/255, green: 167/255, blue: 106/255, alpha: 1.0)
            
            let delete = UIContextualAction(style: .normal, title: "\(TableRowActionType.deny)\n Deny ") { action, view, completionHandler in
                self.removeSubscriptionUser(with: indexPath, type: .Deny)
                completionHandler(true)
            }
            delete.backgroundColor = .red
            
            let config = UISwipeActionsConfiguration(actions: [delete, approved])
            config.performsFirstActionWithFullSwipe = false
            
            return config
            
        default:
            let delete = UIContextualAction(style: .normal, title: "\(TableRowActionType.delete)\n Delete ") { action, view, completionHandler in
                self.removeSubscriptionUser(with: indexPath, type: .Deny)
                completionHandler(true)
            }
            delete.backgroundColor = .red
            
            let config = UISwipeActionsConfiguration(actions: [delete])
            config.performsFirstActionWithFullSwipe = false
            
            return config
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Helper.size(iPhone: 3, iPad: 6)
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func getAllManageSubscriptionMembers() {
        let strURL = kBaseURL + "managesubscription"
        
        let dictParams:  [String: String] = [
            "user_id": UserDetails.loginUserId
        ]
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
            
            response.logShow()
            switch response.result {
            case .success(let json):
                if let j = json as? [String: Any], let users = j["users"] as? [[String: AnyObject]] {
                    self.usersModel = users.map {
                        ManageSubscriptionUser(with: $0)
                    }
                    self.tableView.reloadData()
                    
                    if self.usersModel.count == 0 {
                        AGAlertBuilder(withAlert: "Invite Users", message: "You have no users to manage at this time. Tap on the ¨Invite Users¨ button to start adding users to your account. As users accept they will be displayed here and you can delete any of them from your account at any time.")
                            .addAction(title: "Invites", style: .default, handler: { _ in
                                self.navigationController?.popViewController(animated: true)
                            })
                            .show()
                    }
                }
                else{
                    Helper.AgAlertView(response.message)
                }
                break
                
            case .failure(let error):
                Helper.AgAlertView(error.localizedDescription)
            }
            
            self.tableView.endRefreshing()
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func removeSubscriptionUser(with otherUserId: IndexPath, type: RejectionType) {
        
        let strURL = kBaseURL + "approveordenyuser"
        
        let dictParams:  [String: String] = [
            "user_id": UserDetails.loginUserId,
            "other_user_id": self.usersModel[otherUserId.item].id,
            "status": type.rawValue
        ]
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
            
            response.logShow()
            switch response.result {
            case .success:
                
                if type == .Deny {
                    self.usersModel.remove(at: otherUserId.item)
                }
                else{
                    self.usersModel[otherUserId.item].status = .Delete
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                break
                
            case .failure(let error):
                Helper.AgAlertView(error.localizedDescription)
            }
            
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
}
