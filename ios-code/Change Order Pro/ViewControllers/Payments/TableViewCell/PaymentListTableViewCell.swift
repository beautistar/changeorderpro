//
//  PaymentListTableViewCell.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 10/07/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit

class PaymentListTableViewCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
//    @IBOutlet weak var lblPrices: UILabel!
//    @IBOutlet weak var lblPricesDetails: UILabel!
    @IBOutlet weak var btnSubscription: AGButton!
    @IBOutlet weak var lblBilledTypes: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backView.layer.cornerRadius = 7
        backView.layer.shadowColor = UIColor.black.cgColor
        backView.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        backView.layer.shadowRadius = 2
        backView.layer.shadowOpacity = 0.4
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setUpData(with payment: PaymentOptionsModel, subscription: SubscriptionDetailModel?) {
        if let subscrip = subscription, subscrip.is_active_sub, subscrip.product_id == payment.subscriptionId {
            btnSubscription.setTitle("Subscribed", for: .normal)
            btnSubscription.isUserInteractionEnabled = false
            backView.alpha = 0.5
        }
        else{
            btnSubscription.isUserInteractionEnabled = true
            btnSubscription.setTitle(payment.btnName, for: .normal)
            backView.alpha = 1
        }
        
        lblTitle.text = payment.title
        lblDescription.text = payment.descrip
        lblBilledTypes.text = payment.billedType
    }
}
