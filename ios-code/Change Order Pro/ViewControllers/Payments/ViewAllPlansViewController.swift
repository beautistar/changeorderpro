//
//  ViewAllPlansViewController.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 10/07/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit
import StoreKit
import Alamofire
import SwiftMessages

let ANNUAL_SUBSCRIPTION_PRODUCTID = "com.changeOrders.annualsubscription"
let USER_SUBSCRIBE_STATUS = "USER_SUBSCRIBE_STATUS"
let USER_SUBSCRIBE_VALUE = "SUBSCRIPTION_ACTIVATED"
let INAPP_RECEIPTVALIDATE = "INAPP_RECEIPTVALIDATE"

class ViewAllPlansViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    @IBOutlet weak var tableView: AGTableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnShare: AGButton!
    @IBOutlet weak var btnManageUser: AGButton!
    @IBOutlet weak var bottomView: UIView!
    
    var paymentOptionasData: [PaymentOptionsModel] = []
    var subscriptionDetail: SubscriptionDetailModel? = nil
    
    var iapProducts = [SKProduct]()
    var productID = ""
    var productsRequest = SKProductsRequest()
    
    var myActivityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight(120)
        tableView.registerNib(PaymentListTableViewCell.self)
        scrollView.pullToRefreshScroll = { _ in
            self.getSubscriptionDetails()
        }
        
        self.navigationItem.title = "View All Plans"
       
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0x303030)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(hex: 0x8DD825)]
        self.navigationItem.leftBarButtonItem = barButton(with: #imageLiteral(resourceName: "ic_close_black"), selector: #selector(dissmissView(_:)))
        
        btnShare.backgroundColor = UIColor(hex: 0x8DD825)
        btnShare.setTitleColor(UIColor.white, for: .normal)
        btnShare.action = {
            self.inviteCode()
        }

        btnManageUser.backgroundColor = UIColor(hex: 0x8DD825)
        btnManageUser.setTitleColor(UIColor.white, for: .normal)
        btnManageUser.action = {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManageUserViewController")
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
        if UserDetails.isPurchaseProduct && !UserDetails.isPurchaseReceptSended {
            self.checkValidationofreceipt()
        }
        
        self.btnShareHidden(isHidden: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getSubscriptionDetails()
    }
    
    func setRestoreBtn(isShow: Bool) {
        if isShow {
            let Redeem = UIBarButtonItem(title: "Restore", style: .plain, target: self, action: #selector(redeemBarButtons(_:)))
            Redeem.tintColor = .white
            self.navigationItem.rightBarButtonItem = Redeem
        }
        else{
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    func btnShareHidden(isHidden: Bool) {
        
        btnShare.isHidden = isHidden
        btnManageUser.isHidden = isHidden
        
        if isHidden {
            bottomView.heightConstaint?.constant = 0
        }
        else {
            bottomView.heightConstaint?.constant = Helper.size(iPhone: 50, iPad: 80)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func redeemBarButtons(_ sender: UIBarButtonItem) {
        self.callToRestorePurchaseifAvailable()
    }
    
    func inviteCode() {
        if let subscrip = self.subscriptionDetail, subscrip.active_plan > subscrip.active_user {
            
            let alert = AGAlertBuilder(withAlert: "Please enter email of user", message: "")
            alert.addDefaultAction(title: "Send", handler: { _ in
                if let t = alert.textFields?.first, !t.isNull, t.isValidEmail {
                    self.InviteCodeSuccess(with: t.text ?? "")
                }
                else{
                    AGAlertBuilder(withAlert: "Please Enter proper Email-ID", message: nil)
                        .addDefaultAction(title: "Try again", handler: { _ in
                            self.inviteCode()
                        })
                        .addDefaultAction(title: "Cancel", handler: nil)
                        .show()
                }
            })
            
            alert.addDefaultAction(title: "Cancel", handler: nil)
            
            alert.addTextField { (txt) in
                txt.placeholder = "Enter Email-ID"
                txt.textAlignment = .center
                txt.keyboardType = .emailAddress
            }
            
            alert.show()
        }
        else{
            AgAlertView("You have not ")
        }
    }
    
    func dissmissView(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentOptionasData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClassIdentifier: PaymentListTableViewCell.self, for: indexPath)
        cell.selectionStyle = .none
        cell.setUpData(with: paymentOptionasData[indexPath.row], subscription: self.subscriptionDetail)
        cell.btnSubscription.action = {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.callToSubscriptionProcessYearly1(id: self.paymentOptionasData[indexPath.row].subscriptionId)
        }
        return cell
    }
    
    
    func callToSubscriptionProcessYearly1(id: String) {
        AgLog.debug("call inapp purchase to subscribe")
        let productID: NSSet = NSSet(object: id);
        let productsRequest: SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
        productsRequest.delegate = self;
        productsRequest.start();
    }

    func callToRestorePurchaseifAvailable() {
        
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    //MARK: check Validation Of the receipt user Had already purchase
    func checkValidationofreceipt() {
        
        if Reachability.isConnectedToNetwork() == true {
            if let receiptURL = Bundle.main.appStoreReceiptURL, let isReachable = try? receiptURL.checkResourceIsReachable(), isReachable {
                AgLog.debug("Recept available:\(receiptURL.absoluteString)")
                
                let data = NSData(contentsOf: receiptURL)

                if let d = data?.base64EncodedString() {
                    self.CallApiToSetReceiptData(receipt: d)
                    return
                }
            }
            else{
                AgLog.debug("ther is no recept show purchase alert")
                UserDefaults.standard.set("NO", forKey: INAPP_RECEIPTVALIDATE)
            }
        }
        else{
            AgLog.debug("NO Network user previous set value")
        }
    }
    
    func CallApiToSetReceiptData(receipt: String, times: Int = 1) {

        SwiftMessages.hideAll()
        
        if Reachability.isConnectedToNetwork() == true {
            
            if UserDetails.isPurchaseReceptSended { return }
            
            NetworkActivityIndicatorManagerForPurchase.networkOperationStarted()
            
            let strURL = kBaseURL + "subcriptionCallAPI"
        
            let dictParams:  [String: String] = [
                "receipt" : receipt,
                "user_id": UserDetails.loginUserId
            ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()

                switch response.result {
                case .success:
                    if response.isSuccess {
                        AgLog.debug("Call Success")
                        CheckUserDetails.checkUser(completion: {
                            
                        })
                        UserDetails.isPurchaseReceptSended = true
                    }
                    else{
                        UserDetails.isPurchaseReceptSended = false
                    }
                    break
                    
                case .failure(let error):
                    if error._code == 4 {
                        NetworkActivityIndicatorManagerForPurchase.networkOperationFinished()
                        return
                    }
                    
                    if times == 3 {
                        self.AgAlertView(response.message)
                    }
                    else{
                        self.CallApiToSetReceiptData(receipt: receipt, times: times + 1)
                    }
                }
                
                self.getSubscriptionDetails()
                NetworkActivityIndicatorManagerForPurchase.networkOperationFinished()
            }
        }
    }
    
    //MARK: InApp  Purchase  subscription Delegates method Starts
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse){
        AgLog.debug(" didReceive \(response)")
        
        if (response.products.count > 0) {
            let products = response.products
            
            for p in products {
                AgLog.debug("Found product: \(p.productIdentifier) \(p.localizedTitle) \(p.price.floatValue)")
                iapProducts.append(p)
            }
            purchaseMyProduct(product: iapProducts[0])
        }
        else{
            MBProgressHUD.hide(for: self.view, animated: true)
            AgLog.debug("NoProduct Available")
        }
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        if request is SKProductsRequest {
            AgLog.debug("Subscription Options Failed Loading: \(error.localizedDescription)")
        }
    }
    
    func purchaseMyProduct(product: SKProduct) {
        if SKPaymentQueue.canMakePayments() {
            
            ShowActivityControllerinView()
            
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
            
            AgLog.debug("PRODUCT TO PURCHASE: \(product.productIdentifier)")
            productID = product.productIdentifier
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.showAlertwhilePurchasing(withmessage: "Purchases are disabled in your device!")
        }
    }

    func ShowActivityControllerinView(){
        
        myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        myActivityIndicator.color = UIColor.black
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.startAnimating()

        view.addSubview(myActivityIndicator)
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction: AnyObject in transactions {
            if let trans = transaction as? SKPaymentTransaction {

                switch trans.transactionState {
                case .purchased:
                    MBProgressHUD.hide(for: self.view, animated: true)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    
                    AgLog.debug("User SUBSCRIBE yearly Successfully")
                    self.setValueAfterInAppSuccessfully()
                    AgAlertView("Subscription successful")
                    myActivityIndicator.stopAnimating()
                    break
                    
                case .failed:
                    UserDetails.isPurchaseProduct = false
                    MBProgressHUD.hide(for: self.view, animated: true)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
//                    if let e = trans.error {
//                        AgAlertView(title: "Fail to subscribe", e.localizedDescription)
//                    }
//                    else{
//                        AgAlertView("Failed to subscribe.\nPlease try again later")
//                    }
                    break
                    
                case .restored:
                    UserDetails.isPurchaseProduct = false
                    MBProgressHUD.hide(for: self.view, animated: true)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                    
                default: break
        
                }
                
                myActivityIndicator.stopAnimating()
            }
        }
    }
    
    func setValueAfterInAppSuccessfully(){
        SwiftMessages.hideAll()
        UserDetails.isPurchaseProduct = true
        UserDetails.isPurchaseReceptSended = false
        AgLog.debug("SuccessFullllll Subsription ")
        self.checkValidationofreceipt()
        UserDefaults.standard.set("YES", forKey: INAPP_RECEIPTVALIDATE)
        myActivityIndicator.stopAnimating()
        UserDefaults.standard.set(USER_SUBSCRIBE_VALUE, forKey: USER_SUBSCRIBE_STATUS)
    }
    
    // Restore Purchse Method
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error){
        
        AgLog.debug("Theeee \(error.localizedDescription)")
        self.showAlertwhilePurchasing(withmessage: error.localizedDescription)
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        
        AgLog.debug("SKPaymentQueue \(queue.transactions)")
        
        if queue.transactions.count == 0{
            AgLog.debug("NO Item to restore")
            AgAlertView("No item available to restore")
        }
        else{
            
            for transaction in queue.transactions {
                let t: SKPaymentTransaction = transaction
                let prodID = t.payment.productIdentifier as String
                
                switch prodID {
                case ANNUAL_SUBSCRIPTION_PRODUCTID:
                    
                    self.setValueAfterInAppSuccessfully()
                    self.AgAlertView("Restored Successfully")
                    
                    break
                    
                default:
                    AgLog.debug("iap not found")
                    self.AgAlertView("No item available to restore")
                }
            }
        }
    }
    
    func showAlertwhilePurchasing(withmessage : String)  {
        
        myActivityIndicator.stopAnimating()
        
        AGAlertBuilder(withAlert: "", message: withmessage)
            .addCancelAction(title: "OK", handler: nil)
            .show()
    }
    
    func AgAlertView(title: String = "", _ alertmessage: String, okayHandler: (() -> ())? = nil)  {
        if alertmessage.isNull { return }
        
        let alertCon = UIAlertController(title: title, message: alertmessage, preferredStyle: .alert)
        let okay = UIAlertAction(title: "OK_TEXT".localized, style: .default) { _ in
            if let b = okayHandler {
                b()
            }
        }
        alertCon.addAction(okay)
        self.present(alertCon, animated: true, completion: nil)
    }
    
    //MARK: InApp  Purchase  subscription Delegates method End
    func InviteCodeSuccess(with userEmail: String) {
        let strURL = kBaseURL + "inviteuser"
        
        let dictParams:  [String: String] = [
            "user_id": UserDetails.loginUserId,
            "email": userEmail
        ]
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
            
            response.logShow()
            switch response.result {
            case .success:
                self.AgAlertView(response.message)
                break
                
            case .failure(let error):
                self.AgAlertView(error.localizedDescription)
            }
            
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func getSubscriptionDetails() {
        let strURL = kBaseURL + "getsubscriptiondetail"

        let dictParams:  [String: String] = [
            "user_id": UserDetails.loginUserId
        ]

        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in

            response.logShow()
            switch response.result {
            case .success(let json):
                if let j = json as? [String: AnyObject], let d = j["details"] as? [String: AnyObject]{
                    self.subscriptionDetail = SubscriptionDetailModel(with: d)
                    
                    if let s = self.subscriptionDetail {
                        self.btnShareHidden(isHidden: !s.is_active_sub)
                        self.setRestoreBtn(isShow: s.is_active_sub)
                        
                        //self.btnShareHidden(isHidden: s.is_active_sub)
                        //self.setRestoreBtn(isShow: !s.is_active_sub)
                    }
                    self.paymentOptionasData = PaymentOptionsModel.get()
                }
                else{
                    self.AgAlertView(response.message)
                }
                self.tableView.reloadData()
                break

            case .failure(let error):
                self.AgAlertView(error.localizedDescription)
            }

            self.scrollView.endRefreshing()
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
}


func checkValidationofreceipttest() {

    if Reachability.isConnectedToNetwork() == true {
        if let receiptURL = Bundle.main.appStoreReceiptURL, let isReachable = try? receiptURL.checkResourceIsReachable(), isReachable {
            AgLog.debug("Recept available:\(receiptURL.absoluteString)")

            let data = NSData(contentsOf: receiptURL)

                            let body = [
                                "receipt-data": data?.base64EncodedString(),
                                "password": "5c577178f1c0498289717091f21206ea"
                            ]

                            let bodyData = try! JSONSerialization.data(withJSONObject: body, options: [])
                            let appleServer = receiptURL.lastPathComponent == "sandboxReceipt" ? "sandbox" : "buy"
            
                            let stringURL = "https://\(appleServer).itunes.apple.com/verifyReceipt"
            
                            let url = URL(string: stringURL)!
                            var request = URLRequest(url: url)
                            request.httpMethod = "POST"
                            request.httpBody = bodyData
            
                            let task = URLSession.shared.dataTask(with: request) { (responseData, response, error) in
                                if error != nil {
                                    AgLog.debug("eRROr on validation \(error!.localizedDescription)")
                                }
            
                                else if let responseData = responseData {
            
                                    let json = try! JSONSerialization.jsonObject(with: responseData, options: []) as! [String: Any]
            
            
                                    if let receiptInfo: NSArray = json["latest_receipt_info"] as? NSArray,
                                        let lastReceipt = receiptInfo.lastObject as? NSDictionary {
            
                                        let subID = lastReceipt["product_id"] as? String ?? ""
            
                                        if subID == ANNUAL_SUBSCRIPTION_PRODUCTID {
            
                                            let formatter = DateFormatter()
                                            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            
                                            let expirationDate: NSDate = (formatter.date(from: lastReceipt["expires_date"] as? String ?? "") as NSDate?)!
            
                                            AgLog.debug("Expiration Date  \(expirationDate) with ID \(subID)"  )
            
                                            let todaydate = Date()
            
                                            AgLog.debug("date  today \(todaydate)")
                                            let isGreater = todaydate > expirationDate as Date
                                            if isGreater == true{
                                                AgLog.debug("Subscription expire or not reneuwed by user")
                                                AgLog.debug("Stop access of app services")
                                                UserDefaults.standard.set(USER_SUBSCRIBE_VALUE, forKey: USER_SUBSCRIBE_STATUS)
                                                UserDefaults.standard.set("NO", forKey: INAPP_RECEIPTVALIDATE)
                                            }
                                            else{
                                                AgLog.debug("Enjoy service ,User has subscribe allow to create Orders")
                                                UserDefaults.standard.set("YES", forKey: INAPP_RECEIPTVALIDATE)
                                            }
                                        }
                                    }
                                    else{
            
                                        AgLog.debug("NO date or last receipt info")
                                        UserDefaults.standard.set("NO", forKey: INAPP_RECEIPTVALIDATE)
                                    }
                                }
                            }
            
                            task.resume()
        }
        else{
            AgLog.debug("ther is no recept show purchase alert")
            UserDefaults.standard.set("NO", forKey: INAPP_RECEIPTVALIDATE)
        }
    }
    else{
        AgLog.debug("NO Network user previous set value")
    }
}
