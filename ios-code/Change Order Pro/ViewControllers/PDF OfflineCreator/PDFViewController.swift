//
//  PDFViewController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 21/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit

enum TemplateTypes: String {
    case order_form_1 = "1"
    case order_form_2 = "2"
    case order_form_3 = "3"
    case order_form_4 = "4"
    
    var name: String {
        switch self {
        case .order_form_1: return "order_form_1"
        case .order_form_2: return "order_form_2"
        case .order_form_3: return "order_form_3"
        case .order_form_4: return "order_form_4"
        }
    }
}

class PDFViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet var webView: UIWebView!
    
    var pdf_link = ""
    var myformCreator : MyPdfPreviewCreator!
    var dicallDetail = NSDictionary()
    var htmlFormName = "order_form_1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.delegate = self
        // Do any additional setup after loading the view.
        
        webView.backgroundColor = UIColor.white
        AGProgress.shared.showProgress(with: "Loading...")
      
    }
    
    override func viewDidLayoutSubviews() {
        let contentSize:CGSize = webView.scrollView.contentSize
        AgLog.debug("web view content size \(contentSize) and curent  veiw frame is \(self.webView.frame)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated) // No need for semicolon
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.setNavItems()
        if uni_isWorking_Offline == true{
            
            if let template = UserDefaults.standard.object(forKey: "TEMPLATE") as? String{
                
                let tempType = TemplateTypes(rawValue: template) ?? .order_form_1
                AgLog.debug("Selected Template name: \(tempType.name) id: \(tempType.hashValue)")
                htmlFormName = tempType.name
            }
            else{
                AgLog.debug("No template founded")
            }
            
            let selectedPID = "\(dicallDetail.object(forKey: "project_id") ?? "")"
            let newDic = dicallDetail.mutableCopy() as! NSMutableDictionary
            
            for dic in uni_Array_allProjects {
                
                let dicP = dic as? NSDictionary ?? NSDictionary()
                let pId = "\(dicP.object(forKey: "id") ?? "")"
                
                AgLog.debug("SelctedId = \(selectedPID) Cureent ID = \( pId)")
                
                if selectedPID == pId {
                    
                    let clName = dicP.string(forKey: "client_name")
                    let jobNmbr = dicP.string(forKey: "job_number")
                    let contract_no = dicP.string(forKey: "contract_no")
                    let jobNme = dicP.string(forKey: "job_name")
                    let address = dicP.string(forKey: "address")
                    let address1 = dicP.string(forKey: "address1")
                    let city = dicP.string(forKey: "city")
                    let state = dicP.string(forKey: "state")
                    let zip = dicP.string(forKey: "zip")
                    
                    newDic.setValue(clName, forKey: "client_name")
                    newDic.setValue(jobNmbr, forKey: "job_number")
                    newDic.setValue(jobNme, forKey: "job_name")
                    newDic.setValue(contract_no, forKey: "contract_no")
                    newDic.setValue(address, forKey: "address")
                    newDic.setValue(address1, forKey: "address1")
                    newDic.setValue(city, forKey: "city")
                    newDic.setValue(state, forKey: "state")
                    newDic.setValue(zip, forKey: "zip")
                    dicallDetail = newDic
                    
                    self.createofflineHTML()
                    return
                }
            }
            self.createofflineHTML()
        }
        else if pdf_link.count > 0 {
            
            let prePath = kBaseURL + pdf_link
            if let url = URL (string: prePath) {
                webView.loadRequest(URLRequest(url: url))
            }
        }
    }
    
    func createofflineHTML(){
        
        myformCreator = MyPdfPreviewCreator()
        
        if let invoiceHTML = myformCreator.readAllOrderData(dicData: dicallDetail, fortemplate: htmlFormName){
            
            let baseUrl = Bundle.main.url(forResource: "\(htmlFormName)", withExtension:"html")
            webView.loadHTMLString(invoiceHTML, baseURL: baseUrl)
            webView.scalesPageToFit = true
            self.webView.contentMode = .scaleAspectFit
        }
    }
    
    func setNavItems(){

        let menuBtn = UIBarButtonItem(
            image: UIImage(named: "backWhiteAer"),
            style: .plain,
            target: self,
            action: #selector(backClicked(sender:))
        )
        
        self.navigationItem.leftBarButtonItem = menuBtn
    }
    
    func backClicked(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
        if let previousViewController = self.navigationController?.viewControllers.last as? MyAllOrdersController {
            previousViewController.backFrom = "PDF"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        AGProgress.shared.hideProgress()
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        AGProgress.shared.showProgress(with: "Loading...")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if !webView.isLoading {
            AGProgress.shared.hideProgress()
        }
    }
}

