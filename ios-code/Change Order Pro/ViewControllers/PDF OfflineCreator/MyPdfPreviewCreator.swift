//
//  MyPdfPreviewCreator.swift
//  HtData
//
//  Created by MobiSharnam on 19/01/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit

class MyPdfPreviewCreator: NSObject {
    
    //  let pathToInvoiceHTMLTemplate = Bundle.main.path(forResource: "order_form_1", ofType: "html")
    
    var pathToInvoiceHTMLTemplate = Bundle.main.path(forResource: "order_form_1", ofType: "html")
    
    let pathToSingleItemHTMLTemplate = Bundle.main.path(forResource: "order_items", ofType: "html")
    let pathToitemTotalHTMLTemplate = Bundle.main.path(forResource: "order_Total", ofType: "html")
    let pathToAllImageHTMLTemplate = Bundle.main.path(forResource: "order_AllImages", ofType: "html")
    
    var invoiceNumber: String!
    
    override init() {
        super.init()
    }
    
    func readAllOrderData(dicData : NSDictionary ,fortemplate : String)-> String!{
        
        if fortemplate == "order_form_1"{
            pathToInvoiceHTMLTemplate = Bundle.main.path(forResource: "order_form_1", ofType: "html")
        }
        else if fortemplate == "order_form_2"{
            pathToInvoiceHTMLTemplate = Bundle.main.path(forResource: "order_form_2", ofType: "html")
            
        }
        else if fortemplate == "order_form_3"{
            pathToInvoiceHTMLTemplate = Bundle.main.path(forResource: "order_form_3", ofType: "html")
            
        }
        else if fortemplate == "order_form_4"{
            pathToInvoiceHTMLTemplate = Bundle.main.path(forResource: "order_form_4", ofType: "html")
        }

        self.invoiceNumber = "\(dicData.string(forKey: "change_order_id"))"
        if self.invoiceNumber.contains("_offline"){
            let cID = self.invoiceNumber.replacingOccurrences(of: "_offline", with: "")
            
            let sortID =  String(cID.suffix(5))
            self.invoiceNumber = "CO-\(sortID)"
            
        }
        else{
            self.invoiceNumber = "CO-\(dicData.string(forKey: "top_change_order_id"))"
        }
        
        let crD  = dicData.string(forKey: "created_at")
        
        var mydate: String = ""
        var jobid: String = ""
        
        if let dateServer = Date.dateFormatter.date(from: crD) {
            let calendar = NSCalendar.current
            AgLog.debug(calendar.dateComponents([.year, .month, .day], from: dateServer))
            let yer = calendar.component(.year, from: dateServer)
            let mnth = calendar.component(.month, from: dateServer)
            let dy = calendar.component(.day, from: dateServer)
            
            mydate  = "\(mnth)/\(dy)/\(yer)"

        }
        
        jobid = dicData.string(forKey: "job_number")
        
        let cliname = dicData.string(forKey: "client_name")
        let contract = dicData.string(forKey: "contract_no")
        let projName = dicData.string(forKey: "job_name")
        
        let cAddresss = dicData.string(forKey: "address")
        let cAddresss1 = dicData.string(forKey: "address1")
        let cCity = dicData.string(forKey: "city")
        let cState = dicData.string(forKey: "state")
        let cZip = dicData.string(forKey: "zip")
        
        var fullAddress = ""
        if cAddresss.isNotNull {
            fullAddress += cAddresss + "<br>"
        }
        if cAddresss1.isNotNull {
            fullAddress += cAddresss1 + "<br>"
        }
        if cCity.isNotNull {
            fullAddress += cCity + ", "
        }
        if cState.isNotNull {
            fullAddress += cState + " "
        }
        if cZip.isNotNull {
            fullAddress += cZip
        }
        
        let arryallItem = NSMutableArray(array:dicData.array(forKey: "change_order_items"))
        
        do {
            var HTMLContent = try String(contentsOfFile: pathToInvoiceHTMLTemplate!)
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#CO_NUMBER#", with: invoiceNumber)
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#DATE#", with: mydate)
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#Contract_PO#", with: contract)
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#JOB_ID#", with: jobid)
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#CLIENT_NAME#", with: cliname)
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#PROJECT_NAME#", with: projName)
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#ADDRESS#", with: fullAddress)
            
            var allItems = ""
            
            
            for i in 0..<arryallItem.count {
                
                var itemHTMLContent: String!
                itemHTMLContent = try String(contentsOfFile: pathToSingleItemHTMLTemplate!)
                
                // Replace the description and price placeholders with the actual values.
                let dic  = arryallItem[i] as? NSDictionary ?? NSDictionary()
                let lineNUM  = "\(i+1)"
                let lineTITLE  = dic.string(forKey: "title")
                let lineDESC  = dic.string(forKey: "description")
                let linePRIZE  = dic.string(forKey: "cost")
                
                let mycostF = (linePRIZE as NSString).floatValue
                
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#LINE_ITEM#", with: lineNUM)
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#LINE_TITLE#", with: lineTITLE)
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#LINE_DESCRIP#", with: lineDESC)
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#LINE_PRIZE#", with: mycostF.currency())
                // Add the item's HTML code to the general items string.
                allItems += itemHTMLContent
                
            }
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#ALL_LIEN_ITEMS#", with: allItems)
            
            var itemsTotal = ""
            
            var itemTotalHTMLContent: String!
            itemTotalHTMLContent = try String(contentsOfFile: pathToitemTotalHTMLTemplate!)
            
            var total = Float()
            total = 0
            
            for dici in arryallItem.dict {
                let prz = dici.string(forKey: "cost")
                let myFloat = (prz as NSString).floatValue
                total = total + myFloat
            }
            
            itemsTotal = itemTotalHTMLContent
            HTMLContent = HTMLContent.replacingOccurrences(of: "#TOTAL#", with: itemsTotal)
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#TOTAL_COST#", with: total.currency())
            var count = 0
            var allImages = ""
            
            for i in 0..<arryallItem.count {
                
                
                let dic  = arryallItem[i] as? NSDictionary ?? NSDictionary()
                let myImageArray = dic.object(forKey: "all_images") as! NSArray
                
                
                AgLog.debug("Total image is \(myImageArray.count)")
                
                let mtI = NSMutableArray()
                let dicArr = myImageArray[0] as? NSDictionary ?? NSDictionary()
                
                let img1  = dicArr.string(forKey: "Image1")
                let img2  = dicArr.string(forKey: "Image2")
                let img3  = dicArr.string(forKey: "Image3")
                let img4  = dicArr.string(forKey: "Image4")
                
                if img1 == ImageFlag.notSelected{
                    
                }else{
                    mtI.add(img1)
                }
                if img2 == ImageFlag.notSelected{
                    
                }else{
                    mtI.add(img2)
                }
                if img3 == ImageFlag.notSelected{
                    
                }else{
                    mtI.add(img3)
                }
                if img4 == ImageFlag.notSelected{
                    
                }else{
                    mtI.add(img4)
                }
                
                for ima in mtI {
                    
                    var itemImage_HTMLContent: String!
                    itemImage_HTMLContent = try String(contentsOfFile: pathToAllImageHTMLTemplate!)
                    
                    if count % 5 == 0{
                        AgLog.debug("add new row here ")
                        allImages += "</tr><tr>"
                    }
                    count = count + 1
                    
                    let image = DataController.sharedInstance.getImageFromDirFolder(withName: ima as? String ?? "")
                    let imageData = UIImageJPEGRepresentation(image, 0.3) ?? nil
                    let base64String = imageData?.base64EncodedString() ?? ""
                    itemImage_HTMLContent = itemImage_HTMLContent.replacingOccurrences(of: "#IMAGEDATA#", with: base64String)
                    
                    itemImage_HTMLContent = itemImage_HTMLContent.replacingOccurrences(of: "#ITEM_NUMBER#", with: "\(i+1)")
                    
                    // Add the item's HTML code to the general items string.
                    allImages += itemImage_HTMLContent
                }
            }
            
            for j in 0..<count{
                let wed = "\"width: 20%;\""
                let dumData =  "<td style=\(wed)></td>"
                
                if j == 0{
                    allImages += dumData
                    allImages += dumData
                    allImages += dumData
                    allImages += dumData
                }
                else if j % 5 == 1{
                    allImages += dumData
                    allImages += dumData
                    allImages += dumData
                    allImages += dumData
                }
                else if j % 5 == 2{
                    allImages += dumData
                    allImages += dumData
                    allImages += dumData
                }
                else if j % 5 == 3{
                    allImages += dumData
                    allImages += dumData
                }
                else if j % 5 == 4{
                    allImages += dumData
                }
            }
            HTMLContent = HTMLContent.replacingOccurrences(of: "#ALL_ITEMS_IMAGES#", with: allImages)
            
            if let cDetail = UserDefaults.standard.data(forKey: "COMPNAYDETAIL") {
                if let resultCompnay = NSKeyedUnarchiver.unarchiveObject(with: cDetail) as? NSDictionary {
                    
                    HTMLContent = HTMLContent.replacingOccurrences(of: "#COMPNAYNAME#", with: "\(resultCompnay.string(forKey: "company_name"))")
                    
                    let cAddresss = resultCompnay.string(forKey: "address")
                    let cAddresss1 = resultCompnay.string(forKey: "address1")
                    let cCity = resultCompnay.string(forKey: "city")
                    let cState = resultCompnay.string(forKey: "state")
                    let cZip = resultCompnay.string(forKey: "zip")
                    let cCountry = resultCompnay.string(forKey: "country")
                    let cPhone = resultCompnay.string(forKey: "phone")
                    
                    var compneyAddress = ""
                    if cAddresss.isNotNull {
                        compneyAddress += cAddresss + ", "
                    }
                    if cAddresss1.isNotNull {
                        compneyAddress += cAddresss1 + "; "
                    }
                    if cCity.isNotNull {
                        compneyAddress += cCity + ", "
                    }
                    if cState.isNotNull {
                        compneyAddress += cState + " "
                    }
                    if cZip.isNotNull {
                        compneyAddress += cZip + ""
                    }
                    if cCountry.isNotNull {
                        compneyAddress += " (" + cCountry + ")"
                    }
                    if cPhone.isNotNull {
                        compneyAddress += " <em>&bull;</em> " + cPhone
                    }
                    
                    HTMLContent = HTMLContent.replacingOccurrences(of: "#COMPNAYADDRESSBOTTOM#", with: "\(compneyAddress)")
                    
                    let logoUrl = resultCompnay.string(forKey: "logo_url")
                    if logoUrl.count > 0 {
                        
                        let cId = resultCompnay.integer(forKey: "id")
                        let cLogoName = "off_logo_\(cId).jpg"
                        let image = DataController.sharedInstance.getCompnayLogoImageFromDirFolder(withName: cLogoName)
                        let imageData = UIImagePNGRepresentation(image ) ?? nil
                        let base64String = imageData?.base64EncodedString() ?? ""
                        HTMLContent = HTMLContent.replacingOccurrences(of: "#COMPNAYLOGO#", with: base64String)
                    }
                    else{
                        AgLog.debug("no logo setup")
                        let imgEmpry =  UIImage(named:"emptyClogo.png")
                        let imageData = UIImagePNGRepresentation(imgEmpry! ) ?? nil
                        let base64String = imageData?.base64EncodedString() ?? ""
                        HTMLContent = HTMLContent.replacingOccurrences(of: "#COMPNAYLOGO#", with: base64String)
                    }
                }
            }
            else{
                
                HTMLContent = HTMLContent.replacingOccurrences(of: "#COMPNAYNAME#", with: "")
                HTMLContent = HTMLContent.replacingOccurrences(of: "#COMPNAYPHONE#", with: "")
                HTMLContent = HTMLContent.replacingOccurrences(of: "#COMPNAYADDRESS#", with: "")
                HTMLContent = HTMLContent.replacingOccurrences(of: "#COMPNAYADDRESSBOTTOM#", with: "")
                
                let imgEmpry =  UIImage(named:"emptyClogo.png")
                let imageData = UIImagePNGRepresentation(imgEmpry! ) ?? nil
                let base64String = imageData?.base64EncodedString() ?? ""
                HTMLContent = HTMLContent.replacingOccurrences(of: "#COMPNAYLOGO#", with: base64String)
                
            }
            
            if dicData.string(forKey: "status") == Constants.STATUS_DENIED{
                HTMLContent = HTMLContent.replacingOccurrences(of: "#IMAGESIGNATURE#", with: "-")
                HTMLContent = HTMLContent.replacingOccurrences(of: "#CUSTNAME#", with: "")
                return HTMLContent
            }
            
            if let dicTCust = dicData.object(forKey: "custApiData") as? NSDictionary,
                dicData.string(forKey: "status") == Constants.STATUS_APPROVED  || dicData.string(forKey: "status") == Constants.STATUS_COMPLETED {
                
                let imgSIgn = dicTCust.string(forKey: "customer_signature")
                let image = DataController.sharedInstance.getImageFromDirFolder(withName: imgSIgn)
                let imageData = UIImagePNGRepresentation(image ) ?? nil
                let base64String = imageData?.base64EncodedString() ?? ""
                HTMLContent = HTMLContent.replacingOccurrences(of: "#IMAGESIGNATURE#", with: base64String)
                
                let custName = dicTCust.string(forKey: "customer_name")
                
                var dicplayName = ""
                if let cTitle = dicTCust.object(forKey: "customer_Title") as? String{
                    if custName != ""{
                        dicplayName = "\(custName)"
                        if cTitle != ""{
                            dicplayName += " / \(cTitle)"
                        }
                    }
                    else{
                        if cTitle != ""{
                            dicplayName += "\(cTitle)"
                        }
                    }
                }
                else{
                    dicplayName = custName
                }
                
                HTMLContent = HTMLContent.replacingOccurrences(of: "#CUSTNAME#", with: dicplayName)
                
            }
            else{
                HTMLContent = HTMLContent.replacingOccurrences(of: "#IMAGESIGNATURE#", with: "-")
                HTMLContent = HTMLContent.replacingOccurrences(of: "#CUSTNAME#", with: "")
            }
            
            return HTMLContent
        }
        catch {
            AgLog.debug("Unable to open and use HTML template files.")
        }
        
        return nil
    }
}

