//
//  AppDelegate+Mail.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 13/08/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit
import MessageUI

extension AppDelegate: MFMailComposeViewControllerDelegate {
    static let shared = AppDelegate()
    func sendMail(path filePath: String, subject: String, url: String) {
        debugPrint("FilePath: \(filePath)")
        
        let email = "whollysoftware@gmail.com"
//        let email = "ashvinkgudaliya@gmail.com"
        
        if(MFMailComposeViewController.canSendMail()) && false {
            
            let mailComposer = MFMailComposeViewController()
            
            //Set the subject and message of the email
            mailComposer.mailComposeDelegate = self
            mailComposer.setToRecipients([email])
            mailComposer.setSubject("Error on api \(subject)")
            mailComposer.setMessageBody("\(url)", isHTML: false)
            
            if let fileData = NSData(contentsOfFile: filePath) {
                mailComposer.addAttachmentData(fileData as Data, mimeType: "text/plain", fileName: "\(subject).txt")
            }
            
            AppDelegate.topViewController()?.present(mailComposer, animated: true, completion: nil)
        }
        else{
            Helper.AgAlertView(url)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        controller.dismiss(animated: true, completion: nil)
    }
}
