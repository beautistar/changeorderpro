//
//  CheckListController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 23/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire
import DropDown
import AMTooltip

class CheckListController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet var lblTotal: UILabel!
    
    let cellSpacingHeight: CGFloat = 15
    
    var arrymyApproval = NSMutableArray()
    var arrayServerData = NSMutableArray()
    
    @IBOutlet var tblMyAssigned: UITableView!
    @IBOutlet var btnDropDown: UIButton!
    let infoProjDropDown = DropDown()
    
    @IBOutlet var viewSort: UIView!
    
    var sortingStatus = ""
    var selectedindex = 0
    var chngOrdID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "AllProjCell", bundle: nil)
        tblMyAssigned.register(nib, forCellReuseIdentifier: "allProjcell")
        
        tblMyAssigned.agRefreshControl.attributedTitle = NSAttributedString(string: "Syncing..")
        tblMyAssigned.pullToRefresh = { ref in
            self.tblMyAssigned.agRefreshControl.attributedTitle = NSAttributedString(string: "Syncing..")
            self.calltogetMyassignedProject()
        }
        
        updateUserData { (isStatus) in
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "APPROVE".localized
        self.tabBarController?.tabBar.isHidden = false
        
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)

        self.calltogetMyassignedProject()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let eyeInfo = UserDefaults.standard.value(forKey: "approved") as? NSString
        if (eyeInfo == nil ){
            let view = UIView(frame: self.tblMyAssigned.frame)
            view.frame.size.height = 150;
            view.backgroundColor = UIColor.clear
            self.tblMyAssigned.superview?.addSubview(view)
            AMTooltipView(message: "If you receive any Change Orders to approve, they will be displayed here.", focusView: view, complete: {
                
                view.removeFromSuperview()
                UserDefaults.standard.set("YES", forKey: "approved")
            })
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        endRefreshView()
    }
    
    func endRefreshView() {

        self.tblMyAssigned.agRefreshControl.attributedTitle = NSAttributedString(string: "Last synced : Just Now")
        DispatchQueue.main.asyncAfter(deadline: DispatchTime(uptimeNanoseconds: 2), execute: {
            if self.tblMyAssigned.agRefreshControl.isRefreshing {
                self.tblMyAssigned.agRefreshControl.endRefreshing()
            }
        })
        self.tblMyAssigned.reloadData()
    }

    func filterClicked(sender: UIBarButtonItem) {
        self.showSortingView()
    }
    
    func showSortingView(){
        
        let screenSize: CGRect = UIScreen.main.bounds
        let   screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        viewSort.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(screenWidth), height: CGFloat(screenHeight))
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.view.addSubview(self.viewSort)
    }
    
    @IBAction func btnSortCancleClicked(_ sender: Any) {
        self.viewSort.removeFromSuperview()
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSortByoptionClicked(_ sender: UIButton) {
        
        //    1 Approved
        //    2 Assigned
        //    3 Denied
        //    4 Submitted
        //    5 Saved
        
        self.viewSort.removeFromSuperview()
        
        if sender.tag == 1{
            AgLog.debug("sort by approved")
            sortingStatus = Constants.STATUS_APPROVED
            self.sortorderBystatus()
      
        }
        else if sender.tag == 2{
            
            AgLog.debug("sort by Assigned")
            sortingStatus = Constants.STATUS_SUBMITTED
            self.sortorderBystatus()
        }
        else if sender.tag == 3{
            
            AgLog.debug("sort by Denied")
            sortingStatus = Constants.STATUS_DENIED
            self.sortorderBystatus()
        }
        else if sender.tag == 4{
            
            AgLog.debug("sort by Complated")
            sortingStatus = Constants.STATUS_COMPLETED
            self.sortorderBystatus()
        }
    }
    
    func sortorderBystatus(){
        
        let newSortingArry = NSMutableArray()
        let otherObjectArry = NSMutableArray()

        arrymyApproval.forEach {
            let dicT = $0 as? NSDictionary ?? NSDictionary()
            let status  = dicT.string(forKey: "status")
            if status == sortingStatus {
                newSortingArry.add(dicT)
            }
            else{
                otherObjectArry.add(dicT)
            }
        }
        newSortingArry.addObjects(from: otherObjectArry as [AnyObject])
        arrymyApproval = newSortingArry
        self.tblMyAssigned.reloadData()
        self.countTotal()
    }

    func countTotal(){
        
        var total = Float()
        total = 0
        for dic in arrymyApproval{
            
            let dicT = dic as? NSDictionary ?? NSDictionary()
            let arryChItem = dicT.object(forKey: "change_order_items") as! NSArray
        
            let myFloat = (arryChItem as! [[String:Any]]).map {
                ($0["cost"] as? NSString)?.floatValue ?? 0
            }
            for temp in myFloat{
                total = total + temp
            }
            self.lblTotal?.text = total.currency()
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrymyApproval.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "allProjcell") as! AllProjCell
        
        let dicOrder = arrymyApproval[indexPath.section] as? NSDictionary ?? NSDictionary()
        
        let arryChItem = dicOrder.object(forKey: "change_order_items") as! NSArray
        
        let crD  = dicOrder.string(forKey: "created_at")
        let dateServer = Date.dateFormatter.date(from: crD)
        let calendar = NSCalendar.current
        let yer = calendar.component(.year, from: dateServer!)
        let mnth = calendar.component(.month, from: dateServer!)
        let dy = calendar.component(.day, from: dateServer!)
        
        cell.lblCreatedDate.text = "Created:\(mnth)/\(dy)/\(yer)"
        
        cell.lblProId.text = "CO-\(dicOrder.string(forKey: "top_change_order_id"))"
        cell.lblProjName.text = dicOrder.string(forKey: "job_name")
        
        let myFloat = (arryChItem as! [[String: Any]]).map {
            ($0["cost"] as? NSString)?.integerValue ?? 0
        }
        
        var tempFloat:Int = 0
        for temp in myFloat {
            tempFloat = tempFloat + temp
        }
        
        if tempFloat > 0{
            cell.lblOrderPrice.text = tempFloat.currency()
        }
        else{
            cell.lblOrderPrice.text = ""
        }

        cell.btnorderStatus.isHidden = false
        cell.lblOrderPrice.isHidden = false
        
        cell.lblOrderPrice2.isHidden = true
        
        let status  = dicOrder.string(forKey: "status")
        cell.btnOrderPdf.isHidden = false
        cell.btnorderStatus.addTarget(self,action:#selector(btnClicked), for: .touchUpInside)
        if status == Constants.STATUS_APPROVED{
            
            cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojGreen.png"), for: .normal)
            cell.btnorderStatus.setTitle("Approved", for: .normal)
            cell.btnorderStatus.setTitleColor(.white, for: .normal)
            
        }else if status == Constants.STATUS_DENIED {
            cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojRed.png"), for: .normal)
            cell.btnorderStatus.setTitle("Denied", for: .normal)
            cell.btnorderStatus.setTitleColor(.white, for: .normal)
            cell.btnorderStatus.tag = indexPath.section
            cell.btnorderStatus.addTarget(self,action:#selector(btnStatusOrderClicked), for: .touchUpInside)
            
        }
        else if status == Constants.STATUS_DELETED {
            let temp_status  = dicOrder.string(forKey: "temp_status")
            if temp_status == Constants.STATUS_APPROVED{
                cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojGreen.png"), for: .normal)
                cell.btnorderStatus.setTitle("Approved", for: .normal)
                cell.btnorderStatus.setTitleColor(.white, for: .normal)
                
            }else if temp_status == Constants.STATUS_DENIED {
                
                cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojRed.png"), for: .normal)
                cell.btnorderStatus.setTitle("Denied", for: .normal)
                cell.btnorderStatus.setTitleColor(.white, for: .normal)
                cell.btnorderStatus.tag = indexPath.section
                cell.btnorderStatus.addTarget(self,action:#selector(btnStatusOrderClicked), for: .touchUpInside)
                
            }
            else if temp_status == Constants.STATUS_SUBMITTED{
                
                cell.btnorderStatus.setBackgroundImage(UIImage(named:"myProjWhiteBox.png"), for: .normal)
                cell.btnorderStatus.setTitle("NEW", for: .normal)
                cell.btnorderStatus.setTitleColor(.black, for: .normal)
            }
            else if temp_status == Constants.STATUS_SAVED{
                
                cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojLight.png"), for: .normal)
                cell.btnorderStatus.setTitle("Saved", for: .normal)
                cell.btnOrderPdf.isHidden = true
                cell.btnorderStatus.setTitleColor(.black, for: .normal)
            }
            else if temp_status == Constants.STATUS_COMPLETED{
                
                cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojBlack.png"), for: .normal)
                cell.btnorderStatus.setTitle("Completed", for: .normal)
                cell.btnOrderPdf.isHidden = false
                cell.btnorderStatus.setTitleColor(.white, for: .normal)
            }
            else if temp_status == Constants.STATUS_REBILLED{
                
                cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojGreen.png"), for: .normal)
                cell.btnorderStatus.setTitle("Rebill", for: .normal)
                cell.btnorderStatus.setTitleColor(.white, for: .normal)
            }
        }
        else if status == Constants.STATUS_SUBMITTED{
            
            cell.btnorderStatus.setBackgroundImage(UIImage(named:"myProjWhiteBox.png"), for: .normal)
            cell.btnorderStatus.setTitle("NEW", for: .normal)
            cell.btnorderStatus.setTitleColor(.black, for: .normal)
        }
        else if status == Constants.STATUS_SAVED{
            
            cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojLight.png"), for: .normal)
            cell.btnorderStatus.setTitle("Saved", for: .normal)
            cell.btnOrderPdf.isHidden = true
            cell.btnorderStatus.setTitleColor(.black, for: .normal)
        }
        else if status == Constants.STATUS_COMPLETED{
            
            cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojBlack.png"), for: .normal)
            cell.btnorderStatus.setTitle("Completed", for: .normal)
            cell.btnOrderPdf.isHidden = false
            cell.btnorderStatus.setTitleColor(.white, for: .normal)
        }
        else if status == Constants.STATUS_REBILLED{
            
            cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojGreen.png"), for: .normal)
            cell.btnorderStatus.setTitle("Rebill", for: .normal)
            cell.btnorderStatus.setTitleColor(.white, for: .normal)
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let dic = self.arrymyApproval[indexPath.section] as? NSDictionary,
            let status = dic.object(forKey: "status") as? String else  {
            return
        }

        if status == Constants.STATUS_APPROVED || status == Constants.STATUS_DENIED || status == Constants.STATUS_COMPLETED || status == Constants.STATUS_DELETED {
            
            let chcActin = self.storyboard?.instantiateViewController(withIdentifier: "checkAction") as! CheckActionController
            chcActin.last_status = dic.string(forKey: "status")
            chcActin.top_change_order_id = dic.string(forKey: "top_change_order_id")
            chcActin.change_order_id = dic.string(forKey: "change_order_id")
            chcActin.pdfUrl = dic.object(forKey: "pdf_url") as? String ?? ""
            chcActin.isOnlyDisplay = true
            self.navigationController?.pushViewController(chcActin, animated: true)
        }
        else{
            
            let chcActin = self.storyboard?.instantiateViewController(withIdentifier: "checkAction") as! CheckActionController
            chcActin.top_change_order_id = dic.string(forKey: "top_change_order_id")
            chcActin.change_order_id = dic.string(forKey: "change_order_id")
            chcActin.last_status = dic.string(forKey: "status")
            chcActin.pdfUrl = dic.object(forKey: "pdf_url") as? String ?? ""
            self.navigationController?.pushViewController(chcActin, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if let dicOrder = arrymyApproval.obj(indexPath.section) as? NSDictionary {
            if Constants.STATUS_SUBMITTED == dicOrder.string(forKey: "status") {
                let is_rebilled = dicOrder.bool(forKey: "is_rebilled")
                if is_rebilled {
                    return false
                }
                return true
            }
        }
        return false
    }
    
    //  /* for ios <11.0
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        let dicOrder = arrymyApproval.dictionary(at:editActionsForRowAt.section)
        let status  = dicOrder.string(forKey: "status")
        AgLog.debug("status is \(status)")
        
       if  status == Constants.STATUS_SUBMITTED || status == Constants.STATUS_DENIED {
        
            let editOpt = UITableViewRowAction(style: .normal, title: "\(TableRowActionType.edit)\n Rebill") { action, index in
                self.tblMyAssigned.setEditing(!tableView.isEditing, animated: false)
                self.selectedindex = editActionsForRowAt.section
                let dic = self.arrymyApproval.dictionary(at:self.selectedindex)
                self.chngOrdID =  dic.string(forKey: "change_order_id")
                self.rebuidProjectApiCalled()
            }
            editOpt.backgroundColor = UIColor(red: 49/255, green: 167/255, blue: 106/255, alpha: 1.0)
            return [editOpt]
        }
        return nil
    }

    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let dicOrder = arrymyApproval[indexPath.section] as? NSDictionary ?? NSDictionary()
        self.selectedindex = indexPath.section
        let dic = self.arrymyApproval.dictionary(at:self.selectedindex)
        
        self.chngOrdID = dic.string(forKey: "change_order_id")
        let status  = dicOrder.string(forKey: "status")
        AgLog.debug("status is  \(status)")
        
        if status == Constants.STATUS_SUBMITTED || status == Constants.STATUS_DENIED {
            //action Edit and Delete
            
            let editOpt = UIContextualAction(style: .normal, title: "\(TableRowActionType.edit)\n Rebill") { action, view, completionHandler in
                
                AgLog.debug("Edit Order button tapped")
                self.rebuidProjectApiCalled()
                
                completionHandler(true)
            }
            editOpt.backgroundColor = UIColor(red: 49/255, green: 167/255, blue: 106/255, alpha: 1.0)
            
            let config = UISwipeActionsConfiguration(actions: [editOpt])
            config.performsFirstActionWithFullSwipe = false
            return config
            
        }else{
            return nil
        }
    }
    
    func btnStatusOrderClicked(sender: UIButton) {
        
        AgLog.debug("selected row \(sender.tag)")
        let dicOrder = arrymyApproval.dictionary(at: sender.tag)
        if let denialResn = dicOrder.object(forKey: "denial_reason") as? String{
            AgLog.debug("denialReason is \(denialResn)")
            let strMessage = "Denied by me.\n Reason: \(denialResn)"
            Helper.AgAlertView(strMessage)
        }
        else{
            AgLog.debug("denialReason is Null")
            let strMessage = "Denied by me"
            Helper.AgAlertView(strMessage)
        }
    }
    func btnClicked(sender: UIButton) {
    
    }
    
    func gotoEditOrderMode(){
        
        if uni_isWorking_Offline == true{
            
            arry_EditChangeItems = NSMutableArray()
            let dicOrder = self.arrymyApproval[self.selectedindex] as? NSDictionary ?? NSDictionary()
            
            let chId = "\(dicOrder.object(forKey: "change_order_id") ?? "")"
            
            for dic in uni_array_allChangeOrders{
                
                let dicOr = dic as? NSDictionary ?? NSDictionary()
                
                let savedId = "\(dicOr.object(forKey: "change_order_id") ?? "")"
                
                if savedId == chId{
                    
                    let  index = uni_array_allChangeOrders.index(of: dicOr)
                    uni_selectedindextoEdit = index
                    
                    let arryChItem = dicOrder.object(forKey: "change_order_items") as! NSArray
                    arry_EditChangeItems = NSMutableArray(array:arryChItem)
                    
                    current_lineItemNumber = 0
                    total_ItemsAdded = 0
                    projName = dicOrder.string(forKey: "job_name")
                    
                    let editItem = self.storyboard?.instantiateViewController(withIdentifier: "editchangeOrder") as! EditChangeItemController
                    editItem.jobName = dicOrder.string(forKey: "job_name")
                    editItem.chnOrderId =  self.chngOrdID
                    editItem.projectID  = dicOrder.object(forKey: "project_id") as? String ?? ""
                    editItem.dicmainEditOffline = dicOrder
                    
                    self.navigationController?.pushViewController(editItem, animated: true)
                }
            }
            
        }
        else{
            
            arry_EditChangeItems = NSMutableArray()
            let dicOrder = self.arrymyApproval[self.selectedindex] as? NSDictionary ?? NSDictionary()
            
            let arryChItem = dicOrder.object(forKey: "change_order_items") as! NSArray
            arry_EditChangeItems = NSMutableArray(array:arryChItem)
            
            current_lineItemNumber = 0
            total_ItemsAdded = 0
            projName = dicOrder.string(forKey: "job_name")
            
            let editItem = self.storyboard?.instantiateViewController(withIdentifier: "editchangeOrder") as! EditChangeItemController
            editItem.jobName = dicOrder.string(forKey: "job_name")
            editItem.chnOrderId =  self.chngOrdID
            editItem.projectID  = dicOrder.object(forKey: "project_id") as? String ?? ""
            self.navigationController?.pushViewController(editItem, animated: true)
        }
    }
    
    func calltogetMyassignedProject(withDict: NSArray) {
        for approve in withDict.dict {
            var isAdded: Bool = false
            for i in 0..<self.arrayServerData.dict.count {
                let setver_id = self.arrayServerData.dict[i].string(forKey: "change_order_id")
                if setver_id == approve.string(forKey: "change_order_id") {
                    self.arrayServerData.replaceObject(atIndex: i, with: approve)
                    isAdded = true
                }
            }
            
            if !isAdded {
                self.arrayServerData.insert(approve, at: 0)
            }
        }
        
        self.filterDeletedobject()
        self.endRefreshView()
        calltogetMyassignedProject()
    }
    
    func calltogetMyassignedProject() {
        
        if Reachability.isConnectedToNetwork() == true {

            let strURL = kBaseURL + "getassignedchangeorders/" + UserDetails.loginUserId
            Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { response in
                response.logShow()
                if  let arryServer = response.result.value as? NSArray {
                    self.arrayServerData = NSMutableArray(array: arryServer)
                    self.filterDeletedobject()
                }

                self.endRefreshView()
            }
        }
        else {
            self.endRefreshView()
        }
    }
    
    var newMyArrayTrial = NSMutableArray()
    var allProjname = NSMutableArray()
    
    func filterDeletedobject(){
        
        newMyArrayTrial = NSMutableArray()
        self.arrymyApproval = NSMutableArray()
        allProjname = NSMutableArray()
        allProjname.add("ALL Projects")
        
        for dic in arrayServerData{
            let dicT = dic as? NSDictionary ?? NSDictionary()
            let editDi = dicT.mutableCopy() as! NSMutableDictionary
            let arryChItem = dicT.array(forKey: "change_order_items")
            if arryChItem.count > 0 {
                let isoDate = dicT.object(forKey: "created_at")
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let date = dateFormatter.date(from: isoDate as? String ?? "")!
                editDi.setValue(date, forKey: "chnageOrderSortDate")
                
                self.arrymyApproval.add(dicT)
                newMyArrayTrial.add(editDi)
                let projname = dicT.string(forKey: "job_name")
                if !allProjname.contains(projname){
                    allProjname.add(projname)
                }
            }
        }
        
        self.countTotal()
        self.SorttThisAraybyDate()
    }
    
    @IBAction func btnFilterClicked(_ sender: Any) {
        self.showSortingView()
    }
    
    @IBAction func btnDropClicked(_ sender: UIButton) {
        
        infoProjDropDown.anchorView = sender
        infoProjDropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        infoProjDropDown.dataSource = self.allProjname as! [String]
        infoProjDropDown.show()
        
        infoProjDropDown.selectionAction = { [unowned self] (index, item) in
            if(item == "ALL Projects"){
                selected_projID = "Dummy"
            }
            else{
                let dicOrder = self.arrayServerData.dictionary(at: index-1)
                let ordId = dicOrder.string(forKey: "project_id")
                selected_projID = ordId
            }
            self.btnDropDown.setTitle(item, for: .normal)
            AgLog.debug("selected project Name \( self.allProjname[index])")
            self.callToFilerorderbyID()
        }
    }
    
    func callToFilerorderbyID(){
        
        if selected_projID == "Dummy"{
            filterDeletedobject()
            self.tblMyAssigned.reloadData()
            self.countTotal()
            
        }else{
            
            let totalProj =  arrayServerData.count
            AgLog.debug("Total \(totalProj)")
            arrymyApproval = NSMutableArray(array: arrayServerData.filter{
                let dicOrder = $0 as? NSDictionary ?? NSDictionary()
                let ordId = dicOrder.string(forKey: "project_id")
                return selected_projID == ordId
            })
            self.tblMyAssigned.reloadData()
            self.countTotal()
        }
    }
    
    func SorttThisAraybyDate(){
        
        let swiftArray = newMyArrayTrial as NSArray
        
        let allDates = swiftArray.map {
            ($0 as AnyObject).object(forKey: "chnageOrderSortDate") as! Date
        }
        debugPrint(allDates)
        
        let sortedArray = (swiftArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "chnageOrderSortDate", ascending: false)]) as! [[String:AnyObject]]
    
        self.arrymyApproval = NSMutableArray(array: sortedArray)
        self.tblMyAssigned.reloadData()

    }
    
    func rebuidProjectApiCalled() {
        
        if !CheckUserDetails.isFullAccess {
            Helper.agAlertSubscription("Rebill option not available without a subscription. Please subscribe for full access.")
            return
        }
        
        if Reachability.isConnectedToNetwork() == true {
            
            let strURL = kBaseURL + "rebilledorder"
            let dictParams:  [String: String] = [
                "change_order_id" : self.chngOrdID,
                "user_id" : UserDetails.loginUserId,
            ]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                response.logShow()
                
                if let dicServer = response.result.value as? NSDictionary {
                    
                    if response.isSuccess {
                        if let change_order = dicServer.dictionary(forKey: "change_order").mutableCopy() as? NSMutableDictionary {
                            change_order.setValue("true", forKey: "is_uploaded")
                            uni_array_allChangeOrders.add(change_order)
                            DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
                            self.calltogetMyassignedProject()
                        }
                        Helper.AgAlertView(response.message)
                    }
                    else{
                        Helper.AgAlertView(response.message)
                    }
                }
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
}

