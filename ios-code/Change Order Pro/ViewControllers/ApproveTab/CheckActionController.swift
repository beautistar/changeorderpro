//
//  CheckActionController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 23/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire
import WebKit

class CheckActionController: UIViewController,UIWebViewDelegate,WKUIDelegate,WKNavigationDelegate{
    
    @IBOutlet var txtReson: UITextField!
    @IBOutlet var viewDenyAlert: UIView!
    @IBOutlet var viewApproveAlert: UIView!
    @IBOutlet var webView: UIWebView!
    @IBOutlet var btnApprove: UIButton!
    @IBOutlet var btnDeany: UIButton!
    
    @IBOutlet var viewLast: UIView!
    
    var top_change_order_id = ""
    var change_order_id = ""
    var pdfUrl = ""
    var statusApprove = ""
    var deniReson = ""
    var isOnlyDisplay = false
    var last_status = ""
//    var wkWebView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        btnApprove.isHidden = true
        btnDeany.isHidden = true
        viewLast.isHidden = true
        self.setUserSignature()
    
//        if #available(iOS 11.0, *) {
//            let configuration = WKWebViewConfiguration()
//            configuration.setURLSchemeHandler(CustomeSchemeHandler(), forURLScheme: "changeorder")
//            wkWebView = WKWebView(frame: webView.frame,configuration:configuration)
//        }
//
//        if pdfUrl.count > 0 {
//            self.view.addSubview(wkWebView)
//
//            webView.isHidden = true
//            wkWebView.backgroundColor = UIColor.white
//            wkWebView.uiDelegate = self
//            wkWebView.navigationDelegate = self
//            wkWebView.translatesAutoresizingMaskIntoConstraints = false
//            wkWebView.heightAnchor.constraint(equalTo: webView.heightAnchor, multiplier: 1.0).isActive = true
//            wkWebView.widthAnchor.constraint(equalTo: webView.widthAnchor, multiplier: 1.0).isActive = true
//            wkWebView.centerXAnchor.constraint(equalTo: webView.centerXAnchor).isActive = true
//            wkWebView.centerYAnchor.constraint(equalTo: webView.centerYAnchor).isActive = true
//        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = UIInterfaceOrientationMask.all
        }
        loadData()
    }
    
    func loadData(){
        
        if (last_status == Constants.STATUS_DELETED) {
            isOnlyDisplay = true
        }
        
        if pdfUrl.count > 0{
            
            let prePath = kBaseURL + pdfUrl
            let url = URL (string: prePath)
            let requestObj = URLRequest(url: url!)
            webView.loadRequest(requestObj)
        
//            wkWebView.load(requestObj)
        }
        
        if isOnlyDisplay{
            btnApprove.isHidden = true
            btnDeany.isHidden = true
            viewLast.isHidden = true
            viewLast.heightConstaint?.constant = 0
        }
        else{
            viewLast.isHidden = false
            btnApprove.isHidden = false
            btnDeany.isHidden = false
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            self.tabBarController?.tabBar.isHidden = true
        }
        else {
            self.tabBarController?.tabBar.isHidden = false
        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func btnDenyClicked(_ sender: Any) {

        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        viewDenyAlert.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(screenWidth), height: CGFloat(screenHeight))
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.view.addSubview(self.viewDenyAlert)

    }
    
    @IBAction func btnAlertDEnyClicked(_ sender: Any) {
        
        if txtReson.isNull{
            Helper.AgAlertView("Enter reason for denial")
        }
        else{
            statusApprove = Constants.STATUS_DENIED
            self.deniReson = self.txtReson.text!
            self.callApproveApi()
        }
    }
    
    func navigateafterDenySuccess(){
        
        self.viewDenyAlert.removeFromSuperview()
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnApproveClicked(_ sender: Any) {
        
        if (UserDefaults.standard.value(forKey: "MYSIGNATURE") as? String) != nil {
            statusApprove = Constants.STATUS_APPROVED
            self.deniReson = ""
            self.callApproveApi()
        }
        else{
            Helper.AgAlertView("Please add a signature to your profile in order to approve.")
        }
    }
   
    func showApprovedSuccessAlert(){
        let screenSize: CGRect = UIScreen.main.bounds
        let   screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        viewApproveAlert.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(screenWidth), height: CGFloat(screenHeight))
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.view.addSubview(self.viewApproveAlert)
    }
    
    
    @IBAction func btnApproveGotItClicked(_ sender: Any) {
        self.viewApproveAlert.removeFromSuperview()
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        self.tabBarController?.tabBar.isHidden = false
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func callApproveApi() {
        self.viewDenyAlert.removeFromSuperview()
        if Reachability.isConnectedToNetwork() == true {
            let strURL = kBaseURL + "changeorderstatus"
            
            let dictParams: [String: String] = [
                "top_change_order_id" : self.top_change_order_id,
                "change_order_id" : self.change_order_id,
                "user_id" : UserDetails.loginUserId,
                "status" : self.statusApprove,
                "denial_reason" :  self.deniReson
            ]
            
            MBProgressHUD.showAdded(to: self.view, animated: true)

            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                if response.isSuccess {
                    
                    if self.statusApprove == Constants.STATUS_APPROVED{
                        self.showApprovedSuccessAlert()
                    }
                    else{
                        self.navigateafterDenySuccess()
                    }
                }
                else{
                    Helper.AgAlertView(response.message)
                }
                
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    func setUserSignature(){
        if let imgSignName = UserDefaults.standard.value(forKey: "MYSIGNATURE") as? String {
            print(imgSignName)
        }else{
            calltoGetUserSignature()
        }
    }
    func calltoGetUserSignature() {
        
        if Reachability.isConnectedToNetwork() == true {
            var loginUserName: String = ""
            if let detailUsr = UserDefaults.standard.data(forKey: "USERDETAIL") {
                if let result = NSKeyedUnarchiver.unarchiveObject(with: detailUsr) as? NSDictionary {
                    loginUserName = result.object(forKey: "username") as? String ?? ""
                }
            }
            let strURL = kBaseURL + "getsignature"
            let dictParams:  [String: String] = ["user_id" : UserDetails.loginUserId]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                if  let aryservr = response.result.value as? NSArray {
                    if aryservr.count > 0 {
                        let dicSign = aryservr[0] as? NSDictionary ?? NSDictionary()
                        let imgeSignName = "approverSign_\(loginUserName)\(UserDetails.loginUserId).jpg"
                        
                        UserDefaults.standard.set(imgeSignName, forKey: "MYSIGNATURE")
                        let sId = "\(dicSign.object(forKey: "id") ?? "")"
                        let useTitle = dicSign.object(forKey: "title") as? String ?? ""
                        UserDefaults.standard.set("\(sId)", forKey: "SignatureId")
                        UserDefaults.standard.set("\(useTitle)", forKey: "UserTitle")
                    }
                }
            }
        }
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        loadData()
        AGProgress.shared.hideProgress()
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        AGProgress.shared.showProgress(with: "Loading...")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if !webView.isLoading {
            AGProgress.shared.hideProgress()
        }
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
//    func webView(_ webView: WKWebView, commitPreviewingViewController previewingViewController: UIViewController) {
//
//    }
//    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
//
//    }
//    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
//
//    }
//    func webView(_ webView: WKWebView, previewingViewControllerForElement elementInfo: WKPreviewElementInfo, defaultActions previewActions: [WKPreviewActionItem]) -> UIViewController? {
//        return self
//    }
//    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
//        return temp
//    }
//    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
//
//    }

}

//extension CheckActionController {
//
//    @available(iOS 10.0, *)
//    func webView(_ webView: WKWebView, shouldPreviewElement elementInfo: WKPreviewElementInfo) -> Bool {
//        debugPrint(elementInfo)
//        return true
//    }
//}
//
//enum WebErrors: Error {
//    case RequestFailedError
//}
//
//@available(iOS 11.0, *)
//class CustomeSchemeHandler : NSObject,WKURLSchemeHandler {
//    func webView(_ webView: WKWebView, start urlSchemeTask: WKURLSchemeTask) {
//        DispatchQueue.global().async {
//            if let url = urlSchemeTask.request.url, url.scheme == "changeorder" {
//
//            }
//        }
//    }
//
//    func webView(_ webView: WKWebView, stop urlSchemeTask: WKURLSchemeTask) {
//        urlSchemeTask.didFailWithError(WebErrors.RequestFailedError)
//    }
//}
