//
//  MYProjCell.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit

class MYProjCell: UITableViewCell {
    @IBOutlet var btnred: UIButton!
    @IBOutlet var lblProjTitle: UILabel!

    @IBOutlet var lblProjAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
