//
//  ContactCell.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/10/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {

    @IBOutlet var btnPhn: UIButton!
    @IBOutlet var btnCheck: UIButton!
    @IBOutlet var txtContNum: UITextField!
    @IBOutlet var txtcontName: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
