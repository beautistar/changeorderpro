//
//  AllProjCell.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit

class AllProjCell: UITableViewCell {
    @IBOutlet var lblCreatedDate: UILabel!
    @IBOutlet var lblProId: UILabel!

    @IBOutlet var lblOrderPrice2: UILabel!
    @IBOutlet var lblProjName: UILabel!
    @IBOutlet var lblOrderPrice: UILabel!
    @IBOutlet var btnorderStatus: UIButton!
    @IBOutlet var btnOrderPdf: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
