//
//  CustomizeCell.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 11/11/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit

class CustomizeCell: UICollectionViewCell {
    
   
    @IBOutlet var imgFormat: UIImageView!
  
    @IBOutlet var txtTemplate: UITextField!
}
