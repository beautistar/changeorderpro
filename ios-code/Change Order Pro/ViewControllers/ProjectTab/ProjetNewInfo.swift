//
//  ProjetNewInfo.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 23/10/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import  Alamofire

class ProjetNewInfo: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var tableView: AGTableView!
    @IBOutlet var lblLast: UILabel!
    
    @IBOutlet var lblAdd2: UILabel!
    @IBOutlet var lblAdd1: UILabel!
    @IBOutlet var lblJObName: UILabel!
    @IBOutlet var lblJobNumb: UILabel!
    @IBOutlet var lblCustomer: UILabel!
    @IBOutlet var lblClientName: UILabel!
    
    @IBOutlet var viewAddress2: UIView!
    @IBOutlet var viewAddress2Bottom: NSLayoutConstraint!
    
    @IBOutlet var lblCity: UILabel!
    @IBOutlet var lblState: UILabel!
    @IBOutlet var lblZipCode: UILabel!
    
    var arrySiteCont = NSMutableArray()
    var dicProjDetail = NSDictionary()
    var projectID = ""
    var totalCount = 1
    var arryOriginal =  NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setProjectInformation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setProjectInformation(){
        
        AgLog.debug("selected Project Detail", dicProjDetail)
        self.navigationItem.title = dicProjDetail.string(forKey: "job_name")
        lblClientName.text = dicProjDetail.string(forKey: "client_name")
        lblJobNumb.text = dicProjDetail.string(forKey: "job_number")
        lblJObName.text = dicProjDetail.string(forKey: "job_name")
        lblCustomer.text = dicProjDetail.string(forKey: "contract_no")
        lblAdd1.text = dicProjDetail.string(forKey: "address")
        lblAdd2.text = dicProjDetail.string(forKey: "address1")
        
        if dicProjDetail.string(forKey: "address1").isNull {
            viewAddress2Bottom.isActive = false
            viewAddress2.removeFromSuperview()
        }
        
        lblZipCode.text = dicProjDetail.string(forKey: "zip")
        lblState.text = dicProjDetail.string(forKey: "state")
        lblCity.text = dicProjDetail.string(forKey: "city")
        
        projectID = dicProjDetail.string(forKey: "id")
        
        if uni_isWorking_Offline == true{
            self.setOfflineContact()
        }
        else{
            self.calltogetMyContacts()
        }
    }
    
    func setOfflineContact(){
        self.arryOriginal = dicProjDetail.array(forKey: "site_contact_ids") as NSArray
        self.sortArryByAlphabatic()
    }
    
    func calltogetMyContacts()  {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let strURL = kBaseURL + "getcontacts/" + projectID
            
            Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                if let arryServer = response.result.value as? NSArray {
                    AgLog.debug("Thisis the contacts \(arryServer)")
                    
                    self.arryOriginal = arryServer
                    self.sortArryByAlphabatic()
                }
                
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    func sortArryByAlphabatic(){
        let descriptor: NSSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        let sortedResults: NSArray = arryOriginal.sortedArray(using: [descriptor]) as NSArray
        
        AgLog.debug("sort result", sortedResults)
        
        self.arrySiteCont = NSMutableArray(array:sortedResults)
        
        arry_siteContactList_globle = self.arrySiteCont
        self.tableView.reloadData()
        self.totalCount = self.arrySiteCont.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrySiteCont.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "rContactcell") as! ContactCell
        
        let dicSCont = arrySiteCont[indexPath.row] as!  NSDictionary
        cell.txtcontName.text = dicSCont.string(forKey: "name")
        
        if let txtPP = dicSCont.object(forKey: "mobile") as? NSNull{
            AgLog.debug("mobile is empthy \(txtPP)")
            cell.txtContNum.text = dicSCont.string(forKey: "email")
        }
        else{
            
            let textPhn = dicSCont.string(forKey: "mobile")
            if textPhn.count == 10{
                let fir = textPhn.substring(with: 0..<3)
                let midd = textPhn.substring(with: 3..<6)
                let last = textPhn.substring(with: 6..<10)
                cell.txtContNum.text = "(\(fir)) \(midd)-\(last)"
                
            }
            else if textPhn.count == 0{
                cell.txtContNum.text = dicSCont.string(forKey: "email")
            }
            else{
                cell.txtContNum.text = dicSCont.string(forKey: "mobile")
            }
        }
        
        cell.btnPhn.tag = indexPath.row
        cell.btnPhn.addTarget(self,action:#selector(btnPhoneClicked(sender:)), for: .touchUpInside)
        
        if let isAPp = dicSCont.object(forKey: "is_approver") as? String, isAPp == "1"{
            
            cell.txtContNum.backgroundColor = UIColor(red: 0.5, green: 0.84, blue: 0.14, alpha: 1.0)
            cell.txtcontName.backgroundColor = UIColor(red: 0.5, green: 0.84, blue: 0.14, alpha: 1.0)
        }
        else if let  isapp = dicSCont.object(forKey: "is_approver") as? Bool, isapp == true {
            
            cell.txtContNum.backgroundColor = UIColor(red: 0.5, green: 0.84, blue: 0.14, alpha: 1.0)
            cell.txtcontName.backgroundColor = UIColor(red: 0.5, green: 0.84, blue: 0.14, alpha: 1.0)
        }
        else{
            
            cell.txtContNum.backgroundColor = UIColor.clear
            cell.txtcontName.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Helper.size(iPhone: 45, iPad: 90)
    }
    
    func btnPhoneClicked(sender: UIButton) {
        
        let dicSCont = arrySiteCont[sender.tag] as!  NSDictionary
        if let txtPP = dicSCont.object(forKey: "mobile") as? NSNull{
            AgLog.debug("mobile is empthy \(txtPP)")
        }
        else{
            
            let textPhn = dicSCont.string(forKey: "mobile")
            AgLog.debug("phone number s",textPhn)
            
            if textPhn.count != 0 {
                let aString = textPhn
                var newString = ""
                if aString.contains("(") {
                    newString = aString.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
                    newString = newString.replacingOccurrences(of: "(", with: "", options: .literal, range: nil)
                    newString = newString.replacingOccurrences(of: ")", with: "", options: .literal, range: nil)
                    newString = newString.replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
                }
                else{
                    newString = textPhn
                }
                
                AgLog.debug("phone number s", newString)
                if let url = URL(string: "tel://\(newString)"), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }
    }
}
