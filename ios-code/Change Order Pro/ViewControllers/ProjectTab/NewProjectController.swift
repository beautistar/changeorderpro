//
//  NewProjectController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire

enum ProjectMode {
    case new
    case details
    case edit
}

class NewProjectController: UIViewController ,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate{
    
    var arryTextFields = [UITextField]()
    
    @IBOutlet var scrollViw: UIScrollView!
    @IBOutlet var viwContainer: UIView!
    
    @IBOutlet var txtCliectName: UITextField!
    @IBOutlet var txtContract: UITextField!
    @IBOutlet var txtJobNumber: UITextField!
    @IBOutlet var txtJobName: UITextField!
    @IBOutlet var txtAdd1: UITextField!
    @IBOutlet var txtAddLine2: UITextField!
    @IBOutlet var txtCity: UITextField!
    @IBOutlet var txtState: UITextField!
    @IBOutlet var txtZip: UITextField!
    
    @IBOutlet var btnAddNew: UIButton!
    @IBOutlet var btnCreate: UIButton!
    
    @IBOutlet var imgPlus: UIImageView!
    
    @IBOutlet var lblLast: UILabel!

    var arryOriginal =  NSArray()
    
    @IBOutlet var tblMyContHightConstant: NSLayoutConstraint!
    @IBOutlet var tblmycontact: AGTableView!
  
    var isProjectEdit: ProjectMode = .new

    var projTitle = ""
    var projID = ""
    var parentID = ""
    
    var totalCount = 1
    
    var arrySiteCont = NSMutableArray()
    
    var dicProjDetail = NSDictionary()
    var offlineprojID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arry_siteContactList_globle = NSMutableArray()
        
        if isProjectEdit == .edit{
            setProjectInformation()
        }
        
        self.adjustforTextFieldReturn()
    }
    
    func adjustforTextFieldReturn(){
        
        arryTextFields.append(txtCliectName)
        arryTextFields.append(txtContract)
        arryTextFields.append(txtJobNumber)
        arryTextFields.append(txtJobName)
        arryTextFields.append(txtAdd1)
        arryTextFields.append(txtAddLine2)
        arryTextFields.append(txtCity)
        arryTextFields.append(txtState)
        arryTextFields.append(txtZip)
        
        txtCliectName.delegate = self
        txtContract.delegate = self
        txtJobNumber.delegate = self
        txtJobName.delegate = self
        txtAdd1.delegate = self
        txtAddLine2.delegate = self
        txtCity.delegate = self
        txtState.delegate = self
        txtZip.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let currentIndex = arryTextFields.index(of: textField), currentIndex < arryTextFields.count-1 {
            arryTextFields[currentIndex+1].becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtState{
            if (txtState.text?.count)! >= 2  && range.length == 0{
                return false
            }
        }
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
    
        let allProject = RemovedController.shared.getMyDeletedProjectData()
        AgLog.debug(allProject)
        if isProjectEdit == .details{
            self.navigationItem.title = self.projTitle
            self.setProjectInformation()
        }
        else if isProjectEdit == .edit{
            
            self.btnCreate.setTitle("Save", for: .normal)
            AgLog.debug("site Contact for Edit project", arry_siteContactList_globle)
            
            if arry_siteContactList_globle.count == 0 {
                totalCount = 1
            }
            else{
                totalCount = arry_siteContactList_globle.count
            }
            
            self.setViewtoResize()
        }
        else{
            
            self.navigationItem.title = "New Project"
            AgLog.debug("site Contact for new project \(arry_siteContactList_globle)")
            totalCount = arry_siteContactList_globle.count
            self.setViewtoResize()
        }

        self.arryOriginal = arry_siteContactList_globle
        self.sortArryByAlphabatic()
    }
    
    func setProjectInformation(){
        
        AgLog.debug("selected Project Detail", dicProjDetail)
        self.navigationItem.title = dicProjDetail.string(forKey: "job_name")
        
        txtCliectName.text = dicProjDetail.string(forKey: "client_name")
        txtContract.text = dicProjDetail.string(forKey: "contract_no")
        txtJobNumber.text = dicProjDetail.string(forKey: "job_number")
        txtJobName.text = dicProjDetail.string(forKey: "job_name")
        txtAdd1.text = dicProjDetail.string(forKey: "address")
        txtAddLine2.text = dicProjDetail.string(forKey: "address1")
        txtCity.text = dicProjDetail.string(forKey: "city")
        txtState.text =  dicProjDetail.string(forKey: "state")
        txtZip.text = dicProjDetail.string(forKey: "zip")
        
        self.parentID = dicProjDetail.string(forKey: "parent_id")
        self.projID = dicProjDetail.string(forKey: "id")
        
        if uni_isWorking_Offline == true{
            self.setOfflineContact()
            offlineprojID = dicProjDetail.string(forKey: "offline_id")
        }
        else{
            self.calltogetMyContacts()
        }
    }

    //MARK: show off line contact data
    func setOfflineContact(){
        
        let arrCont  =  dicProjDetail.array(forKey: "site_contact_ids")
        self.arryOriginal = arrCont
        self.sortArryByAlphabatic()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.setViewtoResize()
    }
    
    func setViewtoResize(){
        self.tblmycontact.layoutSubviews()
     //   self.tblMyContHightConstant.constant = Helper.size(iPhone: CGFloat(45 * totalCount), iPad: CGFloat(90 * totalCount))
    }

    @IBAction func btnAddNewClicked(_ sender: Any) {
    
        let newContct = self.storyboard?.instantiateViewController(withIdentifier: "addSiteContact") as! AddSiteContactController
        newContct.statusCont = "New"
        self.navigationController?.pushViewController(newContct, animated: true)
    }
    
    @IBAction func btnContactClicekd(_ sender: Any) {
        
        let newContct = self.storyboard?.instantiateViewController(withIdentifier: "addSiteContact") as! AddSiteContactController
        newContct.statusCont = "Edit"
        self.navigationController?.pushViewController(newContct, animated: true)
    }
    
    @IBAction func btnCreateClicked(_ sender: Any) {
        self.checkAllFieldData()
    }
    
    func checkAllFieldData() {
        
        if self.txtJobName.isNull{
            Helper.AgAlertView("JOB_NAME".localized)
        }
        else if txtAdd1.isNull && txtAddLine2.isNull {
            if self.txtCity.isNull {
                Helper.AgAlertView("CITY".localized)
            }
            else if self.txtState.isNull {
                Helper.AgAlertView("STATE".localized)
            }
            else{
                dataSaveApiActionCalled()
            }
        }
        else if txtAdd1.isNull && !txtAddLine2.isNull {
            Helper.AgAlertView("ADDRESS".localized)
        }
        else if self.txtCity.isNull {
            Helper.AgAlertView("CITY".localized)
        }
        else if self.txtState.isNull {
            Helper.AgAlertView("STATE".localized)
        }
        else if self.txtZip.isNull {
            Helper.AgAlertView("ZIP".localized)
        }
        else{
            dataSaveApiActionCalled()
        }
    }
    
    func dataSaveApiActionCalled(){
        
        if !self.checkSameDataProjectIsAvailableOrNot() {
            
            if isProjectEdit == .edit{
                
                AgLog.debug("Save project Data")
                if uni_isWorking_Offline == true{
                    self.offline_EditandSaveProjectData()
                }
                else{
                    self.projectEditAndSavedataApi()
                }
            }
            else{
                AgLog.debug("uplload project Data")
                if uni_isWorking_Offline == true{
                    self.offline_saveNewProjectData()
                }
                else{
                    self.finalProjectDataforNEwProject()
                }
            }
        }
    }
    
    func checkSameDataProjectIsAvailableOrNot() -> Bool{
        if isProjectEdit != .edit {
            for oldProject in uni_Array_allProjects.dict {
                
                if oldProject.string(forKey: "client_name") == self.txtCliectName.text ?? "",
                    oldProject.string(forKey: "contract_no") == self.txtContract.text ?? "",
                    oldProject.string(forKey: "job_name") == self.txtJobName.text ?? "",
                    oldProject.string(forKey: "job_number") == self.txtJobNumber.text ?? "",
                    oldProject.string(forKey: "address") == self.txtAdd1.text ?? "",
                    oldProject.string(forKey: "address1") == self.txtAddLine2.text ?? "",
                    oldProject.string(forKey: "state") == self.txtState.text ?? "",
                    oldProject.string(forKey: "zip") == self.txtZip.text ?? "",
                    oldProject.string(forKey: "city") == self.txtCity.text ?? "" {
                    
                    Helper.AgAlertView("This data already exists")
                    return true
                }
            }
        }
        return false
    }
    
    //MARK:  Offline work operations
    func offline_saveNewProjectData(){
        
        var offlineID = String(Date().ticks)
        offlineID = "\(offlineID)_offline"
        
        let dictParams:  [String: Any] = [
            "client_name" : self.txtCliectName.text ?? "",
            "contract_no" : self.txtContract.text ?? "",
            "job_name" : self.txtJobName.text! ,
            "job_number" : self.txtJobNumber.text ?? "" ,
            "address" : self.txtAdd1.text ?? "" ,
            "address1" : self.txtAddLine2.text ?? "" ,
            "state" : self.txtState.text! ,
            "zip" : self.txtZip.text! ,
            "city" : self.txtCity.text!,
            "site_contact_ids" : arrySiteCont,
            "id" : offlineID,
            "is_uploaded" : "false"
        ]
        
        uni_Array_allProjects.add(dictParams)
        DataController.sharedInstance.saveMyProjectData(allProject: uni_Array_allProjects)
        isAnyNewChanges = true
        self.navigationController?.popViewController(animated: true)
    }
    
    //Edited Project
    func offline_EditandSaveProjectData(){

        let dictParams:  [String: Any] = [
            "id" : self.projID,
            "user_id" : UserDetails.loginUserId,
            "client_name" : self.txtCliectName.text ?? "",
            "contract_no" : self.txtContract.text ?? "",
            "job_name" : self.txtJobName.text! ,
            "job_number" : self.txtJobNumber.text ?? "" ,
            "address" : self.txtAdd1.text ?? "" ,
            "address1" : self.txtAddLine2.text ?? "" ,
            "state" : self.txtState.text! ,
            "zip" : self.txtZip.text! ,
            "city" : self.txtCity.text!,
            "site_contact_ids" : arrySiteCont,
            "is_uploaded" : "false",
            "parent_id" : parentID,
            "offline_id" : offlineprojID
        ]
        
        let curindex =  uni_Array_allProjects.index(of: dicProjDetail)
        uni_Array_allProjects.replaceObject(atIndex: curindex, with: dictParams)
        DataController.sharedInstance.saveMyProjectData(allProject: uni_Array_allProjects)
        isAnyNewChanges = true
        self.navigationController?.popViewController(animated: true)
    }

    //MARK: API to upload New Project Data
    func finalProjectDataforNEwProject() {
        let serverarray = NSMutableArray()
        for dic in arrySiteCont {
            
            var serDic = [String : Any]()
            let dicn = dic as!  NSMutableDictionary
            serDic["site_contact_id"] = dicn.string(forKey: "id")
            serDic["is_approver"] = dicn.object(forKey: "is_approver")
            serverarray.add(serDic)
        }
    
        let dictParams:  [String: Any] = [
            "user_id" : UserDetails.loginUserId,
            "client_name" : self.txtCliectName.text ?? "",
            "contract_no" : self.txtContract.text ?? "",
            "job_name" : self.txtJobName.text! ,
            "job_number" : self.txtJobNumber.text ?? "" ,
            "address" : self.txtAdd1.text ?? "" ,
            "address1" : self.txtAddLine2.text ?? "" ,
            "state" : self.txtState.text! ,
            "zip" : self.txtZip.text! ,
            "city" : self.txtCity.text!,
            "site_contact_ids" : serverarray.toJson ?? "",
            "offline_id" : ""
        ]
        
        AgLog.debug("Final Dictionary \(dictParams)")
        
        if Reachability.isConnectedToNetwork() == true {

            let strURL = kBaseURL + "createproject"
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                
                if response.isSuccess {
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    Helper.AgAlertView(response.message)
                }
            }
        }
    }
    
    //MARK: API to upload Edit Project Data
    func projectEditAndSavedataApi() {
        let serverarray = NSMutableArray()
        
        for dic in arrySiteCont {
            
            var serDic = [String : Any]()
            let dicn = dic as!  NSDictionary
            serDic["site_contact_id"] = dicn.string(forKey: "id")
            serDic["is_approver"] = dicn.object(forKey: "is_approver")
            serverarray.add(serDic)
        }
        
        let dictParams:  [String: Any] = [
            "project_id" : self.projID,
            "user_id" : UserDetails.loginUserId,
            "contract_no" : self.txtContract.text ?? "",
            "client_name" : self.txtCliectName.text ?? "",
            "job_name" : self.txtJobName.text! ,
            "job_number" : self.txtJobNumber.text ?? "" ,
            "address" : self.txtAdd1.text ?? "" ,
            "address1" : self.txtAddLine2.text ?? "" ,
            "state" : self.txtState.text! ,
            "zip" : self.txtZip.text! ,
            "city" : self.txtCity.text!,
            "site_contact_ids" : serverarray.toJson ?? "",
            "offline_id" : ""
        ]
        
        AgLog.debug("Final Dictionary \(dictParams)")
        
        if Reachability.isConnectedToNetwork() == true {

            let strURL = kBaseURL + "editproject"
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                if response.isSuccess {
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    Helper.AgAlertView(response.message)
                }
            }
        }
    }
    
    func calltogetMyContacts() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "getcontacts/" + self.projID
            
            Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.default)
                .responseJSON { response in
                
                response.logShow()
                if let arryServer = response.result.value as? NSArray {
                        AgLog.debug("Thisis the contacts \(arryServer)")
                        self.arryOriginal = arryServer
                        self.sortArryByAlphabatic()
                }
                    
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    func sortArryByAlphabatic(){
        let descriptor: NSSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        let sortedResults: NSArray = arryOriginal.sortedArray(using: [descriptor]) as NSArray
        
        AgLog.debug("sort result", sortedResults)

        self.arrySiteCont = NSMutableArray(array: sortedResults)
        
        arry_siteContactList_globle = self.arrySiteCont
        self.tblmycontact.reloadData()
        self.totalCount = self.arrySiteCont.count
        self.setViewtoResize()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrySiteCont.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "rContactcell") as! ContactCell
        
        let dicSCont = arrySiteCont[indexPath.row] as? NSDictionary ?? NSDictionary()
        
        cell.txtcontName.text = dicSCont.string(forKey: "name")
        
        if let txtPP = dicSCont.object(forKey: "mobile") as? NSNull{
            AgLog.debug("mobile is empthy \(txtPP)")
            cell.txtContNum.text = dicSCont.string(forKey: "email")
        }
        else{
            
            let textPhn = dicSCont.string(forKey: "mobile")
            if textPhn.count == 10{
                let fir = textPhn.substring(with: 0..<3)
                let midd = textPhn.substring(with: 3..<6)
                let last = textPhn.substring(with: 6..<10)
                cell.txtContNum.text = "(\(fir)) \(midd)-\(last)"
                
            }
            else if textPhn.count == 0{
                cell.txtContNum.text = dicSCont.string(forKey: "email")
            }
            else{
                cell.txtContNum.text = dicSCont.string(forKey: "mobile")
            }
        }
        
        if let isAPp = dicSCont.object(forKey: "is_approver") as? String, isAPp == "1" {
            cell.txtContNum.backgroundColor = UIColor(red: 0.5, green: 0.84, blue: 0.14, alpha: 1.0)
            cell.txtcontName.backgroundColor = UIColor(red: 0.5, green: 0.84, blue: 0.14, alpha: 1.0)
        }
        else if let  isapp = dicSCont.object(forKey: "is_approver") as? Bool, isapp {
            cell.txtContNum.backgroundColor = UIColor(red: 0.5, green: 0.84, blue: 0.14, alpha: 1.0)
            cell.txtcontName.backgroundColor = UIColor(red: 0.5, green: 0.84, blue: 0.14, alpha: 1.0)
        }
        else{
            
            cell.txtContNum.backgroundColor = UIColor.clear
            cell.txtcontName.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newContct = self.storyboard?.instantiateViewController(withIdentifier: "addSiteContact") as! AddSiteContactController
        newContct.statusCont = "Edit"
        newContct.dicsiteCont = arrySiteCont[indexPath.row] as? NSDictionary ?? NSDictionary()
        self.navigationController?.pushViewController(newContct, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .normal, title:"\(TableRowActionType.delete)\n Delete ") { action, index in

            AgLog.debug("Delete button \(editActionsForRowAt.row)")
            self.arrySiteCont.removeObject(at: index.row)
            self.tblmycontact.reloadData()
            self.setViewtoResize()
        }
        
        delete.backgroundColor = .red
        return [delete]
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = UIContextualAction(style: .normal, title: "\(TableRowActionType.delete)\n Delete ") { action, view, completionHandler in
            AgLog.debug("Delete button \(indexPath.row)")
            self.arrySiteCont.removeObject(at: indexPath.row)
            self.tblmycontact.reloadData()
            self.setViewtoResize()
            
            completionHandler(true)
        }
        delete.backgroundColor = .red
        
        let config = UISwipeActionsConfiguration(actions: [delete])
        config.performsFirstActionWithFullSwipe = false
        
        return config
    }
}
