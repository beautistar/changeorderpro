//
//  ProjectDetailController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit

class ProjectDetailController: UIViewController {
    
    var projTitle = ""
    
    @IBOutlet var lblprojTitle: UILabel!
    @IBOutlet var viewMenuu: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.lblprojTitle.text = projTitle
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.title = "My Project"
    }
    
    func setNavItems(){
        
        let menuBtn = UIBarButtonItem(
            image: UIImage(named: "MenuIcon"),
            style: .plain,
            target: self,
            action: #selector(menuClicked(sender:))
        )
        self.navigationItem.rightBarButtonItem = menuBtn
    }
    
    func menuClicked(sender: UIBarButtonItem) {
        self.viewMenuu.isHidden = !self.viewMenuu.isHidden
    }
    
    @IBAction func btn1NewChngOrderClicked(_ sender: Any) {
        
        self.viewMenuu.isHidden = true
        let chngOrder = self.storyboard?.instantiateViewController(withIdentifier: "changeOrdr") as! ChangeOrderController
        self.navigationController?.pushViewController(chngOrder, animated: true)
    }
    
    @IBAction func btn2EditProInfoClicked(_ sender: Any) {
    }
    
    @IBAction func btn3SettingsClicked(_ sender: Any) {
        
        self.viewMenuu.isHidden = true
        let settings = self.storyboard?.instantiateViewController(withIdentifier: "settings") as! SettingsController
        self.navigationController?.pushViewController(settings, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

