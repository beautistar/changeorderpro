//
//  AddSiteContactController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire
import Contacts
import ContactsUI
import PhoneNumberKit

class AddSiteContactController: UIViewController,UITextFieldDelegate,CNContactPickerDelegate {
    
    var arryTextFields = [UITextField]()
    
    @IBOutlet var txtName: UITextField!
    
    @IBOutlet var btnCheckBox: UIButton!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtTitle: UITextField!
    
    @IBOutlet var vDeleteSiteCont: UIView!
    @IBOutlet var btnDeleteSiteCont: UIButton!
    @IBOutlet var txtPhn: UITextField!
    
    var statusCont = ""
    
    @IBOutlet var btnaddOrSave: UIButton!
    
    var statChkBx = ""
    
    let phoneNumberKit = PhoneNumberKit()
    var phNUmber = ""
    var countCode = ""
    var statSameContact = "NO"
    
    var dicsiteCont = NSDictionary()
    var siteContID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtEmail.keyboardType = .emailAddress

        self.adjustforTextFieldReturn()
        
        AgLog.debug("all site contact..", arry_siteContactList_globle)
        AgLog.debug("approver is", self.checkAlredayApprover())
    }
    
    func adjustforTextFieldReturn(){
        
        arryTextFields.append(txtName)
        arryTextFields.append(txtTitle)
        arryTextFields.append(txtPhn)
        arryTextFields.append(txtEmail)
        
        txtName.delegate = self
        txtEmail.delegate = self
        txtTitle.delegate = self
        txtPhn.delegate = self
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let currentIndex = arryTextFields.index(of: textField), currentIndex < arryTextFields.count-1 {
            arryTextFields[currentIndex+1].becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon

        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.title = "Site Contact"
        
        checkContactoperationMode()
    }
    
    @IBAction func btnDeleteSiteContactClicked(_ sender: Any) {
        let index = arry_siteContactList_globle.index(of: dicsiteCont)
        arry_siteContactList_globle.removeObject(at: index)
        self.navigationController?.popViewController(animated: true)
    }
    
    func showSitecontactData(){

        self.txtName.text = self.dicsiteCont.string(forKey: "name")
        self.txtTitle.text = self.dicsiteCont.string(forKey: "title")
        self.txtPhn.text = self.dicsiteCont.string(forKey: "mobile")
        self.txtEmail.text  = self.dicsiteCont.string(forKey: "email")
        
        siteContID = self.dicsiteCont.string(forKey: "id")
        
        AgLog.debug("id of site contact \(siteContID)")
        
        if let isApp = self.dicsiteCont.object(forKey: "is_approver") as? String, isApp == "0"{
            statChkBx = "unselected"
            self.btnCheckBox.setBackgroundImage(UIImage(named:"CheckNotApprover.png"), for: .normal)
            
        }
        else if let isApp = self.dicsiteCont.object(forKey: "is_approver") as? Bool, isApp == false{
            statChkBx = "unselected"
            self.btnCheckBox.setBackgroundImage(UIImage(named:"CheckNotApprover.png"), for: .normal)
        }
        else{
            
            statChkBx = ImageFlag.selected
            self.btnCheckBox.setBackgroundImage(UIImage(named:"checkRight.png"), for: .normal)
            statSameContact = "YES"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let str = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        if textField == self.txtPhn{
            return Helper.phoneNumberFormat(with: self.txtPhn, string: string, str: str)
        }
        
        return true
    }
    
    func checkContactoperationMode(){
        
        if statusCont == "Edit"{
            
            self.btnaddOrSave.setTitle("Save", for: .normal)
            self.btnDeleteSiteCont.isHidden = false
            vDeleteSiteCont.isHidden = false
            self.showSitecontactData()
        }
        else{
            self.btnaddOrSave.setTitle("Add", for: .normal)
            self.btnDeleteSiteCont.isHidden = true
            vDeleteSiteCont.isHidden = true
        }
    }
    
    //MARK: set dummy contact
    @IBAction func btnCheckBoxClicked(_ sender: Any) {
        
        if statChkBx == ImageFlag.selected{
            
            statChkBx = "unselected"
            self.btnCheckBox.setBackgroundImage(UIImage(named:"CheckNotApprover.png"), for: .normal)
        }
        else{
            
            statChkBx = ImageFlag.selected
            self.btnCheckBox.setBackgroundImage(UIImage(named:"checkRight.png"), for: .normal)
        }
    }
    
    //MARK: Load from contactClicked
    @IBAction func btnAddClicked(_ sender: Any) {
        
        let peoplePicker = CNContactPickerViewController()
        // peoplePicker.displayedPropertyKeys = @[CNContactEmailAddressesKey];
        peoplePicker.displayedPropertyKeys = [CNContactPhoneNumbersKey,CNContactEmailAddressesKey]
        // peoplePicker.predicateForSelectionOfContact = NSPredicate(format: "emailAddresses.@count > 0")
        //        peoplePicker.predicateForSelectionOfContact = NSPredicate(format: "postalAddresses.@count <= 1")
        //        peoplePicker.
        peoplePicker.delegate = self
        self.present(peoplePicker, animated: true, completion: nil)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact){
        
        for item in contact.phoneNumbers{
            if item.label == "_$!<Mobile>!$_"{
                if let  mo = (item.value).value(forKey: "digits") as? String{
                    if let  countryCode = (item.value).value(forKey: "countryCode") as? String{
                        self.countCode = countryCode
                        AgLog.debug("CountryCode Code \(countryCode)")
                    }
                    phNUmber = mo
                    parseNumber(mo)
                }
            }
        }
        
        AgLog.debug("****\n email address \(contact.emailAddresses)")
        for item in contact.emailAddresses{
            
            if let  email = (item).value(forKey: "value") as? String{
                AgLog.debug("\nEmail \(email)")
                self.txtEmail.text = email
                
            }
        }
        
        AgLog.debug(contact.givenName)
        
        let name = contact.givenName
        
        let lastName = contact.familyName
        
        self.txtName.text = name + " " + lastName
    }
    
    func parseNumber(_ number: String) {

        do {
            let phoneNumberCustomDefaultRegion = try phoneNumberKit.parse(number, withRegion: "US", ignoreType: true)
            AgLog.debug("Phone national \(phoneNumberKit.format(phoneNumberCustomDefaultRegion, toType: .national))")
            
            if self.countCode == "us" {
                self.txtPhn.text  = "+1\(phoneNumberKit.format(phoneNumberCustomDefaultRegion, toType: .national))"
            }
            else{
                self.txtPhn.text  = phoneNumberKit.format(phoneNumberCustomDefaultRegion, toType: .national)
            }
        }
        catch {
            AgLog.debug("Generic parser error \(error.localizedDescription)")
            self.setUsFormat()
        }
    }
    
    func setUsFormat(){
        
        let textPhn = self.phNUmber
        if textPhn.count == 10{
            let fir = textPhn.substring(with: 0..<3)
            let midd = textPhn.substring(with: 3..<6)
            let last = textPhn.substring(with: 6..<10)
            self.txtPhn.text = "(\(fir)) \(midd)-\(last)"
            
        }
        else{
            self.txtPhn.text  = self.phNUmber
        }
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        //your code
    }
    
    @IBAction func btnAddNewOrsaveClicked(_ sender: Any) {
        self.checkAllFields()
    }
    
    func checkAllFields(){
        
        let app = self.checkAlredayApprover()
        
        var alreadyhAppr = ""
        var message  = ""
        
        if self.txtName.isNull {
            message = "NAME".localized
        }
        else if statChkBx == ImageFlag.selected{
            
            if !self.isValidEmail(testStr: self.txtEmail.text!) ||  self.txtEmail.text == ""{
                message = "Approvers must have a valid email address"
            }
            else if statSameContact == "NO"  && app == true {

                message = "Another contact is currently the approver, change to this one?"
                alreadyhAppr = "YES"
            }
            else{
                message = "checked"
            }
        }
        else if !self.isValidEmail(testStr: self.txtEmail.text!) &&   self.txtPhn.text == ""  {
            message = "Enter valid email or phone"
        }
        else{
            
            message = "checked"
        }
        
        if message == "checked" {
            
            AgLog.debug("call to add site contact")
            if statusCont == "Edit"{
                self.editSiteAfterCheck()
            }
            else{
                self.addnewsiteconactafterNetworkCheck()
            }
        }
        else{
            
            if alreadyhAppr == "YES"{
                self.showAlreadyApproverView(alertmessage: message)
            }
            else{
                Helper.AgAlertView( message)
            }
        }
    }
    
    func addnewsiteconactafterNetworkCheck(){
        if uni_isWorking_Offline == true{
            
            self.Offline_SaveSiteContact()
        }
        else{
            
            self.calltoaddSiteContact()
        }
    }
    
    func editSiteAfterCheck(){
        
        if uni_isWorking_Offline == true{
            self.Offline_EditAndSaveContact()
        }
        else{
            self.updateSiteContactAPi()
        }
    }
    
    //MARK: save site contact data for off line
    func Offline_SaveSiteContact(){

        let dicSCont = NSMutableDictionary()
        let offlineID = String(Date().ticks)
        let cId = "\(offlineID)_offline"
        dicSCont.setValue(cId, forKey: "id")
        dicSCont.setValue(self.txtName.text , forKey: "name")
        dicSCont.setValue(self.txtPhn.text, forKey: "mobile")
        dicSCont.setValue(self.txtTitle.text, forKey: "title")
        dicSCont.setValue(self.txtEmail.text, forKey: "email")
        dicSCont.setValue("false", forKey: "is_uploaded")
        dicSCont.setValue(self.statChkBx == ImageFlag.selected, forKey: "is_approver")
        arry_siteContactList_globle.add(dicSCont)
        self.navigationController?.popViewController(animated: true)
    }
    
    func Offline_EditAndSaveContact(){
        
        let isAPpvar  = self.statChkBx == ImageFlag.selected
        
        let dictParams:  [String: Any] = [
            "user_id" : UserDetails.loginUserId,
            "id" : siteContID,
            "name" : self.txtName.text!,
            "mobile" : self.txtPhn.text ?? "",
            "email" : self.txtEmail.text ?? "",
            "title" : self.txtTitle.text ?? "",
            "is_approver" : isAPpvar,
            "is_uploaded" : "false"
        ]
        
        let index = arry_siteContactList_globle.index(of: dicsiteCont)
        arry_siteContactList_globle.replaceObject(at: index, with: dictParams)
        self.navigationController?.popViewController(animated: true)
    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func checkAlredayApprover()->Bool{
        
        var valApp = false
        
        AgLog.debug("arry site contact to check Approver \(arry_siteContactList_globle)")
        for dict in arry_siteContactList_globle.dict {

            if let isAPp = (dict as AnyObject).object(forKey: "is_approver") as? String, isAPp == "1"{
                valApp =  true

            }
            else if let  isAPp = dict.object(forKey: "is_approver") as? Bool, isAPp == true{
                valApp =  true
            }
        }
        
        return valApp
    }
    
    func makePreviousApprovertoNormal(){
        
        for dict in arry_siteContactList_globle.dict{
            
            let idSit = dict.string(forKey: "id")
            
            var is_approver: Bool = false
            
            if let isAPp = (dict as AnyObject).object(forKey: "is_approver") as? String, isAPp == "1"{
                is_approver = true
            }
            else  if let isAPp = (dict as AnyObject).object(forKey: "is_approver") as? Bool, isAPp == true{
                is_approver = true
            }
            
            if is_approver {
                arry_siteContactList_globle.remove(dict)
                let dicSCont = NSMutableDictionary()
                dicSCont.setValue(idSit, forKey: "id")
                dicSCont.setValue(dict.string(forKey: "name") , forKey: "name")
                dicSCont.setValue(dict.string(forKey: "mobile"), forKey: "mobile")
                dicSCont.setValue(dict.string(forKey: "title"), forKey: "title")
                dicSCont.setValue(dict.string(forKey: "email"), forKey: "email")
                dicSCont.setValue(false, forKey: "is_approver")
                arry_siteContactList_globle.add(dicSCont)
                
                if statusCont == "Edit"{
                    
                    self.editSiteAfterCheck()
                    AgLog.debug("call to Update after change approver")
                }
                else{
                    
                    self.addnewsiteconactafterNetworkCheck()
                    AgLog.debug("call to add after change approver")
                }
            }
        }
    }

    func showAlreadyApproverView(alertmessage:String)  {
    
        let alertCon = UIAlertController(title: "", message: alertmessage, preferredStyle: .alert)
        alertCon.addAction(UIAlertAction(title: "OK_TEXT".localized, style: .default, handler:{ (UIAlertAction)in
            self.makePreviousApprovertoNormal()
            
        }))
        alertCon.addAction(UIAlertAction(title: "CANCEL", style: .default , handler: nil))
        self.present(alertCon, animated: true, completion: nil)
        
    }
    
    func calltoaddSiteContact() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "addcontact"
            let dictParams:  [String: Any] = [
                "user_id" : UserDetails.loginUserId,
                "name" : self.txtName.text!,
                "mobile" : self.txtPhn.text ?? "",
                "email" : self.txtEmail.text ?? "",
                "title" : self.txtTitle.text ?? ""
            ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default)
                .responseJSON { response in
                    response.logShow()
                    if let dicServer = response.result.value as? NSDictionary {
                        
                        if response.isSuccess {
                            
                            let siteId = dicServer.string(forKey: "site_contact_id")
                            let dicSCont = NSMutableDictionary()
                            
                            dicSCont.setValue(siteId, forKey: "id")
                            dicSCont.setValue(self.txtName.text , forKey: "name")
                            dicSCont.setValue(self.txtPhn.text, forKey: "mobile")
                            dicSCont.setValue(self.txtTitle.text, forKey: "title")
                            dicSCont.setValue(self.txtEmail.text, forKey: "email")
                            dicSCont.setValue(self.statChkBx == ImageFlag.selected, forKey: "is_approver")

                            arry_siteContactList_globle.add(dicSCont)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    func updateSiteContactAPi() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "editcontact"
            
            let dictParams:  [String: Any] = [
                "user_id" : UserDetails.loginUserId,
                "site_contact_id" : siteContID,
                "name" : self.txtName.text!,
                "mobile" : self.txtPhn.text ?? "",
                "email" : self.txtEmail.text ?? "",
                "title" : self.txtTitle.text ?? ""
            ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default)
                .responseJSON { response in
                
                    response.logShow()
                    if response.isSuccess {
                        self.checkAndupdateselectedDic()
                    }
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    func checkAndupdateselectedDic(){
        
        for dict in arry_siteContactList_globle.dict{
            
            let idSit = dict.string(forKey: "id")
            
            if idSit == siteContID{
                
                arry_siteContactList_globle.remove(dict)
                
                let dicSCont = NSMutableDictionary()
                
                dicSCont.setValue(siteContID, forKey: "id")
                dicSCont.setValue(self.txtName.text , forKey: "name")
                dicSCont.setValue(self.txtPhn.text, forKey: "mobile")
                dicSCont.setValue(self.txtTitle.text, forKey: "title")
                dicSCont.setValue(self.txtEmail.text, forKey: "email")
                dicSCont.setValue(self.statChkBx == ImageFlag.selected, forKey: "is_approver")
                
                arry_siteContactList_globle.add(dicSCont)
                _ =  self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
