//
//  MYProjectsController.swift
//  Change Order Pro
////@available(iOS 11.0, *)
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire
import AMTooltip

var isAnyNewChanges = true

class MYProjectsController: UIViewController ,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet var tblMyProjects: UITableView?
    
    let arryProj = ["Lake Nona Place","Bainbridge","Sanctuary at Westport"]
    let arryAddress = ["Lake Nona,FL","Casselberry, FL","Port Orange,FL","Kissimmee,FL"]
    
    @IBOutlet var viewMenu: UIView!
    var statusMenu = ""
    
    var selectedindex = 0
    
    var delteProjID = ""
    var arrayAllProj = NSMutableArray()
    
    static let shared = MYProjectsController()
    
    var arrayOnlineProjects = NSMutableArray()
    
    typealias CompletionHandler = (_ success:Bool) -> Void
    
    let arrayRemainProjects = NSMutableArray()
    var arryServerProjects = NSMutableArray()
    
    var remainIdsFromOnline: [String] = []
    var offlineProjectIds: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "MYProjCell", bundle: nil)
        tblMyProjects?.register(nib, forCellReuseIdentifier: "projCell")
        
        tblMyProjects?.pullToRefresh = { ref in
            ref.attributedTitle = NSAttributedString(string: "Syncing..")
            self.calltogetMyallProjects()
        }
        
        tblMyProjects?.agRefreshControl.attributedTitle = NSAttributedString(string: "Syncing..")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.appCameToForeground(notification:)), name: .UIApplicationWillEnterForeground, object: nil)
        updateUserData { (isStatus) in
            
        }
    }
    
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier =  UIBackgroundTaskInvalid
    let backgroundTaskName = "saveData"

    @objc func appCameToForeground(notification: NSNotification){
        
        AgLog.debug("Application came to the foreground")
        if self.backgroundTaskIdentifier != UIBackgroundTaskInvalid{
            AgLog.debug("We need to invalidate our background task")
            self.plist_getAllOfflineProjects()
            UIApplication.shared.endBackgroundTask(
                self.backgroundTaskIdentifier)
            self.backgroundTaskIdentifier = UIBackgroundTaskInvalid
        }
    }
    
    func endBackgroundTask() {
        AgLog.debug("Background task ended.")
        
        if self.backgroundTaskIdentifier != UIBackgroundTaskInvalid{
            UIApplication.shared.endBackgroundTask(
                self.backgroundTaskIdentifier)
            self.backgroundTaskIdentifier = UIBackgroundTaskInvalid
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        endRefreshView()
    }
    
    //MARK: getAllProjects Saved in user Plist
    func plist_getAllOfflineProjects(){
        AgLog.debug("Method Filer 6")
        
        self.arrayAllProj = DataController.sharedInstance.getMyProjectData().mutable
        uni_Array_allProjects = self.arrayAllProj
        self.tblMyProjects?.reloadData()
        self.setAllProjNameForDropDown()
        AgLog.debug("my Offline projects", self.arrayAllProj)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.tabBarController?.tabBar.tintColor = UIColor.white
        self.navigationItem.title = "MY_PROJECTS".localized
        
        self.setNavItems()
        self.calltogetMyallProjects()
        
        if uni_isWorking_Offline {
            self.plist_getAllOfflineProjects()
        }
        
        if isAnyNewChanges {
            self.uploadStartToserver()
        }
        
        //        self.tblMyProjects?.reloadData()
    }
    
    func endRefreshView() {
        
        self.tblMyProjects?.agRefreshControl.attributedTitle = NSAttributedString(string: "Last synced : Just Now")
        DispatchQueue.main.asyncAfter(deadline: DispatchTime(uptimeNanoseconds: 2), execute: {
            if let tbl = self.tblMyProjects{
                if tbl.agRefreshControl.isRefreshing {
                    tbl.agRefreshControl.endRefreshing()
                }
            }
        })
        
        self.tblMyProjects?.reloadData()
    }
    
    func setNavItems(){
        
        let menuBtn = UIBarButtonItem( image: UIImage(named: "newAdd.png"), style: .plain, target: self, action: #selector(addNewProjClicked(sender:)))
        self.navigationItem.rightBarButtonItem = menuBtn
    }
    
    func uploadStartToserver(){
        AgLog.debug("start uploading.....")
        self.backgroundTaskIdentifier =
            UIApplication.shared.beginBackgroundTask(withName: backgroundTaskName, expirationHandler: {
                AgLog.debug("Background task is expired now")
                self.endBackgroundTask()
            })
        self.createBackGroundTask()
    }
    
    func createBackGroundTask(){
        
        if Reachability.isConnectedToNetwork() {
            self.totallSiteContact = 0
            self.arraySC = NSMutableArray()
            self.total_offline_Projects = 0
            self.uploadAllremainSiteContactToServer()
        }
        else{
            isAnyNewChanges = false
        }
    }
    
    func addNewProjClicked(sender: UIBarButtonItem) {
        if !CheckUserDetails.isFullAccess {
            Helper.agAlertSubscription("Projects option not available without a subscription. Please subscribe for full access.")
        }
        else{
            let newProj = self.storyboard?.instantiateViewController(withIdentifier: "newProj") as! NewProjectController
            self.navigationController?.pushViewController(newProj, animated: true)
        }
    }
    
    //MARK: Tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayAllProj.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "projCell") as! MYProjCell
        
        let dic = arrayAllProj.dictionary(at: indexPath.row)
        cell.lblProjTitle.text = dic.string(forKey: "job_name")
        cell.lblProjAddress.text = [dic.string(forKey: "city"), dic.string(forKey: "state")].joined(separator: ", ")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .normal, title:"\(TableRowActionType.delete)\n Delete ") { action, index in
            
            self.tblMyProjects?.setEditing(!tableView.isEditing, animated: false)
            self.selectedindex = editActionsForRowAt.row
            self.showDeleteAlert()
        }
        delete.backgroundColor = .red
        
        let editOpt = UITableViewRowAction(style: .normal, title: "\(TableRowActionType.edit)\n  Edit ") { action, index in
            
            self.tblMyProjects?.setEditing(!tableView.isEditing, animated: false)
            self.selectedindex = editActionsForRowAt.row
            self.gotoEditProject()
        }
        
        editOpt.backgroundColor = UIColor(red: 49/255, green: 167/255, blue: 106/255, alpha: 1.0)
        
        return [delete,editOpt]
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let editOpt = UIContextualAction(style: .normal, title: "\(TableRowActionType.edit)\n Edit") { action, view, completionHandler in
            self.tblMyProjects?.setEditing(!tableView.isEditing, animated: false)
            self.selectedindex = indexPath.row
            self.gotoEditProject()
            
            completionHandler(true)
        }
        editOpt.backgroundColor = UIColor(red: 49/255, green: 167/255, blue: 106/255, alpha: 1.0)
        
        let delete = UIContextualAction(style: .normal, title: "\(TableRowActionType.delete)\n Delete ") { action, view, completionHandler in
            self.tblMyProjects?.setEditing(!tableView.isEditing, animated: false)
            self.selectedindex = indexPath.row
            self.showDeleteAlert()
            completionHandler(true)
        }
        delete.backgroundColor = .red
        let config = UISwipeActionsConfiguration(actions: [delete,editOpt])
        config.performsFirstActionWithFullSwipe = false
        return config
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Helper.size(iPhone: 3, iPad: 6)
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let dict = arrayAllProj.obj(indexPath.row) as? NSDictionary {
            let proinfo = self.storyboard?.instantiateViewController(withIdentifier: "proInfo") as! ProjetNewInfo
            proinfo.dicProjDetail = dict
            self.navigationController?.pushViewController(proinfo, animated: true)
        }
    }
    
    func gotoEditProject(){
        if let dict = arrayAllProj.obj(selectedindex) as? NSDictionary {
            let proDetail = self.storyboard?.instantiateViewController(withIdentifier: "newProj") as! NewProjectController
            proDetail.isProjectEdit = .edit
            proDetail.dicProjDetail = dict
            self.navigationController?.pushViewController(proDetail, animated: true)
        }
    }
    
    func showDeleteAlert(){
        
        guard let dic = self.arrayAllProj.obj(self.selectedindex) as? NSDictionary else {
            return
        }
        
        let alert = UIAlertController(title: "", message: "DELETE_MESSAGE_Project".localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE_TEXT".localized, style: .destructive) { _ in
            
            if uni_isWorking_Offline == true{
                AgLog.debug("delete Project for offline")
                for dicOrd in uni_array_allChangeOrders.dict{
                    let savedId = dicOrd.integer(forKey: "project_id")
                    if savedId == dic.integer(forKey: "id") {
                        
                        uni_array_allChangeOrders.remove(dicOrd)
                        DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
                        
                        let myDeletedOrder  = RemovedController.shared.getMyDeletedOrderItemsData()
                        let arrayDeleted  = NSMutableArray(array:myDeletedOrder)
                        arrayDeleted.add(dic)
                        RemovedController.shared.saveMyDeletedOrdersItems(allDeletedOrders: arrayDeleted)
                    }
                }
                
                uni_Array_allProjects.remove(dic)
                DataController.sharedInstance.saveMyProjectData(allProject: uni_Array_allProjects)
                self.plist_getAllOfflineProjects()
                
                let myDeleted  = RemovedController.shared.getMyDeletedProjectData()
                let arrayDeleted  = NSMutableArray(array:myDeleted)
                arrayDeleted.add(dic)
                RemovedController.shared.saveMyDeletedProjectData(allDeletedProjects: arrayDeleted)
                self.refreshAfterDeleteProject()
                isAnyNewChanges = true
                self.uploadStartToserver()
            }
            else{
                self.delteProjID = dic.string(forKey: "id")
                AgLog.debug("delete id is \( self.delteProjID)")
                self.calltoDeleteProjApi()
            }
        })
        
        alert.addAction(UIAlertAction(title: "CANCLE_TEXT".localized, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: call Delete API
    func calltoDeleteProjApi() {
        
        if Reachability.isConnectedToNetwork() == true {
            if let dic = self.arrayAllProj.obj(self.selectedindex) as? NSDictionary {
                let strURL = kBaseURL + "deleteproject"
                
                let dictParams:  [String: String] = ["project_id" : dic.string(forKey: "id")]
                
                Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default)
                    .responseJSON { response in
                        
                        response.logShow()
                        if !response.isSuccess {
                            Helper.AgAlertView(response.message)
                        }
                }
            }
        }
    }
    
    func refreshAfterDeleteProject(){
        
        for dicT in arrayAllProj.dict {
            
            let cupId = dicT.string(forKey: "id")
            if cupId == self.delteProjID{
                arrayAllProj.remove(dicT)
                setAllProjNameForDropDown()
            }
        }
        self.tblMyProjects?.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let v = self.navigationItem.rightBarButtonItem?.view {
            let InfoProjectShow = UserDefaults.standard.value(forKey: "InfoProjectShow") as? NSString
            if (InfoProjectShow == nil ){
                AMTooltipView(message: "Tap here to add a project.", focusView: v , complete: {
                    UserDefaults.standard.set("YES", forKey: "InfoProjectShow")
                })
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.tblMyProjects?.setEditing(false, animated: true)
    }
    
    //MARK: online and Offline Projects Marge with each other
    func getAllOflineProjectsIds(){
        
        if uni_Array_allProjects.count > 0 {
            
            offlineProjectIds = uni_Array_allProjects.dict.map {
                $0.string(forKey:"id")
            }
            
            AgLog.debug("All Offline project ids \(offlineProjectIds)")
            
            self.getOnlineIDs()
        }
        else{
            self.addAllProjectsTohOfflinePlist()
        }
    }
    
    func getOnlineIDs (){
        
        remainIdsFromOnline = arryServerProjects.dict.filter {
            return !offlineProjectIds.contains($0.string(forKey: "id"))
            
            }.map { $0.string(forKey: "id") }
        
        AgLog.debug("Remain Project Ids \(remainIdsFromOnline)")
        
        if remainIdsFromOnline.count > 0 {
            getorderFOrmOnlineArraybyRemainIds()
        }
        
        self.uploadedDataValueReplace()
        self.endRefreshView()
    }
    
    func getorderFOrmOnlineArraybyRemainIds(){
        AgLog.debug("Method Filer 4")
        for dicProj in arryServerProjects.dict {
            let pId  = dicProj.string(forKey: "id")
            if remainIdsFromOnline.contains(pId) {
                arrayRemainProjects.add(dicProj)
            }
        }
        addRemainProjwithOfflinePlist()
    }
    
    func addRemainProjwithOfflinePlist()  {
        
        AgLog.debug("Method Filer 5")
        for dicProjects in arrayRemainProjects.dict {
            
            let dicEdPrj = dicProjects.mutableCopy() as! NSMutableDictionary
            dicEdPrj.setValue("true", forKey: "is_uploaded")
            uni_Array_allProjects.add(dicEdPrj)
        }
        
        self.endRefreshView()
        DataController.sharedInstance.saveMyProjectData(allProject: uni_Array_allProjects)
        self.plist_getAllOfflineProjects()
    }
    
    func uploadedDataValueReplace() {
        for i in 0..<uni_Array_allProjects.count {
            let offDict = uni_Array_allProjects.dictionary(at: i).mutableCopy() as! NSMutableDictionary
            
            for serverDict in arryServerProjects.dict {
                if offDict.string(forKey: "id") == serverDict.string(forKey: "id") {
                    
                    if offDict.string(forKey: "is_uploaded") == "true" {
                        offDict.replaceValue(with: serverDict)
                        
                        uni_Array_allProjects.replaceObject(at: i, with: offDict)
                        DataController.sharedInstance.saveMyProjectData(allProject: uni_Array_allProjects)
                    }
                }
            }
        }
    }
    
    func addAllProjectsTohOfflinePlist()  {
        
        for i in 0..<arryServerProjects.count{
            let dicProjects = arryServerProjects[i] as? NSDictionary ?? NSDictionary()
            let dicEdPrj = dicProjects.mutableCopy() as! NSMutableDictionary
            dicEdPrj.setValue("true", forKey: "is_uploaded")
            uni_Array_allProjects.add(dicEdPrj)
        }
        
        self.endRefreshView()
        DataController.sharedInstance.saveMyProjectData(allProject: uni_Array_allProjects)
        self.plist_getAllOfflineProjects()
    }
    
    // *****************************************
    func myProjectsCheckandMatchwithserverData()  {
        
        let arrayAllProjectToSave = NSMutableArray()
        for i in 0..<arrayOnlineProjects.count{
            if let dicProjects = arrayOnlineProjects.obj(i) as? NSDictionary{
                let dicEdPrj = dicProjects.mutableCopy() as! NSMutableDictionary
                dicEdPrj.setValue("true", forKey: "is_uploaded")
                dicEdPrj.setValue("", forKey: "offline_id")
                arrayAllProjectToSave.add(dicEdPrj)
            }
        }
        AgLog.debug("It s last Index save to Plist and reload table ,\(arrayAllProjectToSave)")
        
        DataController.sharedInstance.saveMyProjectData(allProject: arrayAllProjectToSave)
        self.plist_getAllOfflineProjects()
    }
    
    //MARK: Document Directory Operation end
    func setAllProjNameForDropDown() {
        AgLog.debug("Method Filer 7")
        array_myOwnProjects = NSMutableArray()
        array_myProjectsID = NSMutableArray()
        
        array_myOwnProjects.add("ALL Projects")
        array_myProjectsID.add("Dummy")
        
        for dic in arrayAllProj.dict {
            
            let proName = dic.string(forKey: "job_name")
            let myproId = dic.string(forKey: "id")
            
            array_myOwnProjects.add(proName)
            array_myProjectsID.add(myproId)
        }
    }
    
    var totallSiteContact = 0
    var arraySC = NSMutableArray()
    var total_offline_Projects = 0
    
    //MARK: upload start  to server if network Reachable
    func uploadAllremainSiteContactToServer(){
        
        if uni_Array_allProjects.count > 0 {
            
            // first upload all site contacts
            if total_offline_Projects >= uni_Array_allProjects.count{
                
                AgLog.debug("all Projects site contact uploaded ")
                self.startUploadAllRemainProjectsToserver()
            }
            else{
                
                let dic = uni_Array_allProjects.dictionary(at: self.total_offline_Projects)
                arraySC = NSMutableArray()
                
                arraySC = dic.array(forKey: "site_contact_ids").mutable
                totallSiteContact = 0
                self.callIncrementSiteContact(dicProjectData: dic)
            }
        }
        else{
            AgLog.debug("nothing To Sync data")
            self.startToDeletedProjectsProcess()
        }
    }
    
    func callIncrementSiteContact(dicProjectData dict: NSDictionary){
        
        if totallSiteContact >= arraySC.count {
            AgLog.debug("All site contact uploaded in current project")
            
            let dictParams:  [String: Any] = [
                "user_id" : dict.string(forKey: "user_id"),
                "client_name" : dict.string(forKey: "client_name"),
                "job_name" : dict.string(forKey: "job_name"),
                "job_number" : dict.string(forKey: "job_number"),
                "contract_no" : dict.string(forKey: "contract_no"),
                "address" : dict.string(forKey: "address"),
                "address1" : dict.string(forKey: "address1"),
                "state" : dict.string(forKey: "state"),
                "zip" :  dict.string(forKey: "zip"),
                "city" : dict.string(forKey: "city"),
                "is_uploaded" : dict.string(forKey: "is_uploaded"),
                "site_contact_ids" : self.arraySC,
                "id" : dict.string(forKey: "id"),
                "offline_id" : dict.string(forKey: "offline_id"),
                "parent_id": dict.string(forKey: "parent_id")
            ]
            
            uni_Array_allProjects.replaceObject(at: self.total_offline_Projects, with: dictParams)
            DataController.sharedInstance.saveMyProjectData(allProject: uni_Array_allProjects)
            
            total_offline_Projects = total_offline_Projects + 1
            self.uploadAllremainSiteContactToServer()
        }
        else {
            
            let dS = arraySC.dictionary(at: self.totallSiteContact)
            self.startSiteContactProcess(dicSiContact: dS, dicProject: dict)
        }
    }
    
    func startSiteContactProcess(dicSiContact: NSDictionary, dicProject: NSDictionary){
        
        let dSit = dicSiContact
        
        // we need to check site contact is uploaded or not
        if let statUp = dSit.object(forKey: "is_uploaded") as? String {
            if statUp == "false"{
                let idofSite = dSit.string(forKey: "id")
                
                AgLog.debug("upload as new contact call Api for add new site contact")
                WebMasterController.shared.calloffline_SiteContactchangeApi(isNew: idofSite.contains("offline"), dicSiteData: dSit) { (dicResponce) in
                    
                    if let dict = dicResponce {
                        AgLog.debug("return dictionary after upload ",dict)
                        let inde = self.arraySC.index(of: dSit)
                        self.arraySC.replaceObject(atIndex: inde, with: dict)
                    }
                    
                    self.totallSiteContact  = self.totallSiteContact + 1
                    self.callIncrementSiteContact(dicProjectData: dicProject)
                }
            }
            else{
                AgLog.debug("it is already on server")
                
                totallSiteContact  = totallSiteContact + 1
                self.callIncrementSiteContact(dicProjectData: dicProject)
            }
        }
        else{
            
            let idofSite = dSit.string(forKey: "id")
            AgLog.debug("upload as new contact call Api for  add new site contact")
            WebMasterController.shared.calloffline_SiteContactchangeApi(isNew: idofSite.contains("offline"), dicSiteData: dSit) { (dicResponce) in
                
                if let dict = dicResponce {
                    AgLog.debug("return dictionary after upload,", dict)
                    let inde = self.arraySC.index(of: dSit)
                    self.arraySC.replaceObject(atIndex: inde, with: dict)
                }
                
                self.totallSiteContact  = self.totallSiteContact + 1
                self.callIncrementSiteContact(dicProjectData: dicProject)
            }
        }
    }
    
    //MARK: start Upload projects data after sitecontact
    func startUploadAllRemainProjectsToserver(){
        uni_Array_allProjects = DataController.sharedInstance.getMyProjectData().mutable
        AgLog.debug("Now my Project data is ", uni_Array_allProjects)
        
        total_offline_Projects = 0
        if uni_Array_allProjects.count > 0 {
            self.uploadOfflineProjectData()
        }
        else{
            AgLog.debug("Projects is empty")
        }
    }
    
    func uploadOfflineProjectData(){
        
        if total_offline_Projects >= uni_Array_allProjects.count{
            AgLog.debug("all projects are uploaded Now start if any project is deleted from offline list")
            startToDeletedProjectsProcess()
            AgLog.debug("uploadOfflineProjectData completion")
        }
        else{
            
            let dProj = uni_Array_allProjects.dictionary(at: total_offline_Projects)
            let statUp = dProj.string(forKey: "is_uploaded")
            if statUp == "false" {
                
                let pID = dProj.string(forKey: "id")
                let indeMain  = uni_Array_allProjects.index(of: dProj)
                WebMasterController.shared.calloffline_ProjectApi(isNew: pID.contains("offline"), dicProjectData: dProj, completion: { (dicMyProj) in
                    
                    if dicMyProj != nil {
                        if let dict = (uni_Array_allProjects.obj(indeMain) as? NSDictionary)?.mutableCopy() as? NSMutableDictionary, let serverDict = dicMyProj{
                            
                            dict.replaceValue(with: serverDict)
                            uni_Array_allProjects.replaceObject(at: indeMain, with: dict)
                            DataController.sharedInstance.saveMyProjectData(allProject: uni_Array_allProjects)
                        }
                    }
                    self.total_offline_Projects =  self.total_offline_Projects + 1
                    self.uploadOfflineProjectData()
                })
            }
            else{
                AgLog.debug("no need to upload")
                self.total_offline_Projects =  self.total_offline_Projects + 1
                self.uploadOfflineProjectData()
            }
        }
    }
    var totalProjectesToDelete = 0
    var array_deletedProjectes = NSMutableArray()
    
    //MARK: offline deleted project operations start
    func startToDeletedProjectsProcess() {
        
        totalProjectesToDelete = 0
        let arrD  = RemovedController.shared.getMyDeletedProjectData()
        array_deletedProjectes = NSMutableArray(array: arrD)
        self.deleteOfflineProjectFromServer()
    }
    
    func deleteOfflineProjectFromServer(){
        
        if totalProjectesToDelete == array_deletedProjectes.count{
            AgLog.debug("all deleted projecte is change on server")
            AgLog.debug("all items are sync with server reload table veiw")
            
            self.plist_getAllOfflineProjects()
            if isAnyNewChanges == true{
                isAnyNewChanges  = false
                AgLog.debug("NO need to upload order here")
            }
            else{
                MyAllOrdersController.shared.startToUploadMyAllOrdersToserver()
            }
        }
        else{
            
            let dicP = array_deletedProjectes.dictionary(at: totalProjectesToDelete)
            let pID = dicP.string(forKey: "id")
            if pID.contains("offline"){
                AgLog.debug("not on Server increment and call method for next project")
                
                totalProjectesToDelete = totalProjectesToDelete + 1
                self.deleteOfflineProjectFromServer()
            }
            else{
                
                AgLog.debug("delete this on server")
                if let statDelted = dicP.object(forKey: "is_Deleted") as? String{
                    
                    AgLog.debug("it is already deleted before ",statDelted)
                    totalProjectesToDelete = totalProjectesToDelete + 1
                    self.deleteOfflineProjectFromServer()
                }
                else{
                    
                    AgLog.debug("Need to delete this projects")
                    let projectID = dicP.string(forKey: "id")
                    WebMasterController.shared.callOffline_deleteProjectFromServer(projectIDToDelete: projectID, completion: { (response) in
                        AgLog.debug("delete Responce: ",response)
                        if response {
                            let newdicProj = dicP.mutableCopy() as! NSMutableDictionary
                            newdicProj.setValue("true", forKey: "is_Deleted")
                            self.array_deletedProjectes.replaceObject(at: self.totalProjectesToDelete, with: newdicProj)
                            RemovedController.shared.saveMyDeletedProjectData(allDeletedProjects: self.array_deletedProjectes)
                        }
                        self.totalProjectesToDelete = self.totalProjectesToDelete + 1
                        self.deleteOfflineProjectFromServer()
                        
                    })
                }
            }
        }
    }
    
    //MARK: Get my Projects from Server
    func calltogetMyallProjects() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            let strURL = kBaseURL + "getprojects/" + UserDetails.loginUserId
            
            Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.default)
                .responseJSON { response in
                    
                    response.logShow()
                    
                    if let arryServer = response.result.value as? NSArray {
                        
                        self.arrayOnlineProjects = NSMutableArray(array: arryServer)
                        
                        if self.arrayOnlineProjects.count > 0 {
                            
                            self.arryServerProjects = self.arrayOnlineProjects
                            let arryDeleted = RemovedController.shared.getMyDeletedProjectData().mutable
                            let arryDeletedIDs = NSMutableArray()
                            
                            if arryDeleted.count == 0{
                                self.getAllOflineProjectsIds()
                            }
                            else{
                                
                                for dict in arryDeleted.dict{
                                    let pId = dict.string(forKey: "id")
                                    arryDeletedIDs.add(pId)
                                }
                                
                                AgLog.debug("Deleted ID \(arryDeletedIDs)")
                                let arryTempServer = NSMutableArray()
                                for dicOnline in self.arrayOnlineProjects.dict {
                                    
                                    let p_onlineId = dicOnline.string(forKey: "id")
                                    
                                    if !arryDeletedIDs.contains(p_onlineId){
                                        arryTempServer.add(dicOnline)
                                    }
                                    else{
                                        if dicOnline.string(forKey: "status") != Constants.STATUS_DELETED {
                                            for i in 0..<arryDeleted.count{
                                                if p_onlineId == arryDeleted.dict[i].string(forKey: "id") {
                                                    arryDeleted.removeObject(at: i)
                                                    RemovedController.shared.saveMyDeletedProjectData(allDeletedProjects: arryDeleted)
                                                }
                                            }
                                            
                                            arryTempServer.add(dicOnline)
                                        }
                                    }
                                }
                                
                                self.arryServerProjects = NSMutableArray()
                                self.arrayOnlineProjects = NSMutableArray()
                                AgLog.debug("call to offline ID")
                                self.arryServerProjects = arryTempServer
                                self.arrayOnlineProjects = arryTempServer
                                self.getAllOflineProjectsIds()
                            }
                        }
                    }
                    self.endRefreshView()
            }
        }
    }
}

class TableViewRowAction: UITableViewRowAction {
    
    var image: UIImage?
    func _setButton(button: UIButton)  {
        
        if let image = image, let titleLabel = button.titleLabel {
            
            let labelString = NSString(string: titleLabel.text!)
            let titleSize = labelString.size(attributes: [NSFontAttributeName: titleLabel.font])
            button.tintColor = UIColor.white
            button.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
            button.imageEdgeInsets.right = -titleSize.width
        }
    }
}
