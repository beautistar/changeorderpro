//
//  EditChangeItemController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 08/11/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit

import Photos
import Alamofire
import AlamofireImage
import DatePickerDialog


class EditChangeItemController: UIViewController, CameraViewControllerDelegate, UITextFieldDelegate,  UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var backFromItemEdit = "NOONE"
    
    var finalEditedItemsarray = NSMutableArray()
    var fakearray = NSMutableArray()
    
    @IBOutlet var viewDelete: UIView!
    @IBOutlet var scrollView: UIScrollView!

    var jobName = ""
    var chnOrderId = ""
    var arryallItemsId = NSMutableArray()
    var projectID = ""
    
    var offline_projID = ""
    
    @IBOutlet var lblLineItems: UILabel!
    
    @IBOutlet var btnNextItem: UIButton!
    @IBOutlet var btnpPeviousItem: UIButton!
    
    @IBOutlet var txtProjName: FormTextField!
    @IBOutlet var txtDate: FormTextField!
    @IBOutlet var txtCost: FormTextField!
    @IBOutlet var txtTitle: FormTextField!
    @IBOutlet var txtDescription: UITextView!
    @IBOutlet var btnDate: UIButton!
    
    @IBOutlet var btnImgONE: UIButton!
    @IBOutlet var btnImgFour: UIButton!
    @IBOutlet var btnImgThree: UIButton!
    @IBOutlet var btnImgTwo: UIButton!
    
    var modeAdditionalItem: AdditionalItemsModes = .Normal
    
    var imagePicker = UIImagePickerController()
    
    var imgStatus1 = ImageFlag.notSelected
    var imgStatus2 = ImageFlag.notSelected
    var imgStatus3 = ImageFlag.notSelected
    var imgStatus4 = ImageFlag.notSelected
    
    var img1 = UIImage()
    var img2 = UIImage()
    var img3 = UIImage()
    var img4 = UIImage()
    
    var projTitle = ""
    
    var ser_proName = ""
    var ser_date = ""
    var ser_cost = ""
    var ser_title = ""
    var ser_description = ""
    var ser_img_1 = UIImage()
    var ser_img_2 = UIImage()
    var ser_img_3 = UIImage()
    var ser_img_4 = UIImage()
    
    var ser_img1Status = ""
    var ser_img2Status = ""
    var ser_img3Status = ""
    var ser_img4Status = ""
    
    var ser_odrItemId = ""
    
    var imageNum = ""
    var checkFromStatus = ""
    
    var newItemAddedstatus: Bool = false
    
    let myTextGroup = DispatchGroup()
    let myeditedImageGroup = DispatchGroup()
    
    let firstGroup = DispatchGroup()
    var firstCounter = 0
    var counterUploaded = 0
    var isLastIndex = ""

    var userTemplateID = ""
    var usercompLogoUrl = ""
    var orderItems = NSArray()
    
    var dicmainEditOffline = NSDictionary()
    var off_image1Name  = ""
    var off_image2Name  = ""
    var off_image3Name  = ""
    var off_image4Name  = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let temp = UserDefaults.standard.object(forKey: "TEMPLATE"){
            userTemplateID = temp as? String ?? ""
            AgLog.debug("Slected Template is \(userTemplateID)")
        }
        
        usercompLogoUrl = UserDetails.logoURL()
        
        let lineText = "LINE_ITEM".localized
        self.lblLineItems.text = "\(lineText) #\(current_lineItemNumber + 1)"
        self.txtProjName.text = jobName
        
        self.btnDate.setTitle(Date().toFormate(format: .MMddyyyy),for: .normal)
        
        self.nextPrevousButtonHideShow()
        
        if uni_isWorking_Offline == true{
            AgLog.debug("set first item for offline")
            self.offlineSetFirstItemfromEditArry()
            
        }else{
            self.setFirstOrderItemDataOnline()
            replaceArray()
        }
        
        txtTitle.returnKeyType = .next
        txtTitle.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtTitle{
            textField.resignFirstResponder()
            txtDescription.perform(#selector(UITextView.becomeFirstResponder), with: nil, afterDelay: 0)
        }
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "CHANGE_ORDER".localized
        self.setNavItems()
        
        if backFromItemEdit == "PREVIEW" {
            modeAdditionalItem = .PreNext
            backFromItemEdit = "NOONE"
            newItemAddedstatus = false
        }
    }
    
    func setNavItems(){
        let menuBtn = UIBarButtonItem(
            image: UIImage(named: "backWhiteAer"),
            style: .plain,
            target: self,
            action: #selector(backClicked(sender:))
        )
        self.navigationItem.leftBarButtonItem = menuBtn
    }
    
    func backClicked(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
        if let previousViewController = self.navigationController?.viewControllers.last as? MyAllOrdersController{
            previousViewController.backFrom = "EDIT"
        }
    }
    
    //MARK: // *********** Offline set First Item *********** //
    func offlineSetFirstItemfromEditArry(){
        
        if arry_EditChangeItems.count  != 0 {
            
            modeAdditionalItem = .PreNext
            
            AgLog.debug("item line number on previous \(current_lineItemNumber)")
            if let dicOrder = arry_EditChangeItems.obj(current_lineItemNumber) as? [String: Any] {

                setDictOrderDetails(dict: dicOrder)
                AgLog.debug("Change order Id to be edited \(self.chnOrderId) and project Id is  \(self.projectID)")
                selected_projID = self.projectID
                
                self.arryallItemsId = NSMutableArray()
                for dicServer in arry_EditChangeItems.dict{
                    self.arryallItemsId.add(dicServer.string(forKey: "order_item_id"))
                }
                
                AgLog.debug("All Items Ids:")
                AgLog.debug(self.arryallItemsId)
                
                offline_projID = dicmainEditOffline.string(forKey: "off_project_id")
            }
        }
    }
    
    //MARK: ********Helper Functions*******
    
    func checkAllFieldIsNilThenRemove() -> Bool{
        if self.txtCost.isNull && self.txtTitle.isNull && self.txtDescription.isNull && imgStatus1 == ImageFlag.notSelected && imgStatus2 == ImageFlag.notSelected && imgStatus3 == ImageFlag.notSelected && imgStatus4 == ImageFlag.notSelected{
            
            if arry_EditChangeItems.count == current_lineItemNumber {
                current_lineItemNumber = arry_EditChangeItems.count - 1
            }
            
            if let dicOrder = arry_EditChangeItems.obj(current_lineItemNumber) as? [String:Any] {
                self.resetImageData()
                setDictDataToNextPreOrders(dicOrder: dicOrder)
                self.scrollView.setContentOffset(CGPoint(x: 0,y: -50 ),animated: true)
            }
            
            return true
        }
        return false
    }
    
    func checkAllfiledVaildation() -> Bool{
        if self.txtProjName.isNull {
            Helper.AgAlertView("Enter project name")
        }
        else if self.txtTitle.isNull {
            Helper.AgAlertView("Enter title")
        }
        else if self.txtDescription.isNull {
            Helper.AgAlertView("Enter description")
        }
        else if imgStatus1 == ImageFlag.notSelected && imgStatus2 == ImageFlag.notSelected && imgStatus3 == ImageFlag.notSelected && imgStatus4 == ImageFlag.notSelected{
            Helper.AgAlertView("Select order image")
        }
        else{
            return true
        }
        return false
    }
    
    func resetImageData() {
        let nilImage = UIImage(named: "ext.png")
        self.btnImgONE.setBackgroundImage(nilImage, for: .normal)
        self.btnImgTwo.setBackgroundImage(nilImage, for: .normal)
        self.btnImgThree.setBackgroundImage(nilImage, for: .normal)
        self.btnImgFour.setBackgroundImage(nilImage, for: .normal)
        
        imgStatus1 = ImageFlag.notSelected
        imgStatus2 = ImageFlag.notSelected
        imgStatus3 = ImageFlag.notSelected
        imgStatus4 = ImageFlag.notSelected
        
        img1 = UIImage()
        img2 = UIImage()
        img3 = UIImage()
        img4 = UIImage()
    }
    
    func setDictOrderDetails(dict: [String: Any]) {
        let lineText = "LINE_ITEM".localized
        self.lblLineItems.text = "\(lineText) #\(current_lineItemNumber + 1)"
        
        self.txtCost.text = dict["cost"] as? String
        self.txtTitle.text = dict["title"] as? String
        self.txtDescription.text = dict["description"] as? String
        
        if let arrayImgs = dict["all_images"] as? [Any] {
            self.offlineSetImagefFromEditedArray(arrayImgs: arrayImgs)
        }
    }
    
    func setDictDataToNextPreOrders(dicOrder: [String: Any]) {
        let lineText = "LINE_ITEM".localized
        self.lblLineItems.text = "\(lineText) #\(current_lineItemNumber + 1)"
        
        self.txtCost.text = dicOrder["cost"] as? String
        self.txtTitle.text = dicOrder["title"] as? String
        self.txtDescription.text = dicOrder["description"] as? String
        
        if let arrayImgs = dicOrder["all_images"] as? [Any] {
            self.offlineSetImagefFromEditedArray(arrayImgs: arrayImgs)
        }
        else{
            self.setImgeFomUrl(dicOrder: dicOrder)
        }
        self.nextPrevousButtonHideShow()
    }
    
    func offlineSetImagefFromEditedArray(arrayImgs: [Any]){
        
        let nilImage = UIImage()
        guard var dicimage = arrayImgs.first as? [String: Any] else { return }
        
        if let stImg1 = dicimage["Image1"] as? String{
            
            if stImg1 ==  ImageFlag.notSelected{
                imgStatus1 = ImageFlag.notSelected
                self.btnImgONE.setBackgroundImage(nilImage, for: .normal)
                off_image1Name = ImageFlag.notSelected
                
            }
            else{
                let img = DataController.sharedInstance.getImageFromDirFolder(withName: stImg1 )
                self.btnImgONE.setBackgroundImage(img, for: .normal)
                imgStatus1 = ImageFlag.selected
                self.img1 = img
                off_image1Name = stImg1
            }
        }
        
        if let stImg2 = dicimage["Image2"] as? String{
            
            if stImg2 ==  ImageFlag.notSelected{
                imgStatus2 = ImageFlag.notSelected
                self.btnImgTwo.setBackgroundImage(nilImage, for: .normal)
                off_image2Name = ImageFlag.notSelected
            }
            else{
                let img = DataController.sharedInstance.getImageFromDirFolder(withName: stImg2 )
                self.btnImgTwo.setBackgroundImage(img, for: .normal)
                imgStatus2 = ImageFlag.selected
                self.img2 = img
                off_image2Name = stImg2
            }
        }
        
        if let stImg3 = dicimage["Image3"] as? String{
            
            if stImg3 ==  ImageFlag.notSelected{
                imgStatus3 = ImageFlag.notSelected
                self.btnImgThree.setBackgroundImage(nilImage, for: .normal)
                off_image3Name  = ImageFlag.notSelected
            }
            else{
                let img = DataController.sharedInstance.getImageFromDirFolder(withName: stImg3 )
                self.btnImgThree.setBackgroundImage(img, for: .normal)
                imgStatus3 = ImageFlag.selected
                self.img3 = img
                off_image3Name = stImg3
            }
        }
        
        if let stImg4 = dicimage["Image4"] as? String{
            
            if stImg4 ==  ImageFlag.notSelected{
                imgStatus4 = ImageFlag.notSelected
                AgLog.debug("status of image four \(stImg4)")
                self.btnImgFour.setBackgroundImage(nilImage, for: .normal)
                off_image4Name = ImageFlag.notSelected
            }
            else{
                let img = DataController.sharedInstance.getImageFromDirFolder(withName: stImg4)
                self.btnImgFour.setBackgroundImage(img, for: .normal)
                imgStatus4 = ImageFlag.selected
                self.img4 = img
                off_image4Name = stImg4
            }
        }
    }
    
    func setImageFromEditedArray(arrayImgs: [Any]){
        
        let nilImage = UIImage(named: "ext.png")
        if var dicimage = arrayImgs.first as? [String: Any] {
            
            img1 = UIImage()
            if let img = dicimage["Image1"] as? UIImage{
                self.btnImgONE.setBackgroundImage(img, for: .normal)
                imgStatus1 = ImageFlag.selected
                self.img1 = img
            }
            else{
                self.btnImgONE.setBackgroundImage(nilImage, for: .normal)
                imgStatus1 = ImageFlag.notSelected
            }
            
            img2 = UIImage()
            if let img = dicimage["Image2"] as? UIImage{
                imgStatus2 = ImageFlag.selected
                self.btnImgTwo.setBackgroundImage(img, for: .normal)
                self.img2 = img
            }
            else{
                imgStatus2 = ImageFlag.notSelected
                self.btnImgTwo.setBackgroundImage(nilImage, for: .normal)
            }
            
            img3 = UIImage()
            if let img = dicimage["Image3"] as? UIImage{
                self.btnImgThree.setBackgroundImage(img, for: .normal)
                imgStatus3 = ImageFlag.selected
                self.img3 = img
            }
            else{
                imgStatus3 = ImageFlag.notSelected
                self.btnImgThree.setBackgroundImage(nilImage, for: .normal)
            }
            
            img4 = UIImage()
            if let img = dicimage["Image4"] as? UIImage{
                self.btnImgFour.setBackgroundImage(img, for: .normal)
                self.img4 = img
                imgStatus4 = ImageFlag.selected
            }
            else{
                imgStatus4 = ImageFlag.notSelected
                self.btnImgFour.setBackgroundImage(nilImage, for: .normal)
            }
        }
    }
    
    func setImgeFomUrl(dicOrder: [String: Any]){
        
        let nilImage = UIImage(named: "ext.png")
        
        let image1Path = dicOrder["image1"] as? String
        
        if image1Path?.count == 0 {
            imgStatus1 = ImageFlag.notSelected
            self.btnImgONE.setBackgroundImage(nilImage, for: .normal)
        }
        else{
            
            imgStatus1 = ImageFlag.selected
            let path1 = kBaseURL + "public/" + image1Path!
            Alamofire.request(path1).responseImage { response in
                if let image = response.result.value {
                    AgLog.debug("image downloaded: \(image)")
                    self.btnImgONE.setBackgroundImage(image, for: .normal)
                    self.img1 = image
                }
            }
        }
        
        let image2Path   = dicOrder["image2"] as? String
        
        if image2Path?.count == 0 {
            imgStatus2 = ImageFlag.notSelected
            self.btnImgTwo.setBackgroundImage(nilImage, for: .normal)
        }
        else{
            imgStatus2 = ImageFlag.selected
            let path1 = kBaseURL + "public/" + image2Path!
            Alamofire.request(path1).responseImage { response in
                
                if let image = response.result.value {
                    AgLog.debug("image downloaded: \(image)")
                    self.btnImgTwo.setBackgroundImage(image, for: .normal)
                    self.img2 = image
                    
                }
            }
        }
        
        let image3Path   = dicOrder["image3"] as? String
        
        if image3Path?.count == 0 {
            imgStatus3 = ImageFlag.notSelected
            self.btnImgThree.setBackgroundImage(nilImage, for: .normal)
            
        }
        else{
            imgStatus3 = ImageFlag.selected
            let path1 = kBaseURL + "public/" + image3Path!
            Alamofire.request(path1).responseImage { response in
                
                if let image = response.result.value {
                    AgLog.debug("image downloaded: \(image)")
                    self.btnImgThree.setBackgroundImage(image, for: .normal)
                    self.img3 = image
                }
            }
        }
        
        let image4Path = dicOrder["image4"] as? String
        
        if image4Path?.count == 0 {
            imgStatus4 = ImageFlag.notSelected
            self.btnImgFour.setBackgroundImage(nilImage, for: .normal)
            
        }
        else{
            imgStatus4 = ImageFlag.selected
            let path1 = kBaseURL + "public/" + image4Path!
            Alamofire.request(path1).responseImage { response in
                
                if let image = response.result.value {
                    AgLog.debug("image downloaded: \(image)")
                    self.btnImgFour.setBackgroundImage(image, for: .normal)
                    self.img4 = image
                }
            }
        }
    }
    
    func currentItemsBasicDictData() -> [String: Any]{
        var dicItem: [String: Any] = [:]
        
        dicItem["projName"] = self.txtProjName.text
        dicItem["Date"] = self.btnDate.titleLabel?.text
        
        if self.txtCost.text!.count == 0{
            dicItem["cost"] = "0"
            isCostSet = false
        }
        else{
            isCostSet = true
            dicItem["cost"] = self.txtCost.text ?? "0"
        }
        
        dicItem["title"] = self.txtTitle.text ?? ""
        dicItem["description"] = self.txtDescription.text ?? ""
        return dicItem
    }
    
    func getOffLineImageNameDictSorted(isLiveSet: Bool = false) -> [String:Any]{
        var dicimage = [String:Any]()
        
        if imgStatus1 == ImageFlag.selected{
            dicimage["Image1"] = isLiveSet ? img1 : off_image1Name
        }
        else{
            dicimage["Image1"] = ImageFlag.notSelected
        }
        
        if imgStatus2 == ImageFlag.selected{
            dicimage["Image2"] = isLiveSet ? img2 : off_image2Name
        }
        else{
            dicimage["Image2"] = ImageFlag.notSelected
        }
        
        if imgStatus3 == ImageFlag.selected{
            dicimage["Image3"] = isLiveSet ? img3 : off_image3Name
        }
        else{
            dicimage["Image3"] = ImageFlag.notSelected
        }
        
        if imgStatus4 == ImageFlag.selected{
            dicimage["Image4"] = isLiveSet ? img4 : off_image4Name
        }
        else{
            dicimage["Image4"] = ImageFlag.notSelected
        }
        
        return dicimage
    }
    
    //MARK: // *********** Online  *********** //
    func setFirstOrderItemDataOnline() {
        
        if arry_EditChangeItems.count != 0 {
            
            modeAdditionalItem = .PreNext
            
            AgLog.debug("item line number on previous \(current_lineItemNumber)")
            if let dicOrder = arry_EditChangeItems.obj(current_lineItemNumber) as? [String:Any] {
                setDictDataToNextPreOrders(dicOrder: dicOrder)
            
                AgLog.debug("Change order Id to be edited \(self.chnOrderId) and project Id is  \(self.projectID)")
                
                self.arryallItemsId = NSMutableArray()
                for dic in arry_EditChangeItems.dict{
                    let ordritmId = dic.string(forKey: "order_item_id")
                    self.arryallItemsId.add(ordritmId)
                }
            }
        }
    }
    
    //MARK: Image Converted To my Array
    func replaceArray(){

        MBProgressHUD.showAdded(to: self.view, animated: true)
        calltoeditObject()
    }
    
    //MARK: Edit First Object
    func calltoeditObject(){

        guard var dicOrder = arry_EditChangeItems.obj(firstCounter) as? [String:Any] else {
            return
        }
        
        var arrayImgs = [Any]()
        var dicimage = [String: Any]()

        let image1Path = dicOrder["image1"] as? String ?? ""
        
        if image1Path.count == 0 {
            dicimage["Image1"] = ImageFlag.notSelected
        }
        else {
            let path1 = kBaseURL + "public/" + image1Path
            Alamofire.request(path1).responseImage { response in
                if let image = response.result.value {
                    AgLog.debug("image downloaded: 1\(image)")
                    dicimage["Image1"] = image
                }
            }
        }
        
        let image2Path = dicOrder["image2"] as? String ?? ""
        if image2Path.count == 0 {
            dicimage["Image2"] = ImageFlag.notSelected
        }
        else{
            
            let path1 = kBaseURL + "public/" + image2Path
            Alamofire.request(path1).responseImage { response in
                if let image = response.result.value {
                    AgLog.debug("image downloaded:2 \(image)")
                    dicimage["Image2"] = image
                }
            }
        }

        let image3Path   = dicOrder["image3"] as? String ?? ""
        
        if image3Path.count == 0 {
            dicimage["Image3"] = ImageFlag.notSelected
        }
        else{
            
            let path1 = kBaseURL + "public/" + image3Path
            Alamofire.request(path1).responseImage { response in
                
                if let image = response.result.value {
                    AgLog.debug("image downloaded:3 \(image)")
                    dicimage["Image3"] = image
                }
            }
        }
        
        let image4Path = dicOrder["image4"] as? String  ?? ""
        
        if image4Path.count == 0 {
            dicimage["Image4"] = ImageFlag.notSelected
        }
        else{
            
            let path1 = kBaseURL + "public/" + image4Path
            Alamofire.request(path1).responseImage { response in
                if let image = response.result.value {
                    AgLog.debug("image downloaded:4 \(image)")
                    dicimage["Image4"] = image
                }
            }
        }
        
        let when = DispatchTime.now() + 4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            
            arrayImgs.append(dicimage)
            dicOrder["all_images"] = arrayImgs
            arry_EditChangeItems.replaceObject(at: self.firstCounter, with: dicOrder)
            
            self.firstCounter = self.firstCounter + 1
            
            if self.firstCounter == arry_EditChangeItems.count{

                AgLog.debug("new array after download image and set new dic \(arry_EditChangeItems)")
                MBProgressHUD.hide(for: self.view, animated: true)
            }
            else{
                self.calltoeditObject()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func resetAllData(){
        
        modeAdditionalItem = .Normal
        
        self.txtCost.text = ""
        self.txtTitle.text = ""
        self.txtDescription.text = ""
        
        self.resetImageData()
        let lineText = "LINE_ITEM".localized
        
        current_lineItemNumber = current_lineItemNumber + 1
        self.lblLineItems.text = "\(lineText) #\(current_lineItemNumber + 1)"
        
        AgLog.debug("Scroll top\( -self.scrollView.contentInset.top)")
        self.scrollView.setContentOffset(CGPoint(x: 0,y: -50 ),animated: true)
        
        self.btnpPeviousItem.isHidden = current_lineItemNumber == 0

        self.btnNextItem.isHidden = true
    }
    
    // MARK: PreviousItem Data check and setup method start
    @IBAction func btnPreviousItemClicked(_ sender: Any) {
        
        if newItemAddedstatus {
            if uni_isWorking_Offline == true{
                self.offline_checkTextForPreviAll()
                //self.chekTextFieldsforPrevious()
            }
            else{
                self.chekTextFieldsforPreviousifAllFilled()
            }
        }
        else{
            self.chekTextFieldsforPrevious()
        }
    }
    
    @IBAction func btnDeleteItemClicked(_ sender: UIButton) {
  
        if arry_EditChangeItems.count == 0 { return }
        
        if arry_EditChangeItems.count == current_lineItemNumber {
            current_lineItemNumber = arry_EditChangeItems.count - 1
        }
        else{
            arry_EditChangeItems.removeObject(at: current_lineItemNumber)
        }
        
        if arry_EditChangeItems.count == 0 {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        AgLog.debug("item line number next \(current_lineItemNumber)")
        
        if(current_lineItemNumber == arry_EditChangeItems.count) {
            current_lineItemNumber = arry_EditChangeItems.count - 1
        }
        
        modeAdditionalItem = .PreNext
        AgLog.debug("item line number next \(current_lineItemNumber)")
       
        if let dicOrder = arry_EditChangeItems.obj(current_lineItemNumber) as? [String:Any] {
            self.resetImageData()
            setDictDataToNextPreOrders(dicOrder: dicOrder)
            self.scrollView.setContentOffset(CGPoint(x: 0,y: -50 ),animated: true)
        }
    }
    
    func chekTextFieldsforPreviousifAllFilled(){
        
        if checkAllfiledVaildation() {
            addItemToEditedUnivarray()
            setDataWithoutSave()
        }
        else{
            self.arryallItemsId.removeLastObject()
            AgLog.debug("All Items IDs \(arryallItemsId)")
            setDataWithoutSave()
        }
    }
    
    func chekTextFieldsforPrevious(){
        
        if checkAllFieldIsNilThenRemove() {
            return
        }
        
        if checkAllfiledVaildation() {
            if uni_isWorking_Offline == true{
                self.offline_setOrderItem(withNext: false)
            }
            else{
                self.gotoOrderItemDataSetup(withNext: false)
            }
        }
    }
    
    func setDataWithoutSave(){
        
        modeAdditionalItem = .PreNext
        AgLog.debug("item line number on previous \(current_lineItemNumber)")
        current_lineItemNumber = current_lineItemNumber - 1

        if let dicOrder = arry_EditChangeItems.obj(current_lineItemNumber)  as? [String:Any] {
            setDictDataToNextPreOrders(dicOrder: dicOrder)
            self.nextPrevousButtonHideShow()
        }
    }
    
    //MARK: Next Previoue Data Login and data
    func offline_setOrderItem(withNext next: Bool){
        
        guard let dict = arry_EditChangeItems.obj(current_lineItemNumber) as? [String:Any] else {
            return
        }
        
        let index = arry_EditChangeItems.index(of: dict)
        
        var dicItem: [String:Any] = currentItemsBasicDictData()
        dicItem["is_uploaded"] = dict["is_uploaded"]
        dicItem["order_item_id"] = dict["order_item_id"]
        dicItem["logo_url"] = usercompLogoUrl
        dicItem["template_id"] = userTemplateID
        dicItem["all_images"] = [getOffLineImageNameDictSorted(isLiveSet: false)]
        
        arry_EditChangeItems.replaceObject(at: index, with: dicItem)
        
        modeAdditionalItem = .PreNext
        
        AgLog.debug("item line number on previous \(current_lineItemNumber)")
        if next {
            current_lineItemNumber = current_lineItemNumber + 1
        }
        else{
            current_lineItemNumber = current_lineItemNumber - 1
        }
        
        if let dicOrder =  arry_EditChangeItems.obj(current_lineItemNumber) as? [String:Any] {
            setDictOrderDetails(dict: dicOrder)
            self.nextPrevousButtonHideShow()
        }
    }
    
    func gotoOrderItemDataSetup(withNext next: Bool){
        
        guard let dict = arry_EditChangeItems.obj(current_lineItemNumber) as? [String:Any] else {
            return
        }
        
        let index = arry_EditChangeItems.index(of: dict)
        
        var dicItem: [String:Any] = currentItemsBasicDictData()
        dicItem["all_images"] = [getOffLineImageNameDictSorted(isLiveSet: true)]
        
        arry_EditChangeItems.replaceObject(at: index, with: dicItem)
        
        modeAdditionalItem = .PreNext
        AgLog.debug("item line number next \(current_lineItemNumber)")
        if next {
            current_lineItemNumber = current_lineItemNumber + 1
        }
        else{
            current_lineItemNumber = current_lineItemNumber - 1
        }
        
        if let dicOrder = arry_EditChangeItems.obj(current_lineItemNumber) as? [String:Any] {
            setDictDataToNextPreOrders(dicOrder: dicOrder)
            self.nextPrevousButtonHideShow()
        }
    }
    
    // MARK: PreviousItem Data  End
    func nextPrevousButtonHideShow(){
        
        self.btnpPeviousItem.isHidden = current_lineItemNumber == 0
        
        if current_lineItemNumber == arry_EditChangeItems.count - 1 {
            self.btnNextItem.isHidden = true
        }
        else{
            self.btnNextItem.isHidden = current_lineItemNumber == arry_EditChangeItems.count
        }
    }
    
    //MARK: NextItem >> Data check and setup method start
    @IBAction func btnNextItemCLicked(_ sender: Any) {
        if checkAllfiledVaildation() {
            if uni_isWorking_Offline == true{
                self.offline_setOrderItem(withNext: true)
            }
            else{
                self.gotoOrderItemDataSetup(withNext: true)
            }
        }
    }
    
    //MARK: NextItem Data check End
    @IBAction func btnDateClicked(_ sender: Any) {
        
        DatePickerDialog().show("Select Date", doneButtonTitle: "Select", cancelButtonTitle: "Cancel",datePickerMode: .date) {
            (date) -> Void in
            
            if let d = date {
                self.btnDate.setTitle(d.toFormate(format: .MMddyyyy),for: .normal)
            }
        }
    }
    
    @IBAction func btnPhotoONEClicked(_ sender: UIButton) {
        imageNum = "ONE"
        let camera = CameraViewController.getInstant()
        camera.delegate = self
        camera.sourceView = sender
        self.present(camera, animated: true, completion: nil)
    }
    
    @IBAction func btnPhotoTwoClicked(_ sender: UIButton) {
        imageNum = "TWO"
        let camera = CameraViewController.getInstant()
        camera.delegate = self
        camera.sourceView = sender
        self.present(camera, animated: true, completion: nil)
    }
    
    @IBAction func btnPhotoThreeClicked(_ sender: UIButton) {
        imageNum = "THREE"
        let camera = CameraViewController.getInstant()
        camera.delegate = self
        camera.sourceView = sender
        self.present(camera, animated: true, completion: nil)
    }
    
    @IBAction func btnphotoFourClicked(_ sender: UIButton) {
        imageNum = "FOUR"
        let camera = CameraViewController.getInstant()
        camera.delegate = self
        camera.sourceView = sender
        self.present(camera, animated: true, completion: nil)
    }
    
    //MARK: 📷 Picker Delegete
    func cameraViewControllerDidCancel(_ picker: CameraViewController, isClose: UIView?) {
        if let v = isClose {
            picker.dismiss(animated: true, completion: nil)
            AGImagePickerController(with: self, type: .photoLibrary, allowsEditing: false, iPadSetup: v)
        }
        else{
            picker.dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        
        self.cameraViewController(nil, didFinishPickingMedia: image)
    }
    
    func cameraViewController(_ picker: CameraViewController?, didFinishPickingMedia image: UIImage) {
        if let p = picker {
            p.dismiss(animated: true, completion: nil)
        }
        
        let offlineName = String(Date().ticks)
        
        if imageNum == "ONE"{
            self.imgStatus1 = ImageFlag.selected
            self.btnImgONE.setBackgroundImage(image, for: .normal)
            self.img1 = image.resizeImage(newWidth: 320)
            
            if uni_isWorking_Offline == true{
                
                self.off_image1Name = "off_\(offlineName).jpg"
                DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.img1, imageName: self.off_image1Name)
            }
        }
            
        else if imageNum == "TWO"{
            self.imgStatus2 = ImageFlag.selected
            self.btnImgTwo.setBackgroundImage(image, for: .normal)
            self.img2 = image.resizeImage(newWidth: 320)
            
            if uni_isWorking_Offline == true{
                
                self.off_image2Name = "off_\(offlineName).jpg"
                DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.img2, imageName: self.off_image2Name)
            }
        }
            
        else if imageNum == "THREE"{
            self.imgStatus3 = ImageFlag.selected
            self.btnImgThree.setBackgroundImage(image, for: .normal)
            self.img3 = image.resizeImage(newWidth: 320)
            
            if uni_isWorking_Offline == true{
                
                self.off_image3Name = "off_\(offlineName).jpg"
                DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.img3, imageName: self.off_image3Name)
            }
            
        }
        else if imageNum == "FOUR"{
            self.imgStatus4 = ImageFlag.selected
            self.btnImgFour.setBackgroundImage(image, for: .normal)
            self.img4 = image.resizeImage(newWidth: 320)
            
            if uni_isWorking_Offline == true{
                
                self.off_image4Name = "off_\(offlineName).jpg"
                DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.img4, imageName: self.off_image4Name)
            }
        }

        picker?.dismiss(animated: true, completion: nil);
    }
    
    //MARK: additional item clicked
    @IBAction func btnAddaditionalCliked(_ sender: Any) {
        newItemAddedstatus = true
        checkFromStatus = "aditionalItem"
        chekTextFieldsForAdditianalItems()
    }
    
    func chekTextFieldsForAdditianalItems(){
        
        if checkAllfiledVaildation() {
            
            let addiitionalItem = "-1"
            self.arryallItemsId.add(addiitionalItem)
            
            if uni_isWorking_Offline == true{
                if modeAdditionalItem == .PreNext{
                    self.offline_editOrderItemwithChanges()
                }
                else{
                    self.offline_finishEditingOnDone()
                }

            }
            else{
                if modeAdditionalItem == .PreNext{
                    self.editItemtoUniArraywithChanges()
                }
                else{
                    self.gotoAddnewLineItemCreate()
                }
            }
        }
    }
    
    func gotoAddnewLineItemCreate(){
        
        let anotherQueue = DispatchQueue(label: "com.mobisharnamm.ChangeOrderPro", qos: .utility)
        
        anotherQueue.sync {
            print("check Fields")
            self.addItemToEditedUnivarray()
        }

        anotherQueue.sync {
            for i in 1..<50{
                print("😡",i)
            }
        }
        
        anotherQueue.sync {
            
            total_ItemsAdded = arry_EditChangeItems.count
            self.resetAllData()
        }
    }
    
    //MARK: Edit Order Item
    func editItemtoUniArraywithChanges(){

        if let dic = arry_EditChangeItems.obj(current_lineItemNumber) as? [String:Any] {
            let index = arry_EditChangeItems.index(of: dic)
            
            var dicItem: [String:Any] = currentItemsBasicDictData()
            dicItem["all_images"] = [getOffLineImageNameDictSorted(isLiveSet: true)]
            
            arry_EditChangeItems.replaceObject(at: index, with: dicItem)
            
            total_ItemsAdded = arry_EditChangeItems.count
            current_lineItemNumber = total_ItemsAdded - 1
            
            self.resetAllData()
        }
    }
    
    func addItemToEditedUnivarray(){
        
        var dicItem: [String:Any] = currentItemsBasicDictData()
        dicItem["all_images"] = [getOffLineImageNameDictSorted(isLiveSet: true)]
        
        if modeAdditionalItem == .Normal {
            arry_EditChangeItems.add(dicItem)
        }
        else{
            if let dic = arry_EditChangeItems.obj(current_lineItemNumber) as? [String:Any] {
                let index = arry_EditChangeItems.index(of: dic)
                arry_EditChangeItems.replaceObject(at: index, with: dicItem)
            }
        }
        
        AgLog.debug("edited final done array \(arry_EditChangeItems)")
        if  checkFromStatus == "DoneItem"{
            self.createQueforItemsEditedTextPart()
            AgLog.debug("method called for final done item *****")
        }
    }
    
    //MARK: Done button Clicked
    @IBAction func btnDoneCLicked(_ sender: Any) {
        checkFromStatus = "DoneItem"
        self.chekTextFieldsForDone()
    }
    
    func chekTextFieldsForDone(){
        
        if checkAllfiledVaildation() {
            if uni_isWorking_Offline == true {
                AgLog.debug("save to doc directory")
                self.offline_finishEditingOnDone()
            }
            else{
                addItemToEditedUnivarray()
            }
        }
    }
    
    //MARK: ********** Offline Operations ***********
    func offline_checkTextForPreviAll(){
        
        if self.txtTitle.isNull && self.txtCost.isNull && self.txtDescription.isNull && ( imgStatus1 == ImageFlag.notSelected && imgStatus2 == ImageFlag.notSelected && imgStatus3 == ImageFlag.notSelected && imgStatus4 == ImageFlag.notSelected ) {
            
            self.arryallItemsId.removeLastObject()
            AgLog.debug("Item IDDDDDs \(arryallItemsId)")
            offline_setDataWithoutSave()
        }
        else{
            if checkAllfiledVaildation() {
                AgLog.debug("show previous item After  save This added data")
                //Add This  item to our array
                self.offline_finishEditingOnDone()
                offline_setDataWithoutSave()
                newItemAddedstatus = false
                self.chekTextFieldsforPrevious()
            }
        }
    }
    
    func offline_setDataWithoutSave(){
        modeAdditionalItem = .PreNext
        
        AgLog.debug("item line number on previous \(current_lineItemNumber)")
        current_lineItemNumber = current_lineItemNumber - 1

        if let dicOrder =  arry_EditChangeItems.obj(current_lineItemNumber) as? [String:Any] {
            setDictOrderDetails(dict: dicOrder)
        }
        self.nextPrevousButtonHideShow()
    }
    
    func offline_editOrderItemwithChanges(){
        
        guard let dict = arry_EditChangeItems.obj(current_lineItemNumber) as? [String:Any] else {
            return
        }
        
        let index = arry_EditChangeItems.index(of: dict)
        
        var dicItem: [String:Any] = currentItemsBasicDictData()
        dicItem["is_uploaded"] = dict["is_uploaded"]
        dicItem["order_item_id"] = dict["order_item_id"]
        dicItem["logo_url"] = usercompLogoUrl
        dicItem["template_id"] = userTemplateID
        dicItem["all_images"] = [getOffLineImageNameDictSorted(isLiveSet: false)]
        
        arry_EditChangeItems.replaceObject(at: index, with: dicItem)
        
        total_ItemsAdded = arry_EditChangeItems.count
        current_lineItemNumber = total_ItemsAdded - 1
        
        self.resetAllData()
        self.cleareAllForOffline()
    }
    
    func cleareAllForOffline(){
        off_image1Name  = ""
        off_image2Name  = ""
        off_image3Name  = ""
        off_image4Name  = ""
    }
    
    func offline_finishEditingOnDone(){
        AgLog.debug("lien item number is final done \(current_lineItemNumber)")
        
        var dicItem: [String:Any] = currentItemsBasicDictData()
        dicItem["logo_url"] = usercompLogoUrl
        dicItem["template_id"] = userTemplateID
        dicItem["all_images"] = [getOffLineImageNameDictSorted(isLiveSet: false)]
        
        if modeAdditionalItem == .Normal {
            dicItem["order_item_id"] = Date.genrateOfflineId
            dicItem["is_uploaded"] = "false"
            arry_EditChangeItems.add(dicItem)
        }
        else{
            if let dic = arry_EditChangeItems.obj(current_lineItemNumber) as? [String: Any] {
                let index = arry_EditChangeItems.index(of: dic)
                dicItem["is_uploaded"] = dic["is_uploaded"]
                dicItem["order_item_id"] = dic["order_item_id"]
                arry_EditChangeItems.replaceObject(at: index, with: dicItem)
            }
        }
        
        AgLog.debug(" Final  Offline Array  Edited", arry_EditChangeItems)
        
        if checkFromStatus == "DoneItem"{
            AgLog.debug("save Edited for Done")
            uni_fromEdit = "YES"
            
            let dicEd = dicmainEditOffline.mutableCopy() as! NSMutableDictionary
            
            dicEd.setValue(arry_EditChangeItems, forKey: "change_order_items")
            dicEd.setValue(selected_projID, forKey: "project_id")
            dicEd.setValue(offline_projID, forKey: "off_project_id")
            dicEd.setValue("false", forKey: "is_uploaded")
            
            AgLog.debug("Final Done Offline \(dicEd)")
            
            let ordrPrevu = self.storyboard?.instantiateViewController(withIdentifier: "changePreview") as! ChangePreViewController
            ordrPrevu.dicallDetail =  NSMutableDictionary(dictionary: dicEd)
            self.navigationController?.pushViewController(ordrPrevu, animated: true)
            
        }
        else{
            AgLog.debug("save Edited for Additional Item")
            self.resetAllData()
            self.cleareAllForOffline()
        }
    }
    
    //MARK: Queue for Textpart API
    func createQueforItemsEditedTextPart(){
        
        finalEditedItemsarray = NSMutableArray()
        
        for dicItem in arry_EditChangeItems as? [[String: Any]] ?? [] {
            
            myTextGroup.enter()

            let curindex = arry_EditChangeItems.index(of: dicItem)
            AgLog.debug("current index \(curindex)")
            
            ser_odrItemId = "\(self.arryallItemsId[curindex])"
            
            ser_proName  = self.txtProjName.text!
            ser_date = (self.btnDate.titleLabel?.text)!
            
            ser_cost = dicItem["cost"] as? String ?? ""
            ser_title = dicItem["title"] as? String ?? ""
            ser_description = dicItem["description"] as? String ?? ""
            
            let parameters:  [String: String] = [
                "cost" : ser_cost,
                "title" : ser_title,
                "description" : ser_description,
                "order_item_id": ser_odrItemId
            ]
            
            finalEditedItemsarray.add(parameters)
            
            AgLog.debug("Total Count \(arry_EditChangeItems.count)")
            let index = arry_EditChangeItems.index(of: dicItem)
            
            if index == arry_EditChangeItems.count - 1{
                self.postDataEditedTextPart()
            }
        }
        
        myTextGroup.notify(queue: .main) {
            print("Finished textpart")
        }
    }
    
    //MARK: call Order item Textpart API
    func postDataEditedTextPart() {
        
        if Reachability.isConnectedToNetwork() == true {
         
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "editchangeorder"
         
            let dictParams: [String: Any] = [
                "change_order_id" : chnOrderId,
                "change_order_items" : finalEditedItemsarray.toJson ?? "",
                ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
            
                response.logShow()
                if let dicServer = response.result.value as? NSDictionary{
                    
                    if response.isSuccess {
                        self.orderItems = dicServer.object(forKey: "order_item_ids") as! NSArray
                        AgLog.debug("IDS is \(self.orderItems)")
                        self.createQueforEditedItemsImagePart()
                    }
                    
                    self.myTextGroup.leave()
                }
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    //MARK: queue for item Image API
    func createQueforEditedItemsImagePart(){
        
        let anotherQueue = DispatchQueue(label: "com.mobisharnamm.ChangeOrderPro", qos: .utility)
        anotherQueue.sync {
            for i in 1..<4{
                print("Image",i)
            }
        }
        
        anotherQueue.sync {
            
            fakearray = NSMutableArray()
            self.takeValueanduploadEdited()
        }
    
        myeditedImageGroup.notify(queue: .main) {
            AgLog.debug("Finished Image part also")
            AgLog.debug("counter  \(self.counterUploaded)")
            AgLog.debug("total   \(arry_EditChangeItems.count)")

            if self.counterUploaded == arry_EditChangeItems.count{
                AgLog.debug("no need to call api")
            }
        }
    }
    
    func takeValueanduploadEdited(){
        
        myeditedImageGroup.enter()
        
        guard var dicItem = arry_EditChangeItems.obj(counterUploaded) as? [String: Any] else {
            return
        }
        
        let curindex = arry_EditChangeItems.index(of: dicItem)
        ser_odrItemId = "\(self.orderItems[curindex])"
        AgLog.debug("Upload OrderItem Id is \(ser_odrItemId)")
        
        ser_img_1 = UIImage()
        ser_img_2 = UIImage()
        ser_img_3 = UIImage()
        ser_img_4 = UIImage()
        
        var arrayImgs = dicItem["all_images"] as! [Any]
        
        var dicimage = arrayImgs[0] as! [String:Any]
        
        if let stImg1 = dicimage["Image1"] as? String{
            AgLog.debug("status of image One \(stImg1)")
            ser_img1Status = ImageFlag.notSelected
            
        }
        else{
            ser_img_1 = dicimage["Image1"] as! UIImage
            ser_img1Status = ImageFlag.selected
            AgLog.debug("iamge one old \(ser_img_1)")
        }
        
        if let stImg2 = dicimage["Image2"] as? String{
            ser_img2Status = ImageFlag.notSelected
            AgLog.debug("status of image Two \(stImg2)")
        }
        else{
            ser_img_2 = dicimage["Image2"] as! UIImage
            ser_img2Status = ImageFlag.selected
            AgLog.debug("iamge Two \(ser_img_2)")
        }
        
        if let stImg3 = dicimage["Image3"] as? String{
            ser_img3Status = ImageFlag.notSelected
            AgLog.debug("status of image Three \(stImg3)")
        }
        else{
            ser_img_3 = dicimage["Image3"] as! UIImage
            ser_img3Status = ImageFlag.selected
        }
        
        if let stImg4 = dicimage["Image4"] as? String{
            ser_img4Status = ImageFlag.notSelected
            AgLog.debug("status of image four \(stImg4)")
        }
        else{
            ser_img_4 = dicimage["Image4"] as! UIImage
            ser_img4Status = ImageFlag.selected
        }

        let index = arry_EditChangeItems.index(of: dicItem)
        isLastIndex = (index == arry_EditChangeItems.count - 1) ? "YES" : "NO"
        self.uploadOrderEditedItemsPicturePart()
    }
    
    //MARK: Order item Image API
    func uploadOrderEditedItemsPicturePart() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            var parameters:  [String: String] = [
                "change_order_id" : self.chnOrderId,
                "project_id" : self.projectID,
                "order_item_id" : ser_odrItemId,
                "is_update" : "yes",
                "template_id" : userTemplateID,
                "logo_url" : usercompLogoUrl,
                "user_id" : UserDetails.loginUserId
            ]
            
            if isLastIndex == "YES" {
                parameters["is_last"] = "true"
            }
            
            let strURL = kBaseURL + "uploadimages"
            let headers1 = ["Authorization": "123456"]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                multipartFormData.append(self.ser_img_1, withName: "image1", status: self.ser_img1Status)
                multipartFormData.append(self.ser_img_2, withName: "image2", status: self.ser_img2Status)
                multipartFormData.append(self.ser_img_3, withName: "image3", status: self.ser_img3Status)
                multipartFormData.append(self.ser_img_4, withName: "image4", status: self.ser_img4Status)
    
                for (key, value) in parameters {
                    multipartFormData.append((value.data(using: .utf8))!, withName: key)
                    
                }}, to: strURL, method: .post, headers: headers1,
                    encodingCompletion: { encodingResult in
                        
                        switch encodingResult {
                        case .success(let upload, _, _):
                            
                            upload.responseJSON { [weak self] response in
                                
                                guard (self != nil) else { return }
                                
                                response.logShow()
                                
                                if let dict = response.result.value as? NSDictionary {
                                    
                                    let status = dict.string(forKey: "status")
                                    
                                    if status == "success" {
                                        
                                        prev_Pdf = dict.string(forKey: "change_order_pdf")
                                        
                                        self!.counterUploaded = (self!.counterUploaded) + 1
                                        AgLog.debug("counter uploaded \(self!.counterUploaded)")
                                        
                                        self?.fakearray.add(dict)
                                        
                                        if self?.fakearray.count == arry_EditChangeItems.count{
                                            arry_allOrderID = NSMutableArray()
                                            arry_allOrderID.add(self!.chnOrderId)
                                            chng_ordID = (self?.chnOrderId)!
                                            selected_projID = (self?.projectID)!
                                            
                                            AgLog.debug("Last object navigate to main controller or show Preveiw of order")
                                            self?.gotoChangeOrderPreveiw()
                                            
                                        }
                                        else{
                                            AgLog.debug("continue ......")
                                            self?.myeditedImageGroup.leave()
                                            self?.takeValueanduploadEdited()
                                        }
                                    }
                                    else{
                                        Helper.AgAlertView("Error in created order")
                                    }
                                }
                            }
                            
                        case .failure(let encodingError):
                            
                            AgLog.debug("error:\(encodingError)")
                        }
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
            })
        }
    }
    
    func gotoChangeOrderPreveiw() {
        let ordrPrevu = self.storyboard?.instantiateViewController(withIdentifier: "changePreview") as! ChangePreViewController
        self.navigationController?.pushViewController(ordrPrevu, animated: true)
    }
}

