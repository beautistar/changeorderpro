//
//  ChangeOrderItems.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Photos
import Alamofire
import DatePickerDialog

enum AdditionalItemsModes: String {
    case PreNext
    case Normal
}

class ChangeOrderItems: UIViewController, UITextFieldDelegate {
    
    var backFromItem = "NOONE"
    var newItemsAddedStatus: Bool = true

    var finalItemsarray = NSMutableArray()
    var fakearray = NSMutableArray()
    
    @IBOutlet var viewDelete: UIView!
    
    var imgFirst = UIImage()
    @IBOutlet var lblLineItems: UILabel!
    @IBOutlet var scrollView: UIScrollView!
    
    var imageNum = ""
    @IBOutlet var btnImgONE: UIButton!
    @IBOutlet var btnImgFour: UIButton!
    @IBOutlet var btnImgThree: UIButton!
    @IBOutlet var btnImgTwo: UIButton!
    
    @IBOutlet var txtProjName: FormTextField!
    @IBOutlet var txtDate: FormTextField!
    @IBOutlet var txtCost: FormTextField!
    @IBOutlet var txtTitle: FormTextField!
    @IBOutlet var txtDescription: UITextView!
    
    @IBOutlet var btnDate: UIButton!
    
    var arry_ChangeItems = NSMutableArray()
    
    var imagePicker = UIImagePickerController()
    
    var imgStatus1 = ImageFlag.notSelected
    var imgStatus2 = ImageFlag.notSelected
    var imgStatus3 = ImageFlag.notSelected
    var imgStatus4 = ImageFlag.notSelected
    
    var img1 = UIImage()
    var img2 = UIImage()
    var img3 = UIImage()
    var img4 = UIImage()
    
    var projTitle = ""
    
    var ser_proName = ""
    var ser_date = ""
    var ser_cost = ""
    var ser_title = ""
    var ser_description = ""
    var ser_img1 = UIImage()
    var ser_img2 = UIImage()
    var ser_img3 = UIImage()
    var ser_img4 = UIImage()
    
    var ser_img1Status = ""
    var ser_img2Status = ""
    var ser_img3Status = ""
    var ser_img4Status = ""
    
    var ser_TotalImages = ""
    
    var ser_projID = ""
    
    let myGroup = DispatchGroup()
    let imagedispGroup = DispatchGroup()
    var isLastIndex = ""
    var isSuccessUpload = ""
    
    var changeOrder_id = ""
    var userTemplateID = ""
    var usercompLogoUrl = ""
    
    @IBOutlet var btnNextItem: UIButton!
    @IBOutlet var btnpPeviousItem: UIButton!
    var modeAdditionalItem: AdditionalItemsModes = .Normal
    
    var checkFromStatus = ""
    
    var orderItems = NSArray()
    var ser_odrItemId = ""
    
    var counterUploaded = 0
    
    var off_ProjectDic = NSDictionary()
    var off_image1Name  = ""
    var off_image2Name  = ""
    var off_image3Name  = ""
    var off_image4Name  = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let temp = UserDefaults.standard.object(forKey: "TEMPLATE"){
            userTemplateID = temp as? String ?? ""
            AgLog.debug("Slected Template is \(userTemplateID)")
        }
        
        usercompLogoUrl = UserDetails.logoURL()
        
        AgLog.debug("project id  \(selected_projID)")
        arry_allOrderID = NSMutableArray()
        chng_ordID = ""
        
        //need +1 because it was 0 in starting position
        self.lblLineItems.text = "\("LINE_ITEM".localized) #\(current_lineItemNumber + 1)"
        
        self.txtProjName.text = projName
        
        self.btnDate.setTitle(Date().toFormate(format: .MMddyyyy), for: .normal)
        
        if current_lineItemNumber == 0{
            setImagOneForLineItemOne()
        }
        
        self.nextPrevousButtonHideShow()
        
        offline_IfanyItemSetinUniArray()
        
        txtTitle.returnKeyType = .next
        txtTitle.delegate = self
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "backWhiteAer"), style: .plain, target: self, action: #selector(backButtonAction(_:)))
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtTitle{
            textField.resignFirstResponder()
            txtDescription.perform(#selector(UITextView.becomeFirstResponder), with: nil, afterDelay: 0)
        }
        return true
    }
    
    func offline_IfanyItemSetinUniArray(){
        
        if arry_ChangeItems.count > 0 {
            
            modeAdditionalItem = .PreNext
            AgLog.debug("item line number next \(current_lineItemNumber)")
            
            if let dic = arry_ChangeItems.obj(current_lineItemNumber) as? [String:Any] {
                //need +1 already plus into main value
                self.lblLineItems.text = "\("LINE_ITEM".localized) #\(current_lineItemNumber + 1 )"
                self.setOffLineData(withDic: dic)
            }
        }
    }
    
    func nextPrevousButtonHideShow(){
        self.btnpPeviousItem.isHidden = current_lineItemNumber == 0
        if current_lineItemNumber == arry_ChangeItems.count - 1 {
            self.btnNextItem.isHidden = true
        }
        else{
            if current_lineItemNumber == arry_ChangeItems.count{
                self.btnNextItem.isHidden = true
            }
            else{
                self.btnNextItem.isHidden = false
            }
        }
    }
    
    func setImagOneForLineItemOne(){
        
        self.imgStatus1 = ImageFlag.selected
        self.btnImgONE.setBackgroundImage(imgFirst, for: .normal)
        self.img1 = imgFirst.resizeImage(newWidth: 320)
        
        if uni_isWorking_Offline == true{
            let offlineName = String(Date().ticks)
            self.off_image1Name = "off_\(offlineName).jpg"
            DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.img1, imageName: self.off_image1Name)
            
            self.findSelectedProjectDetail()
        }
    }
    
    func findSelectedProjectDetail(){
        for pDic in uni_Array_allProjects.dict {
            if pDic.string(forKey: "id") == selected_projID{
                AgLog.debug("Selected Project Details")
                AgLog.debug(pDic)
                off_ProjectDic = pDic
            }
        }
    }
    
    @IBAction func btnDateClicked(_ sender: Any) {
        DatePickerDialog().show("Select Date", doneButtonTitle: "Select", cancelButtonTitle: "Cancel", datePickerMode: .date) { (date) -> Void in
            
            if let d = date {
                self.btnDate.setTitle(d.toFormate(format: .MMddyyyy),for: .normal)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.title = "CHANGE_ORDER".localized
        
        AgLog.debug("view Will appear call \(current_lineItemNumber) and Mode is \(modeAdditionalItem)")
        
        if backFromItem == "PREVIEW" {
            
            AgLog.debug("Backed From Preveiw chane required mode")
            modeAdditionalItem = .PreNext
            backFromItem = "NOONE"
            newItemsAddedStatus = false
        }
    }
    
    @objc func backButtonAction(_ sender: UIBarButtonItem) {

        let alertCon = UIAlertController(title: "", message: "Your Change Order has not been saved", preferredStyle: .alert)
        let okay = UIAlertAction(title: "Save", style: .default) { _ in
            AgLog.debug("save to doc directory")
            if self.checkAllfiledVaildation() {
                self.checkFromStatus = "DoneItem"
                self.offline_SaveChangeOrder_ItemOnDone(isSaveAction: true)
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        
        let cancel = UIAlertAction(title: "Delete", style: .cancel) { _ in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        alertCon.addAction(cancel)
        alertCon.addAction(okay)
        AppDelegate.topViewController()?.present(alertCon, animated: true, completion: nil)
    }
    
    //MARK:  <<< Previous Item  and  Next Item >>> button action
    @IBAction func btnPreviousItemClicked(_ sender: Any) {
 
        if newItemsAddedStatus{
            newItemsAddedStatus = false
            if uni_isWorking_Offline == true{
                //silently check and add offline if all fields is filled
                AgLog.debug("silently check This data and add if it is full item")
                self.offline_checkTextForPreviousForAdditionalItem()
            }
            else{
                self.checkFieldsforOnlinesilentlyAdd()
            }
        }
        else{
            if checkAllfiledVaildation() {
                if uni_isWorking_Offline == true{
                    showItem_Offline(isNext: false)
                }
                else{
                    showItemAndReplaceWithServer(isNext: false)
                }
            }
        }
    }
    
    @IBAction func btnDeleteItemClicked(_ sender: UIButton) {
        
        if arry_ChangeItems.count == 0 { return }
        
        if arry_ChangeItems.count == current_lineItemNumber {
            current_lineItemNumber = arry_ChangeItems.count - 1
        }
        else{
            arry_ChangeItems.removeObject(at: current_lineItemNumber)
        }
        
        if arry_ChangeItems.count == 0 {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        modeAdditionalItem = .PreNext
        AgLog.debug("item line number next \(current_lineItemNumber)")
        
        if(current_lineItemNumber == arry_ChangeItems.count) {
            current_lineItemNumber = arry_ChangeItems.count - 1
        }
        
        if let dicNext = arry_ChangeItems.obj(current_lineItemNumber) as? [String:Any] {
            
            let lineText = "LINE_ITEM".localized
            self.lblLineItems.text = "\(lineText) #\(current_lineItemNumber + 1)"
            
            self.resetImageData()
            
            self.setOffLineData(withDic: dicNext)
            self.scrollView.setContentOffset(CGPoint(x: 0,y: -50 ),animated: true)
        }
    }
    
    func offline_checkTextForPreviousForAdditionalItem(){
        
        if self.txtProjName.isNull || self.txtTitle.isNull || self.txtDescription.isNull  || ( imgStatus1 == ImageFlag.notSelected && imgStatus2 == ImageFlag.notSelected && imgStatus3 == ImageFlag.notSelected && imgStatus4 == ImageFlag.notSelected ) {
            
            AgLog.debug("show previous item without save")
            showSlilentlyPreviousItemForPreviousButton()
        }
        else{
            AgLog.debug("show previous item After  save This added data")
            //Add This  item to our array
            offline_SaveChangeOrder_ItemOnDone()
            
            //Show Previous Item Now
            showSlilentlyPreviousItemForPreviousButton()
        }
    }
    
    func showSlilentlyPreviousItemForPreviousButton(){
        
        modeAdditionalItem = .PreNext
        
        AgLog.debug("item line number on previous \(current_lineItemNumber)")
        current_lineItemNumber = current_lineItemNumber - 1
        
        if let dictPreVious = arry_ChangeItems.obj(current_lineItemNumber) as? [String: Any] {
            //  need +1 it already substracted before text setup
            self.lblLineItems.text = "\("LINE_ITEM".localized) #\(current_lineItemNumber + 1)"
            self.setOffLineData(withDic: dictPreVious)
        }
    }
    
    //MARK: ********* Internal Helpers ********
    func checkAllfiledVaildation() -> Bool{
        if self.txtProjName.isNull {
            Helper.AgAlertView("Enter project name")
        }
        else if self.txtTitle.isNull {
            Helper.AgAlertView("Enter title")
        }
        else if self.txtDescription.isNull {
            Helper.AgAlertView("Enter description")
        }
        else if imgStatus1 == ImageFlag.notSelected && imgStatus2 == ImageFlag.notSelected && imgStatus3 == ImageFlag.notSelected && imgStatus4 == ImageFlag.notSelected {
            Helper.AgAlertView("Select order image")
        }
        else{
            return true
        }
        return false
    }
    
    func resetImageData() {
        let nilImage = UIImage(named: "ext.png")
        self.btnImgONE.setBackgroundImage(nilImage, for: .normal)
        self.btnImgTwo.setBackgroundImage(nilImage, for: .normal)
        self.btnImgThree.setBackgroundImage(nilImage, for: .normal)
        self.btnImgFour.setBackgroundImage(nilImage, for: .normal)
        imgStatus1 = ImageFlag.notSelected
        imgStatus2 = ImageFlag.notSelected
        imgStatus3 = ImageFlag.notSelected
        imgStatus4 = ImageFlag.notSelected
        
        img1 = UIImage()
        img2 = UIImage()
        img3 = UIImage()
        img4 = UIImage()
    }
    
    func showCurrentItemsData(withIndex: Int) {
        if let dict =  arry_ChangeItems.obj(withIndex) as? [String: Any] {

            self.lblLineItems.text = "\("LINE_ITEM".localized) #\(withIndex + 1)"
            
            AgLog.debug("Previous seted item is \(dict)")
            self.txtCost.text = dict["cost"] as? String
            self.txtTitle.text = dict["title"] as? String
            self.txtDescription.text = dict["description"] as? String
            
            self.setAllImagesFromDict(withDict: dict)
        }
    }
    
    func setAllImagesFromDict(withDict dict: [String: Any]) {
        let arrayImgs = dict["all_images"] as? [Any] ?? []
        AgLog.debug("imagesArray \(arrayImgs)")
        
        let nilImage = UIImage(named: "ext.png")
        
        if var dicimage = arrayImgs.first as? [String:Any] {
            
            img1 = UIImage()
            if let img = dicimage["Image1"] as? UIImage{
                self.btnImgONE.setBackgroundImage(img, for: .normal)
                imgStatus1 = ImageFlag.selected
                self.img1 = img
            }
            else{
                self.btnImgONE.setBackgroundImage(nilImage, for: .normal)
                imgStatus1 = ImageFlag.notSelected
            }
            
            img2 = UIImage()
            if let img = dicimage["Image2"] as? UIImage{
                imgStatus2 = ImageFlag.selected
                self.btnImgTwo.setBackgroundImage(img, for: .normal)
                self.img2 = img
            }
            else{
                imgStatus2 = ImageFlag.notSelected
                self.btnImgTwo.setBackgroundImage(nilImage, for: .normal)
            }
            
            img3 = UIImage()
            if let img = dicimage["Image3"] as? UIImage{
                self.btnImgThree.setBackgroundImage(img, for: .normal)
                imgStatus3 = ImageFlag.selected
                self.img3 = img
            }
            else{
                imgStatus3 = ImageFlag.notSelected
                self.btnImgThree.setBackgroundImage(nilImage, for: .normal)
            }
            
            img4 = UIImage()
            if let img = dicimage["Image4"] as? UIImage{
                self.btnImgFour.setBackgroundImage(img, for: .normal)
                self.img4 = img
                imgStatus4 = ImageFlag.selected
            }
            else{
                imgStatus4 = ImageFlag.notSelected
                self.btnImgFour.setBackgroundImage(nilImage, for: .normal)
            }
        }
    }
    
    func currentItemsBasicDictData() -> [String: Any]{
        var dicItem: [String: Any] = [:]
        
        dicItem["projName"] = self.txtProjName.text
        dicItem["Date"] = self.btnDate.titleLabel?.text
        
        if self.txtCost.text!.count == 0{
            dicItem["cost"] = "0"
            isCostSet = false
        }
        else{
            isCostSet = true
            dicItem["cost"] = self.txtCost.text ?? "0"
        }
        
        dicItem["title"] = self.txtTitle.text ?? ""
        dicItem["description"] = self.txtDescription.text ?? ""
        return dicItem
    }
    
    func getOffLineImageNameDictSorted(isLiveSet: Bool = false) -> [String:Any]{
        var dicimage = [String:Any]()
        
        if imgStatus1 == ImageFlag.selected{
            dicimage["Image1"] = isLiveSet ? img1 : off_image1Name
        }
        else{
            dicimage["Image1"] = ImageFlag.notSelected
        }
        
        if imgStatus2 == ImageFlag.selected{
            dicimage["Image2"] = isLiveSet ? img2 : off_image2Name
        }
        else{
            dicimage["Image2"] = ImageFlag.notSelected
        }
        
        if imgStatus3 == ImageFlag.selected{
            dicimage["Image3"] = isLiveSet ? img3 : off_image3Name
        }
        else{
            dicimage["Image3"] = ImageFlag.notSelected
        }
        
        if imgStatus4 == ImageFlag.selected{
            dicimage["Image4"] = isLiveSet ? img4 : off_image4Name
        }
        else{
            dicimage["Image4"] = ImageFlag.notSelected
        }
        
        return dicimage
    }
    
    func checkFieldsforOnlinesilentlyAdd(){
        
        if self.txtProjName.isNull || self.txtTitle.isNull || self.txtDescription.isNull  || ( imgStatus1 == ImageFlag.notSelected && imgStatus2 == ImageFlag.notSelected && imgStatus3 == ImageFlag.notSelected && imgStatus4 == ImageFlag.notSelected ) {
            
            //Show Previous Item Without add new Item
            AgLog.debug("show previous item Online without save")
            showPreviousNextItem(isNext: false)
        }
        else{
            AgLog.debug("show previous item Onlie after Save")
            //Add This  item to our array online
            self.addItemToUniarray()
            
            //Show Previous Item Now online
            
            showPreviousNextItem(isNext: false)
        }
    }

    func showPreviousNextItem(isNext: Bool){
        
        modeAdditionalItem = .PreNext
        AgLog.debug("item line number on previous \(current_lineItemNumber)")
        if isNext {
            current_lineItemNumber = current_lineItemNumber - 1
        }
        else{
            current_lineItemNumber = current_lineItemNumber + 1
        }
        self.showCurrentItemsData(withIndex: current_lineItemNumber)
        self.nextPrevousButtonHideShow()
    }
    
    func showItemAndReplaceWithServer(isNext: Bool){
        self.replaceCurrentItemOnline()
        showPreviousNextItem(isNext: isNext)
    }
    
    func replaceCurrentItemOnline(){
        
        guard let dic = arry_ChangeItems.obj(current_lineItemNumber) as? [String:Any] else {
            return
        }
        
        let index = arry_ChangeItems.index(of: dic)
        
        var dicItem: [String: Any] = self.currentItemsBasicDictData()

        var dicimage = [String:Any]()
        dicimage["Image1"] = imgStatus1 == ImageFlag.selected ? img1 : ImageFlag.notSelected
        dicimage["Image2"] = imgStatus2 == ImageFlag.selected ? img2 : ImageFlag.notSelected
        dicimage["Image3"] = imgStatus3 == ImageFlag.selected ? img3 : ImageFlag.notSelected
        dicimage["Image4"] = imgStatus4 == ImageFlag.selected ? img4 : ImageFlag.notSelected
    
        dicItem["all_images"] = [dicimage]
        arry_ChangeItems.replaceObject(at: index, with: dicItem)
    }
    
    @IBAction func btnNextItemCLicked(_ sender: Any) {
        if checkAllfiledVaildation() {
            
            if uni_isWorking_Offline == true{
                showItem_Offline(isNext: true)
            }
            else{
                showItemAndReplaceWithServer(isNext: true)
            }
        }
    }
    
    func resetAllData(){
        
        modeAdditionalItem = .Normal
        
        self.txtCost.text = ""
        self.txtTitle.text = ""
        self.txtDescription.text = ""
        
        let lineText = "LINE_ITEM".localized
        
        current_lineItemNumber = current_lineItemNumber + 1
        self.lblLineItems.text = "\(lineText) #\(current_lineItemNumber + 1)"
        
        AgLog.debug("Scroll top\( -self.scrollView.contentInset.top)")
        self.scrollView.setContentOffset(CGPoint(x: 0,y: -50 ),animated: true)
        
        self.resetImageData()
        
        self.btnpPeviousItem.isHidden = current_lineItemNumber == 0
        self.btnNextItem.isHidden = true
    }
    
    
    @IBAction func btnPhotoONEClicked(_ sender: UIButton) {
        let camera = CameraViewController.getInstant()
        camera.delegate = self
        camera.sourceView = sender
        self.present(camera, animated: true, completion: nil)
        imageNum = "ONE"
    }
    
    @IBAction func btnPhotoTwoClicked(_ sender: UIButton) {
        
        let camera = CameraViewController.getInstant()
        camera.delegate = self
        camera.sourceView = sender
        self.present(camera, animated: true, completion: nil)
        imageNum = "TWO"
    }
    
    @IBAction func btnPhotoThreeClicked(_ sender: UIButton) {
        let camera = CameraViewController.getInstant()
        camera.delegate = self
        camera.sourceView = sender
        self.present(camera, animated: true, completion: nil)
        
        imageNum = "THREE"
    }
    
    @IBAction func btnphotoFourClicked(_ sender: UIButton) {
        let camera = CameraViewController.getInstant()
        camera.delegate = self
        camera.sourceView = sender
        self.present(camera, animated: true, completion: nil)
        
        imageNum = "FOUR"
    }
    
    //MARK: additional item clicked
    @IBAction func btnAddaditionalCliked(_ sender: Any) {
        newItemsAddedStatus = true
        checkFromStatus = "aditionalItem"
        chekTextFieldsForAdditianalItems()
    }
    
    func chekTextFieldsForAdditianalItems(){
        
        if checkAllfiledVaildation() {
            
            if uni_isWorking_Offline == true {
                AgLog.debug("add item in plist for off line save")
                if modeAdditionalItem == .PreNext{
                    self.offline_EditChangeOrderitem()
                }
                else{
                    self.offline_SaveChangeOrder_ItemOnDone()
                }
            }
            else {
                if modeAdditionalItem == .PreNext{
                    self.editItemtoUniArraywithChanges()
                }
                else{
                    self.gotoAddnewLineItemCreate()
                }
            }
        }
    }
    
    //MARK: Done button Clicked
    @IBAction func btnDoneCLicked(_ sender: Any) {
        checkFromStatus = "DoneItem"
        
        if checkAllfiledVaildation() {
            if uni_isWorking_Offline == true {
                AgLog.debug("save to doc directory")
                self.offline_SaveChangeOrder_ItemOnDone()
            }
            else{
                addItemToUniarray()
            }
        }
    }
    
    //MARK: Save Order Item IN PLIST on DONE
    func offline_SaveChangeOrder_ItemOnDone(isSaveAction: Bool = false){
        AgLog.debug("My Mode is \(modeAdditionalItem)")
 
        var dicItem: [String: Any] = self.currentItemsBasicDictData()
        dicItem["order_item_id"] = Date.genrateOfflineId
        dicItem["is_uploaded"] = "false"
        dicItem["template_id"] = self.userTemplateID
        dicItem["logo_url"] = usercompLogoUrl
        dicItem["all_images"] = [getOffLineImageNameDictSorted()]
        
        if modeAdditionalItem == .Normal {
            AgLog.debug("addesd new")
            arry_ChangeItems.add(dicItem)
        }
        else{
            if let dic = arry_ChangeItems.obj(current_lineItemNumber) as? [String:Any] {
                let index = arry_ChangeItems.index(of: dic)
                arry_ChangeItems.replaceObject(at: index, with: dicItem)
            }
        }
        
        if checkFromStatus == "DoneItem"{
            AgLog.debug("save all items to off line with required value")
        
            let cliName = self.off_ProjectDic.string(forKey: "client_name")
            let a1 = self.off_ProjectDic.string(forKey: "address")
            let city = self.off_ProjectDic.string(forKey: "city")
            let state = self.off_ProjectDic.string(forKey: "state")
            let zip = self.off_ProjectDic.string(forKey: "zip")
            
            let addres = [a1, city, zip, state].joined(separator: ", ")
            
            let jobNm = self.off_ProjectDic.string(forKey: "job_number")
            
            let dictParams:  [String: Any] = [
                "assigner_id" : "0",
                "authority_email" : "",
                "authority_name" : "",
                "created_at" : Date.getCurrentDate(),
                "change_order_id" : Date.genrateOfflineId,
                "offline_change_order_id" : Date.genrateOfflineId,
                "job_name" : projName,
                "project_id" : selected_projID,
                "status" : Constants.STATUS_SAVED,
                "change_order_items" : arry_ChangeItems,
                "client_name" : cliName,
                "address" : addres,
                "job_number" : jobNm,
                "is_uploaded" : "false"
            ]
            
            if isSaveAction {
                uni_array_allChangeOrders.add(NSMutableDictionary(dictionary: dictParams))
                DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
                uni_array_allChangeOrders = NSMutableArray()
                resetAllDeclaredValue()
                isAnyNewOrderOrChanges = true
                return
            }
            else{
                let ordrPrevu = self.storyboard?.instantiateViewController(withIdentifier: "changePreview") as! ChangePreViewController
                
                let dicEd = off_ProjectDic.mutableCopy() as! NSMutableDictionary
                
                dicEd.setValue(arry_ChangeItems, forKey: "change_order_items")
                dicEd.setValue(selected_projID, forKey: "project_id")
                dicEd.setValue(Date.genrateOfflineId, forKey: "change_order_id")
                dicEd.setValue(projName, forKey: "job_name")
                dicEd.setValue("false", forKey: "is_uploaded")
                dicEd.setValue("0", forKey: "assigner_id")
                dicEd.setValue("", forKey: "authority_email")
                dicEd.setValue("", forKey: "authority_name")
                dicEd.setValue(Date.getCurrentDate(), forKey: "created_at")
                dicEd.setValue(Constants.STATUS_SAVED, forKey: "status")
                
                ordrPrevu.dicallDetail = dicEd
                self.navigationController?.pushViewController(ordrPrevu, animated: true)
            }
        }
        else{
            AgLog.debug("off Add This item and reset scroll")
            self.resetAllData()
            self.cleareallforoffline()
        }
    }
    
    func cleareallforoffline(){
        
        off_image1Name  = ""
        off_image2Name  = ""
        off_image3Name  = ""
        off_image4Name  = ""
        
        if Environment.isSimulator{
            
            self.imgStatus2 = ImageFlag.selected
            if let imgFF = UIImage(named:"celling_1.jpg") {
            
                self.img2 = imgFF.resizeImage(newWidth: 320)
                self.btnImgTwo.setBackgroundImage(img2, for: .normal)
                
                let offline2Name = String(Date().ticks)
                self.off_image2Name = "off_\(offline2Name).jpg"
                DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.img2, imageName: self.off_image2Name)
                
                self.img1 = imgFF.resizeImage(newWidth: 320)
                self.btnImgONE.setBackgroundImage(img1, for: .normal)
                
                let offline1Name = String(Date().ticks)
                self.off_image1Name = "off_\(offline1Name).jpg"
                DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.img1, imageName: self.off_image1Name)
                
                self.img3 = imgFF.resizeImage(newWidth: 320)
                self.btnImgThree.setBackgroundImage(img3, for: .normal)
                
                let offline3Name = String(Date().ticks)
                self.off_image3Name = "off_\(offline3Name).jpg"
                DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.img3, imageName: self.off_image3Name)
                
                self.txtCost.text = "100"
                self.txtTitle.text = "Celling Lights"
                self.txtDescription.text = "Fitting of all celling lights"
            }
        }
    }
    
    func showItem_Offline(isNext: Bool){
        
        guard let dic = arry_ChangeItems.obj(current_lineItemNumber) as? [String:Any] else{
            return
        }
        
        let index = arry_ChangeItems.index(of: dic)
        
        var dicItem: [String: Any] = currentItemsBasicDictData()
        dicItem["order_item_id"] = dic["order_item_id"]
        dicItem["is_uploaded"] = "false"
        dicItem["template_id"] = self.userTemplateID
        dicItem["logo_url"] = usercompLogoUrl
        dicItem["all_images"] = [getOffLineImageNameDictSorted()]
        arry_ChangeItems.replaceObject(at: index, with: dicItem)
        
        modeAdditionalItem = .PreNext
        AgLog.debug("item line number next \(current_lineItemNumber)")
        if isNext {
            current_lineItemNumber = current_lineItemNumber + 1
        }
        else{
            current_lineItemNumber = current_lineItemNumber - 1
        }
        
        if let dicNext =  arry_ChangeItems.obj(current_lineItemNumber) as? [String: Any] {
            let lineText = "LINE_ITEM".localized
            self.lblLineItems.text = "\(lineText) #\(current_lineItemNumber + 1)"
            self.setOffLineData(withDic: dicNext)
        }
        else{
            AgLog.debug("\(current_lineItemNumber) Not founded in array")
        }
    }
    
    func offline_EditChangeOrderitem(){
        
        guard let dict = arry_ChangeItems.obj(current_lineItemNumber) as? [String: Any] else {
            return
        }
        
        let index = arry_ChangeItems.index(of: dict)
        
        var dicItem: [String: Any] = currentItemsBasicDictData()
        dicItem["order_item_id"] = dict["order_item_id"]
        dicItem["is_uploaded"] = "false"
        dicItem["template_id"] = self.userTemplateID
        dicItem["logo_url"] = usercompLogoUrl
        dicItem["all_images"] = [getOffLineImageNameDictSorted()]
        
        arry_ChangeItems.replaceObject(at: index, with: dicItem)
        
        total_ItemsAdded = arry_ChangeItems.count
        current_lineItemNumber = total_ItemsAdded - 1
        
        self.resetAllData()
        self.cleareallforoffline()
    }
    
    func setOffLineData(withDic: [String:Any]){
        
        var dic = withDic
        
        AgLog.debug("item Offline \(dic)")
        self.txtCost.text = dic["cost"] as? String
        self.txtTitle.text = dic["title"] as? String
        self.txtDescription.text = dic["description"] as? String
        
        var arrayImgs = dic["all_images"] as! [Any]
        
        let dicimage = arrayImgs[0] as! [String:Any]
        
        img1 = UIImage()
        img2 = UIImage()
        img3 = UIImage()
        img4 = UIImage()
        
        let nilImage = UIImage()
        
        if let stImg1 = dicimage["Image1"] as? String{
            
            if stImg1 == ImageFlag.notSelected {
                
                imgStatus1 = ImageFlag.notSelected
                AgLog.debug("status of image one \(stImg1)")
                self.btnImgONE.setBackgroundImage(nilImage, for: .normal)
                off_image1Name = ImageFlag.notSelected
                
            }else{
                
                let img = DataController.sharedInstance.getImageFromDirFolder(withName: stImg1 )
                self.btnImgONE.setBackgroundImage(img, for: .normal)
                imgStatus1 = ImageFlag.selected
                self.img1 = img
                off_image1Name = stImg1
            }
        }
        
        if let stImg2 = dicimage["Image2"] as? String{
            
            if stImg2 == ImageFlag.notSelected {
                
                imgStatus2 = ImageFlag.notSelected
                AgLog.debug("status of image Two \(stImg2)")
                self.btnImgTwo.setBackgroundImage(nilImage, for: .normal)
                
                off_image2Name = ImageFlag.notSelected
            }else{
                
                let img = DataController.sharedInstance.getImageFromDirFolder(withName: stImg2 )
                self.btnImgTwo.setBackgroundImage(img, for: .normal)
                imgStatus2 = ImageFlag.selected
                self.img2 = img
                off_image2Name = stImg2
                
            }
        }
        
        if let stImg3 = dicimage["Image3"] as? String{
            
            if stImg3 == ImageFlag.notSelected {
                
                imgStatus3 = ImageFlag.notSelected
                AgLog.debug("status of image Three \(stImg3)")
                self.btnImgThree.setBackgroundImage(nilImage, for: .normal)
                off_image3Name = ImageFlag.notSelected
                
            }else{
                
                let img = DataController.sharedInstance.getImageFromDirFolder(withName: stImg3 )
                self.btnImgThree.setBackgroundImage(img, for: .normal)
                imgStatus3 = ImageFlag.selected
                self.img3 = img
                
                off_image3Name = stImg3
            }
        }
        
        if let stImg4 = dicimage["Image4"] as? String{
            
            if stImg4 == ImageFlag.notSelected {
                
                imgStatus4 = ImageFlag.notSelected
                AgLog.debug("status of image four \(stImg4)")
                self.btnImgFour.setBackgroundImage(nilImage, for: .normal)
                off_image4Name = ImageFlag.notSelected
                
            }else{
                
                let img = DataController.sharedInstance.getImageFromDirFolder(withName: stImg4 )
                self.btnImgFour.setBackgroundImage(img, for: .normal)
                imgStatus4 = ImageFlag.selected
                self.img4 = img
                off_image4Name = stImg4
            }
        }
        self.nextPrevousButtonHideShow()
    }
    
    //MARK: Edit Order Item
    func editItemtoUniArraywithChanges(){
        
        if let dict =  arry_ChangeItems.obj(current_lineItemNumber) as? [String: Any] {
            let index = arry_ChangeItems.index(of: dict)
            
            var dicItem: [String: Any] = currentItemsBasicDictData()
            let dicimage: [String: Any] = getOffLineImageNameDictSorted(isLiveSet: true)
            dicItem["all_images"] = [dicimage]
            
            arry_ChangeItems.replaceObject(at: index, with: dicItem)
            total_ItemsAdded = arry_ChangeItems.count
            current_lineItemNumber = total_ItemsAdded - 1
            
            self.resetAllData()
        }
    }
    
    func addItemToUniarray(){
        
        AgLog.debug("My Mode is \(modeAdditionalItem.rawValue)")
        
        var index = 0
        
        if arry_ChangeItems.count != 0{
            if modeAdditionalItem != .Normal {
                if let dict = arry_ChangeItems.obj(current_lineItemNumber) as? [String: Any] {
                    index = arry_ChangeItems.index(of: dict)
                }
            }
        }
        
        var dicItem: [String: Any] = currentItemsBasicDictData()
        let dicimage: [String: Any] = getOffLineImageNameDictSorted(isLiveSet: true)
        dicItem["all_images"] = [dicimage]
        
        if modeAdditionalItem == .Normal {
            AgLog.debug("addesd new")
            arry_ChangeItems.add(dicItem)
        }
        else{
            arry_ChangeItems.replaceObject(at: index, with: dicItem)
        }
        
        AgLog.debug("ChangedItemsArray: \(arry_ChangeItems)")
        if checkFromStatus == "DoneItem"{
            self.createQueforItemsTextPart()
        }
    }
    
    func gotoAddnewLineItemCreate(){
        
        let anotherQueue = DispatchQueue(label: "com.mobisharnamm.ChangeOrderPro", qos: .utility)
        
        anotherQueue.sync {
            AgLog.debug("check Fields")
            self.addItemToUniarray()
        }
        
        anotherQueue.sync {
            for i in 1..<50{
                print("😡",i)
            }
        }
        
        anotherQueue.sync {
            total_ItemsAdded = arry_ChangeItems.count
            self.resetAllData()
        }
    }
    
    func createQueforItemsTextPart(){
        
        let anotherQueue = DispatchQueue(label: "com.mobisharnamm.ChangeOrderPro", qos: .utility)
        
        anotherQueue.sync {
            for i in 1..<40{
                print("😡",i)
            }
        }
        
        anotherQueue.sync {
            
            finalItemsarray = NSMutableArray()
            
            for dicItem in arry_ChangeItems as? [[String: Any]] ?? []{
                
                myGroup.enter()
                
                ser_projID =  selected_projID
                ser_proName = dicItem["projName"] as? String ?? ""
                ser_date =  dicItem["Date"] as? String ?? ""
                ser_cost = dicItem["cost"] as? String ?? ""
                ser_title = dicItem["title"] as? String ?? ""
                ser_description = dicItem["description"] as? String ?? ""
                
                let parameters: [String: String] = [
                    "cost" : ser_cost,
                    "title" : ser_title,
                    "description" : ser_description
                ]
                
                finalItemsarray.add(parameters)
                
                let index = arry_ChangeItems.index(of: dicItem)
                if index == arry_ChangeItems.count - 1{
                    AgLog.debug("There s go last index  Text !!")
                    self.postDataIntoArrayofItemsTextPart()
                }
            }
        }
        
        myGroup.notify(queue: .main) {
            AgLog.debug("Finished textpart start Image Part \(self.orderItems) and change Order id \(self.changeOrder_id)")
        }
    }
    
    //MARK: Order item Textpart API
    
    func postDataIntoArrayofItemsTextPart() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            AgLog.debug("Internet Connection Available!")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "addchangeorder"
            
            let dictParams:  [String: Any] = [
                "user_id" : UserDetails.loginUserId,
                "project_id" : selected_projID ,
                "change_order_items" : finalItemsarray.toJson ?? "",
                ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                
                if let dicServer = response.result.value as? NSDictionary{
                    
                    let status = dicServer.string(forKey: "status")
                    if status == "success" {
                        
                        let ordId = dicServer.string(forKey: "change_order_id")
                        self.changeOrder_id = ordId
                        
                        self.orderItems = dicServer.object(forKey: "order_item_ids") as! NSArray
                        
                        self.createQueforItemsImagePart()
                        
                    }
                    self.myGroup.leave()
                }
                
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    //MARK: Order item ImagePart Queue
    func createQueforItemsImagePart(){
        
        let anotherQueue = DispatchQueue(label: "com.mobisharnamm.ChangeOrderPro", qos: .utility)
        
        anotherQueue.sync {
            for i in 1..<40{
                print("Image",i)
            }
        }
        
        anotherQueue.sync {
            fakearray = NSMutableArray()
            self.takeValueandupload()
        }
        imagedispGroup.notify(queue: .main) {
            AgLog.debug("Finished Image part also")
        }
    }
    
    func takeValueandupload(){
        
        //for dic in arry_ChangeItems {
        
        imagedispGroup.enter()
        
        guard let dicItem = arry_ChangeItems.obj(counterUploaded) as? [String:Any] else {
            return
        }
        
        let curindex = arry_ChangeItems.index(of: dicItem)
        
        ser_odrItemId = "\(self.orderItems[curindex])"
        ser_projID =  selected_projID
        
        var arrayImgs = dicItem["all_images"] as! [Any]
        
        var dicimage = arrayImgs[0] as! [String:Any]
        
        if let stImg1 = dicimage["Image1"] as? String{
            AgLog.debug("status of image One \(stImg1)")
            ser_img1Status = ImageFlag.notSelected
            
        }else{
            
            ser_img1 = dicimage["Image1"] as! UIImage
            ser_img1Status = ImageFlag.selected
            AgLog.debug("image ONe \(ser_img1)")
        }
        
        if let stImg2 = dicimage["Image2"] as? String{
            ser_img2Status = ImageFlag.notSelected
            AgLog.debug("status of image Two \(stImg2)")
            
        }else{
            ser_img2 = dicimage["Image2"] as! UIImage
            ser_img2Status = ImageFlag.selected
        }
        
        if let stImg3 = dicimage["Image3"] as? String{
            ser_img3Status = ImageFlag.notSelected
            AgLog.debug("status of image Three \(stImg3)")
            
        }else{
            
            ser_img3 = dicimage["Image3"] as! UIImage
            ser_img3Status = ImageFlag.selected
        }
        
        if let stImg4 = dicimage["Image4"] as? String{
            
            ser_img4Status = ImageFlag.notSelected
            AgLog.debug("status of image four \(stImg4)")
            
        }else{
            
            ser_img4 = dicimage["Image4"] as! UIImage
            ser_img4Status = ImageFlag.selected
        }
        
        let index = arry_ChangeItems.index(of: dicItem)
        
        if index == arry_ChangeItems.count - 1{
            isLastIndex = "YES"
            AgLog.debug("There s go last index !!")
            
        }else{
            isLastIndex = "NO"
            AgLog.debug("not  last index")
        }
        uploadOrderItemsPicturePart()
    }
    
    //MARK: Order item Image API
    func uploadOrderItemsPicturePart() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            var parameters: [String: String] = [
                "change_order_id": self.changeOrder_id,
                "project_id": ser_projID,
                "order_item_id": ser_odrItemId,
                "is_update": "no" ,
                "template_id": userTemplateID ,
                "logo_url": usercompLogoUrl,
                "user_id": UserDetails.loginUserId
            ]
            
            if isLastIndex == "YES"{
                parameters["is_last"] = "true"
            }
            AgLog.debug("upload image with parameters \(parameters)")
            
            let strURL = kBaseURL + "uploadimages"
            
            let headers1 = ["Authorization": "123456"]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                multipartFormData.append(self.ser_img1, withName: "image1", status: self.ser_img1Status)
                multipartFormData.append(self.ser_img2, withName: "image2", status: self.ser_img2Status)
                multipartFormData.append(self.ser_img3, withName: "image3", status: self.ser_img3Status)
                multipartFormData.append(self.ser_img4, withName: "image4", status: self.ser_img4Status)
                
                for (key, value) in parameters {
                    multipartFormData.append((value.data(using: .utf8))!, withName: key)
                }
                
            }, to: strURL, method: .post, headers: headers1,
               encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { [weak self] response in
                        response.logShow()
                        
                        if let dict = response.result.value as? NSDictionary {
                            
                            let status = dict.string(forKey: "status")
                            if status == "success" {
                                
                                prev_Pdf = dict.string(forKey: "change_order_pdf")
                                AgLog.debug("pdf url is \(prev_Pdf)")
                                
                                let ordId = dict.string(forKey: "change_order_id")
                                self?.changeOrder_id = "\(ordId)"
                                self?.counterUploaded = (self!.counterUploaded) + 1
                                AgLog.debug("counter uploaded \(self!.counterUploaded)")
                                self?.isSuccessUpload = "SUCCESS"
                                
                                self?.fakearray.add(dict)
                                // self?.imagedispGroup.leave()
                                
                                if self?.fakearray.count == self?.arry_ChangeItems.count{
                                    arry_allOrderID.add(self!.changeOrder_id)
                                    chng_ordID = (self?.changeOrder_id)!
                                    AgLog.debug("Last object navigate to main controller or show Preveiw of order")
                                    self?.gotoChangeOrderPreveiw()
                                    
                                }else{
                                    AgLog.debug("continue ......")
                                    self?.imagedispGroup.leave()
                                    self?.takeValueandupload()
                                }
                            }
                            else{
                                Helper.AgAlertView("Error in created order")
                            }
                        }
                    }
                case .failure(let encodingError):
                    AgLog.debug("error:\(encodingError)")
                }
                MBProgressHUD.hide(for: self.view, animated: true)
            })
        }
    }
    
    
    func gotoChangeOrderPreveiw(){
        let ordrPrevu = self.storyboard?.instantiateViewController(withIdentifier: "changePreview") as! ChangePreViewController
        self.navigationController?.pushViewController(ordrPrevu, animated: true)
    }
}

extension ChangeOrderItems: CameraViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func cameraViewControllerDidCancel(_ picker: CameraViewController, isClose: UIView?) {
        if let v = isClose {
            picker.dismiss(animated: true, completion: nil)
            AGImagePickerController(with: self, type: .photoLibrary, allowsEditing: false, iPadSetup: v)
        }
        else{
            picker.dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        
        self.cameraViewController(nil, didFinishPickingMedia: image)
    }

    func cameraViewController(_ picker: CameraViewController?, didFinishPickingMedia image: UIImage) {
        if let p = picker {
            p.dismiss(animated: true, completion: nil)
        }
        
        AgLog.debug("Iamge size Original \(image)") //{2448, 3264}
        let offlineName = String(Date().ticks)
        
        if imageNum == "ONE"{
            let offlineName = String(Date().ticks)
            self.imgStatus1 = ImageFlag.selected
            self.btnImgONE.setBackgroundImage(image, for: .normal)
            self.img1 = image.resizeImage(newWidth: 320)
            
            if uni_isWorking_Offline == true{
                
                self.off_image1Name = "off_\(offlineName).jpg"
                DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.img1, imageName: self.off_image1Name)
                
            }
        }
        else if imageNum == "TWO"{
            let offlineName = String(Date().ticks)
            self.imgStatus2 = ImageFlag.selected
            self.btnImgTwo.setBackgroundImage(image, for: .normal)
            self.img2 = image.resizeImage(newWidth: 320)
            
            
            if uni_isWorking_Offline == true{
                self.off_image2Name = "off_\(offlineName).jpg"
                DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.img2, imageName: self.off_image2Name)
                
            }
        }
        else if imageNum == "THREE"{
            let offlineName = String(Date().ticks)
            self.imgStatus3 = ImageFlag.selected
            self.btnImgThree.setBackgroundImage(image, for: .normal)
            self.img3 = image.resizeImage(newWidth: 320)
            
            if uni_isWorking_Offline == true{
                self.off_image3Name = "off_\(offlineName).jpg"
                DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.img3, imageName: self.off_image3Name)
                
            }
        }
        else if imageNum == "FOUR"{
            self.imgStatus4 = ImageFlag.selected
            self.btnImgFour.setBackgroundImage(image, for: .normal)
            self.img4 = image.resizeImage(newWidth: 320)
            
            if uni_isWorking_Offline == true{
                
                self.off_image4Name = "off_\(offlineName).jpg"
                DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.img4, imageName: self.off_image4Name)
                
            }
        }
    }
}
