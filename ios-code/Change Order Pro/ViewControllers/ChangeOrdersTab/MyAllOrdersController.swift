//
//  MyAllOrdersController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import StoreKit
import AMTooltip

enum UploadStatus: String {
    case IsCreated
    case InProcess
    
    var key: String {
        return self.rawValue
    }
}

var isAnyNewOrderOrChanges = true

var allOrdersUploadScceessFully: Bool = false

class MyAllOrdersController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    static let shared = MyAllOrdersController()
    @IBOutlet var viewmenu: UIView!
    
    var selectedindex = 0
    var chngOrdID = ""
    var topChngOrdID = ""
    
    @IBOutlet var viewCompleted: UIView!
    @IBOutlet var lblTotalCost: UILabel!
    @IBOutlet var tblMyOrders: UITableView!
    
    @IBOutlet var viewinfo: UIView!
    @IBOutlet var viewSort: UIView!
    var backFrom = "NOONE"
    
    @IBOutlet var btnAllProj: UIButton!
    @IBOutlet var btnSort: UIButton!
    
    var statusMenu = ""
    let cellSpacingHeight: CGFloat = 15
    let infoProjDropDown = DropDown()
    
    var arrayAllOrders = NSMutableArray()
    var arrayFinalOrders = NSMutableArray()
    var arrayMainAllOrders  = NSMutableArray()
    var sortingStatus = ""
    
    var offline_totalOrders = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "AllProjCell", bundle: nil)
        tblMyOrders.register(nib, forCellReuseIdentifier: "allProjcell")
        
        self.checkAllProjectUploaded()
        self.calltogetallMyChangeOrders()
        updateUserData { (isStatus) in
            
        }
        self.setNavItems()
        tblMyOrders.agRefreshControl.attributedTitle = NSAttributedString(string: "Syncing..")
        tblMyOrders.pullToRefresh = { ref in
            if allOrdersUploadScceessFully {
                self.tblMyOrders.agRefreshControl.attributedTitle = NSAttributedString(string: "Syncing..")
                self.calltogetallMyChangeOrders()
            }
            else{
                ref.endRefreshing()
                self.checkAllProjectUploaded()
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.appCameToForeground(notification:)), name: .UIApplicationWillEnterForeground, object: nil)
    }
    
    func checkAllProjectUploaded() {
        allOrdersUploadScceessFully = true
        for obj in uni_array_allChangeOrders.dict {
            if !obj.bool(forKey: "is_uploaded") {
                allOrdersUploadScceessFully = false
                isAnyNewOrderOrChanges = true
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        endRefreshView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        // pull to refresh every time
        if allOrdersUploadScceessFully {
            self.tblMyOrders.agRefreshControl.attributedTitle = NSAttributedString(string: "Syncing..")
            self.calltogetallMyChangeOrders()
        }
        else{
            self.tblMyOrders.agRefreshControl.endRefreshing()
            self.checkAllProjectUploaded()
        }
        
        uni_fromEdit = "NO"
        self.lblTotalCost.text = "\(Currnecy.current)0.00"
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.title = "Change Orders"
        
        self.navigationController?.popViewController(animated: false)
        
        AgLog.debug("fro value is \(backFrom)")
        
        if self.btnAllProj.title(for: .normal) == "ALL Projects"{
            self.btnAllProj.setTitle("ALL Projects", for: .normal)
            selected_projID = ""
            self.plist_getAllOffline_MyChangeOrders()
        }
        else{
            var isFounded: Bool = false
            for dict in uni_Array_allProjects as? [NSDictionary] ?? [] {
                if dict.string(forKey: "job_name") == self.btnAllProj.title(for: .normal) {
                    isFounded = true
                    break
                }
            }
            
            if !isFounded {
                selected_projID = ""
                self.plist_getAllOffline_MyChangeOrders()
            }
        }
        
        if self.backFrom == "NOONE"{
            if uni_isWorking_Offline == true{
                self.plist_getAllOffline_MyChangeOrders()
            }
        }
        else{
            self.backFrom = "NOONE"
        }
        AgLog.debug("Any Change in Order \(isAnyNewOrderOrChanges)")
        if isAnyNewOrderOrChanges == true{
            if Reachability.isConnectedToNetwork() == true {
                isAnyNewOrderOrChanges = false
                self.startToUploadMyAllOrdersToserver()
            }
        }
        else {
            self.calltogetallMyChangeOrders()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.tblMyOrders.setEditing(false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        displayAllInfoWindows()
    }
    
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier =  UIBackgroundTaskInvalid
    let backgroundTaskName = "saveOrderData"
    
    @objc func appCameToForeground(notification: NSNotification){
        
        AgLog.debug("Application came to the foreground")
        
        if self.backgroundTaskIdentifier != UIBackgroundTaskInvalid{
            AgLog.debug("We need to invalidate our background task")
            
            self.plist_getAllOffline_MyChangeOrders()
            UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier)
            self.backgroundTaskIdentifier = UIBackgroundTaskInvalid
        }
    }
    
    func endBackgroundTask() {
        AgLog.debug("Background task ended.")
        
        if self.backgroundTaskIdentifier != UIBackgroundTaskInvalid {
            UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier)
            self.backgroundTaskIdentifier = UIBackgroundTaskInvalid
        }
    }
    
    //MARK: offline get all Orders
    func plist_getAllOffline_MyChangeOrders(){
        let arr = DataController.sharedInstance.getMyChangeOrderData()
        if arr.count == 0 {
            self.arrayAllOrders = NSMutableArray()
            self.arrayFinalOrders = NSMutableArray()
            self.arrayMainAllOrders = NSMutableArray()
        }
        else{
            self.arrayAllOrders = NSMutableArray(array: arr)
        }
        
        uni_array_allChangeOrders = self.arrayAllOrders
        self.countTotal()
        self.endRefreshView()
    }
    
    var eyeButton = UIBarButtonItem()
    var isEyeOpen = true
    
    func setNavItems(){
        isEyeOpen = UserDefaults.standard.value(forKey: "EYESTATUSOPEN") as? Bool ?? true
        eyeButton = UIBarButtonItem(image: UIImage(named: "eyeOpen"),  style: .plain, target: self, action: #selector(btnEyeClicked(sender:)))
        eyeButton.image = isEyeOpen ? UIImage(named: "eyeOpen") : UIImage(named: "eyeClose")
        let addNew = UIBarButtonItem( image: UIImage(named: "newAdd.png"), style: .plain, target: self, action: #selector(addNewOrderClicked(sender:)) )
        self.checkTotalProject()
        navigationItem.rightBarButtonItems = [addNew, eyeButton]
    }
    
    func displayAllInfoWindows() {
        
        let newOrder = UserDefaults.standard.value(forKey: "newOrder") as? NSString
        if (newOrder == nil ){
            AMTooltipView(message: "Tap this icon to create a Change Order.",
                          focusView: self.navigationItem.rightBarButtonItems![0].view!, complete: {
                            UserDefaults.standard.set("YES", forKey: "newOrder")
                            self.displayAllInfoWindows()
            })
            return
        }
        
        let eyeInfo = UserDefaults.standard.value(forKey: "eyeInfo") as? NSString
        if (eyeInfo == nil ){
            AMTooltipView(message: "Tap here to hide all completed Change Orders.",
                          focusView: self.navigationItem.rightBarButtonItems![1].view!, complete: {
                            UserDefaults.standard.set("YES", forKey: "eyeInfo")
                            self.displayAllInfoWindows()
            })
            return
        }
        
        let sortInfo = UserDefaults.standard.value(forKey: "sortInfo") as? NSString
        if (sortInfo == nil ){
            AMTooltipView(message: "Tap here to sort Change Orders by status.",
                          focusView: btnSort , complete: {
                            UserDefaults.standard.set("YES", forKey: "sortInfo")
                            self.displayAllInfoWindows()
            })
            return
        }
        
        let dropdownInfo = UserDefaults.standard.value(forKey: "dropdownInfo") as? NSString
        if (dropdownInfo == nil ){
            AMTooltipView(message: "Select a project to create a Change Order.",
                          focusView: self.btnAllProj , complete: {
                            UserDefaults.standard.set("YES", forKey: "dropdownInfo")
                            self.displayAllInfoWindows()
            })
            return
        }
    }
    func btnEyeClicked(sender: UIBarButtonItem) {
        
        if isEyeOpen == true{
            eyeButton.image =  UIImage(named: "eyeClose")
            isEyeOpen = false
            AgLog.debug("hide completed orders")
            countTotal()
            UserDefaults.standard.setValue(false, forKey: "EYESTATUSOPEN")
            
        }
        else{
            isEyeOpen = true
            eyeButton.image = UIImage(named: "eyeOpen")
            AgLog.debug("Show completed Oders")
            countTotal()
            UserDefaults.standard.setValue(true, forKey: "EYESTATUSOPEN")
        }
    }
    
    func checkTotalProject(){
        AgLog.debug("Total",array_myOwnProjects.count)
        
        if array_myOwnProjects.count == 2 {
            let name = array_myOwnProjects[0] as? String ?? ""
            selected_projID = array_myProjectsID[0] as? String ?? ""
            AgLog.debug("project id and name is ",name,selected_projID)
            self.btnAllProj.setTitle(name, for: .normal)
            self.sortorderBystatus()
        }
    }
    
    //MARK: Project DropDown Clicked
    
    @IBAction func btnProjFilterClicked(_ sender: UIButton) {
        
        infoProjDropDown.anchorView = sender
        infoProjDropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        infoProjDropDown.dataSource = array_myOwnProjects as! [String]
        infoProjDropDown.show()
        infoProjDropDown.selectionAction = { [unowned self] (index, item) in
            
            self.btnAllProj.setTitle(item, for: .normal)
            AgLog.debug("Selected project ID \( array_myProjectsID[index])")
            selected_projID = array_myProjectsID[index] as? String ?? ""
            self.callToFilerorderbyID()
        }
    }
    
    func callToFilerorderbyID(){
        if selected_projID == "Dummy"{
            self.countTotal()
        }
        else{
            let totalProj =  arrayMainAllOrders.count
            AgLog.debug("Total \(totalProj)")
            //project_ids
            arrayFinalOrders = NSMutableArray(array: arrayMainAllOrders.dict.filter {
                
                return selected_projID == $0.string(forKey: "project_id")
            })
            
            SorttThisAraybyDate()
            self.countNewFilteredTotal()
        }
    }
    
    func countNewFilteredTotal(){
        
        var total:Float = 0
        for dict in arrayFinalOrders.dict{
            
            if let arryChItem = dict.object(forKey: "change_order_items") as? [[String:Any]] {
                for f in arryChItem {
                    if f.values.count != 0 {
                        total += ((f["cost"] as? NSString)?.floatValue ?? 0)
                    }
                }
            }
        }
        AgLog.debug("Final cost = \(total)")
        self.lblTotalCost.text = total.currency()
    }
    
    //MARK: Add New Order Event
    func addNewOrderClicked(sender: UIBarButtonItem) {
        if !CheckUserDetails.isFullAccess {
            Helper.agAlertSubscription("Change Order option not available without a subscription. Please subscribe for full access.")
        }
        else{
            let newOrdr = self.storyboard?.instantiateViewController(withIdentifier: "changeOrdr") as! ChangeOrderController
            newOrdr.projTitle = (self.btnAllProj.titleLabel?.text)!
            self.navigationController?.pushViewController(newOrdr, animated: true)
        }
    }
    
    //MARK: InApp  Purchase  subscription Delegates method End
    @IBAction func btnFilterClicked(_ sender: Any) {
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        viewSort.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(screenWidth), height: CGFloat(screenHeight))
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.view.addSubview(self.viewSort)
    }

    @IBAction func btnSortCancleClicked(_ sender: Any) {
        
        self.viewSort.removeFromSuperview()
    }
    @IBAction func btnSortByoptionClicked(_ sender: UIButton) {
        
        //    1 Approved
        //    2 Assigned
        //    3 Submitted
        //    4 Denied
        //    5 Saved
        //    6 completed
        //    7 new
        
        self.viewSort.removeFromSuperview()
        
        if sender.tag == 1{
            AgLog.debug("sort by approved")
            sortingStatus = Constants.STATUS_APPROVED
        }
        else if sender.tag == 2{
            AgLog.debug("sort by Assigned")
            sortingStatus = Constants.STATUS_ASSIGNED
        }
        else if sender.tag == 3{
            AgLog.debug("sort by Submitted")
            sortingStatus = Constants.STATUS_SUBMITTED
        }
        else if sender.tag == 4{
            AgLog.debug("sort by Denied")
            sortingStatus = Constants.STATUS_DENIED
        }
        else if sender.tag == 5{
            AgLog.debug("sort by Saved")
            sortingStatus = Constants.STATUS_SAVED
        }
        else if sender.tag == 6{
            AgLog.debug("sort by completed")
            sortingStatus = Constants.STATUS_COMPLETED
        }
        else if sender.tag == 7{
            AgLog.debug("sort by new")
            sortingStatus = Constants.STATUS_NEW
        }
        self.sortorderBystatus()
    }
    
    func sortorderBystatus(){
        let newSortingArry = NSMutableArray()
        let otherObjectArry = NSMutableArray()

        for dict in arrayFinalOrders.dict{
            if dict.string(forKey: "status") == sortingStatus {
                newSortingArry.add(dict)
            }
            else{
                otherObjectArry.add(dict)
            }
        }
        arrayFinalOrders = NSMutableArray()
        newSortingArry.addObjects(from: otherObjectArry as [AnyObject])
        AgLog.debug("New Arry total \(newSortingArry)")
        arrayFinalOrders = newSortingArry
        self.tblMyOrders.reloadData()
    }
    
    func showInfoView(){
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        viewinfo.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(screenWidth), height: CGFloat(screenHeight))
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.view.addSubview(self.viewinfo)
    }
    
    @IBAction func btnHideInfoViwClicked(_ sender: Any) {
        
        UserDefaults.standard.set("YES", forKey: "INFOSHOWED")
        self.viewinfo.removeFromSuperview()
    }
    
    //MARK: Tableveiw Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayFinalOrders.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "allProjcell") as! AllProjCell
        let dicOrder = arrayFinalOrders[indexPath.section] as? NSDictionary ?? NSDictionary()
        
        let crD  = dicOrder.string(forKey: "created_at")
        if let dateServer = Date.dateFormatter.date(from: crD) {
            let calendar = NSCalendar.current
            let yer = calendar.component(.year, from: dateServer)
            let mnth = calendar.component(.month, from: dateServer)
            let dy = calendar.component(.day, from: dateServer)
            cell.lblCreatedDate.text = "Created:\(mnth)/\(dy)/\(yer)"
        }
        
        let arryChItem = dicOrder.object(forKey: "change_order_items")

        var tempFloat: Int = 0
        for f in arryChItem as? [[String: Any]] ?? [] {
            if f.values.count != 0 {
                tempFloat += ((f["cost"] as? NSString)?.integerValue ?? 0)
            }
        }
        
        if tempFloat > 0{
            cell.lblOrderPrice.text = tempFloat.currency()
        }
        else{
            cell.lblOrderPrice.text = ""
        }
        
        cell.lblProId.textColor = cell.lblCreatedDate.textColor
        
        var changeOdID = dicOrder.string(forKey: "change_order_id")
        if changeOdID.contains("_offline"){
            let chID = changeOdID.replacingOccurrences(of: "_offline", with: "")
            let sortID =  String(chID.suffix(5))
            cell.lblProId.text = "CO-\(sortID)"
            changeOdID = sortID
        }
        else{
            let topCh = dicOrder.string(forKey: "top_change_order_id")
            if topCh.isNotNull &&  topCh != "0" {
                cell.lblProId.text = "CO-\(topCh)"
            }
            else {
                cell.lblProId.text = "CO-\(changeOdID)"
            }
        }
        
        cell.lblProjName.text = dicOrder.string(forKey: "job_name")
        let status = dicOrder.string(forKey: "status")
        
        if status == Constants.STATUS_SUBMITTED {
            
            cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojOrange.png"), for: .normal)
            cell.btnorderStatus.setTitle("Submitted", for: .normal)
            cell.btnOrderPdf.isHidden = false
            cell.btnorderStatus.setTitleColor(.white, for: .normal)
        }
        else if status == Constants.STATUS_APPROVED{
            
            cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojGreen.png"), for: .normal)
            cell.btnorderStatus.setTitle("Approved", for: .normal)
            cell.btnOrderPdf.isHidden = false
            cell.btnorderStatus.setTitleColor(.white, for: .normal)
        }
        else if status == Constants.STATUS_SAVED{
            
            cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojLight.png"), for: .normal)
            cell.btnorderStatus.setTitle("Saved", for: .normal)
            
            cell.btnOrderPdf.isHidden = true
            cell.btnorderStatus.setTitleColor(.black, for: .normal)
            
        }
        else if status == Constants.STATUS_REBILLED{
            
            cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojLight.png"), for: .normal)
            cell.btnorderStatus.setTitle("Saved", for: .normal)
            cell.btnOrderPdf.isHidden = true
            cell.btnorderStatus.setTitleColor(.black, for: .normal)
            
            let topCh = dicOrder.string(forKey: "rebilled_no")
            if topCh.isNotNull {
                cell.lblProId.textColor = .red
                cell.lblProId.text = topCh
            }
        }
        else if status == Constants.STATUS_DENIED {
            
            cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojRed.png"), for: .normal)
            cell.btnorderStatus.setTitle("Denied", for: .normal)
            cell.btnOrderPdf.isHidden = false
            cell.btnorderStatus.setTitleColor(.white, for: .normal)
            
        }
        else if status == Constants.STATUS_ASSIGNED{

            cell.btnorderStatus.setBackgroundImage(UIImage(named:"myProjGrey.png"), for: .normal)
            cell.btnorderStatus.setTitle("Assigned", for: .normal)
            cell.btnOrderPdf.isHidden = false
            cell.btnorderStatus.setTitleColor(.white, for: .normal)

        }
        else if status == Constants.STATUS_NEW {
            cell.btnorderStatus.setBackgroundImage(UIImage(named:"myProjWhiteBox.png"), for: .normal)
            cell.btnorderStatus.setTitle("NEW", for: .normal)
            cell.btnOrderPdf.isHidden = false
            cell.btnorderStatus.setTitleColor(.black, for: .normal)
        }
        else if status == Constants.STATUS_COMPLETED{
            cell.btnorderStatus.setBackgroundImage(UIImage(named:"myprojBlack.png"), for: .normal)
            cell.btnorderStatus.setTitle("Completed", for: .normal)
            cell.btnOrderPdf.isHidden = false
            cell.btnorderStatus.setTitleColor(.white, for: .normal)
        }
        
        if status != Constants.STATUS_SAVED{
            cell.btnorderStatus.tag = indexPath.section
            cell.btnorderStatus.addTarget(self,action:#selector(btnStatusOrderClicked), for: .touchUpInside)
        }
        return cell
    }
    
    func btnstatusSavedClicked(sender: UIButton) {
        AgLog.debug("savedClicked")
    }

    func btnStatusOrderClicked(sender: UIButton) {
        
        AgLog.debug("selected row \(sender.tag)")
        
        let dicOrder = arrayFinalOrders.dictionary(at: sender.tag)
        let status  = dicOrder.string(forKey: "status")
        let authName = dicOrder.string(forKey: "authority_name")
        AgLog.debug("authority Nme \(authName)")
        var strMessage = ""
        
        switch status {
        case Constants.STATUS_SUBMITTED:
            strMessage = "Submitted to \(authName)"
            Helper.AgAlertView(strMessage)
            break
            
        case Constants.STATUS_APPROVED:
            strMessage = "Approved by \(authName)"
            Helper.AgAlertView(strMessage)
            break
            
        case Constants.STATUS_DENIED:
            if let denialResn = dicOrder.object(forKey: "denial_reason") as? String{
                AgLog.debug("denialReason is \(denialResn)")
                strMessage = "Denied by \(authName).\n Reason: \(denialResn)"
                Helper.AgAlertView(strMessage)
            }
            else{
                AgLog.debug("denialReason is Null")
                strMessage = "Denied by \(authName)"
                Helper.AgAlertView(strMessage)
            }
            break
            
        case Constants.STATUS_ASSIGNED:
            if let assID = dicOrder.object(forKey: "assigner_id") as? String{
                if UserDetails.loginUserId == assID{
                    let assignerName  = dicOrder.string(forKey: "assigner_username")
                    strMessage = "This Change Order was assigned to you by \(assignerName)"
                }
                else{
                    strMessage = "Assigned to \(authName)"
                }
            }
            else{
                
                strMessage = "Assigned to \(authName)"
            }
            Helper.AgAlertView(strMessage)
            break
            
        default:
            AgLog.debug("clicked Order Status: \(status)")
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        let dicOrder = arrayFinalOrders[indexPath.section] as? NSDictionary ?? NSDictionary()
        let status  = dicOrder.string(forKey: "status")

        if let assID = dicOrder.object(forKey: "assigner_id") as? String{
            if UserDetails.loginUserId == assID {
                
                if status == Constants.STATUS_SUBMITTED  {
                    return false
                }
            }
            else{
                if status == Constants.STATUS_SUBMITTED || status == Constants.STATUS_ASSIGNED{
                    return false
                }
            }
        }
        else{
            
            if status == Constants.STATUS_SUBMITTED || status == Constants.STATUS_ASSIGNED{
                return false
            }
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        let indexPath = editActionsForRowAt
        let dicOrder = arrayFinalOrders[editActionsForRowAt.section] as? NSDictionary ?? NSDictionary()
        let status  = dicOrder.string(forKey: "status")
        AgLog.debug("status is \(status)")
        
        let delete = UITableViewRowAction(style: .normal, title:"\(TableRowActionType.delete)\n Delete ") { action, index in
            
            self.tblMyOrders.setEditing(!tableView.isEditing, animated: false)
            let dic = self.arrayFinalOrders.dictionary(at:self.selectedindex)
            self.chngOrdID = dic.string(forKey: "change_order_id")
            self.showDeleteAlert()
        }
        delete.backgroundColor = .red
        
        if status == Constants.STATUS_APPROVED {
            
            self.selectedindex = indexPath.section
            let dic = self.arrayFinalOrders.dictionary(at:self.selectedindex)
            self.chngOrdID = dic.string(forKey: "change_order_id")
            self.topChngOrdID = dic.string(forKey: "top_change_order_id")
            
            let complete = UITableViewRowAction(style: .normal, title: "\(TableRowActionType.check)\n  Completed ") { action, index in
                self.tblMyOrders.setEditing(!tableView.isEditing, animated: false)
                self.calltoCompletedChangeOrder()
            }
            complete.backgroundColor = UIColor(red: 49/255, green: 167/255, blue: 106/255, alpha: 1.0)
            
            var btn = [complete]
            if isDeleteButtonShow(dicOrder: dicOrder) {
                btn.insert(delete, at: 0)
            }
            
            return btn
        }
        else if status == Constants.STATUS_DENIED {
            self.selectedindex = indexPath.section
            let dic = self.arrayFinalOrders.dictionary(at: self.selectedindex)
            
            self.chngOrdID = dic.string(forKey: "change_order_id")
            
            let rebill = UITableViewRowAction(style: .normal, title: "\(TableRowActionType.edit)\n Rebill") { action, index in
                
                AgLog.debug("Edit Order button tapped")
                self.rebuidProjectApiCalled(curentChangeOrder: dic)
                
            }
            rebill.backgroundColor = UIColor(red: 49/255, green: 167/255, blue: 106/255, alpha: 1.0)
            
            var btn = [rebill]
            let is_rebilled = dic.bool(forKey: "is_to_rebilled")
            if is_rebilled {
                btn.removeAll()
            }
            
            if isDeleteButtonShow(dicOrder: dicOrder) {
                btn.insert(delete, at: 0)
            }

            return btn
        }
        else if status == Constants.STATUS_SAVED || status == Constants.STATUS_ASSIGNED || status == Constants.STATUS_REBILLED || status == Constants.STATUS_NEW{
            //action Edit and Delete
            
            self.selectedindex = indexPath.section
            let dic = self.arrayFinalOrders.dictionary(at: self.selectedindex)
            
            self.chngOrdID = dic.string(forKey: "change_order_id")
            
            let editOpt = UITableViewRowAction(style: .normal, title: "\(TableRowActionType.edit)\n Edit") { action, index in
                
                self.tblMyOrders.setEditing(!tableView.isEditing, animated: false)
                self.gotoEditOrderMode()
            }
            editOpt.backgroundColor = UIColor(red: 49/255, green: 167/255, blue: 106/255, alpha: 1.0)
            
            var btn = [editOpt]
            if isDeleteButtonShow(dicOrder: dicOrder) {
                btn.insert(delete, at: 0)
            }

            return btn
        }
        else if status == Constants.STATUS_COMPLETED {
            if isDeleteButtonShow(dicOrder: dicOrder) {
                return [delete]
            }
        }
        
        return nil
    }
    
    func isDeleteButtonShow(dicOrder: NSDictionary) -> Bool {
        
        let status  = dicOrder.string(forKey: "status")
        switch status {
        case Constants.STATUS_APPROVED, Constants.STATUS_SUBMITTED, Constants.STATUS_NEW:
            return false
            
        case Constants.STATUS_SAVED:
            let top_chane_order = dicOrder.string(forKey: "top_change_order_id")
            let chane_order = dicOrder.string(forKey: "change_order_id")
            
            if chane_order.contains("offline") { return true }
            
            if top_chane_order != chane_order {
                return false
            }
            
        case Constants.STATUS_ASSIGNED:
            if let assID = dicOrder.object(forKey: "assigner_id") as? String{
                if UserDetails.loginUserId == assID{
                    return false
                }
            }
            break
        default:
            return true
        }
        return true
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let dicOrder = arrayFinalOrders[indexPath.section] as? NSDictionary ?? NSDictionary()
        let status  = dicOrder.string(forKey: "status")
        
        let delete = UIContextualAction(style: .normal, title: "\(TableRowActionType.delete)\n Delete ") { action, view, completionHandler in
            //ByGaurav  AgLog.debug("delete button tapped")
            self.tblMyOrders.setEditing(!tableView.isEditing, animated: false)
            let dic = self.arrayFinalOrders.dictionary(at:self.selectedindex)
            self.chngOrdID = dic.string(forKey: "change_order_id")
            self.showDeleteAlert()
            completionHandler(true)
        }
        delete.backgroundColor = UIColor.red
        
        if status == Constants.STATUS_APPROVED {
            
            self.selectedindex = indexPath.section
            let dic = self.arrayFinalOrders.dictionary(at:self.selectedindex)
            self.chngOrdID = dic.string(forKey: "change_order_id")
            self.topChngOrdID = dic.string(forKey: "top_change_order_id")
            
            let complete = UIContextualAction(style: .normal, title: "\(TableRowActionType.check)\n Completed") { action, view, completionHandler in
                self.tblMyOrders.setEditing(!tableView.isEditing, animated: false)
                self.calltoCompletedChangeOrder()
                completionHandler(true)
            }
            complete.backgroundColor = UIColor(red: 49/255, green: 167/255, blue: 106/255, alpha: 1.0)
            
            var btn = [complete]
            if isDeleteButtonShow(dicOrder: dicOrder) {
                btn.insert(delete, at: 0)
            }
            
            let config = UISwipeActionsConfiguration(actions: btn)
            config.performsFirstActionWithFullSwipe = false
            return config
        }
        else if status == Constants.STATUS_DENIED {
            self.selectedindex = indexPath.section
            let dic = self.arrayFinalOrders.dictionary(at: self.selectedindex)
            
            self.chngOrdID = dic.string(forKey: "change_order_id")
            
            let rebill = UIContextualAction(style: .normal, title: "\(TableRowActionType.edit)\n Rebill") { action, view, completionHandler in
                
                AgLog.debug("Edit Order button tapped")
                self.rebuidProjectApiCalled(curentChangeOrder: dic)
                
                completionHandler(true)
            }
            rebill.backgroundColor = UIColor(red: 49/255, green: 167/255, blue: 106/255, alpha: 1.0)
            
            var btn = [rebill]
            let is_rebilled = dic.bool(forKey: "is_to_rebilled")
            if is_rebilled {
                btn.removeAll()
            }
            
            if isDeleteButtonShow(dicOrder: dicOrder) {
                btn.insert(delete, at: 0)
            }

            let config = UISwipeActionsConfiguration(actions: btn)
            config.performsFirstActionWithFullSwipe = false
            return config
        }
        else if status == Constants.STATUS_SAVED || status == Constants.STATUS_ASSIGNED || status == Constants.STATUS_REBILLED || status == Constants.STATUS_NEW{
            //action Edit and Delete
            
            self.selectedindex = indexPath.section
            let dic = self.arrayFinalOrders.dictionary(at: self.selectedindex)
            
            self.chngOrdID = dic.string(forKey: "change_order_id")
            
            let editOpt = UIContextualAction(style: .normal, title: "\(TableRowActionType.edit)\n Edit") { action, view, completionHandler in
                //ByGaurav   AgLog.debug("Edit Order button tapped")
                self.gotoEditOrderMode()
                self.tblMyOrders.setEditing(!tableView.isEditing, animated: false)
                completionHandler(true)
            }
            editOpt.backgroundColor = UIColor(red: 49/255, green: 167/255, blue: 106/255, alpha: 1.0)

            var btn = [editOpt]
            if isDeleteButtonShow(dicOrder: dicOrder) {
                btn.insert(delete, at: 0)
            }
            
            let config = UISwipeActionsConfiguration(actions: btn)
            config.performsFirstActionWithFullSwipe = false
            return config
        }
        else if status == Constants.STATUS_COMPLETED {
            if isDeleteButtonShow(dicOrder: dicOrder) {
                let config = UISwipeActionsConfiguration(actions: [delete])
                config.performsFirstActionWithFullSwipe = false
                return config
            }
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dic = self.arrayFinalOrders.dictionary(at: indexPath.section)
        let status =  dic.string(forKey: "status")
        if status == Constants.STATUS_SAVED || status == Constants.STATUS_REBILLED{
            AgLog.debug("saved status")
            
        }
        else{
            if uni_isWorking_Offline == true{
                let pdfView = self.storyboard?.instantiateViewController(withIdentifier: "pdfView") as! PDFViewController
                pdfView.dicallDetail = dic
                self.navigationController?.pushViewController(pdfView, animated: true)
            }else{
                let pdfView = self.storyboard?.instantiateViewController(withIdentifier: "pdfView") as! PDFViewController
                pdfView.pdf_link = dic.string(forKey: "pdf_url")
                self.navigationController?.pushViewController(pdfView, animated: true)
            }
        }
    }
    
    func gotoEditOrderMode(){
        
        if uni_isWorking_Offline == true {
            
            AgLog.debug("Its Edited Mode offline for selected Index \(self.selectedindex)")
            
            arry_EditChangeItems = NSMutableArray()
            let dicOrder = self.arrayFinalOrders[self.selectedindex] as? NSDictionary ?? NSDictionary()
            let chId = dicOrder.string(forKey: "change_order_id")
            
            for dicOr in uni_array_allChangeOrders.dict{
                
                let savedId = dicOr.string(forKey: "change_order_id")
                let change_order_id_offline = dicOr.string(forKey: "change_order_id_offline")
                if savedId == chId || change_order_id_offline == chId {
                    AgLog.debug("we found")
                    let index = uni_array_allChangeOrders.index(of: dicOr)
                    uni_selectedindextoEdit = index
                    
                    let arryChItem = dicOrder.array(forKey: "change_order_items")
                    arry_EditChangeItems = NSMutableArray(array: arryChItem)
                    current_lineItemNumber = 0
                    total_ItemsAdded = 0
                    projName = dicOrder.string(forKey: "job_name")
                    
                    let editItem = self.storyboard?.instantiateViewController(withIdentifier: "editchangeOrder") as! EditChangeItemController
                    editItem.jobName = dicOrder.string(forKey: "job_name")
                    editItem.chnOrderId =  self.chngOrdID
                    editItem.projectID  = dicOrder.string(forKey: "project_id")
                    editItem.dicmainEditOffline = dicOrder
                    self.navigationController?.pushViewController(editItem, animated: true)
                    return
                }
            }
        }
        else{
            
            arry_EditChangeItems = NSMutableArray()
            let dicOrder = self.arrayFinalOrders.dictionary(at: self.selectedindex)
    
            arry_EditChangeItems = dicOrder.array(forKey: "change_order_items").mutable
            
            current_lineItemNumber = 0
            total_ItemsAdded = 0
            projName = dicOrder.string(forKey: "job_name")
            let editItem = self.storyboard?.instantiateViewController(withIdentifier: "editchangeOrder") as! EditChangeItemController
            editItem.jobName = dicOrder.string(forKey: "job_name")
            editItem.chnOrderId =  self.chngOrdID
            editItem.projectID  = dicOrder.string(forKey: "project_id")
            self.navigationController?.pushViewController(editItem, animated: true)
        }
    }
    
    func showDeleteAlert(){
        
        let dic = self.arrayFinalOrders.dictionary(at: self.selectedindex)
        let alert = UIAlertController(title: "", message: "DELETE_MESSAGE".localized, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "DELETE_TEXT".localized, style: .destructive) { _ in
            
            if uni_isWorking_Offline == true{
                let chId = dic.string(forKey: "change_order_id")
                for dicN in uni_array_allChangeOrders{
                    
                    let dicOrd = dicN as? NSDictionary ?? NSDictionary()
                    let savedId = dicOrd.string(forKey: "change_order_id")
                    if savedId == chId {
                        
                        uni_array_allChangeOrders.remove(dicOrd)
                        DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
                        self.plist_getAllOffline_MyChangeOrders()
                        let myDeletedOrder  = RemovedController.shared.getMyDeletedOrderItemsData()
                        let arrayDeleted  = NSMutableArray(array: myDeletedOrder)
                        arrayDeleted.add(dic)
                        RemovedController.shared.saveMyDeletedOrdersItems(allDeletedOrders: arrayDeleted)
                        self.calltoDeleteChangeOrder(change: chId)
                    }
                }
                
            }else{
                self.calltoDeleteChangeOrder(change: self.chngOrdID)
            }
        })
        alert.addAction(UIAlertAction(title: "CANCLE_TEXT".localized, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getDeleted(){
        AgLog.debug("My Delted Orderes plist Data \(RemovedController.shared.getMyDeletedOrderItemsData())")
    }
    
    func calltoCompletedChangeOrder() {
        
        if uni_isWorking_Offline == true{
            
            let dicOrder = self.arrayFinalOrders.dictionary(at: self.selectedindex)
            let chId = dicOrder.string(forKey: "top_change_order_id")
            for dicOr in uni_array_allChangeOrders.dict{
                
                let savedId = dicOr.string(forKey: "top_change_order_id")
                if savedId == chId{
                    
                    AgLog.debug("we found")
                    let  index = uni_array_allChangeOrders.index(of: dicOr)
                    
                    let dicedited = dicOr.mutableCopy() as! NSMutableDictionary
                    dicedited.setValue("completed", forKey: "status")
                    dicedited.setValue("false", forKey: "is_uploaded")

                    uni_array_allChangeOrders.replaceObject(at: index, with: dicedited)
                    DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
                    self.plist_getAllOffline_MyChangeOrders()
                    self.chngOrdID = dicOr.string(forKey: "change_order_id")
                    self.topChngOrdID = chId
                    self.startToUploadMyAllOrdersToserver()
                    self.showCompletedInfoView()
                }
            }
        }
        else{
            self.callApiToCompleteOrder()
        }
        
    }
    
    func showCompletedInfoView(){
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        viewCompleted.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(screenWidth), height: CGFloat(screenHeight))
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.view.addSubview(self.viewCompleted)
    }
    
    @IBAction func btnCompletedGotItClicked(_ sender: Any) {
        
        self.viewCompleted.removeFromSuperview()
    }
    
    //MARK: Download images and order data function Start here
    var arrayOnlineAllOrders = NSMutableArray()
    
    var arrayOfflineIds: [String] = []
    
    func getAllOfflineStorageProjectsIds(){
        AgLog.debug("Method Order Filer 1")
        if uni_array_allChangeOrders.count > 0 {
            arrayOfflineIds = uni_array_allChangeOrders.dict.map {
                $0.string(forKey: "change_order_id")
            }
            self.getOnlineProjectsIds()
        }
        else{
            self.arrayRemainOrders = self.arrayOnlineAllOrders
            self.orderIndexToDownload = 0
            self.arrayFinalOrderstoSave = NSMutableArray()
            self.processToSaveItOffline()
        }
    }
    
    var arryserIDS: [String] = []
    
    func getOnlineProjectsIds() {
        
        self.arryserIDS = arrayOnlineAllOrders.dict.filter{
            return !arrayOfflineIds.contains($0.string(forKey: "change_order_id"))
            
            }.map {
                $0.string(forKey: "change_order_id")
        }
        
        AgLog.debug(arryserIDS)
        if arryserIDS.count > 0{
            getorderFOrmOnlineArraybyRemainIds()
        }
        else{
            AgLog.debug("No new Orders Change status if any")
            self.updateStatusofflineOrdersIfAnyChangesfromServer()
        }
    }
    
    var arrayRemainOrders = NSMutableArray()
    
    func getorderFOrmOnlineArraybyRemainIds(){
        
        self.arrayRemainOrders = NSMutableArray()
        for dicOrder in arrayOnlineAllOrders.dict{
            
            let chId = dicOrder.string(forKey: "change_order_id")
            if arryserIDS.contains(chId) {
                arrayRemainOrders.add(dicOrder)
            }
        }
        
        self.orderIndexToDownload = 0
        self.arrayFinalOrderstoSave = NSMutableArray()
        self.processToSaveItOffline()
    }
    
    var orderIndexToDownload = 0
    var arrayFinalOrderstoSave = NSMutableArray()

    //MARK: Start Process to save Offline of remain Orders data
    func updateStatusofflineOrdersIfAnyChangesfromServer(){

        for dictOffOrder in uni_array_allChangeOrders.dict {

            let editedDic = dictOffOrder.mutableCopy() as! NSMutableDictionary
            let id = dictOffOrder.string(forKey: "change_order_id")
            let curIndex = uni_array_allChangeOrders.index(of: dictOffOrder)

            for dictServer in arrayOnlineAllOrders.dict {
                let serserverId = dictServer.string(forKey: "change_order_id")
                if serserverId == id {
                    
                    if dictOffOrder.bool(forKey: "is_uploaded") {
                        editedDic.replaceValue(with: dictServer)
                        
                        if let v = dictServer.value(forKey: "custApiData") as? NSDictionary{
                            if let mv = v.mutableCopy() as? NSMutableDictionary {
                                if let i = v.value(forKey: "customer_signature") as? String {
                                    let name = self.downloadImageFromServerAndSavetoLocal(serverImage: i)
                                    mv.setValue(name, forKey: "customer_signature")
                                }
                                editedDic.setValue(mv, forKey: "custApiData")
                            }
                        }
                        
                        AgLog.debug("Replace value with server data \(serserverId)")
                        
                        let dicServerItem = dictServer.array(forKey: "change_order_items").mutable
                        
                        for i in 0..<dicServerItem.count {
                            
                            if let chnageDict = dicServerItem.dictionary(at: i).mutableCopy() as? NSMutableDictionary {
                                
                                let imgArray = chnageDict.array(forKey: "all_images")
                                
                                var arrayImgs: [Any] = []
                                if let dicImages = imgArray.firstObject as? NSDictionary {
                                    arrayImgs.append(self.downloadOrderImages(dicImages: dicImages))
                                }
                                
                                chnageDict.setValue(arrayImgs, forKey: "all_images")
                                dicServerItem.replaceObject(at: i, with: chnageDict)
                            }
                        }
                        
                        editedDic.setValue(dicServerItem, forKey: "change_order_items")
                        uni_array_allChangeOrders.replaceObject(at: curIndex, with: editedDic)
                        
                    }
                    else{
                        AgLog.debug("Is some value chnages in local order \(serserverId)")
                    }
                }
            }

            if curIndex == uni_array_allChangeOrders.count - 1 {
                DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
                self.plist_getAllOffline_MyChangeOrders()
            }
        }
    
        self.endRefreshView()
    }
    
    func endRefreshView() {
        tblMyOrders.agRefreshControl.attributedTitle = NSAttributedString(string: "Last synced : Just Now")
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime(uptimeNanoseconds: 2), execute: {
            if self.tblMyOrders.agRefreshControl.isRefreshing {
                self.tblMyOrders.agRefreshControl.endRefreshing()
            }
        })
            
        tblMyOrders.reloadData()
    }
    
    func processToSaveItOffline(){
        
        if orderIndexToDownload >= arrayRemainOrders.count{
            AgLog.debug("ALl Orders Data is downloaded save Finally with already saved orders ,", arrayFinalOrderstoSave)
            
            if arrayFinalOrderstoSave.count != 0{
                for dicOrd in arrayFinalOrderstoSave.dict {
                    if let array = dicOrd.object(forKey: "change_order_items") as? [Any], !array.isEmpty {
                        let allTopChangeOrderIds =
                            uni_array_allChangeOrders.filter{ temp in
                                if(dicOrd.string(forKey: "top_change_order_id") == "0"){
                                    return false
                                }
                                if((temp as! NSMutableDictionary).string(forKey: "top_change_order_id") == dicOrd.string(forKey: "top_change_order_id")){
                                    return true
                                }
                                return false
                            }
                        if(allTopChangeOrderIds.count == 0){
                           uni_array_allChangeOrders.add(dicOrd)
                        }
                        else{
                             AgLog.debug("This order already exist in list so no need to add in database,", dicOrd)
                        }
                    }
                    else{
                        AgLog.debug(dicOrd)
                    }
                }
                self.updateStatusofflineOrdersIfAnyChangesfromServer()
            }
        }
        else{
            
            let dicOrder  = arrayRemainOrders.dictionary(at: orderIndexToDownload)
            dicEditedOrder = dicOrder.mutableCopy() as! NSMutableDictionary
            
            itemsForthisOrder = dicOrder.array(forKey: "change_order_items").mutable
            arryAllItemsToSave = NSMutableArray()
            currentItemTosave = 0
            self.DownloadImagesandSetChangeItmes()
        }
    }
    
    var currentItemTosave = 0
    var itemsForthisOrder = NSMutableArray()
    var arryAllItemsToSave = NSMutableArray()
    var dicEditedOrder = NSMutableDictionary()
    
    func DownloadImagesandSetChangeItmes(){
        
        if currentItemTosave >= itemsForthisOrder.count {

            dicEditedOrder.setValue(arryAllItemsToSave, forKey: "change_order_items")
            dicEditedOrder.setValue("true", forKey: "is_uploaded")
            AgLog.debug("Final Dictionary for this order is  \(dicEditedOrder)")
            arrayFinalOrderstoSave.add(dicEditedOrder)
            
            orderIndexToDownload  = orderIndexToDownload + 1
            self.processToSaveItOffline()
        }
        else{
            
            let dicServerItem  = itemsForthisOrder.dictionary(at: currentItemTosave)
            let imgArray = dicServerItem.object(forKey: "all_images") as! NSArray
            
            var arrayImgs = [Any]()
            
            if let dicImages = imgArray.firstObject as? NSDictionary {
                arrayImgs.append(self.downloadOrderImages(dicImages: dicImages))
            }
            
            var dicItem = [String: Any]()
            
            dicItem["all_images"] = arrayImgs
            dicItem["cost"] = dicServerItem.object(forKey: "cost")
            dicItem["description"] = dicServerItem.object(forKey: "description")
            dicItem["order_item_id"] = dicServerItem.object(forKey: "order_item_id")
            dicItem["title"] = dicServerItem.object(forKey: "title")
            dicItem["is_uploaded"] = "true"
            
            arryAllItemsToSave.add(dicItem)
            
            AgLog.debug("orderItem Id is \(dicServerItem.string(forKey: "order_item_id"))")
            self.getNextitem()
        }
    }
    
    func downloadOrderImages(dicImages: NSDictionary) -> [String : Any]{
        
        let img1 = dicImages.string(forKey: "Image1")
        let img2 = dicImages.string(forKey: "Image2")
        let img3 = dicImages.string(forKey: "Image3")
        let img4 = dicImages.string(forKey: "Image4")
        
        var dicimage = [String: Any]()
        
        dicimage["Image1"] = self.downloadImageFromServerAndSavetoLocal(serverImage: img1)
        dicimage["Image2"] = self.downloadImageFromServerAndSavetoLocal(serverImage: img2)
        dicimage["Image3"] = self.downloadImageFromServerAndSavetoLocal(serverImage: img3)
        dicimage["Image4"] = self.downloadImageFromServerAndSavetoLocal(serverImage: img4)
        
        return dicimage
    }

    func getNextitem (){
        
        currentItemTosave = currentItemTosave + 1
        self.DownloadImagesandSetChangeItmes()
    }
    
    //MARK: Count total cost of all orders
    func countTotal(){
            
        self.arrayFinalOrders = NSMutableArray()
        self.arrayMainAllOrders = NSMutableArray()
        
        var total: Float = 0
        
        for dicT in arrayAllOrders.dict {
            
            let editDi = dicT.mutableCopy() as! NSMutableDictionary
            
            let status  = dicT.string(forKey: "status")
            if status != Constants.STATUS_DELETED {

                let arryChItem = dicT.array(forKey: "change_order_items")
                
                if arryChItem.count > 0{
                    let isoDate = dicT.string(forKey: "created_at")
                    
                    if let date = isoDate.toDate() {
                        editDi.setValue(date, forKey: "chnageOrderSortDate")
                    }
                    else{
                        editDi.setValue(Date.getCurrentDate().toDate(), forKey: "chnageOrderSortDate")
                    }
                    
                    if isEyeOpen == false{
                        if status != Constants.STATUS_COMPLETED {
                            arrayFinalOrders.add(editDi)
                            arrayMainAllOrders.add(editDi)
                            
                            for f in arryChItem as? [[String: Any]] ?? [] {
                                if f.values.count != 0 {
                                     total += ((f["cost"] as? NSString)?.floatValue ?? 0)
                                }
                            }
                        }
                    }
                    else{
                        arrayFinalOrders.add(editDi)
                        arrayMainAllOrders.add(editDi)

                        for f in arryChItem as? [[String: Any]] ?? [] {
                            if f.values.count != 0 {
                                total += ((f["cost"] as? NSString)?.floatValue ?? 0)
                            }
                        }
                    }
                }
            }
        }
        
        if selected_projID == "" || selected_projID == "Dummy"{
            self.lblTotalCost.text = total.currency()
            SorttThisAraybyDate()
        }
        else{
            AgLog.debug("Already selected project ID \(selected_projID)")
            for item in array_myProjectsID as? [String] ?? []{
                if item == selected_projID{
                    let inde = array_myProjectsID.index(of: item)
                    let projtName = array_myOwnProjects[inde] as? String ?? ""
                    AgLog.debug("Selected Project Index is \(inde) Name is \(projtName)")
                    self.btnAllProj.setTitle(projtName, for: .normal)
                }
            }
            self.callToFilerorderbyID()
        }
    }
    
    func SorttThisAraybyDate(){

        let swiftArray = (arrayFinalOrders.removeDuplicates()) as NSArray
        self.arrayFinalOrders = NSMutableArray(array: swiftArray)
        let sortedArray = (arrayFinalOrders as NSArray).sortedArray(using: [NSSortDescriptor(key: "chnageOrderSortDate", ascending: false)]) as! [[String:AnyObject]]
        
        self.arrayFinalOrders = NSMutableArray(array: sortedArray)
        self.tblMyOrders.reloadData()
    }
    
    //MARK: Start Upload Orders Data
    func startToUploadMyAllOrdersToserver(){
    
        self.backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(withName: backgroundTaskName, expirationHandler: {
            AgLog.debug("Background task is expired now")
            self.endBackgroundTask()
        })
        self.callBackgroundUpload()
    }
    
    func callBackgroundUpload (){
        
        self.offline_totalOrders = 0
        
        let arr =  DataController.sharedInstance.getMyChangeOrderData()
        AgLog.debug("Get my all orders")
        
        if arr.count == 0 {
            uni_array_allChangeOrders = NSMutableArray()
            AgLog.debug("No Orders in offline data")
            self.offline_startDeleteRemovedOrderfromPlist()
        }
        else{
            
            uni_array_allChangeOrders = arr.mutable
            AgLog.debug("startUploadOrderItemDatatoServer")
            self.offline_startUploadOrderItemDatatoServer()
        }
    }
    
    func offline_startUploadOrderItemDatatoServer(){
        
        if offline_totalOrders >= uni_array_allChangeOrders.count {
            AgLog.debug("all orders is uploaded succssfully no more orders")
            self.checkAllProjectUploaded()
            self.offline_startDeleteRemovedOrderfromPlist()
        }
        else{
            
            let dicOrdrmain = uni_array_allChangeOrders.dictionary(at: offline_totalOrders)
            let statUP = dicOrdrmain.string(forKey: "is_uploaded")
            allOrdersUploadScceessFully = false
            if statUP == "false" {
                
                let prIdMain = dicOrdrmain.string(forKey: "project_id")
                
                if prIdMain.contains("offline"){
                    
                    let arrayAll_ChangeItems = dicOrdrmain.array(forKey: "change_order_items")
                    
                    let finalArray = NSMutableArray()
                    for dicItem in arrayAll_ChangeItems.dict {
                        let parameters:  [String: String] = [
                            "cost" : dicItem["cost"] as? String ?? "",
                            "title" : dicItem["title"] as? String ?? "",
                            "description" : dicItem["description"] as? String ?? ""
                        ]
                        
                        finalArray.add(parameters)
                    }
                    
                    self.offline_checkIfThisItemsProjectUploadedToServer(dicMain: dicOrdrmain, finalArray: finalArray)
                    
                }
                else{
                    
                    let cID = dicOrdrmain.string(forKey: "change_order_id")
                    if cID.contains("offline") {
                        
                        AgLog.debug("This items for uploaded Project")
                        let arrayAll_ChangeItems = dicOrdrmain.array(forKey: "change_order_items")
                    
                        let finalArray = NSMutableArray()
                        
                        for dicItem in arrayAll_ChangeItems.dict{
                            let parameters:  [String: String] = [
                                "cost" : dicItem["cost"] as? String ?? "",
                                "title" : dicItem["title"] as? String ?? "",
                                "description" : dicItem["description"] as? String ?? ""
                            ]
                            
                            finalArray.add(parameters)
                        }
                        self.offline_thisItemsProjectalreadyOnServeruploadItemdata(dicMain: dicOrdrmain, finalArray: finalArray)
                    }
                    else{

                        let arrayAll_ChangeItems = dicOrdrmain.array(forKey: "change_order_items")
                        
                        let finalArray = NSMutableArray()
                        
                        for dicItem in arrayAll_ChangeItems as? [[String: Any]] ?? [] {
                            
                            let offser_cost = dicItem["cost"] as? String ?? ""
                            let offser_title = dicItem["title"] as? String ?? ""
                            let offser_description = dicItem["description"] as? String ?? ""
                            
                            var itemId = "\(dicItem["order_item_id"] ?? "")"
                            
                            if itemId.contains("offline"){
                                itemId = "-1"
                            }
                            
                            let parameters:  [String: String] = [
                                "cost" : offser_cost,
                                "title" : offser_title,
                                "description" : offser_description,
                                "order_item_id" : itemId
                            ]
                            
                            finalArray.add(parameters)
                        }
                        
                        self.offline_editedItemUploadedToServer(dicMain: dicOrdrmain, finalArray: finalArray)
                    }
                }
            }
            else {
                AgLog.debug("move to next Order")
                offline_totalOrders = offline_totalOrders + 1
                self.offline_startUploadOrderItemDatatoServer()
            }
        }
    }
    
    //MARK: Change Order First Time upload If Project uploaded
    func offline_thisItemsProjectalreadyOnServeruploadItemdata(dicMain: NSDictionary, finalArray: NSMutableArray){
        
        OrderMasterController.shared.callOffline_uploadOrderTextPart(project_id: dicMain.string(forKey: "project_id"), offlineID: dicMain.string(forKey: "change_order_id"), finalItemsarray: finalArray, completion: { (dicSer) in
            
            let ch_OrderID = dicSer.integer(forKey: "change_order_id")
            let orderItemsIDS = dicSer.array(forKey: "order_item_ids")
            let dicEd = dicMain.mutableCopy() as! NSMutableDictionary
            
            let oldCID = dicMain.string(forKey: "change_order_id")
            if oldCID != ""{
                if oldCID.contains("offline"){
                    dicEd.setValue(oldCID, forKey: "change_order_id_offline")
                }
            }
            
            let offline_change_order_id = dicSer.string(forKey: "offline_change_order_id")
            dicEd.setValue(offline_change_order_id, forKey: "offline_change_order_id")
            
            dicEd.setValue(ch_OrderID, forKey: "change_order_id")
            dicEd.setValue(ch_OrderID, forKey: "top_change_order_id")
            dicEd.setValue(dicMain.string(forKey: "project_id"), forKey: "project_id")
            
            if let offPrId = dicMain.object(forKey: "offline_id") as? String{
                dicEd.setValue(offPrId, forKey: "off_project_id")
            }
            else{
                dicEd.setValue("", forKey: "off_project_id")
            }
            
            let arryMuItems = dicEd.array(forKey: "change_order_items").mutable
            
            for i in 0..<orderItemsIDS.count{
                
                let oeIteId = "\(orderItemsIDS[i])"
                let dicIte = arryMuItems[i] as? NSDictionary ?? NSDictionary()
                let mutableDic = dicIte.mutableCopy() as! NSMutableDictionary
                mutableDic.setValue(oeIteId, forKey: "order_item_id")
                mutableDic.setValue("true", forKey: "is_uploaded")
                
                arryMuItems.replaceObject(at: i, with: mutableDic)
            }
            
            dicEd.setValue(arryMuItems, forKey: "change_order_items")
            self.offline_startUploadImagePartfromthisOrder(dicOrder: dicEd)
        })
    }
    
    //MARK: Change Order First Time upload If Project Id is not same as real ID
    func offline_checkIfThisItemsProjectUploadedToServer(dicMain: NSDictionary, finalArray: NSMutableArray){
        
        let thisOrderProjectID = dicMain.string(forKey: "project_id")
        for dicProj in uni_Array_allProjects.dict {
        
            if let offPrId = dicProj.object(forKey: "offline_id") as? String {
                
                AgLog.debug("project is uploaded to server now check with our selected project ID")
                
                if offPrId == thisOrderProjectID {
                    
                    AgLog.debug("Now start to Upload Text Part")
                    
                    OrderMasterController.shared.callOffline_uploadOrderTextPart(project_id: dicProj.string(forKey: "id"), offlineID:dicMain.string(forKey: "change_order_id"), finalItemsarray: finalArray, completion: { (dicSer) in
                        
                        let ch_OrderID = dicSer.integer(forKey: "change_order_id")
                        let orderItemsIDS = dicSer.array(forKey: "order_item_ids")
                        
                        let dicEd = dicMain.mutableCopy() as! NSMutableDictionary
                        if let oldCID = dicMain.object(forKey: "change_order_id") as? String, oldCID.contains("offline") {
                            
                            dicEd.setValue(oldCID, forKey: "change_order_id_offline")
                        }
                        
                        let offline_change_order_id = dicSer.string(forKey: "offline_change_order_id")
                        dicEd.setValue(offline_change_order_id, forKey: "offline_change_order_id")
                        
                        //create new order that time both ids is same
                        dicEd.setValue(ch_OrderID, forKey: "change_order_id")
                        dicEd.setValue(ch_OrderID, forKey: "top_change_order_id")
                        dicEd.setValue(dicProj.string(forKey: "id"), forKey: "project_id")
                        dicEd.setValue(offPrId, forKey: "off_project_id")
                        
                        let arryMuItems = dicEd.array(forKey: "change_order_items").mutable
                        
                        for i in 0..<orderItemsIDS.count{
                            
                            let oeIteId = "\(orderItemsIDS[i])"
                            let dicIte = arryMuItems.dictionary(at: i)
                            let mutableDic = dicIte.mutableCopy() as! NSMutableDictionary
                            mutableDic.setValue(oeIteId, forKey: "order_item_id")
                            mutableDic.setValue("true", forKey: "is_uploaded")
                            
                            arryMuItems.replaceObject(at: i, with: mutableDic)
                        }
                        dicEd.setValue(arryMuItems, forKey: "change_order_items")
                        self.offline_startUploadImagePartfromthisOrder(dicOrder: dicEd)
                    })
                }
                else{
                    AgLog.debug("Other projects ID")
                }
                
            }
            else{
                AgLog.debug("This items project is not on server check next if any")
                
                offline_totalOrders = offline_totalOrders + 1
                self.offline_startUploadOrderItemDatatoServer()
            }
        }
    }
    
    var offline_arrayItems_Imageupload = NSMutableArray()
    var offline_imageTotal = 0
    var offline_EditedFinalDic = NSMutableDictionary()
    
    func offline_startUploadImagePartfromthisOrder(dicOrder: NSMutableDictionary) {
        
        offline_EditedFinalDic = dicOrder
        offline_arrayItems_Imageupload = dicOrder.array(forKey: "change_order_items").mutable
        offline_imageTotal = 0
        
        if offline_totalOrders < uni_array_allChangeOrders.count{
            uni_array_allChangeOrders.replaceObject(at: offline_totalOrders, with: offline_EditedFinalDic)
            DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
            self.uploadAllImagesIntoServer()
        }
    }
    
    func uploadAllImagesIntoServer(times: Int = 1){
        
        if offline_imageTotal >= offline_arrayItems_Imageupload.count {
            
            AgLog.debug("All iamges is uploaded form this order now save this order dic to offline plist with required chagnes and go to next order in Process")
            
            if offline_totalOrders < uni_array_allChangeOrders.count{
                offline_EditedFinalDic.setValue("true", forKey: "is_uploaded")
                uni_array_allChangeOrders.replaceObject(at: offline_totalOrders, with: offline_EditedFinalDic)
                DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
            }
            // for next step
            AgLog.debug("check Status and call Api if status other then saved All Upload for this order Then increase")
            self.offline_checkStatusofThisChangeOrdr()
            self.plist_getAllOffline_MyChangeOrders()
        }
        else {
            
            let dicItm = offline_arrayItems_Imageupload.dictionary(at: offline_imageTotal)
            var status = "NO"
            let indexS = offline_arrayItems_Imageupload.index(of:dicItm)
            
            if indexS == offline_arrayItems_Imageupload.count - 1 {
                status = "YES"
            }
            
            let change_order_id = offline_EditedFinalDic.string(forKey: "change_order_id")
            let project_id = offline_EditedFinalDic.string(forKey: "project_id")
            
            OrderMasterController.shared.callOffline_uploadItemsALLImagesPart(changeID: change_order_id, projectId: project_id, orderItem: dicItm, isLastIndex: status) { (valResponce) in
                
                AgLog.debug("Upload Images APi responce \(valResponce)")
                if valResponce {
                    self.offline_imageTotal = self.offline_imageTotal + 1
                    self.uploadAllImagesIntoServer()
                }
                else{
                    if times != 4 {
                        self.uploadAllImagesIntoServer(times: times + 1)
                    }
                }
            }
        }
    }
    
    //MARK: Common Method for both
    func offline_checkStatusofThisChangeOrdr(){
        let status = offline_EditedFinalDic.string(forKey: "status")
        let cID = offline_EditedFinalDic.string(forKey: "change_order_id")
        let topC_ID = offline_EditedFinalDic.string(forKey: "top_change_order_id")
        approve_projectId = offline_EditedFinalDic.string(forKey: "project_id")
        
        if status == Constants.STATUS_SAVED {
            AgLog.debug("its saved only no nedd to process go to next order")
            offline_totalOrders = offline_totalOrders + 1
            self.offline_startUploadOrderItemDatatoServer()
        }
        else if status == Constants.STATUS_APPROVED {
            AgLog.debug("call customer approve api here")
            let dicCust = offline_EditedFinalDic.dictionary(forKey: "custApiData")
            ApproverMasterController.shared.offline_customerApprovealAPI(dicCustApprove: dicCust,ChangeOrderId:cID , completion: { (Responce) in
                AgLog.debug("Respo Approver is \(Responce)")
                self.offline_totalOrders = self.offline_totalOrders + 1
                self.offline_startUploadOrderItemDatatoServer()
            })
        }
        else if status == Constants.STATUS_SUBMITTED{
            let dicRemote:NSMutableDictionary = NSMutableDictionary(dictionary: offline_EditedFinalDic.dictionary(forKey: "remoteApiData"))
            let reApID = "\(dicRemote.string(forKey: "approver_id"))"
            if reApID.contains("_offline"){
                var temp = 0
                for dicPr in uni_Array_allProjects.dict {
                    if approve_projectId == dicPr.string(forKey: "id") {
                        dicRemote["site_contact_ids"] = dicPr.array(forKey: "site_contact_ids").mutable
                        for dic in dicPr.array(forKey: "site_contact_ids").mutable.dict{
                            if dic.bool(forKey: "is_approver"){
                                dicRemote["approver_id"] = dic.string(forKey: "id")
                            }
                        }
                    }
                    temp += 1
                }
                offline_EditedFinalDic.setValue("false", forKey: "is_uploaded")
                offline_EditedFinalDic.setValue(dicRemote, forKey: "remoteApiData")
                uni_array_allChangeOrders.replaceObject(at: temp, with: offline_EditedFinalDic)
                DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
                let reApID = "\(dicRemote.string(forKey: "approver_id"))"
                if reApID.contains("_offline"){
                    self.offline_totalOrders = self.offline_totalOrders + 1
                    self.offline_startUploadOrderItemDatatoServer()
                }else{
                ApproverMasterController.shared.offline_remoteApproverAPI(dicRemoteApprove: dicRemote, ChangeOrderId: cID, completion: { (remoteResponce) in
                        AgLog.debug("Respo Remote Approver  \(remoteResponce)")
                        self.offline_totalOrders = self.offline_totalOrders + 1
                        self.offline_startUploadOrderItemDatatoServer()
                    })
                }
            }else{
                ApproverMasterController.shared.offline_remoteApproverAPI(dicRemoteApprove: dicRemote, ChangeOrderId: cID, completion: { (remoteResponce) in
                    AgLog.debug("Respo Remote Approver  \(remoteResponce)")
                    self.offline_totalOrders = self.offline_totalOrders + 1
                    self.offline_startUploadOrderItemDatatoServer()
                })
            }
        }
        else if status ==  Constants.STATUS_ASSIGNED{
            let dicAssign = offline_EditedFinalDic.dictionary(forKey: "assignedApiData")
            ApproverMasterController.shared.offline_sendDataAssignedToOthersAPi(dicAssignedto: dicAssign, ChangeOrderId: cID, completion: { (assiResponce) in
                AgLog.debug("\(assiResponce)")
                self.offline_totalOrders = self.offline_totalOrders + 1
                self.offline_startUploadOrderItemDatatoServer()
            })
        }
        else if status == Constants.STATUS_COMPLETED{
            AgLog.debug("This is Mark as completed***** this ordre")
            OrderMasterController.shared.callOffline_markOrderAsCompleted(orderIDToComplete: topC_ID, completion: { (completeresponce) in
                AgLog.debug("mark completed***** as yes")
                self.offline_totalOrders = self.offline_totalOrders + 1
                self.offline_startUploadOrderItemDatatoServer()
            })
        }
    }
    
    //MARK: Change Order Edited  Offine Operations
    
    func offline_editedItemUploadedToServer(dicMain: NSDictionary, finalArray: NSMutableArray){
        
        OrderMasterController.shared.callOffline_editedOrderItemsTextPart(changOrderId: dicMain.string(forKey: "change_order_id"), finalEditedItemsarray: finalArray, completion: { (dicSer) in
            let orderItemsIDS = dicSer.array(forKey: "order_item_ids")
            let dicEd = dicMain.mutableCopy() as! NSMutableDictionary
            let arryMuItems = dicEd.array(forKey: "change_order_items").mutable
            for i in 0..<orderItemsIDS.count{
                let oeIteId = "\(orderItemsIDS[i])"
                let dicIte = arryMuItems.dictionary(at: i)
                let mutableDic = dicIte.mutableCopy() as! NSMutableDictionary
                mutableDic.setValue(oeIteId, forKey: "order_item_id")
                mutableDic.setValue("true", forKey: "is_uploaded")
                arryMuItems.replaceObject(at: i, with: mutableDic)
            }
            dicEd.setValue(arryMuItems, forKey: "change_order_items")
            AgLog.debug("Now we Edited images start to upload" ,dicEd)
            self.offline_editedItemImageUploadtoserver(dicOrder: dicEd)
        })
    }
    
    func offline_editedItemImageUploadtoserver(dicOrder:NSMutableDictionary){
        
        offline_EditedFinalDic = dicOrder
        offline_arrayItems_Imageupload = dicOrder.array(forKey: "change_order_items").mutable
        offline_imageTotal = 0
        self.callApiForEditedOrderImages()
        
    }
    
    func callApiForEditedOrderImages(){
        
        if offline_imageTotal >= offline_arrayItems_Imageupload.count{
            AgLog.debug("All iamges is uploaded form this order now save this order dic to offline plist with required chagnes and go to next order in Process")
            
            if offline_totalOrders < uni_array_allChangeOrders.count{
                offline_EditedFinalDic.setValue("true", forKey: "is_uploaded")
                uni_array_allChangeOrders.replaceObject(at: offline_totalOrders, with: offline_EditedFinalDic)
                DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
            }
            
            // for next step
            AgLog.debug("check Status and call Api if status other then saved after Edited Upload then increase this")
            
            self.offline_checkStatusofThisChangeOrdr()
            
        }
        else{
            
            let dicItm = offline_arrayItems_Imageupload.dictionary(at: offline_imageTotal)
            var status = "NO"
            let indexS = offline_arrayItems_Imageupload.index(of:dicItm)
            if indexS == offline_arrayItems_Imageupload.count - 1 {
                status = "YES"
            }
            
            let change_order_id = offline_EditedFinalDic.string(forKey: "change_order_id")
            let project_id = offline_EditedFinalDic.string(forKey: "project_id")
            
            OrderMasterController.shared.callOffline_eitedItemsUploadImages(changeID: change_order_id, projectId: project_id, orderItem: dicItm, isLastIndex: status) { (valResponce) in
                
                AgLog.debug("This is uploaded iamge APi responce \(valResponce)")
                
                if valResponce == true{
                    AgLog.debug("success Upload Iamges")
                }
                
                self.offline_imageTotal = self.offline_imageTotal + 1
                self.callApiForEditedOrderImages()
            }
        }
    }
    
    var totalItemsToDelete = 0
    var array_deletedOrders = NSMutableArray()
    
    func offline_startDeleteRemovedOrderfromPlist(){
        
        array_deletedOrders = RemovedController.shared.getMyDeletedOrderItemsData().mutable
        totalItemsToDelete = 0
        if array_deletedOrders.count > 0 {
            self.deleteOfflineOrdersItemFromServer()
        }
        else{
            AgLog.debug("get fresh orders and reload my data table")
            self.showAllsyncAlert()
        }
    }
    
    func showAllsyncAlert(){
        if self.tblMyOrders == nil{
            return
        }
        if isAnyNewOrderOrChanges {
        
            isAnyNewOrderOrChanges = false
            self.plist_getAllOffline_MyChangeOrders()
        }
        self.tblMyOrders.reloadData()
    }
    
    func deleteOfflineOrdersItemFromServer(){
        
        if totalItemsToDelete == array_deletedOrders.count{
            
            AgLog.debug("all removed order chagne on Server")
            AgLog.debug("completed all sync process , reload our orders table ")
            self.showAllsyncAlert()
        }
        else{
            
            let dicOrder = array_deletedOrders.dictionary(at: totalItemsToDelete)
            
            let cID = dicOrder.string(forKey: "change_order_id")
            if cID.contains("offline"){
                AgLog.debug("It is not on server ")
                
                totalItemsToDelete = totalItemsToDelete + 1
                self.deleteOfflineOrdersItemFromServer()
            }
            else{
                
                if let statDelted = dicOrder.object(forKey: "is_Deleted") as? String {
                    
                    AgLog.debug("This order was already deleted before ",statDelted)
                    totalItemsToDelete = totalItemsToDelete + 1
                    self.deleteOfflineOrdersItemFromServer()
                }
                else{
                    if let orderID = dicOrder.object(forKey: "change_order_id") as? String {
                        AgLog.debug("Now delete This Order")
                        OrderMasterController.shared.callOffline_deleteOrderFromServer(orderIDToDelete: orderID, completion: { (responce) in
                            
                            let newdicOrder = dicOrder.mutableCopy() as! NSMutableDictionary
                            newdicOrder.setValue("true", forKey: "is_Deleted")
                            
                            self.array_deletedOrders.replaceObject(at: self.totalItemsToDelete, with: newdicOrder)
                            RemovedController.shared.saveMyDeletedOrdersItems(allDeletedOrders: self.array_deletedOrders)
                            self.totalItemsToDelete = self.totalItemsToDelete + 1
                            self.deleteOfflineOrdersItemFromServer()
                        })
                    }
                }
            }
        }
    }
    
    //MARK: Webserver Called
    @discardableResult
    func downloadImageFromServerAndSavetoLocal(serverImage: String, saveName: String? = nil, callCount: Int = 0) -> String {
        
        if serverImage.isNull {
            return ImageFlag.notSelected
        }
        
        let path1 = kBaseURL + "public/" + serverImage
        
        let offlineName = String(Date().ticks)
        var genrateSaveName = URL(string: serverImage)?.lastPathComponent ?? "off_\(offlineName).jpg"
        
        if DataController.sharedInstance.isImageDownloadded(withName: genrateSaveName) {
            return genrateSaveName
        }
        
        if saveName != nil {
            genrateSaveName = saveName!
        }
        
        if callCount == 4  { return ImageFlag.notSelected }
        else{
            
            Alamofire.request(path1).responseImage { response in
                
                switch response.result {
                case .success(let value):
                    DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: value, imageName: genrateSaveName)
                    break
                    
                case .failure(let error):
                    AgLog.debug("Image Download Failed", error)
                    self.downloadImageFromServerAndSavetoLocal(serverImage: serverImage, saveName: genrateSaveName, callCount: callCount + 1)
                }
            }
            
            return genrateSaveName
        }
    }
    
    func calltogetallMyChangeOrders() {
        
        if !allOrdersUploadScceessFully {
            return
        }
        
        if Reachability.isConnectedToNetwork() == true {
            
            let strURL = kBaseURL + "getchangeorders/" + UserDetails.loginUserId
            AgLog.debug("getchangeorders")
            Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                
                if let arryServer = response.result.value as? NSArray {
                    
                    self.arrayOnlineAllOrders = NSMutableArray(array: arryServer)
                    
                    if self.arrayOnlineAllOrders.count != 0{
                       
                        let arrayRemovedOrders = RemovedController.shared.getMyDeletedOrderItemsData()
                        if arrayRemovedOrders.count != 0 {
                            let arryRemChId = NSMutableArray()
                            for dicORd in arrayRemovedOrders.dict {
                                let chId = dicORd.string(forKey: "change_order_id")
                                arryRemChId.add(chId)
                            }
                            
                            let arraynewOrdersData = NSMutableArray()
                            for dicOrderNew in self.arrayOnlineAllOrders.dict {
                                
                                let chOnId = dicOrderNew.string(forKey: "change_order_id")
                                if !arryRemChId.contains(chOnId){
                                    arraynewOrdersData.add(dicOrderNew)
                                }
                            }
                            self.arrayOnlineAllOrders = NSMutableArray()
                            self.arrayOnlineAllOrders = arraynewOrdersData
                        }
                    }
                    
                    self.getAllOfflineStorageProjectsIds()
                }
                
                self.endRefreshView()
            }
        }
        else {
            
            self.endRefreshView()
            AgLog.debug("Internet Connection not Available!")
        }
    }
    
    func rebuidProjectApiCalled(curentChangeOrder: NSDictionary) {
        
        if Reachability.isConnectedToNetwork() == true {
            
            let strURL = kBaseURL + "rebilledorder"
            let dictParams:  [String: String] = [
                "change_order_id" : self.chngOrdID,
                "user_id" : UserDetails.loginUserId,
                ]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                response.logShow()
                
                if let dicServer = response.result.value as? NSDictionary {
                    
                    if response.isSuccess {
                        if let change_order = dicServer.dictionary(forKey: "change_order").mutableCopy() as? NSMutableDictionary {
                            change_order.setValue("true", forKey: "is_uploaded")
                            uni_array_allChangeOrders.add(change_order)
                            DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
                            self.calltogetallMyChangeOrders()
                        }
                        Helper.AgAlertView(response.message)
                    }
                    else{
                        Helper.AgAlertView(response.message)
                    }
                }
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    func calltoDeleteChangeOrder(change: String) {
        
        if Reachability.isConnectedToNetwork() == true{
            
            let strURL = kBaseURL + "deletechangeorder"
            let dictParams:  [String: String] = [
                "change_order_id" :  change,
                "user_id": UserDetails.loginUserId
            ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                response.logShow()
                if let dicServer = response.result.value as? NSDictionary {
                    
                    if response.isSuccess {
                        self.calltogetallMyChangeOrders()
                    }
                    else{
                        
                        let message = dicServer.string(forKey: "message")
                        AgLog.debug("\(message)")
                        Helper.AgAlertView(message)
                    }
                }
            }
        }
    }
    
    func callApiToCompleteOrder(){
        
        if Reachability.isConnectedToNetwork() == true {
            
            let strURL = kBaseURL + "completechangeorder"
            let dictParams:  [String: String] = [
                "top_change_order_id" :  self.topChngOrdID,
                "user_id": UserDetails.loginUserId]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                if response.isSuccess {
                    self.showCompletedInfoView()
                }
            }
        }
    }
}
