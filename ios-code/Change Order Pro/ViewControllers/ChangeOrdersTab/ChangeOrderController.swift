//
//  ChangeOrderController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import DropDown
import Photos

class ChangeOrderController: UIViewController, CameraViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @IBOutlet var viewMenu: UIView!
    @IBOutlet var btnProj: UIButton!
    
    let infoProjDropDown = DropDown()
    var projTitle = ""
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        if self.projTitle == "ALL Projects"{
            selected_projID = ""
        }
        else{
            self.btnProj.setTitle(projTitle, for: .normal)
        }
        
        projName = projTitle
        AgLog.debug("Project Title is \(projName)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnMyProjClicked(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnSettingClicked(_ sender: Any) {
        let settings = self.storyboard?.instantiateViewController(withIdentifier: "settings") as! SettingsController
        self.navigationController?.pushViewController(settings, animated: true)
    }
    
    @IBAction func btnSelectProjClicked(_ sender: UIButton) {
        
        infoProjDropDown.anchorView = sender
        infoProjDropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        infoProjDropDown.dataSource = array_myOwnProjects as! [String]
        infoProjDropDown.show()
        infoProjDropDown.selectionAction = { [unowned self] (index, item) in
            
            if item == "ALL Projects" {
                self.btnProj.setTitle("Select Project", for: .normal)
            }
            else{
                self.btnProj.setTitle(item, for: .normal)
            }
            
            if let selectedProject = array_myProjectsID.object(at: index) as? String {
                selected_projID = selectedProject
                AgLog.debug("Selected project title \(item), id \(selected_projID)")
//                arry_ChangeItems = NSMutableArray()
            }
        }
    }
    
    //MARK: Camera Icon Clicked
    @IBAction func btnCameraClicked(_ sender: UIButton) {
        
        if selected_projID.isNull || selected_projID.isDummy {
            Helper.AgAlertView("SELECT_PROJECT".localized)
        }
        else{
            if Environment.isSimulator {
                current_lineItemNumber = 0
                total_ItemsAdded = 0
                
                let chngOrderitemm = self.storyboard?.instantiateViewController(withIdentifier: "cahngeItem") as! ChangeOrderItems
                chngOrderitemm.projTitle = (self.btnProj.titleLabel?.text)!
                projName = chngOrderitemm.projTitle
                
                chngOrderitemm.imgFirst = UIImage(named:"celling_1.jpg")!
                
                self.navigationController?.pushViewController(chngOrderitemm, animated: true)
            }
            else{
                let camera = CameraViewController.getInstant()
                camera.delegate = self
                camera.sourceView = sender
                self.present(camera, animated: true, completion: nil)
            }
        }
    }
    
    func cameraViewControllerDidCancel(_ picker: CameraViewController, isClose: UIView?) {
        if let v = isClose {
            picker.dismiss(animated: true, completion: nil)
            AGImagePickerController(with: self, type: .photoLibrary, allowsEditing: false, iPadSetup: v)
        }
        else{
            picker.dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        
        self.cameraViewController(nil, didFinishPickingMedia: image)
    }
    
    func cameraViewController(_ picker: CameraViewController?, didFinishPickingMedia image: UIImage) {
        if let p = picker {
            p.dismiss(animated: true, completion: nil)
        }
        
        current_lineItemNumber = 0
        total_ItemsAdded = 0
        
        let chngOrderitemm = self.storyboard?.instantiateViewController(withIdentifier: "cahngeItem") as! ChangeOrderItems
        chngOrderitemm.projTitle = (self.btnProj.titleLabel?.text)!
        projName = chngOrderitemm.projTitle
        chngOrderitemm.imgFirst = image
        self.navigationController?.pushViewController(chngOrderitemm, animated: true)

        picker?.dismiss(animated: true, completion: nil);
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon

        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "CHANGE_ORDER".localized
//        self.setNavItems()
    }
    
    func setNavItems(){
        let menuBtn = UIBarButtonItem(
            image: UIImage(named: "MenuIcon"),
            style: .plain,
            target: self,
            action: #selector(menuClicked(sender:))
        )
        
        self.navigationItem.rightBarButtonItem = menuBtn
    }
    
    func menuClicked(sender: UIBarButtonItem) {
        self.viewMenu.isHidden = !self.viewMenu.isHidden
    }
    
}
