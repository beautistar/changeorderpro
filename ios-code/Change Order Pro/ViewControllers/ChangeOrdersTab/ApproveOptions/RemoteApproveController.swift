//
//  RemoteApproveController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire

class RemoteApproveController: UIViewController ,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var vieApprove: UIView!
    
    var remoteApprovrId = ""
    var remoteApprovrName = ""
    
    var arrySiteCont = NSMutableArray()
    var arryApprover = NSMutableArray()
    var arryOtherCont = NSMutableArray()
    
    @IBOutlet var tblMyContact: UITableView!
    
    let arryContName = ["Charles Johnson","Herry Mark","Lenny Page"]
    let arryContNumber = ["407-123-4567","498-658-9874","462-458-6325"]
    
    let arrytitle = ["Approver","Send copy to:"]
    
    var arrAllApproverID = NSMutableArray()
    
    var dicallDetail = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.title = "REMOTE_APPROVE".localized
        offline_showSiteContactForThisProject()
        self.checkSiteContactIfchanged()
    }
    
    func reloadAsignData() {
        if uni_isWorking_Offline == true{
            self.offline_showSiteContactForThisProject()
        }else{
            self.calltogetMyContacts()
        }
        
        AgLog.debug("all data with Remote Approval", dicallDetail)
    }
    
    func checkSiteContactIfchanged() {
        if isCostSet == true{
            if checkfilterApprover() == true{
                reloadAsignData()
            }
            else{
                Helper.AgAlertView("An approver must be setup under this project in order to use this option.") {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else{
            Helper.AgAlertView("Please enter a total for remote approval") {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func checkfilterApprover()->Bool{
        
        var isFoundApprover = false
        
        for dict in arrySiteCont.dict{
            
            let email = dict.string(forKey: "email")
            if email.count > 0 {
                if let apprver  = dict.object(forKey: "is_approver") as? String {
                    AgLog.debug("approver value is \(apprver)")
                    if apprver == "1"{
                        isFoundApprover = true
                    }
                }
                else if let isapp = dict.object(forKey: "is_approver") as? Bool {
                    
                    if isapp == true {
                        isFoundApprover = true
                    }
                }
            }
            else{
                AgLog.debug("NO email found")
            }
        }
        return isFoundApprover
    }
    
    func offline_showSiteContactForThisProject(){
        
        AgLog.debug("selected Project ID \(selected_projID)")
        for dicPr in uni_Array_allProjects.dict {
            if dicallDetail.string(forKey: "project_id") == dicPr.string(forKey: "id") {
                self.arrySiteCont = dicPr.array(forKey: "site_contact_ids").mutable
            }
        }
        
        self.filterApprover()
    }
    
    func calltogetMyContacts() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let strURL = kBaseURL + "getcontacts/" + selected_projID
            AgLog.debug("get contacts stringUrl \(strURL)")
            Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { response in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                response.logShow()
                if response.result.value != nil{
                    if   let arryServer = response.result.value as? NSArray {
                        AgLog.debug("Thisis the contacts \(arryServer)")
                        self.arrySiteCont = NSMutableArray(array:arryServer)
                        self.filterApprover()
                    }
                }
            }
        }
    }
    
    func filterApprover(){
        
        arryApprover.removeAllObjects()
        arryOtherCont.removeAllObjects()
        
        for dict in arrySiteCont.dict{
            
            let email = dict.string(forKey: "email")
            if email.count > 0 {
                if  let apprver  = dict.object(forKey: "is_approver") as? String {
                    AgLog.debug("approver value is \(apprver)")
                    
                    if apprver == "1"{
                        
                        let aprID = dict.string(forKey: "id")
                        remoteApprovrId = aprID
                        remoteApprovrName = dict.string(forKey: "name")
                        arryApprover.add(dict)
                    }
                    else{
                        arryOtherCont.add(dict)
                    }
                }
                else if let isapp = dict.object(forKey: "is_approver") as? Bool {
                    
                    if isapp == true {
                        
                        let aprID = dict.string(forKey: "id")
                        remoteApprovrName = dict.string(forKey: "name")
                        remoteApprovrId = aprID
                        arryApprover.add(dict)
                    }
                    else{
                        
                        arryOtherCont.add(dict)
                    }
                }
            }
            else{
                AgLog.debug("NO email found")
            }
        }
        self.tblMyContact.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return arryApprover.count
        }
        else{
            return arryOtherCont.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "rContactcell") as! ContactCell

        if indexPath.section == 0 {
            let dicSCont = arryApprover[indexPath.row] as!  NSDictionary
            
            cell.txtcontName.text = dicSCont.string(forKey: "name")
            cell.txtContNum.text = dicSCont.string(forKey: "email")
            
            cell.btnCheck.isHidden = true
            
            return cell
        }
        else{

            let dicSCont = arryOtherCont[indexPath.row] as!  NSDictionary
            
            cell.txtcontName.text = dicSCont.string(forKey: "name")
            cell.txtContNum.text = dicSCont.string(forKey: "email")
            
            cell.btnCheck.setBackgroundImage(UIImage(named:"checkGreenEmpty.png"), for: .normal)
            cell.btnCheck.setBackgroundImage(UIImage(named:"checkRight.png"), for: .selected)
            cell.btnCheck.tag = indexPath.row
            cell.btnCheck.addTarget(self,action:#selector(btnCheckClicked(sender:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 5, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: "Open Sans Semibold", size: 11)
        headerLabel.textColor = UIColor (red: 82/255.0, green: 82/255.0, blue: 82/255.0, alpha: 1.0)
        headerLabel.text = arrytitle[section]
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func btnCheckClicked(sender: UIButton) {
        
        if sender.isSelected == true {
            sender.isSelected = false
            
            let dicSCont = arryOtherCont[sender.tag] as!  NSDictionary
            
            let aprID = dicSCont.string(forKey: "id")
            let copytoID = "\(aprID)"
            arrAllApproverID.remove(copytoID)
            AgLog.debug("removed \(copytoID)")
        }
        else{
            
            sender.isSelected = true
            let dicSCont = arryOtherCont[sender.tag] as!  NSDictionary
            
            let aprID = dicSCont.string(forKey: "id")
            let copytoID = "\(aprID)"
            arrAllApproverID.add(copytoID)
            AgLog.debug("added \(copytoID)")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0{
            
            let conDetail = self.storyboard?.instantiateViewController(withIdentifier: "contDetail") as! RemoteContactDetail
            let dicSCont = arryApprover[indexPath.row] as!  NSDictionary
            conDetail.dicContDetail = dicSCont
            self.navigationController?.pushViewController(conDetail, animated: true)
            
        }
        else{
            
            let conDetail = self.storyboard?.instantiateViewController(withIdentifier: "contDetail") as! RemoteContactDetail
            let dicSCont = arryOtherCont[indexPath.row] as!  NSDictionary
            conDetail.dicContDetail = dicSCont
            self.navigationController?.pushViewController(conDetail, animated: true)
        }
    }
    
    @IBAction func btnReqAppClciked(_ sender: Any) {
        if arrAllApproverID.count == 0 && self.remoteApprovrId == "" {
            Helper.AgAlertView("You have not selected any Remote contact")
        }
        else{
            
            if CompanyDetailsModel.isCompanyDetails() {
                if uni_isWorking_Offline == true{
                    AgLog.debug("save for offline with status change  to 'submitted' ")
                    if uni_fromEdit == "YES" {
                        self.offlineSaveRemoteApproveEditedChangeOrder()
                    }
                    else {
                        self.offlineSaveRemoteApproveNewOrder()
                    }
                }
                else {
                    self.callremoteApprovealApi()
                }
            }
            else{
                Helper.AgAlertView("Please setup your company info in app more section.")
            }
        }
    }
    
    func offlineSaveRemoteApproveNewOrder(){
        
        AgLog.debug("all Other Id aray os is \(self.arrAllApproverID)")
        
        let dictRemoteData:  [String: Any] = [
            "change_order_id" : dicallDetail.string(forKey: "change_order_id"),
            "approver_id" : remoteApprovrId,
            "site_contact_ids" : self.arrAllApproverID
        ]
        
        let dictParams:  [String: Any] = [
            "assigner_id" : dicallDetail.string(forKey: "assigner_id"),
            "authority_email" : dicallDetail.string(forKey: "authority_email"),
            "top_change_order_id" : dicallDetail.string(forKey: "top_change_order_id"),
            "change_order_id" : dicallDetail.string(forKey: "change_order_id") ,
            "change_order_id_offline" : dicallDetail.string(forKey: "change_order_id_offline") ,
            "created_at" : dicallDetail.string(forKey: "created_at"),
            "job_name" : dicallDetail.string(forKey: "job_name"),
            "project_id" : dicallDetail.string(forKey: "project_id"),
            "status" : Constants.STATUS_SUBMITTED,
            "change_order_items" : dicallDetail.object(forKey: "change_order_items") ?? "",
            "client_name" : dicallDetail.string(forKey: "client_name"),
            "address" : dicallDetail.string(forKey: "address"),
            "job_number" : dicallDetail.string(forKey: "job_number"),
            "approvedBy" :  Constants.REMOTEAPPROVER,
            "remoteApiData" : dictRemoteData,
            "is_uploaded" : "false",
            "authority_name" : remoteApprovrName
        ]
        
        let dicF = dictParams as NSDictionary
        AgLog.debug("dic Final for Remote Approver Offline \(dicF)")
        
        uni_array_allChangeOrders.add(dicF)
        DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
        uni_array_allChangeOrders = NSMutableArray()
        
        self.showRemoteAproveSuccessView()
        uni_fromEdit = "NO"
    }
    
    //Edited offline Order:
    func offlineSaveRemoteApproveEditedChangeOrder(){
        
        AgLog.debug("all Other Id aray os is \(self.arrAllApproverID)")
        
        let dictRemoteData:  [String: Any] = [
            "change_order_id" : dicallDetail.string(forKey: "change_order_id"),
            "approver_id" : remoteApprovrId,
            "site_contact_ids" : self.arrAllApproverID
        ]
        
        let dictParams:  [String: Any] = [
            "assigner_id" : dicallDetail.string(forKey: "assigner_id"),
            "authority_email" : dicallDetail.string(forKey: "authority_email"),
            "change_order_id" : dicallDetail.string(forKey: "change_order_id") ,
            "top_change_order_id" : dicallDetail.string(forKey: "top_change_order_id"),
            "change_order_id_offline" : dicallDetail.string(forKey: "change_order_id_offline") ,
            "created_at" : dicallDetail.string(forKey: "created_at"),
            "job_name" : dicallDetail.string(forKey: "job_name"),
            "project_id" : dicallDetail.string(forKey: "project_id"),
            "status" : Constants.STATUS_SUBMITTED,
            "change_order_items" : dicallDetail.object(forKey: "change_order_items") ?? "",
            "client_name" : dicallDetail.string(forKey: "client_name"),
            "address" : dicallDetail.string(forKey: "address"),
            "job_number" : dicallDetail.string(forKey: "job_number"),
            "approvedBy" :  Constants.REMOTEAPPROVER,
            "remoteApiData" : dictRemoteData,
            "is_uploaded" : "false",
            "authority_name" : remoteApprovrName
        ]
        
        let dicF = dictParams as NSDictionary
        AgLog.debug("dic Final for Remote Approver Offline Edited \(dicF)")
        
        uni_array_allChangeOrders.replaceObject(at: uni_selectedindextoEdit, with: dicF)
        
        DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
        uni_array_allChangeOrders = NSMutableArray()
        
        self.showRemoteAproveSuccessView()
        uni_fromEdit = "NO"
        
    }
    
    func showRemoteAproveSuccessView(){
        
        let screenSize: CGRect = UIScreen.main.bounds
        let   screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        vieApprove.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(screenWidth), height: CGFloat(screenHeight))
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.view.addSubview(self.vieApprove)
        
    }
    
    func callremoteApprovealApi() {

        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let strURL = kBaseURL + "remoteapproval"
            let dictParams:  [String: String] = [
                "change_order_id" : chng_ordID,
                "approver_id" : remoteApprovrId,
                "site_contact_ids" : self.arrAllApproverID.toJson ?? "",
                "first_name" : approve_user_FirstName,
                "last_name" : approve_user_Lastname,
                "company_name" : approve_compny_Name,
            ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                
                if response.isSuccess {
                    chng_ordID = ""
                    self.showRemoteAproveSuccessView()
                }
                else{
                    Helper.AgAlertView(response.message)
                }
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    @IBAction func btnGotItClicked(_ sender: Any) {
        self.resetAllDeclaredValue()
        self.vieApprove.removeFromSuperview()
        isAnyNewOrderOrChanges = true
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func resetAllDeclaredValue(){
        AgLog.debug("reseteddd all uni value")
        
//        arry_ChangeItems = NSMutableArray()
        arry_allOrderID = NSMutableArray()
        //  selected_projID = ""
        prev_Pdf = ""
        chng_ordID = ""
    }
    
    @IBAction func bntAddNEwClicked(_ sender: Any) {
        
        let newContct = self.storyboard?.instantiateViewController(withIdentifier: "addSiteContact") as! AddSiteContactController
        self.navigationController?.pushViewController(newContct, animated: true)
    }
}

