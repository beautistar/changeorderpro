//
//  CustomerApproveController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 11/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire

class CustomerApproveController: UIViewController ,YPSignatureDelegate{
    
    @IBOutlet var viewApproved: UIView!
    @IBOutlet var mySignView: YPDrawSignatureView!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var txtCustName: UITextField!
    
    @IBOutlet var txtTitle: FormTextField!
    @IBOutlet var btnResign: UIButton!
    
    var statusSign = ""
    var signImage = UIImage()
    
    var usercompLogoUrl = ""
    
    var dicallDetail = NSDictionary()
    
    var fromStatus = ""
    
    @IBOutlet var lblSignRequired: UILabel!
    @IBOutlet var viewSignTopConstrain: NSLayoutConstraint! // Original 158
    
    @IBOutlet var viewSignBottomConstrain: NSLayoutConstraint! // Original 92
    
    @IBOutlet var btnSubmit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if  UIDevice.current.userInterfaceIdiom == .phone{
            
            self.viewSignTopConstrain.constant = 158 + 64
            self.viewSignBottomConstrain.constant = 92
            
            
        }else{
            
            self.viewSignTopConstrain.constant = 351 + 64
            self.viewSignBottomConstrain.constant = 197
        }
        
        mySignView.delegate = self
        mySignView.strokeColor = .black
        mySignView.strokeWidth = 1
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        usercompLogoUrl = UserDetails.logoURL()
        
        if let resultCompnay = UserDetails.getCompnayDetailsDict() {
            approve_compny_Name = resultCompnay.string(forKey: "company_name")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = UIInterfaceOrientationMask.all
        }
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            
            if UIDevice.current.userInterfaceIdiom == .phone{
                self.viewSignTopConstrain.constant = 40
                self.viewSignBottomConstrain.constant = 0
            }else{
                self.viewSignTopConstrain.constant = 64
                self.viewSignBottomConstrain.constant = 0
            }
            self.tabBarController?.tabBar.isHidden = true
            lblSignRequired.isHidden = true
            
        } else {
            self.tabBarController?.tabBar.isHidden = false
            lblSignRequired.isHidden = false
            
            if UIDevice.current.userInterfaceIdiom == .phone{
                
                self.viewSignTopConstrain.constant = 158 + 64
                self.viewSignBottomConstrain.constant = 92
            }else{
                
                self.viewSignTopConstrain.constant = 351 + 64
                self.viewSignBottomConstrain.constant = 197
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: Submit Customer Data Clicked
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        if self.txtCustName.isNull {
            Helper.AgAlertView("Enter Name")
        }
        else if self.txtTitle.isNull{
            Helper.AgAlertView("Enter Title")
        }
        else if statusSign != "DoneSign"{
            Helper.AgAlertView("Signature required to submit")
        }
        else{
            
            if uni_isWorking_Offline == true{
                
                if uni_fromEdit == "YES" {
                    
                    AgLog.debug("Edited  my offline array")
                    self.offlineSavebyCustomerApproveForEditedChangeOrder()
                }
                else{
                    AgLog.debug("new Order add in my offline array")
                    self.offlineSavebyCustomerApproveForNewOrder()
                }
            }
            else{
                callCustapprovewithSign()
            }
        }
    }
    
    func offlineSavebyCustomerApproveForNewOrder(){
        
        let offlineName = String(Date().ticks)
        let imgeSignName = "off_\(offlineName).jpg"
        DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.signImage, imageName: imgeSignName)
        
        var changeOrdOfflineId = ""
        if let chOffId  = dicallDetail.object(forKey: "change_order_id_offline") as? String{
            changeOrdOfflineId = chOffId
        }
        
        let dictCustData:  [String: Any] = [
            "change_order_id" : dicallDetail.object(forKey: "change_order_id") ?? "",
            "customer_name" : self.txtCustName.text!,
            "customer_Title" : self.txtTitle.text!,
            "project_id" : dicallDetail.object(forKey: "project_id") ?? "",
            "user_id" : UserDetails.loginUserId,
            "logo_url" : usercompLogoUrl,
            "customer_signature" : imgeSignName
        ]
        
        let dictParams:  [String: Any] = [
            "assigner_id" : dicallDetail.object(forKey: "assigner_id") ?? "",
            "authority_email" : dicallDetail.object(forKey: "authority_email") ?? "",
            "change_order_id" : dicallDetail.object(forKey: "change_order_id") ?? "" ,
            "top_change_order_id" : dicallDetail.object(forKey: "top_change_order_id") ?? "" ,
            "change_order_id_offline" : changeOrdOfflineId ,
            "created_at" : dicallDetail.object(forKey: "created_at") ?? "",
            "job_name" : dicallDetail.object(forKey: "job_name") ?? "",
            "project_id" : dicallDetail.object(forKey: "project_id") ?? "",
            "status" : Constants.STATUS_APPROVED,
            "change_order_items" : dicallDetail.object(forKey: "change_order_items") ?? "",
            "client_name" : dicallDetail.object(forKey: "client_name") ?? "",
            "address" : dicallDetail.object(forKey: "address") ?? "",
            "job_number" : dicallDetail.object(forKey: "job_number") ?? "",
            "approvedBy" :  Constants.CUSTOMER,
            "custApiData" : dictCustData,
            "is_uploaded" : "false",
            "authority_name" : self.txtCustName.text!
        ]
        
        let dicF = dictParams as NSDictionary
        AgLog.debug("dic Final for Customer Approve Offline \(dicF)")
        
        uni_array_allChangeOrders.add(dicF)
        
        DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
        uni_array_allChangeOrders = NSMutableArray()
        
        self.showAproveSuccessView()
        uni_fromEdit = "NO"
    }
    
    //Edited Order Method
    func  offlineSavebyCustomerApproveForEditedChangeOrder(){
        
        let offlineName = String(Date().ticks)
        let imgeSignName = "off_\(offlineName).jpg"
        DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.signImage, imageName: imgeSignName)
        
        var changeOrdOfflineId = ""
        if let chOffId  = dicallDetail.object(forKey: "change_order_id_offline") as? String{
            changeOrdOfflineId = chOffId
        }
        
        let dictCustData:  [String: Any] = [
            "change_order_id" : dicallDetail.object(forKey: "change_order_id") ?? "",
            "customer_name" : self.txtCustName.text!,
            "customer_Title" : self.txtTitle.text!,
            "project_id" : dicallDetail.object(forKey: "project_id") ?? "",
            "user_id" : UserDetails.loginUserId,
            "logo_url" : usercompLogoUrl,
            "customer_signature" : imgeSignName
        ]
        
        
        let dictParams:  [String: Any] = [
            "assigner_id" : dicallDetail.object(forKey: "assigner_id") ?? "",
            "authority_email" : dicallDetail.object(forKey: "authority_email") ?? "",
            "change_order_id" : dicallDetail.object(forKey: "change_order_id") ?? "",
            "top_change_order_id" : dicallDetail.object(forKey: "top_change_order_id") ?? "",
            "change_order_id_offline" : changeOrdOfflineId,
            "created_at" : dicallDetail.object(forKey: "created_at") ?? "",
            "job_name" : dicallDetail.object(forKey: "job_name") ?? "",
            "project_id" : dicallDetail.object(forKey: "project_id") ?? "",
            "status" : Constants.STATUS_APPROVED,
            "change_order_items" : dicallDetail.object(forKey: "change_order_items") ?? "",
            "client_name" : dicallDetail.object(forKey: "client_name") ?? "",
            "address" : dicallDetail.object(forKey: "address") ?? "",
            "job_number" : dicallDetail.object(forKey: "job_number") ?? "",
            "approvedBy" :  Constants.CUSTOMER,
            "custApiData" : dictCustData,
            "is_uploaded" : "false",
            "authority_name" : self.txtCustName.text!
        ]
        
        
        let dicF = dictParams as NSDictionary
        AgLog.debug("dic Final for Customer Approve Offline  Edited \(dicF)")
        
        uni_array_allChangeOrders.replaceObject(at: uni_selectedindextoEdit, with: dicF)
        
        DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
        uni_array_allChangeOrders = NSMutableArray()
        self.showAproveSuccessView()
        uni_fromEdit = "NO"
    }
    
    func showAproveSuccessView(){
        
        let screenSize: CGRect = UIScreen.main.bounds
        let   screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        viewApproved.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(screenWidth), height: CGFloat(screenHeight))
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.view.addSubview(self.viewApproved)
    }
    
    @IBAction func btnGotItClicked(_ sender: Any) {
        
        resetAllDeclaredValue()
        AgLog.debug("got it clicked")
        self.viewApproved.removeFromSuperview()
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        self.tabBarController?.tabBar.isHidden = false
        isAnyNewOrderOrChanges = true
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnrefreshClicked(_ sender: Any) {
        statusSign = "NotSigned"
        self.mySignView.clear()
        self.btnResign.isHidden = true
    }
    
    func didStart() {
        AgLog.debug("Started Drawing")
        statusSign = "DoneSign"
        self.btnResign.isHidden = false
    }
    
    // didFinish() is called rigth after the last touch of a gesture is registered in the view.
    // Can be used to enabe scrolling in a scroll view if it has previous been disabled.
    func didFinish() {
        AgLog.debug("Finished Drawing")
        if let signatureImage = self.mySignView.getSignature(scale: 1) {
            signImage = signatureImage
        }
    }
    
    func callCustapprovewithSign() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let parameters:  [String: String] = [
                "change_order_id" : chng_ordID,
                "customer_name" : self.txtCustName.text!,
                "project_id" : selected_projID,
                "user_id" : UserDetails.loginUserId,
                "logo_url" : usercompLogoUrl,
                "title" : self.txtTitle.text!
            ]
            
            AgLog.debug("send to CustApprove \(parameters)")
            
            let strURL = kBaseURL + "customerapproval"
            
            let headers1 = ["Authorization": "123456"]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if let imageData1 = UIImageJPEGRepresentation(self.signImage, 1) {
                    multipartFormData.append(imageData1, withName: "customer_signature", fileName: "custsign.jpg", mimeType: "image/jpeg")
                    AgLog.debug("image Xize \(imageData1.count)")
                }
                
                for (key, value) in parameters {
                    multipartFormData.append((value.data(using: .utf8))!, withName: key)
                    
                    AgLog.debug("multipart \(multipartFormData)")
                }}, to: strURL, method: .post, headers: headers1,
                    encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .success(let upload, _, _):
                            
                            upload.responseJSON { response in
                                
                                response.logShow()
                                if response.isSuccess {
                                    AgLog.debug("Success Upload all data ")
                                    //   self?.gotoChangeOrderPreveiw()
                                    chng_ordID = ""
                                    self.showAproveSuccessView()
                                }
                                else{
                                    Helper.AgAlertView("Error in submit customer signature")
                                }
                                MBProgressHUD.hide(for: self.view, animated: true)
                            }
                        case .failure(let encodingError):
                            AgLog.debug("error:\(encodingError)")
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
            })
        }
    }
}


