//
//  RemoteContactDetail.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/10/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit

class RemoteContactDetail: UIViewController {
    
    @IBOutlet var txtCEmail: UITextField!
    @IBOutlet var txtCMoNum: UITextField!
    @IBOutlet var txtCTitle: UITextField!
    @IBOutlet var txtCName: UITextField!
    
    var CName = ""
    var CNum = ""
    
    var dicContDetail = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        AgLog.debug("conta detail \(dicContDetail)")

        self.txtCName.text = self.dicContDetail.object(forKey: "name") as? String
        self.txtCTitle.text = self.dicContDetail.object(forKey: "title") as? String
        
        let textPhn = self.dicContDetail.object(forKey: "mobile") as? String
        if textPhn?.count == 10{
            let fir = textPhn!.substring(with: 0..<3)
            let midd = textPhn!.substring(with: 3..<6)
            let last = textPhn!.substring(with: 6..<10)
            self.txtCMoNum.text = "(\(fir)) \(midd)-\(last)"
            
        }
        else{
            
            self.txtCMoNum.text = self.dicContDetail.object(forKey: "mobile") as? String
        }
        self.txtCEmail.text = self.dicContDetail.object(forKey: "email") as? String

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

