//
//  AssignOtherController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire
import Contacts
import ContactsUI

class AssignOtherController: UIViewController,CNContactPickerDelegate {
    @IBOutlet var viewAlertDailog: UIView!
    
    @IBOutlet var lblPdfname: UILabel!
    @IBOutlet var txtEmPhone: UITextField!
    
    var emailAssign = ""
    var photneAssifn = ""
    var dicallDetail = NSDictionary()
    
    var myEmail = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let detailUsr = UserDefaults.standard.data(forKey: "USERDETAIL") {
            if let result = NSKeyedUnarchiver.unarchiveObject(with: detailUsr) as? NSDictionary {
                self.myEmail = result.string(forKey: "email")
            }
        }
        
        // Do any additional setup after loading the view.
        txtEmPhone.keyboardType = .emailAddress
        
        if uni_isWorking_Offline == true{
            
            var chID = "\(dicallDetail.object(forKey: "change_order_id") ?? "")"
            
            if chID.contains("_offline"){
                chID = chID.replacingOccurrences(of: "_offline", with: "")
            }
            
            self.lblPdfname.text = "CO : \(chID) \(projName)"
            
        }
        else{
            
            self.lblPdfname.text = "CO : \(chng_ordID) \(projName)"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnLoadFromContactClicked(_ sender: Any) {
        
        let peoplePicker = CNContactPickerViewController()
        
        // peoplePicker.displayedPropertyKeys = @[CNContactEmailAddressesKey];
        peoplePicker.displayedPropertyKeys = [CNContactEmailAddressesKey]
        // peoplePicker.predicateForSelectionOfContact = NSPredicate(format: "emailAddresses.@count > 0")
        
        
        //        peoplePicker.predicateForSelectionOfContact = NSPredicate(format: "postalAddresses.@count <= 1")
        //        peoplePicker.
        peoplePicker.delegate = self
        
        self.present(peoplePicker, animated: true, completion: nil)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact){
        
        AgLog.debug("****\n email address \(contact.emailAddresses)")
        for item in contact.emailAddresses{
            
            if let  email = (item).value(forKey: "value") as? String{
                AgLog.debug("\nEmail", email)
                self.txtEmPhone.text = email
            }
        }
        AgLog.debug(contact.givenName)
    }
    
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        //your code
    }
    
    @IBAction func btnAssignCLicked(_ sender: Any) {
        //   self.navigationController?.popToRootViewController(animated: true)
        
        self.chekTextFields()
    }
    
    func chekTextFields(){
        
        if self.txtEmPhone.isNull {
            Helper.AgAlertView("Enter Email")
            
        }
        else if !self.isValidEmail(testStr: self.txtEmPhone.text!){
            Helper.AgAlertView("Enter Valid Email")
            
        }
        else if (self.myEmail == self.txtEmPhone.text!){
            Helper.AgAlertView("You cannot assign a Change Order to yourself.")
        }
        else{
            emailAssign = self.txtEmPhone.text!
            self.photneAssifn = ""
            
            AgLog.debug("checked all fields Email value is \(emailAssign) and Phone is \(self.photneAssifn)")
            
            if uni_isWorking_Offline {
                if uni_fromEdit == "YES" {
                    self.offlineSaveAssignToOtherEditedChangeOrder()
                    
                }
                else{
                    self.offlineSaveAssignToOtherNewOrder()
                }
            }
            else{
                self.callAssignTootherApi()
                
            }
        }
    }
    
    func offlineSaveAssignToOtherNewOrder(){
        
        let auTname = self.txtEmPhone.text
        let dictAssignedData:  [String: Any] = [
            "email" : self.emailAssign,
            "phone" : self.photneAssifn,
            "change_order_id" : dicallDetail.object(forKey: "change_order_id") ?? ""
        ]
        
        var changeOrdOfflineId = ""
        if let chOffId  = dicallDetail.object(forKey: "change_order_id_offline") as? String{
            changeOrdOfflineId = chOffId
        }
        
        var dictParams: [String: Any] = [
            "assigner_id" : dicallDetail.object(forKey: "assigner_id") ?? "",
            "authority_email" : dicallDetail.object(forKey: "authority_email") ?? "",
            "change_order_id" : dicallDetail.object(forKey: "change_order_id") ?? "" ,
            "top_change_order_id" : dicallDetail.object(forKey: "top_change_order_id") ?? "" ,
            "change_order_id_offline" : changeOrdOfflineId ,
            "created_at" : dicallDetail.object(forKey: "created_at") ?? "",
            "job_name" : dicallDetail.object(forKey: "job_name") ?? "",
            "project_id" : dicallDetail.object(forKey: "project_id") ?? "",
            "status" : Constants.STATUS_ASSIGNED,
            "change_order_items" : dicallDetail.object(forKey: "change_order_items") ?? "",
            "client_name" : dicallDetail.object(forKey: "client_name") ?? "",
            "address" : dicallDetail.object(forKey: "address") ?? "",
            "job_number" : dicallDetail.object(forKey: "job_number") ?? "",
            "approvedBy" :  Constants.ASSIGNER,
            "assignedApiData" : dictAssignedData,
            "is_uploaded" : "false",
            "authority_name" : auTname ?? ""
        ]
        
        if UserDetails.loginUserId == dicallDetail.string(forKey: "assigner_id") {
            dictParams["assigner_id"] = "0"
        }
        
        let dicF = dictParams as NSDictionary
        AgLog.debug("dic Final for Assign Other Offline \(dicF)")
        
        uni_array_allChangeOrders.add(dicF)
        
        DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
        uni_array_allChangeOrders = NSMutableArray()
        self.successAlertAssigned()
        
        uni_fromEdit = "NO"
    }
    
    // Edited Change order:
    
    func offlineSaveAssignToOtherEditedChangeOrder (){
        
        let auTname = self.txtEmPhone.text
        
        let dictAssignedData:  [String: Any] = [
            "email" : self.emailAssign,
            "phone" : self.photneAssifn,
            "change_order_id" : dicallDetail.object(forKey: "change_order_id") ?? ""
        ]
        
        var changeOrdOfflineId = ""
        if let chOffId  = dicallDetail.object(forKey: "change_order_id_offline") as? String{
            changeOrdOfflineId = chOffId
        }
        
        var dictParams:  [String: Any] = [
            "assigner_id" : dicallDetail.object(forKey: "assigner_id") ?? "",
            "authority_email" : dicallDetail.object(forKey: "authority_email") ?? "",
            "change_order_id" : dicallDetail.object(forKey: "change_order_id") ?? "" ,
            "top_change_order_id" : dicallDetail.object(forKey: "top_change_order_id") ?? "" ,
            "change_order_id_offline" : changeOrdOfflineId ,
            "created_at" : dicallDetail.object(forKey: "created_at") ?? "",
            "job_name" : dicallDetail.object(forKey: "job_name") ?? "",
            "project_id" : dicallDetail.object(forKey: "project_id") ?? "",
            "status" : Constants.STATUS_ASSIGNED,
            "change_order_items" : dicallDetail.object(forKey: "change_order_items") ?? "",
            "client_name" : dicallDetail.object(forKey: "client_name") ?? "",
            "address" : dicallDetail.object(forKey: "address") ?? "",
            "job_number" : dicallDetail.object(forKey: "job_number") ?? "",
            "approvedBy" :  Constants.ASSIGNER,
            "assignedApiData" : dictAssignedData,
            "is_uploaded" : "false",
            "authority_name" : auTname ?? ""
        ]
        
        if UserDetails.loginUserId == dicallDetail.string(forKey: "assigner_id") {
            dictParams["assigner_id"] = "0"
        }
        
        let dicF = dictParams as NSDictionary
        AgLog.debug("dic Final for Assign Other Offline  Edited \(dicF)")
        
        uni_array_allChangeOrders.replaceObject(at: uni_selectedindextoEdit, with: dicF)
        
        DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
        uni_array_allChangeOrders = NSMutableArray()
        self.successAlertAssigned()
        
        uni_fromEdit = "NO"
        
        
    }
    
    func checkDigit(){
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func callAssignTootherApi()
    {
        if Reachability.isConnectedToNetwork() == true
        {
            
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "assignchangeorder"
            
            //    email, first_name, last_name, company_name and change_order_id
            
            let dictParams:  [String: String] = [
                "email" : self.emailAssign,
                "phone" : self.photneAssifn,
                "change_order_id" : chng_ordID,
                "first_name" : approve_user_FirstName,
                "last_name" : approve_user_Lastname,
                "company_name" : approve_compny_Name,
                "user_id": UserDetails.loginUserId
                ]

            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                if response.result.value != nil{
                    
                    let dicServer = response.result.value as? NSDictionary ?? NSDictionary()
                    
                    AgLog.debug("Thisis the Dictionary \(dicServer)")
                    
                    let status = dicServer.string(forKey: "status")
                    
                    if status == "success" {
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.successAlertAssigned()
                        
                    }
                    else{
                        
                        Helper.AgAlertView(response.message)
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                }
            }
        }
    }
    
    func successAlertAssigned(){
        
        let screenSize: CGRect = UIScreen.main.bounds
        let   screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        viewAlertDailog.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(screenWidth), height: CGFloat(screenHeight))
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.view.addSubview(self.viewAlertDailog)
    }
    
    func resetAllDeclaredValue(){
        AgLog.debug("reseteddd all uni value")
        
//        arry_ChangeItems = NSMutableArray()
        arry_allOrderID = NSMutableArray()
        //  selected_projID = ""
        prev_Pdf = ""
        
        chng_ordID = ""
        isCostSet = true
    }
    
    @IBAction func btnGotItClicked(_ sender: Any) {
        self.resetAllDeclaredValue()
        self.viewAlertDailog.removeFromSuperview()
        isAnyNewOrderOrChanges = true
        self.navigationController?.popToRootViewController(animated: true)
    }
}
