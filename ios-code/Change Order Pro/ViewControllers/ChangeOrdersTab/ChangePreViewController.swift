//
//  ChangePreViewController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit

class ChangePreViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet var webPreview: UIWebView!
    var myformCreator : MyPdfPreviewCreator!
    var dicallDetail = NSMutableDictionary()
    var htmlFormName = "order_form_1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AgLog.debug("Selected Index is to edited \(uni_selectedindextoEdit)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.title = "CHANGE_ORDER".localized
        
        self.setNavItems()
        
        self.webPreview.delegate = self
        
        if uni_isWorking_Offline != true{
            
            if prev_Pdf.count > 0 {
                let prePath = kBaseURL + prev_Pdf
                let url = URL (string: prePath)
                let requestObj = URLRequest(url: url!)
                
                webPreview.loadRequest(requestObj)
            }
        }
        
        if let cDetail = UserDefaults.standard.data(forKey: "COMPNAYDETAIL") {
            if let resultCompnay = NSKeyedUnarchiver.unarchiveObject(with: cDetail) as? NSDictionary {
                approve_compny_Name = resultCompnay.object(forKey: "company_name") as? String ?? ""
            }
        }
        
        webPreview.backgroundColor = UIColor.white
        
        if uni_isWorking_Offline {
            
            if let template = UserDefaults.standard.object(forKey: "TEMPLATE") as? String{
                
                let tempType = TemplateTypes(rawValue: template) ?? .order_form_1
                AgLog.debug("Selected Template name: \(tempType.name) id: \(tempType.hashValue)")
                htmlFormName = tempType.name
            }
            else{
                AgLog.debug("No template founded")
            }
            
            let selectedPID = "\(dicallDetail.object(forKey: "project_id") ?? "")"
            let newDic = dicallDetail.mutableCopy() as! NSMutableDictionary
            
            for dic in uni_Array_allProjects {
                
                let dicP = dic as? NSDictionary ?? NSDictionary()
                let pId = "\(dicP.object(forKey: "id") ?? "")"
                
                AgLog.debug("SelctedId = \(selectedPID) Cureent ID = \( pId)")
                
                if selectedPID == pId {
                    
                    let clName = dicP.string(forKey: "client_name")
                    let jobNmbr = dicP.string(forKey: "job_number")
                    let contract_no = dicP.string(forKey: "contract_no")
                    let jobNme = dicP.string(forKey: "job_name")
                    let address = dicP.string(forKey: "address")
                    let address1 = dicP.string(forKey: "address1")
                    let city = dicP.string(forKey: "city")
                    let state = dicP.string(forKey: "state")
                    let zip = dicP.string(forKey: "zip")
                    
                    newDic.setValue(clName, forKey: "client_name")
                    newDic.setValue(jobNmbr, forKey: "job_number")
                    newDic.setValue(jobNme, forKey: "job_name")
                    newDic.setValue(contract_no, forKey: "contract_no")
                    newDic.setValue(address, forKey: "address")
                    newDic.setValue(address1, forKey: "address1")
                    newDic.setValue(city, forKey: "city")
                    newDic.setValue(state, forKey: "state")
                    newDic.setValue(zip, forKey: "zip")
                    dicallDetail = newDic
                    
                    self.createofflineHTML()
                    return
                }
            }
            self.createofflineHTML()
        }
    }
    
    func setNavItems(){
        
        let menuBtn = UIBarButtonItem(
            image: UIImage(named: "backWhiteAer"),
            style: .plain,
            target: self,
            action: #selector(backClicked(sender:))
        )
        
        self.navigationItem.leftBarButtonItem = menuBtn
    }
    
    func backClicked(sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
        
        if let previousViewController = self.navigationController?.viewControllers.last as? ChangeOrderItems{
            previousViewController.backFromItem = "PREVIEW"
        }
        
        if let previousViewController = self.navigationController?.viewControllers.last as? EditChangeItemController{
            previousViewController.backFromItemEdit = "PREVIEW"
        }
    }
    
    func createofflineHTML(){

        myformCreator = MyPdfPreviewCreator()
        AgLog.debug("Seleced 2", dicallDetail)
        
        if let invoiceHTML = myformCreator.readAllOrderData(dicData : dicallDetail, fortemplate : htmlFormName){
            
            let baseUrl = Bundle.main.url(forResource: "\(htmlFormName)", withExtension:"html")
            
            webPreview.loadHTMLString(invoiceHTML, baseURL: baseUrl)
            webPreview.scalesPageToFit = true
            self.webPreview.contentMode = .scaleAspectFit
        }
    }
    
    @IBAction func btnSaveClicked(_ sender: Any) {
        
        if uni_isWorking_Offline == true{
            
            dicallDetail.setValue(Constants.STATUS_SAVED, forKey: "status")
            dicallDetail.setValue("false", forKey: "is_uploaded")
            if uni_fromEdit == "YES" {
                
                AgLog.debug("Replace current Order Item with change and save")
                uni_array_allChangeOrders.replaceObject(at: uni_selectedindextoEdit, with: dicallDetail)
                DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
                uni_array_allChangeOrders = NSMutableArray()
                resetAllDeclaredValue()
                isAnyNewOrderOrChanges = true
                self.navigationController?.popToRootViewController(animated: true)
                uni_fromEdit = "NO"
                
            }
            else{
                if uni_fromEdit == "YES" {
                    AgLog.debug("Replace current Order Item with change and save double checked")
                    
                    uni_array_allChangeOrders.replaceObject(at: uni_selectedindextoEdit, with: dicallDetail)
                    DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
                    uni_array_allChangeOrders = NSMutableArray()
                    resetAllDeclaredValue()
                    isAnyNewOrderOrChanges = true
                    
                    self.navigationController?.popToRootViewController(animated: true)
                    uni_fromEdit = "NO"
                }
                else{
                    
                    uni_array_allChangeOrders.add(dicallDetail)
                    DataController.sharedInstance.saveMyChangeOrderData(allChangeOrders: uni_array_allChangeOrders)
                    uni_array_allChangeOrders = NSMutableArray()
                    resetAllDeclaredValue()
                    isAnyNewOrderOrChanges = true
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
        else{
            
            resetAllDeclaredValue()
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func btnrequestAppClicked(_ sender: Any) {
        if CompanyDetailsModel.isCompanyDetails() {
            let apprvOpt = self.storyboard?.instantiateViewController(withIdentifier: "approvView") as! ApprovalOptionController
            apprvOpt.dicallDetail = self.dicallDetail
            self.navigationController?.pushViewController(apprvOpt, animated: true)
        }
        else{
            Helper.AgAlertView("Please setup your company info in app more section.")
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        AGProgress.shared.hideProgress()
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        AGProgress.shared.showProgress(with: "Loading...")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if !webView.isLoading {
            AGProgress.shared.hideProgress()
        }
    }
}

