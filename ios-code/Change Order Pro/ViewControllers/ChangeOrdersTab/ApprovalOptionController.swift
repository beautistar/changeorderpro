//
//  ApprovalOptionController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit

class ApprovalOptionController: UIViewController {
    
    var dicallDetail = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.title = "CHANGE_ORDER".localized
        
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
    
        getSiteContactsForSelectedProject()
        
        if uni_isWorking_Offline == true {
            self.checkAnyCostIsZiro()
        }
    }
    
    func checkAnyCostIsZiro(){
        let arryItem =  dicallDetail.array(forKey: "change_order_items")
        for dic in arryItem.dict{

            let cost = dic.string(forKey: "cost")
            if cost == "0"{
                isCostSet = false
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
    }
    
    @IBAction func btnCustomerClicked(_ sender: Any) {
        if isCostSet == true{
            if uni_isWorking_Offline == true {
                
                let apprvcust = self.storyboard?.instantiateViewController(withIdentifier: "customerApp") as! CustomerApproveController
                apprvcust.dicallDetail = self.dicallDetail
                self.navigationController?.pushViewController(apprvcust, animated: true)
            }
            else{
                
                let apprvcust = self.storyboard?.instantiateViewController(withIdentifier: "customerApp") as! CustomerApproveController
                self.navigationController?.pushViewController(apprvcust, animated: true)
            }
        }
        else{
            Helper.AgAlertView("Please enter a total for customer approval")
        }
    }
    
    @IBAction func btn2RemoteAppClicked(_ sender: Any) {
        
         if isCostSet == true{
            if filterApprover() == true{
                if uni_isWorking_Offline == true {
                
                    let apprvRemote = self.storyboard?.instantiateViewController(withIdentifier: "remoteApprove") as! RemoteApproveController
                    apprvRemote.dicallDetail = self.dicallDetail
                    self.navigationController?.pushViewController(apprvRemote, animated: true)
                }else{
                    let apprvRemote = self.storyboard?.instantiateViewController(withIdentifier: "remoteApprove") as! RemoteApproveController
                    self.navigationController?.pushViewController(apprvRemote, animated: true)
                }
            }
            else{
                Helper.AgAlertView("An approver must be setup under this project in order to use this option.")
            }
         }
         else{
            Helper.AgAlertView("Please enter a total for remote approval")
        }
    }

     var arrySiteCont = NSMutableArray()
    
    func getSiteContactsForSelectedProject(){
        for dicPr in uni_Array_allProjects.dict {
            
            if dicallDetail.string(forKey: "project_id") == dicPr.string(forKey: "id") {
                self.arrySiteCont = dicPr.array(forKey: "site_contact_ids").mutable
            }
//            else if selected_projID == dicPr.string(forKey: "id"){
//                self.arrySiteCont = dicPr.array(forKey: "site_contact_ids").mutable
//            }
//            else if selected_projID == dicPr.string(forKey: "parent_id"){
//                self.arrySiteCont = dicPr.array(forKey: "site_contact_ids").mutable
//            }
//            else if dicallDetail.string(forKey: "change_order_id") == dicPr.string(forKey: "id") {
//                self.arrySiteCont = dicPr.array(forKey: "site_contact_ids").mutable
//            }
//            else if dicallDetail.string(forKey: "main_project_id") == dicPr.string(forKey: "parent_id") {
//                self.arrySiteCont = dicPr.array(forKey: "site_contact_ids").mutable
//            }
        }
    }
 
    func filterApprover()->Bool{
        
        var isFoundApprover = false
        
        for dict in arrySiteCont.dict{
            
            let email = dict.string(forKey: "email")
            if email.count > 0 {
                if let apprver  = dict.object(forKey: "is_approver") as? String {
                    AgLog.debug("approver value is \(apprver)")
                    if apprver == "1"{
                        isFoundApprover = true
                    }
                }
                else if let isapp = dict.object(forKey: "is_approver") as? Bool {
                    
                    if isapp == true {
                        isFoundApprover = true
                    }
                }
            }
            else{
                AgLog.debug("NO email found")
            }
        }
       return isFoundApprover
    }
    
    @IBAction func btn3assignOtherClicked(_ sender: Any) {
        
        if uni_isWorking_Offline == true{
            let assiOther = self.storyboard?.instantiateViewController(withIdentifier: "assignOther") as! AssignOtherController
            assiOther.dicallDetail = self.dicallDetail
            self.navigationController?.pushViewController(assiOther, animated: true)
        }
        else{
            
            let assiOther = self.storyboard?.instantiateViewController(withIdentifier: "assignOther") as! AssignOtherController
            self.navigationController?.pushViewController(assiOther, animated: true)
        }
    }
}


