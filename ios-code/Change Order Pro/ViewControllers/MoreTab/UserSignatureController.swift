//
//  UserSignatureController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 28/02/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire
class UserSignatureController: UIViewController ,YPSignatureDelegate{
    @IBOutlet var mySignView: YPDrawSignatureView!
    @IBOutlet var txtCustName: UITextField!
    
    var statusSign = ""
    var statusNoChagne = ""
    var signImage = UIImage()
    var loginUserName = ""
    @IBOutlet var lblSignRequired: UILabel!
    var signalreadySet = ""
    var signatureID = ""
    
    @IBOutlet var viewSignTopConstrain: NSLayoutConstraint! // Original 158
    
    @IBOutlet var txtTitle: FormTextField!
    @IBOutlet var viewSignBottomConstrain: NSLayoutConstraint! // Original 92
    
    @IBOutlet var btnResign: UIButton!
    
    @IBOutlet var imgSignatuView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if UIDevice.current.userInterfaceIdiom == .phone{
            self.viewSignTopConstrain.constant = 158 + 64
            self.viewSignBottomConstrain.constant = 92
        }
        else{
            self.viewSignTopConstrain.constant = 351 + 64
            self.viewSignBottomConstrain.constant = 197
        }
        
        mySignView.delegate = self
        mySignView.strokeColor = .black
        mySignView.strokeWidth = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.checkUserLoginorNot()
        AgLog.debug("selected Projet ID \(selected_projID)")
        
    }
    
    func checkUserLoginorNot(){
        
        if let detailUsr = UserDefaults.standard.data(forKey: "USERDETAIL") {
            if let result = NSKeyedUnarchiver.unarchiveObject(with: detailUsr) as? NSDictionary {
                
                AgLog.debug("user is logged in  setup info")
                
                let userID = result.integer(forKey: "id")
                
                loginUserName = result.object(forKey: "username") as? String ?? ""
                
                AgLog.debug("User ID is \(UserDetails.loginUserId)")
                
                self.txtCustName.text = "\(result.object(forKey: "first_name") ?? "") \(result.object(forKey: "last_name") ?? "")"
                if let imgSignName = UserDefaults.standard.value(forKey: "MYSIGNATURE") as? String
                {
                    let signSavedName =   "approverSign_\(loginUserName)\(UserDetails.loginUserId).jpg"
                    
                    if imgSignName == signSavedName{
                        
                        if let sID = UserDefaults.standard.value(forKey: "SignatureId") as? String {
                            signalreadySet = "YES"
                            signatureID = sID
                            
                            AgLog.debug("signature Id is \(sID)")
                        }
                        
                        let mySign = DataController.sharedInstance.getImageFromDirFolder(withName: imgSignName)
                        self.imgSignatuView.image = mySign
                        
                        self.signImage = mySign
                        statusSign = "DoneSign"
                        statusNoChagne = "NoChange"
                        if let u_title = UserDefaults.standard.value(forKey: "UserTitle") as? String{
                            self.txtTitle.text = u_title
                        }else{
                            self.txtTitle.text = ""
                        }
                        
                        
                        self.imgSignatuView.isHidden = false
                        btnResign.isHidden = false
                        self.mySignView.isUserInteractionEnabled = false
                    }
                    AgLog.debug("mm\(imgSignName)")
                    
                }
                else{
                    Helper.AgAlertView("Signature will display on all Change Orders approved by you.")
                }
            }
        }
    }
    
    @IBAction func btnClearClicked(_ sender: Any) {
        self.imgSignatuView.isHidden = true
        self.mySignView.isUserInteractionEnabled = true
        UserDefaults.standard.removeObject(forKey: "MYSIGNATURE")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnrefreshClicked(_ sender: Any) {
        statusNoChagne = "something"
        self.imgSignatuView.isHidden = true
        statusSign = "NotSigned"
        self.mySignView.clear()
        self.btnResign.isHidden = true
        self.mySignView.isUserInteractionEnabled = true
    }
    
    func didStart() {
        AgLog.debug("Started Drawing")
        statusNoChagne = "something"
        statusSign = "DoneSign"
        self.btnResign.isHidden = false
    }
    
    // didFinish() is called rigth after the last touch of a gesture is registered in the view.
    // Can be used to enabe scrolling in a scroll view if it has previous been disabled.
    func didFinish() {
        AgLog.debug("Finished Drawing")
        if let signatureImage = self.mySignView.getSignature(scale: 2) {
            signImage = signatureImage
        }
    }
    
    @IBAction func btnAddSignClicked(_ sender: Any) {
        
        if self.txtCustName.isNull{
            Helper.AgAlertView("Enter Name")
        }
        else if self.txtTitle.isNull{
            Helper.AgAlertView("Enter Title")
        }
        else if statusSign != "DoneSign"{
            Helper.AgAlertView("Signature required to submit")
        }
        else{
            if statusNoChagne == "NoChange" {
                self.goBackToProfileView()
            }
            else{
                self.callAPIForSaveMYProfileSignature()
            }
        }
    }
    
    func callAPIForSaveMYProfileSignature(){
        
        let imgeSignName = "approverSign_\(loginUserName)\(UserDetails.loginUserId).jpg"
        AgLog.debug("Signature Image name for Approve order assign to me \(imgeSignName)")
        UserDefaults.standard.set(imgeSignName, forKey: "MYSIGNATURE")
        DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: self.signImage, imageName: imgeSignName)
        
        if Reachability.isConnectedToNetwork() == true {
            
            var parameters  = [String : String]()
            
            var strURL = ""
            if signalreadySet == "YES" {
                
                strURL = kBaseURL + "updatesignature"
                parameters = [
                    "name" : self.txtCustName.text!,
                    "user_id" : UserDetails.loginUserId,
                    "signature_id" : signatureID,
                    "title":self.txtTitle.text!
                ]
            }
            else{
                
                strURL = kBaseURL + "savesignature"
                parameters = [
                    "name" : self.txtCustName.text!,
                    "user_id" : UserDetails.loginUserId,
                    "title":self.txtTitle.text!
                ]
            }
            
            let headers1 = ["Authorization": "123456"]
            
            let tiStm = String(Date().ticks)
            let sigInName = "\(tiStm).jpg"
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if let imageData1 = UIImageJPEGRepresentation(self.signImage, 1) {
                    multipartFormData.append(imageData1, withName: "signature", fileName: sigInName, mimeType: "image/jpeg")
                    AgLog.debug("image Xize \(imageData1.count)")
                }
                
                for (key, value) in parameters {
                    multipartFormData.append((value.data(using: .utf8))!, withName: key)
                    
                    AgLog.debug("multipart \(multipartFormData)")
                }}, to: strURL, method: .post, headers: headers1,
                    encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .success(let upload, _, _):
                            
                            upload.responseJSON { response in
                                
                                response.logShow()
                                
                                if let dict = response.result.value as? NSDictionary {
                                    
                                    let status = dict.string(forKey: "status")
                                    
                                    if status == "success" {
                                        
                                        if let sId = dict.object(forKey: "signature_id") as? Int {
                                            AgLog.debug("signatureId available \(sId)")
                                            UserDefaults.standard.set("\(sId)", forKey: "SignatureId")
                                        }
                                        
                                        Helper.AgAlertView("Your signature saved successfully") {
                                            self.goBackToProfileView()
                                        }
                                    }
                                }
                                MBProgressHUD.hide(for: self.view, animated: true)
                            }
                            
                        case .failure(let encodingError):
                            AgLog.debug("error:\(encodingError)")
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
            })
        }
        else{
            
            Helper.AgAlertView("Your signature saved successfully") {
                self.goBackToProfileView()
            }
        }
    }
    
    func callApiToremoveMysignature(){
        
        if Reachability.isConnectedToNetwork() == true {
            let strURL = kBaseURL + "deletesignature"
            
            let dictParams:  [String: String] = ["signature_id":  self.signatureID]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                
                if response.isSuccess {
                    Helper.AgAlertView("Your signature removed successfully") {
                        self.goBackToProfileView()
                    }
                }
                
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    func goBackToProfileView(){
        
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnRemoveSignCLicked(_ sender: Any) {
        
        statusSign = "NotSigned"
        self.mySignView.isUserInteractionEnabled = true
        self.imgSignatuView.isHidden = true
        self.mySignView.clear()
        UserDefaults.standard.removeObject(forKey: "MYSIGNATURE")
        
        if signalreadySet == "YES" {
            self.callApiToremoveMysignature()
        }
        else{
            self.goBackToProfileView()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = UIInterfaceOrientationMask.all
        }
        
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            
            if UIDevice.current.userInterfaceIdiom == .phone{
                self.viewSignTopConstrain.constant = 40
                self.viewSignBottomConstrain.constant = 0
            }
            else{
                
                self.viewSignTopConstrain.constant = 64
                self.viewSignBottomConstrain.constant = 0
            }
            self.tabBarController?.tabBar.isHidden = true
            lblSignRequired.isHidden = true
            
        }
        else {
            self.tabBarController?.tabBar.isHidden = false
            lblSignRequired.isHidden = false
            
            if UIDevice.current.userInterfaceIdiom == .phone{
                self.viewSignTopConstrain.constant = 158 + 64
                self.viewSignBottomConstrain.constant = 92
            }
            else{
                self.viewSignTopConstrain.constant = 351 + 64
                self.viewSignBottomConstrain.constant = 197
            }
        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        self.tabBarController?.tabBar.isHidden = false
    }
}


