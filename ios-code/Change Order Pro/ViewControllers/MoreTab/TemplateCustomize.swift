//
//  TemplateCustomize.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 20/11/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit

class TemplateCustomize: UIViewController {
    
    @IBOutlet var webView: UIWebView!
    var selected = 0
    @IBOutlet var lbltempSelected: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let pdfName = "CFormat_\(selected)"
        self.lbltempSelected.text = "Template \(selected) Selected"
    
        if let pdf = Bundle.main.url(forResource: pdfName, withExtension: "pdf", subdirectory: nil, localization: nil)  {
            let req = NSURLRequest(url: pdf)
            webView.loadRequest(req as URLRequest)
        }
        Helper.AgAlertView("Template \(selected) has been selected for all Change Orders")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSkipClicked(_ sender: Any) {
        AgLog.debug("show first my Project tab")
        
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.selectedIndex = 0
    }
}
