//
//  CompanyInfoController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import Photos
class CompanyInfoController: UIViewController,UITextFieldDelegate, CameraViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var arryTextFields = [UITextField]()
    
    @IBOutlet var txtCompneyName: UITextField!
    @IBOutlet var txtAddress1: UITextField!
    @IBOutlet var txtAddress2: FormTextField!
    @IBOutlet var txtCity: FormTextField!
    @IBOutlet var txtZip: UITextField!
    @IBOutlet var txtState: UITextField!
    @IBOutlet var txtCountry: UITextField!
    @IBOutlet var txtPhoneNum: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtWebsite: UITextField!
    @IBOutlet var btnCurrenct: UIButton!
    @IBOutlet var bntCreate: UIButton!
    @IBOutlet var imgLogo: UIImageView!
    
    var companyDetails: CompanyDetailsModel? = CompanyDetailsModel.getDetails()
    
    var compnayLogoStatus = ""
    var logoUrl = ""
    var compLogoImage = UIImage()
    
    var isEditCompanyInfo: Bool = false
    let imagePicker = UIImagePickerController()
    let currnecyDropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.compnayLogoStatus = ImageFlag.notSelected
        
        self.callToGetCompnayDetails()
        
        txtCountry.tapped { _ in
            self.openCountryPicker()
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(imageTapped(img:)))
        self.imgLogo.isUserInteractionEnabled = true
        self.imgLogo.addGestureRecognizer(tapGestureRecognizer)
        
        self.adjustforTextFieldReturn()
    }
    
    func openCountryPicker() {
        let picker = MICountryPicker { (name, code) -> () in
            self.txtCountry.text = name
            self.setupCompnayCurrentcy()
        }
        
        picker.didSelectCountryClosure = { name, code in
            self.txtCountry.text = name
            self.setupCompnayCurrentcy()
            picker.navigationController?.popViewController(animated: true)
        }
        
        self.navigationController?.pushViewController(picker, animated: true)
    }
    
    func adjustforTextFieldReturn(){
        
        arryTextFields.append(txtCompneyName)
        arryTextFields.append(txtAddress1)
        arryTextFields.append(txtAddress2)
        arryTextFields.append(txtCity)
        
        arryTextFields.append(txtState)
        arryTextFields.append(txtZip)
        arryTextFields.append(txtPhoneNum)
        arryTextFields.append(txtEmail)
        arryTextFields.append(txtWebsite)
        
        txtCompneyName.delegate = self
        txtAddress1.delegate = self
        txtAddress2.delegate = self
        txtCity.delegate = self
        txtState.delegate = self
        txtZip.delegate = self
        txtCountry.delegate = self
        txtPhoneNum.delegate = self
        txtEmail.delegate = self
        txtWebsite.delegate = self
        
        //arryTextFields.sort { $0.frame.origin.y < $1.frame.origin.y }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let currentIndex = arryTextFields.index(of: textField), currentIndex < arryTextFields.count-1 {
            arryTextFields[currentIndex+1].becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }

    func imageTapped(img: AnyObject) {
        let camera = CameraViewController.getInstant()
        camera.delegate = self
        camera.sourceView = imgLogo
        self.present(camera, animated: true, completion: nil)
    }
    
    func cameraViewControllerDidCancel(_ picker: CameraViewController, isClose: UIView?) {
        if let v = isClose {
            picker.dismiss(animated: true, completion: nil)
            AGImagePickerController(with: self, type: .photoLibrary, allowsEditing: false, iPadSetup: v)
        }
        else{
            picker.dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        
        self.cameraViewController(nil, didFinishPickingMedia: image)
    }
    
    func cameraViewController(_ picker: CameraViewController?, didFinishPickingMedia image: UIImage) {
        if let p = picker {
            p.dismiss(animated: true, completion: nil)
        }
        
        self.compnayLogoStatus = ImageFlag.selected
        let img = resizeImage(image: image, newWidth: 250) as UIImage
        self.imgLogo.image = img
        self.compLogoImage = img
        picker?.dismiss(animated: true, completion: nil);
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(  CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: newWidth, height: newHeight)))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func callToGetCompnayDetails(time: Int = 1) {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "getcompanydetails/" + UserDetails.loginUserId
            
            Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                switch response.result{
                case .success(let value):
                    if let arryServer = value as? NSArray,
                        let dict = arryServer.firstObject as? NSDictionary {

                        self.companyDetails = CompanyDetailsModel(dict: dict)
                        self.setMyCompneyInfo()
                        
                        self.isEditCompanyInfo = true
                        self.bntCreate.setTitle("Update", for: .normal)
                        AgLog.debug("already registerd show detail of compnay")
                    }
                    break
                    
                case .failure:
                    if time != 4 {
                        self.callToGetCompnayDetails(time: time + 1)
                    }
                    break
                }
                
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    func setMyCompneyInfo(){
        
        guard let details = self.companyDetails else {
            return
        }
        
        self.txtCompneyName.text = details.company_name
        self.txtAddress1.text = details.address
        self.txtAddress2.text = details.address1
        self.txtCountry.text = details.country
        self.txtCity.text = details.city
        self.txtState.text = details.state
        self.txtZip.text = details.zip
        self.logoUrl = details.logo_url
        
        if !details.currency.isNull {
            self.btnCurrenct.setTitle(details.currency, for: .normal)
            setupCompnayCurrentcy()
        }
        
        if logoUrl.count > 0{
            
            let path1 = kBaseURL + "public/" + logoUrl
            
            UserDefaults.standard.setValue(path1, forKey: "LOGO_URL")
            
            Alamofire.request(path1).responseImage { response in
                AgLog.debug("respomce Image \(response)")
                if let image = response.result.value {
                    self.compLogoImage = image
                }
            }
            
            if let url = URL(string: path1 ) {
                AgLog.debug("\(url)")
                
                self.imgLogo.af_setImage(
                    withURL:  URL(string: path1)!,
                    placeholderImage: UIImage(named: "load1.gif"),
                    filter: nil,
                    imageTransition: UIImageView.ImageTransition.crossDissolve(0.5),
                    runImageTransitionIfCached: false) { response in
                        if response.response != nil {
                            
                        }
                }
            }
        }
        
        let textPhn = details.phone
        if details.phone.count == 10{
            let fir = textPhn.substring(with: 0..<3)
            let midd = textPhn.substring(with: 3..<6)
            let last = textPhn.substring(with: 6..<10)
            self.txtPhoneNum.text = "(\(fir)) \(midd)-\(last)"
        }
        else{
            self.txtPhoneNum.text = details.phone
        }
        
        self.txtEmail.text = details.email
        self.txtWebsite.text = details.website
    }
    
    func setupCompnayCurrentcy() {
        let allAvailable = CountryWithCurrnecy.getAllAvailable()
        
        currnecyDropDown.dataSource = allAvailable.map { $0.country }
        var currency: String? = ""
        var country: String? = ""

        let edit = allAvailable.filter { return self.txtCountry.text == $0.name }.first
        if edit != nil {
            currency = edit!.curnecy
            country = edit!.name
        }
        
        var isSymblSet: Bool = false
        
        if let cur = currency {
            for i in 0..<allAvailable.count {
                if cur == allAvailable[i].curnecy && allAvailable[i].name == country {
                    Currnecy.current = allAvailable[i].curnecy
                    currnecyDropDown.selectRow(i)
                    self.btnCurrenct.setTitle(allAvailable[i].country, for: .normal)
                    isSymblSet = true
                }
            }
        }
        
        if !isSymblSet {
            if let d = allAvailable.first {
                Currnecy.current = d.curnecy
                currnecyDropDown.selectRow(0)
                self.btnCurrenct.setTitle(d.country, for: .normal)
            }
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if txtCountry == textField {
            self.openCountryPicker()
            return false
        }
        else{
            return true
        }
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if txtCountry == textField { return false }
        
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if textField == self.txtPhoneNum{
            return Helper.phoneNumberFormat(with: self.txtPhoneNum, string: string, str: str)
        }
        else{
            return true
        }
    }
    
    @IBAction func btnCurrencyClicked(_ sender: UIButton) {
        let allAvailable = CountryWithCurrnecy.getAllAvailable()
        currnecyDropDown.anchorView = sender
        currnecyDropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        self.setupCompnayCurrentcy()
        currnecyDropDown.show()
        currnecyDropDown.selectionAction = { [unowned self] (index, item) in
            self.btnCurrenct.setTitle(item, for: .normal)
            for i in 0..<allAvailable.count {
                if item == allAvailable[i].country {
                    Currnecy.current = allAvailable[i].curnecy
                }
            }
        }
    }
    
    @IBAction func btnCreateClicked(_ sender: Any) {
        self.chekTextFields()
    }
    
    func chekTextFields(){
        
        if self.txtCompneyName.isNull{
            Helper.AgAlertView("COMPNAY_NAME".localized)
        }
        else if self.txtAddress1.text == "" {
            Helper.AgAlertView("ADDRESS".localized)
        }
        else if self.txtCity.isNull{
            Helper.AgAlertView("CITY".localized)
        }
        else if self.txtZip.isNull {
            Helper.AgAlertView("ZIP".localized)
        }
        else if self.txtCountry.text == "" {
            Helper.AgAlertView("COUNTRY".localized)
        }
        else if self.txtPhoneNum.text == "" {
            Helper.AgAlertView("PHONE".localized)
        }
        else if !self.txtEmail.isValidEmail {
            Helper.AgAlertView("EMAIL".localized)
        }
        else{
            AgLog.debug("checked all fields")
            calluploadcompneyLogowithDataApi()
        }
    }
    
    func calluploadcompneyLogowithDataApi() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let strURL = kBaseURL + (isEditCompanyInfo ? "editcompany" : "createcompany")

            let dictParams:  [String: String] = [
                "user_id" : UserDetails.loginUserId,
                "company_name" : self.txtCompneyName.text!,
                "address" : self.txtAddress1.text ?? "",
                "address1" : self.txtAddress2.text ?? "",
                "city" : self.txtCity.text!,
                "state" : self.txtState.text!,
                "zip" : self.txtZip.text!,
                "country" : self.txtCountry.text!,
                "phone" : self.txtPhoneNum.text!,
                "website" : txtWebsite.text ?? "",
                "currency" : self.btnCurrenct.titleLabel!.text!,
                "email" : self.txtEmail.text!,
            ]
            
            let headers1 = ["Authorization": "123456"]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if let imageData1 = UIImageJPEGRepresentation(self.compLogoImage, 1) {
                    multipartFormData.append(imageData1, withName: "company_logo", fileName: "companylogo1.jpg", mimeType: "image/jpeg")
                }
                
                for (key, value) in dictParams {
                    multipartFormData.append((value.data(using: .utf8))!, withName: key)
                    
                }}, to: strURL, method: .post, headers: headers1,
                    encodingCompletion: { encodingResult in
                        
                        switch encodingResult {
                        case .success(let upload, _, _):
                            
                            upload.responseJSON { response in

                                response.logShow()
                                
                                if let dict = response.result.value as? NSDictionary {
                                    if response.isSuccess {
                                    
                                        if let arry = dict.object(forKey: "details") as? [NSDictionary],
                                            let dictComp = arry.first {
                                            self.companyDetails = CompanyDetailsModel(dict: dictComp)
                                            self.companyDetails?.downloadLogo()
                                            self.setupCompnayCurrentcy()
                                        }
                                        
                                        Helper.AgAlertView("COMPNY_SUCCESS".localized, okayHandler: {
                                            self.navigationController?.popViewController(animated: true)
                                        })
                                    }
                                    else{
                                        Helper.AgAlertView("An error has occurred. Please try again, if the error persists contact tech support")
                                    }
                                }
                                MBProgressHUD.hide(for: self.view, animated: true)
                            }
                            
                        case .failure(let encodingError):
                            AgLog.debug("error:\(encodingError)")
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Company Info"
    }
}

