//
//  SettingsController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire
class SettingsController: UIViewController {
    
    var uploadBtn = UIBarButtonItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.tabBarController?.tabBar.tintColor = UIColor.white
        self.navigationItem.title = "MORE".localized
    }

    @IBAction func btn1MyCompnyInfoClicked(_ sender: Any) {
        
        let compInfo = self.storyboard?.instantiateViewController(withIdentifier: "compnayInfo") as! CompanyInfoController
        self.navigationController?.pushViewController(compInfo, animated: true)
    }
    
    @IBAction func btn2ChangeOrderClicked(_ sender: Any) {
        
        let custo = self.storyboard?.instantiateViewController(withIdentifier: "customize") as! CustomizeController
        self.navigationController?.pushViewController(custo, animated: true)
    }
    
    @IBAction func btn3ProfileCicked(_ sender: Any) {
        
        let profile = self.storyboard?.instantiateViewController(withIdentifier: "profileView") as! ProfileViewController
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
    @IBAction func paymentProfileCicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let payment = storyboard.instantiateViewController(withIdentifier: "ViewAllPlansViewController") as! ViewAllPlansViewController
        let navi = UINavigationController(rootViewController: payment)
        self.present(navi, animated: true, completion: nil)
    }
    
    @IBAction func btnSendFedCLicked(_ sender: Any) {
        
        let feedbck = self.storyboard?.instantiateViewController(withIdentifier: "feedBack") as! FeedBackController
        self.navigationController?.pushViewController(feedbck, animated: true)
    }

    @IBAction func btnSignOutCicked(_ sender: Any) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        UserDetails.logoutApiCalled()
    }
}

