//
//  ChangePasswordController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 09/11/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import  Alamofire
class ChangePasswordController: UIViewController {

    @IBOutlet var newPassword: FormTextField!
    @IBOutlet var txtOldPassword: FormTextField!
    
    var oldPass = ""

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnChangPassClcike(_ sender: Any) {
        self.chekTextFields()
    }
    
    func chekTextFields(){
        let passOld = UserDefaults.standard.value(forKey: "PASSWORD") as? String ?? ""
        oldPass = passOld
 
        if self.txtOldPassword.isNull {
            Helper.AgAlertView("Enter Old Password")
        }
        else  if self.txtOldPassword.text != passOld {
            Helper.AgAlertView("Old Password is wrong")
        }
        else if (self.newPassword.text?.count)! < 6 {
            Helper.AgAlertView("PASSWORDLENGTH".localized)
            
        }
        else{
            AgLog.debug("checked all fields")
            self.callchangePasswordApi()
            
        }
    }
    
    func callchangePasswordApi() {
        
        if Reachability.isConnectedToNetwork() == true {

            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "changepassword"
       
            let dictParams:  [String: String] = [
                "old_password" : oldPass,
                "new_password" : self.newPassword.text!,
                "user_id" : UserDetails.loginUserId
            ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                
                if response.isSuccess {
                    let userPass = self.newPassword.text!
                    UserDefaults.standard.setValue(userPass, forKey: "PASSWORD")
                    self.navigationController?.popToRootViewController(animated: true)
                }
                else{
                    Helper.AgAlertView(response.message)
                }
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
}
