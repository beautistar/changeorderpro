//
//  CustomizeController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 10/11/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire
class CustomizeController: UIViewController {
    //  UICollectionViewDelegateFlowLayout
    //  UICollectionViewDataSource,UICollectionViewDelegate
    
    @IBOutlet var collTemp: UICollectionView!
    
    @IBOutlet var lbltempSelected: UILabel!
    @IBOutlet var webViePdf: UIWebView!
    var selecteIndex = 1
    
    
    var myTempNum = 1
    @IBOutlet var imgBIg: UIImageView!
    
    var arryTemplet = ["Format_1.jpeg","Format_2.jpeg","Format_3.png","Format_4.jpeg","Format_4.jpeg","Format_4.jpeg",]
    var arryText = ["Template 1","Template 2","Template 3","Template 4"]
    
    @IBOutlet var btnPClose: UIButton!
    // @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var btnNext: UIButton!
   
    @IBOutlet var btnPrevious: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let temp = UserDefaults.standard.object(forKey: "TEMPLATE") as? String{
            selecteIndex = Int(temp) ?? 0
            Helper.AgAlertView("Template \(selecteIndex) is currently the default template")
            setNewPdf()
        }
        else{
            setNewPdf()
        }
        self.setNavItems()
        self.webViePdf.contentMode = .scaleToFill
        self.btnPrevious.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = UIInterfaceOrientationMask.all
        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            self.tabBarController?.tabBar.isHidden = true
        } else {
            self.tabBarController?.tabBar.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLayoutSubviews() {
    
    }
    
    func showCustomizeTemplateAlertFirstTIme(){
        
        let alert = UIAlertController(title: "", message: "Please select the defaults template to use.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK_TEXT".localized, style: .default, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Skip", style: .default, handler:{ (UIAlertAction)in
            self.showAlertOnSkip()
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showAlertOnSkip(){
        
        Helper.AgAlertView("This information can be entered any time under the MORE section.") {
            self.tabBarController?.tabBar.isHidden = false
            self.tabBarController?.selectedIndex = 0
        }
    }

    func setNavItems(){
        let cancleBtn = UIBarButtonItem(
            title: "Use Template",
            style: .plain,
            target: self,
            action: #selector(previewPressed(sender:))
        )
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.rightBarButtonItem = cancleBtn
    }
    func previewPressed(sender: UIBarButtonItem) {
        self.calltoUpldateTemplateApi()
    }
    
    @IBAction func btnPreClicked(_ sender: Any) {
        
        // self.moveToPreviousPage()
        
        if myTempNum == 1{
            
        }else{
            myTempNum = myTempNum - 1
            
            if myTempNum == 1{
                self.btnPrevious.isHidden = true
            }
            self.btnNext.isHidden = false
            setNewPdf()
        }
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        // self.moveToNextPage()
        
        if myTempNum == 4{
            
        }else{
            myTempNum = myTempNum + 1
            
            if myTempNum == 4{
                self.btnNext.isHidden = true
            }
            self.btnPrevious.isHidden = false
            
            self.setNewPdf()
        }
        
    }
    func setNewPdf(){
        
        let namePdf = "CFormat_\(myTempNum)"
        self.lbltempSelected.text = "Template \(myTempNum)"
        if let pdf = Bundle.main.url(forResource: "\(namePdf)", withExtension: "pdf", subdirectory: nil, localization: nil)  {
            let req = NSURLRequest(url: pdf)
            
            webViePdf.loadRequest(req as URLRequest)
            
        }
        
        if selecteIndex == myTempNum {
            
            //  webViePdf.layer.borderWidth = 5
            //    webViePdf.layer.borderColor = UIColor(red: 0.5, green: 0.84, blue: 0.14, alpha: 1.0).cgColor
            //    webViePdf.layer.masksToBounds=true
            
            viewContainer.layer.borderWidth = 5
            viewContainer.layer.borderColor = UIColor(red: 0.5, green: 0.84, blue: 0.14, alpha: 1.0).cgColor
            viewContainer.layer.masksToBounds=true
            
            
            
            
        }else{
            
            //   webViePdf.layer.borderWidth = 0
            
            viewContainer.layer.borderWidth = 0
        }
        
    }
 
    @IBAction func btnPcloseClicked(_ sender: Any) {
        
        //  self.scrollView.removeFromSuperview()
        self.viewContainer.isHidden = true
        self.btnPClose.isHidden = true
    }
    
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnUpdateTemplateClicked(_ sender: Any) {
        
        
    }
    
    func calltoUpldateTemplateApi() {
        
        let seleteTemplate = "\(myTempNum)"
        
        if Reachability.isConnectedToNetwork() == true {

            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "updatetemplate"
            
            let dictParams:  [String: String] = [
                "user_id" : UserDetails.loginUserId,
                "template_id" : seleteTemplate
            ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                if response.isSuccess {
                    
                    UserDefaults.standard.setValue(seleteTemplate, forKey: "TEMPLATE")
                    Helper.AgAlertView("Template \(seleteTemplate) has been selected for all Change Orders", okayHandler: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }
                else{
                    Helper.AgAlertView(response.message)
                }
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
        else {
            UserDefaults.standard.setValue(seleteTemplate, forKey: "TEMPLATE")
            Helper.AgAlertView("Template \(seleteTemplate) has been selected for all Change Orders", okayHandler: {
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
}

