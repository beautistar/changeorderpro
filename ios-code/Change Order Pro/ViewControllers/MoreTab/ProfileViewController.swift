//
//  ProfileViewController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire

extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
}

extension Double {
    func formatNumber() -> String {
        let formater = NumberFormatter()
        formater.groupingSeparator = ","
        formater.numberStyle = .decimal
        return formater.string(from: NSNumber(value: self))!
    }
}

class ProfileViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var viwContainer: UIView!
    
    @IBOutlet var mysignHIghtConstrain: NSLayoutConstraint!
    @IBOutlet var mySign: UIImageView!
    @IBOutlet var lblAddSign: UILabel!
    
    @IBOutlet var txtuserEmail: UITextField!
  //  @IBOutlet var txtUserPhone: UITextField!
    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtLastName: UITextField!
    @IBOutlet var txtUserId: UITextField!
    
    var loginUserName = ""
    var signTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let detailUsr = UserDefaults.standard.data(forKey: "USERDETAIL") {
            if let result = NSKeyedUnarchiver.unarchiveObject(with: detailUsr) as? NSDictionary {
                
                AgLog.debug("user is logged in  setup info")
                
                loginUserName = result.object(forKey: "username") as? String ?? ""
                self.txtUserId.text = loginUserName
                
                self.txtFirstName.text = result.string(forKey: "first_name")
                self.txtLastName.text = result.string(forKey: "last_name")
                self.txtuserEmail.text = result.string(forKey: "email")
                
                // let str = "123456789012354"
                
                let userID = result.integer(forKey: "id")
                UserDetails.loginUserId = "\(userID)"
                
//                let textPhn = result.object(forKey: "phone") as? String
//                if textPhn?.count == 10{
//
//                    let fir = textPhn!.substring(with: 0..<3)
//                    let midd = textPhn!.substring(with: 3..<6)
//                    let last = textPhn!.substring(with: 6..<10)
//                    self.txtUserPhone.text = "(\(fir)) \(midd)-\(last)"
//                }
//                else{
//                    self.txtUserPhone.text = result.object(forKey: "phone") as? String
//                }
                
                self.txtUserId.isUserInteractionEnabled = false  
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
       
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.tabBarController?.tabBar.tintColor = UIColor.white
        
        self.navigationItem.title = "Profile"
        self.setUserSignature()
    }
    
    func setUserSignature(){
        
        // UserDefaults.standard.removeObject(forKey: "MYSIGNATURE")
        if let imgSignName = UserDefaults.standard.value(forKey: "MYSIGNATURE") as? String {
        
            let signSavedName =  "approverSign_\(loginUserName)\(UserDetails.loginUserId).jpg"
            
            if imgSignName == signSavedName{
                
                AgLog.debug("Image Namge for signature \(imgSignName)")
                // imgeSignName = "approverSign_\(offlineName).jpg"
                let imageSign = DataController.sharedInstance.getImageFromDirFolder(withName: imgSignName)
                self.mySign.image = imageSign
                self.lblAddSign.text = "Edit Signature:"
                self.mySign.isHidden = false
                self.mySign.backgroundColor = UIColor.white
                
                self.mysignHIghtConstrain.constant = Helper.size(iPhone: 93, iPad: 221)

                calltoGetUserSignature(isShowLoder: false)
                
            }else{
                AgLog.debug("get image sign from server")
                calltoGetUserSignature(isShowLoder: true)
            }
        
        }else{
            calltoGetUserSignature(isShowLoder: true)
            AgLog.debug("NO sign Setup")
            self.lblAddSign.text = "Add Signature:"
            self.mySign.isHidden = true
            self.mysignHIghtConstrain.constant = 0
        }
    }
    
    @IBAction func btnAddSignatureClicked(_ sender: Any) {
        let userSign = self.storyboard?.instantiateViewController(withIdentifier: "userSIgn") as! UserSignatureController
        self.navigationController?.pushViewController(userSign, animated: true)
    }
    
    @IBAction func btnUpdateProfileCLicked(_ sender: Any) {
        self.chekTextFields()
    }
    
    func chekTextFields(){

        if self.txtUserId.isNull {
            Helper.AgAlertView("NAME".localized)
        }
        else if self.txtFirstName.isNull {
            Helper.AgAlertView("Enter First name")
        }
        else if self.txtLastName.isNull {
            Helper.AgAlertView("Enter Last name")
        }
//        else if self.txtUserPhone.isNull {
//            Helper.AgAlertView("TELEPHONE".localized)
//        }
        else if !self.txtuserEmail.isValidEmail {
            Helper.AgAlertView("EMAIL".localized)
        }
        else{
            AgLog.debug("checked all fields")
            self.callupdateProfile()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
//        if textField == self.txtUserPhone{
//            return Helper.phoneNumberFormat(with: self.txtUserPhone, string: string, str: str)
//        }
//        else{
//            return true
//        }
        return true
    }

    func callupdateProfile() {
    
        if Reachability.isConnectedToNetwork() == true {
          
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "edituser"

            let dictParams:  [String: String] = [
                "first_name" : self.txtFirstName.text ?? "",
            //    "phone" : self.txtUserPhone.text ?? "",
                "email" : self.txtuserEmail.text ?? "",
                "last_name" : self.txtLastName.text ?? "",
                "username" : self.txtUserId.text ?? "",
                "user_id" : UserDetails.loginUserId,
            ]

            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                if let dicServer = response.result.value as? NSDictionary {
                
                    if response.isSuccess {
                        
                        if let userDetail = dicServer["user_details"] as? NSDictionary {
                            UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: userDetail), forKey: "USERDETAIL")
                        }
                        
                        Helper.AgAlertView("Profile updated successfully.", okayHandler: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                    else{
                        self.ShownEmailorPhoneRegisteralready(alertmessage: response.message)
                    }
                }
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    func ShownEmailorPhoneRegisteralready(alertmessage:String){
        
        let alertCon = UIAlertController(title: "", message: alertmessage, preferredStyle: .alert)
        alertCon.addAction(UIAlertAction(title: "Forgot Your Password?", style: .default, handler: {(alertCon: UIAlertAction!) in
                                            
            let pass = self.storyboard?.instantiateViewController(withIdentifier: "password") as! PasswordController
            self.navigationController?.pushViewController(pass, animated: true)
        }))
        
        alertCon.addAction(UIAlertAction(title: "Cancel",
                                         style: .default,
                                         handler: nil))
        self.present(alertCon, animated: true, completion: nil)
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnChngePasswordClicked(_ sender: Any) {
        
        let chPass = self.storyboard?.instantiateViewController(withIdentifier: "chngePassword") as! ChangePasswordController
        self.navigationController?.pushViewController(chPass, animated: true)
    }
    
    func calltoGetUserSignature(isShowLoder:Bool) {

        if Reachability.isConnectedToNetwork() == true {
            if(isShowLoder){
                MBProgressHUD.showAdded(to: self.view, animated: true)
            }
            let strURL = kBaseURL + "getsignature"
            let dictParams:  [String: String] = ["user_id" : UserDetails.loginUserId]
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                if let aryservr = response.result.value as? NSArray {
                
                    if aryservr.count > 0 {
                        
                        let dicSign = aryservr[0] as? NSDictionary ?? NSDictionary()
                        let imgSignName = dicSign.object(forKey: "image") as? String ?? ""
                        let path1 = kBaseURL + "public/" + imgSignName
                        self.mysignHIghtConstrain.constant = Helper.size(iPhone: 93, iPad: 221)
                        
                        Alamofire.request(path1).responseImage { response in
                            
                            if let image = response.result.value {
                                
                                let imgeSignName = "approverSign_\(self.loginUserName)\(UserDetails.loginUserId).jpg"
                                AgLog.debug("Signature Image name for Approve order assign to me \(imgeSignName)")
                                UserDefaults.standard.set(imgeSignName, forKey: "MYSIGNATURE")
                                DataController.sharedInstance.saveImageToDocumentDirFolder(withImage: image, imageName: imgeSignName)
                                self.mySign.image = image
                                self.lblAddSign.text = "Edit Signature:"
                                self.mySign.isHidden = false
                                self.mySign.backgroundColor = UIColor.white
                            }
                        }
        
                        let sId = "\(dicSign.object(forKey: "id") ?? "")"
                        let useTitle = dicSign.object(forKey: "title") as? String ?? ""
                        UserDefaults.standard.set("\(sId)", forKey: "SignatureId")
                        UserDefaults.standard.set("\(useTitle)", forKey: "UserTitle")
                    }
                }
                if(isShowLoder){
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }
        }
    }
}
