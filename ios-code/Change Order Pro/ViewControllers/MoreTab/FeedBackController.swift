//
//  FeedBackController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire
class FeedBackController: UIViewController {
    
    @IBOutlet var txtSubject: UITextField!
    @IBOutlet var txtMessage: UITextView!
    @IBOutlet var viewAlert: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "FEEDBACK".localized
    }
    
    @IBAction func btnFeedSendClicked(_ sender: Any) {
        if self.txtSubject.isNull {
            Helper.AgAlertView("Enter Subject")
        }
        else if self.txtMessage.text == "" || self.txtMessage.text!.count > 500{
            Helper.AgAlertView("Enter Your message in 500 words")
        }
        else{
            AgLog.debug("checked all fields")
            self.callsendFeedBackApi()
        }
    }
    
    func callsendFeedBackApi(){
        if Reachability.isConnectedToNetwork() == true {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "sendfeedback"
            let dictParams:  [String: String] = [
                "subject" : self.txtSubject.text!,
                "feedback" : self.txtMessage.text!,
                "user_id" : UserDetails.loginUserId
            ]
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                response.logShow()
                switch response.result {
                case .success:
                    if response.isSuccess {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.feedBackSuccessAlert()
                    }else{
                        Helper.AgAlertView(response.message)
                    }
                case .failure(let error):
                    if error._code != 4 {
                        Helper.AgAlertView(response.message)
                    }
                }
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    func feedBackSuccessAlert(){
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        viewAlert.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(screenWidth), height: CGFloat(screenHeight))
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.view.addSubview(self.viewAlert)
        
    }
    
    @IBAction func feedGotItclicked(_ sender: Any) {
        self.viewAlert.removeFromSuperview()
        self.navigationController?.popToRootViewController(animated: true)
    }
}
