//
//  CompanyInfoController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import Photos

class FirstTimeCompanyInfoController: UIViewController, UITextFieldDelegate {
    
    var arryTextFields = [UITextField]()
    
    @IBOutlet var txtCompneyName: UITextField!
    @IBOutlet var txtAddress1: UITextField!
    @IBOutlet var txtAddress2: UITextField!
    
    @IBOutlet var txtCity: FormTextField!
    
    @IBOutlet var txtZip: UITextField!
    @IBOutlet var txtState: UITextField!
    @IBOutlet var txtCountry: UITextField!
    @IBOutlet var txtPhoneNum: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtWebsite: UITextField!
    @IBOutlet var btnCurrenct: UIButton!
    @IBOutlet var bntCreate: UIButton!
    
    var compnayLogoStatus = ""
    var logoUrl = ""
    
    @IBOutlet var scrollView: UIScrollView!
    
    var compnyStatus = ""
    let imagePicker = UIImagePickerController()
    let currnecyDropDown = DropDown()
    var dicCompnayInfo = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
        
        self.compnayLogoStatus = ImageFlag.notSelected
        self.calltogetCompnayDetail()
        self.setNavItems()
        
        self.adjustforTextFieldReturn()

        txtCountry.tapped { _ in
            self.openCountryPicker()
        }
    }
    
    func openCountryPicker() {
        let picker = MICountryPicker { (name, code) -> () in
            self.txtCountry.text = name
            self.setupCompnayCurrentcy()
        }
        
        picker.didSelectCountryClosure = { name, code in
            self.txtCountry.text = name
            self.setupCompnayCurrentcy()
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            picker.navigationController?.popViewController(animated: true)
        }
        
        self.navigationController?.pushViewController(picker, animated: true)
    }
    
    func  adjustforTextFieldReturn(){
        
        arryTextFields.append(txtCompneyName)
        arryTextFields.append(txtAddress1)
        arryTextFields.append(txtAddress2)
        arryTextFields.append(txtCity)
        
        arryTextFields.append(txtState)
        arryTextFields.append(txtZip)
        arryTextFields.append(txtPhoneNum)
        arryTextFields.append(txtEmail)
        arryTextFields.append(txtWebsite)
        
        txtCompneyName.delegate = self
        txtAddress1.delegate = self
        txtAddress2.delegate = self
        txtCity.delegate = self
        txtState.delegate = self
        txtZip.delegate = self
        txtCountry.delegate = self
        txtPhoneNum.delegate = self
        txtEmail.delegate = self
        txtWebsite.delegate = self
        
        //arryTextFields.sort { $0.frame.origin.y < $1.frame.origin.y }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        if let currentIndex = arryTextFields.index(of: textField), currentIndex < arryTextFields.count-1 {
            arryTextFields[currentIndex+1].becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func setNavItems(){
        
        self.tabBarController?.tabBar.isHidden = true
        
        let skipBtn = UIBarButtonItem(
            title: "Skip",
            style: .plain,
            target: self,
            action: #selector(skipedPressed(sender:))
        )
        
        self.navigationItem.rightBarButtonItem = skipBtn
        
        self.showCompneyInfoAlertFirstTIme()
    }
    
    func showCompneyInfoAlertFirstTIme(){
        
        let alert = UIAlertController(title: "", message: "Please fill out your company information to show on every Change Order form", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK_TEXT".localized, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Skip", style: .default, handler: { _ in
            self.showAlertOnSkip()
        }))
        
        self.present(alert, animated: true, completion: { })
    }
    
    func showTemplateSelection() {
        UserDefaults.standard.setValue("1", forKey: "is_setup_company_info")
        
        let custo = self.storyboard?.instantiateViewController(withIdentifier: "FirstTimeCompanyLogo") as! FirstTimeCompanyLogo
        self.navigationController?.pushViewController(custo, animated: true)
    }
    
    func skipedPressed(sender: UIBarButtonItem) {
        self.showAlertOnSkip()
    }
    
    func showAlertOnSkip(){
        
        Helper.AgAlertView("You can fill out this information by selecting the My Company Info under the More section.") {
            
            self.showTemplateSelection()
        }
    }
    
    func calltogetCompnayDetail() {
        
        if Reachability.isConnectedToNetwork() == true {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let strURL = kBaseURL + "getcompanydetails/" + UserDetails.loginUserId
            
            Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                if let arryServer = response.result.value as? NSArray {
                    
                    if arryServer.count != 0{
                        self.dicCompnayInfo = arryServer[0] as? NSDictionary ?? NSDictionary()
                        self.setMyCompneyInfo()
                        
                        self.compnyStatus = "alreadyset"
                        self.bntCreate.setTitle("Update", for: .normal)
                        AgLog.debug("already registerd show detail of compnay")
                    }
                }
            }
            
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func setMyCompneyInfo(){
        
        self.txtCompneyName.text = dicCompnayInfo.object(forKey: "company_name") as? String
        self.txtAddress1.text = dicCompnayInfo.object(forKey: "address") as? String
        self.txtAddress2.text = dicCompnayInfo.object(forKey: "address1") as? String
        self.txtCountry.text = dicCompnayInfo.object(forKey: "country") as? String
        self.txtState.text = dicCompnayInfo.object(forKey: "state") as? String
        self.txtCity.text = dicCompnayInfo.object(forKey: "city") as? String
        self.txtZip.text = dicCompnayInfo.object(forKey: "zip") as? String
        
        logoUrl = (dicCompnayInfo.object(forKey: "logo_url") as? String)!
        
        
        let textPhn = dicCompnayInfo.object(forKey: "phone") as? String
        if textPhn?.count == 10{
            let fir = textPhn!.substring(with: 0..<3)
            let midd = textPhn!.substring(with: 3..<6)
            let last = textPhn!.substring(with: 6..<10)
            self.txtPhoneNum.text = "(\(fir)) \(midd)-\(last)"
            
        }else{
            
            self.txtPhoneNum.text = dicCompnayInfo.object(forKey: "phone") as? String
        }
        
        self.txtEmail.text = dicCompnayInfo.object(forKey: "email") as? String
        self.txtWebsite.text = dicCompnayInfo.object(forKey: "website") as? String
        setupCompnayCurrentcy()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if txtCountry == textField {
            self.openCountryPicker()
            return false
        }
        else{
            return true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if txtCountry == textField {
            return false
        }
        
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if textField == self.txtPhoneNum{
            
            return Helper.phoneNumberFormat(with: self.txtPhoneNum, string: string, str: str)
        }
        else{
            return true
        }
    }
    
    func setupCompnayCurrentcy() {
        let allAvailable = CountryWithCurrnecy.getAllAvailable()
        
        currnecyDropDown.dataSource = allAvailable.map { $0.country }
        var currency: String? = ""
        var country: String? = ""
        
        let edit = allAvailable.filter { return self.txtCountry.text == $0.name }.first
        if edit != nil {
            currency = edit!.curnecy
            country = edit!.name
        }
        
        var isSymblSet: Bool = false
        
        if let cur = currency {
            for i in 0..<allAvailable.count {
                if cur == allAvailable[i].curnecy && allAvailable[i].name == country {
                    Currnecy.current = allAvailable[i].curnecy
                    currnecyDropDown.selectRow(i)
                    self.btnCurrenct.setTitle(allAvailable[i].country, for: .normal)
                    isSymblSet = true
                }
            }
        }
        
        if !isSymblSet {
            if let d = allAvailable.first {
                Currnecy.current = d.curnecy
                currnecyDropDown.selectRow(0)
                self.btnCurrenct.setTitle(d.country, for: .normal)
            }
        }
    }
    
    @IBAction func btnCurrencyClicked(_ sender: UIButton) {
        
        currnecyDropDown.anchorView = sender
        currnecyDropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        let allAvailable = CountryWithCurrnecy.getAllAvailable()
        setupCompnayCurrentcy()
        
        currnecyDropDown.show()
        currnecyDropDown.selectionAction = { [unowned self] (index, item) in
            self.btnCurrenct.setTitle(item, for: .normal)
            for i in 0..<allAvailable.count {
                if item == allAvailable[i].country && self.txtCountry.text == allAvailable[i].name {
                    Currnecy.current = allAvailable[i].curnecy
                }
            }
        }
    }
    
    
    @IBAction func btnCreateClicked(_ sender: Any) {
        self.chekTextFields()
    }
    
    @IBAction func btnSkipClicked(_ sender: Any) {
        self.showAlertOnSkip()
    }
    
    func chekTextFields(){

        if self.txtCompneyName.isNull{
            Helper.AgAlertView("COMPNAY_NAME".localized)
        }
        else if self.txtAddress1.isNull {
            Helper.AgAlertView("ADDRESS".localized)
        }
        else if self.txtCity.isNull{
            Helper.AgAlertView("CITY".localized)
        }
        else if self.txtState.isNull{
            Helper.AgAlertView("STATE".localized)
        }
        else if self.txtZip.isNull {
            Helper.AgAlertView("ZIP".localized)
        }
        else if self.txtCountry.text == "" {
            Helper.AgAlertView("COUNTRY".localized)
        }
        else if self.txtPhoneNum.text == "" {
            Helper.AgAlertView("PHONE".localized)
        }
        else if !self.txtEmail.isValidEmail {
            Helper.AgAlertView("EMAIL".localized)
        }
        else{
            calluploadcompneyLogowithDataApi()
        }
    }
    
    func isStringLink(string: String) -> Bool {
        let types: NSTextCheckingResult.CheckingType = [.link]
        let detector = try? NSDataDetector(types: types.rawValue)
        guard (detector != nil && string.count > 0) else { return false }
        if detector!.numberOfMatches(in: string, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, string.count)) > 0 {
            return true
        }
        return false
    }
    
    func calluploadcompneyLogowithDataApi() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            var strURL = ""
            if  self.compnyStatus == "alreadyset"{
                strURL  = kBaseURL + "editcompany"
            }
            else{
                strURL  = kBaseURL + "createcompany"
            }

            let dictParams:  [String: String] = [
                "user_id" : UserDetails.loginUserId,
                "company_name" : self.txtCompneyName.text ?? "",
                "address" : self.txtAddress1.text ?? "",
                "address1" : self.txtAddress2.text ?? "",
                "city" : self.txtCity.text ?? "",
                "state" : self.txtState.text ?? "",
                "zip" : self.txtZip.text ?? "",
                "country" : self.txtCountry.text ?? "",
                "phone" : self.txtPhoneNum.text ?? "",
                "website" : txtWebsite.text ?? "",
                "currency" : self.btnCurrenct.titleLabel?.text ?? "",
                "email" : self.txtEmail.text ?? "",
            ]
            
            let headers1 = ["Authorization": "123456"]
            
            AgLog.debug("Create Compnay", dictParams)
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                for (key, value) in dictParams {
                    multipartFormData.append((value.data(using: .utf8))!, withName: key)
                }}, to: strURL, method: .post, headers: headers1,
                    encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .success(let upload, _, _):
                            
                            upload.responseJSON { [weak self] response in
                                
                                MBProgressHUD.hide(for: self?.view, animated: true)
                                
                                guard self != nil else {
                                    return
                                }
                                
                                response.logShow()
                                
                                if(response.result.value == nil) {
                                    return
                                }
                                
                                let dict = response.result.value as? NSDictionary ?? NSDictionary()
                            
                                if response.isSuccess {
                                    
                                    let arry = dict.object(forKey: "details") as! NSArray
                                    let dictComp = arry[0] as? NSDictionary ?? NSDictionary()
                                    let logoUrl = dictComp.object(forKey: "logo_url") as? String ?? ""
                                    
                                    UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: dictComp), forKey: "COMPNAYDETAIL")
                                    
                                    if logoUrl.count > 0 {
                                        
                                        let path1 = kBaseURL + "public/" + logoUrl
                                        UserDefaults.standard.setValue(path1, forKey: "LOGO_URL")
                                        
                                        Alamofire.request(path1).responseImage { response in
                                            
                                            if let image = response.result.value {
                                                AgLog.debug("logo downloaded: 1\(image)")
                                                let cId = dictComp.integer(forKey: "id")
                                                let cLogoName = "off_logo_\(cId).jpg"
                                                DataController.sharedInstance.saveCompnayLogoImageToDocumentDirFolder(withImage: image, imageName: cLogoName)
                                            }
                                        }
                                        
                                    }
                                    self?.showTemplateSelection()
                                }else{
                                    
                                    Helper.AgAlertView("An error has occurred. Please try again, if the error persists contact tech support")
                                }
                            }
                        case .failure(let encodingError):
                            AgLog.debug("error:\(encodingError)")
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
            })
        }
    }
    
    func showSuccessCompnayRegisterAlert(){
        
        Helper.AgAlertView("COMPNY_SUCCESS".localized) {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.switchViewControllers()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationItem.title = "Company Info"
        
    }
}

@IBDesignable
class FormTextField: UITextField {
    
    @IBInspectable var inset: CGFloat = 0
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: inset, dy: inset)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
}

