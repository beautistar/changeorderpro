//
//  CompanyInfoController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 05/09/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import Photos

class FirstTimeCompanyLogo: UIViewController, CameraViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var bntCreate: UIButton!
    @IBOutlet var imgLogo: UIImageView!
    
    var compnayLogoStatus = ""
    var logoUrl = ""
    var compLogoImage = UIImage()
    
    var compnyStatus = ""
    let imagePicker = UIImagePickerController()
    let currnecyDropDown = DropDown()
    var dicCompnayInfo = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.compnayLogoStatus = ImageFlag.notSelected
        
        self.showCompneyInfoAlertFirstTIme()
        self.calltogetCompnayDetail()
        self.setNavItems()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(imageTapped(img:)))
        self.imgLogo.isUserInteractionEnabled = true
        self.imgLogo.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func setNavItems(){
        
        self.tabBarController?.tabBar.isHidden = true
        
        let skipBtn = UIBarButtonItem(
            title: "Skip",
            style: .plain,
            target: self,
            action: #selector(skipedPressed(sender:))
        )
        self.navigationItem.rightBarButtonItem = skipBtn
    }
    
    
    func showCompneyInfoAlertFirstTIme(){
        
        let alert = UIAlertController(title: "", message: "Please add your company Logo. This Logo will appear on all Change Order forms you create.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK_TEXT".localized, style: .default, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Skip", style: .default, handler:{ (UIAlertAction)in
            self.showAlertOnSkip()
        }))
        
        AppDelegate.topViewController()?.present(alert, animated: true, completion: nil)
    }
    
    func showTemplateSelection() {
        UserDefaults.standard.setValue("1", forKey: "is_setup_company_info")
        
        let custo = self.storyboard?.instantiateViewController(withIdentifier: "FirstTimeCustomizeController") as! FirstTimeCustomizeController
        self.navigationController?.pushViewController(custo, animated: true)
    }
    
    func skipedPressed(sender: UIBarButtonItem) {
        self.showAlertOnSkip()
    }
    
    func showAlertOnSkip(){
        
        Helper.AgAlertView("You can add your company logo at any time under the More section of the App.") {
            self.showTemplateSelection()
        }
    }
    
    @IBAction func btnSkipClicked(_ sender: Any) {
        showAlertOnSkip()
    }
    
    func imageTapped(img: AnyObject) {
        let camera = CameraViewController.getInstant()
        camera.delegate = self
        camera.sourceView = imgLogo
        self.present(camera, animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(  CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: newWidth, height: newHeight)))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func cameraViewControllerDidCancel(_ picker: CameraViewController, isClose: UIView?) {
        if let v = isClose {
            picker.dismiss(animated: true, completion: nil)
            AGImagePickerController(with: self, type: .photoLibrary, allowsEditing: false, iPadSetup: v)
        }
        else{
            picker.dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        
        self.cameraViewController(nil, didFinishPickingMedia: image)
    }
    
    func cameraViewController(_ picker: CameraViewController?, didFinishPickingMedia image: UIImage) {
        if let p = picker {
            p.dismiss(animated: true, completion: nil)
        }
        
        self.compnayLogoStatus = ImageFlag.selected
        let img = resizeImage(image: image, newWidth: 250) as UIImage
        self.imgLogo.image = img
        self.compLogoImage = img
    }
    
    func calltogetCompnayDetail() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "getcompanydetails/" + UserDetails.loginUserId
            
            Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                if let arryServer = response.result.value as? NSArray {
                    
                    if arryServer.count != 0 {
                        self.dicCompnayInfo = arryServer[0] as? NSDictionary ?? NSDictionary()
                        self.setMyCompneyInfo()
                        
                        self.compnyStatus = "alreadyset"
                        self.bntCreate.setTitle("Update", for: .normal)
                        AgLog.debug("already registerd show detail of compnay")
                    }
                }
                
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    func setMyCompneyInfo(){
        
        logoUrl = (dicCompnayInfo.object(forKey: "logo_url") as? String)!
        
        if logoUrl.count > 0 {
            
            let path1 = kBaseURL + "public/" + logoUrl
            UserDefaults.standard.setValue(path1, forKey: "LOGO_URL")
            
            Alamofire.request(path1).responseImage { response in
                AgLog.debug("respomce Image \(response)")
                if let image = response.result.value {
                    self.compLogoImage = image
                }
            }
            
            if let url = URL(string: path1) {
                self.imgLogo.af_setImage(
                    withURL:  url,
                    placeholderImage: UIImage(named: "load1.gif"),
                    filter: nil,
                    imageTransition: UIImageView.ImageTransition.crossDissolve(0.5),
                    runImageTransitionIfCached: false) { response in
                        if response.response != nil {
                            
                        }
                }
            }
        }
    }
    
    @IBAction func btnCreateClicked(_ sender: Any) {
        self.chekTextFields()
    }
    
    func chekTextFields(){
        if self.compnayLogoStatus == ImageFlag.selected {
            calluploadcompneyLogowithDataApi()
        }
        else{
            self.showTemplateSelection()
        }
    }
    
    func isStringLink(string: String) -> Bool {
        let types: NSTextCheckingResult.CheckingType = [.link]
        let detector = try? NSDataDetector(types: types.rawValue)
        guard (detector != nil && string.count > 0) else { return false }
        if detector!.numberOfMatches(in: string, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, string.count)) > 0 {
            return true
        }
        return false
    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func calluploadcompneyLogowithDataApi() {
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            var strURL = ""
            
            strURL  = kBaseURL + "editcompany"
            
            let dictParams:  [String: String] = [
                "user_id" : UserDetails.loginUserId,
                "company_name" : dicCompnayInfo.string(forKey: "company_name"),
                "address" : dicCompnayInfo.string(forKey: "address"),
                "address1" : dicCompnayInfo.string(forKey: "address1"),
                "city" : dicCompnayInfo.string(forKey: "city"),
                "state" : dicCompnayInfo.string(forKey: "state"),
                "zip" : dicCompnayInfo.string(forKey: "zip"),
                "country" : dicCompnayInfo.string(forKey: "country"),
                "phone" : dicCompnayInfo.string(forKey: "phone"),
                "website" : dicCompnayInfo.string(forKey: "website"),
                "currency" : dicCompnayInfo.string(forKey: "currency"),
                "email" : dicCompnayInfo.string(forKey: "email")
            ]
            
            let headers1 = ["Authorization": "123456"]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if let imageData1 = UIImageJPEGRepresentation(self.compLogoImage, 1) {
                    multipartFormData.append(imageData1, withName: "company_logo", fileName: "companylogo1.jpg", mimeType: "image/jpeg")
                }
                
                for (key, value) in dictParams {
                    multipartFormData.append((value.data(using: .utf8))!, withName: key)
                }}, to: strURL, method: .post, headers: headers1,
                    encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .success(let upload, _, _):
                            
                            upload.responseJSON { [weak self] response in
                                guard self != nil else {
                                    return
                                }
                                
                                AgLog.debug(response)
                                
                                guard let dict = response.result.value as? NSDictionary else {
                                    return
                                }
                                
                                if response.isSuccess {
                                    AgLog.debug("Success Upload data")
                                    
                                    let arry = dict.object(forKey: "details") as! NSArray
                                    let dictComp = arry[0] as? NSDictionary ?? NSDictionary()
                                    let logoUrl = dictComp.object(forKey: "logo_url") as? String ?? ""
                                    AgLog.debug("logoUrl is \(logoUrl)")
                                    
                                    UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: dictComp), forKey: "COMPNAYDETAIL")
                                    
                                    if logoUrl.count > 0 {
                                        
                                        let path1 = kBaseURL + "public/" + logoUrl
                                        UserDefaults.standard.setValue(path1, forKey: "LOGO_URL")
                                        
                                        Alamofire.request(path1).responseImage { response in
                                            
                                            if let image = response.result.value {
                                                AgLog.debug("logo downloaded: 1\(image)")
                                                let cId = dictComp.integer(forKey: "id")
                                                let cLogoName = "off_logo_\(cId).jpg"
                                                DataController.sharedInstance.saveCompnayLogoImageToDocumentDirFolder(withImage: image, imageName: cLogoName)
                                            }
                                        }
                                        
                                    }
                                    self?.showTemplateSelection()
                                }
                                else{
                                    Helper.AgAlertView("An error has occurred. Please try again, if the error persists contact tech support")
                                }
                                
                                MBProgressHUD.hide(for: self?.view, animated: true)
                            }
                            
                        case .failure(let encodingError):
                            AgLog.debug("error:\(encodingError)")
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.title = "Company Logo"
    }
}

