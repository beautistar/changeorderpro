//
//  FirstTimeCustomizeController.swift
//  Change Order Pro
//
//  Created by MobiSharnam on 10/11/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire
class FirstTimeCustomizeController: UIViewController {

    @IBOutlet var collTemp: UICollectionView!
    
    @IBOutlet var lbltempSelected: UILabel!
    @IBOutlet var webViePdf: UIWebView!
    var selecteIndex = 1
    
    var myTempNum = 1
    @IBOutlet var imgBIg: UIImageView!

    @IBOutlet var btnPClose: UIButton!
    // @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var btnNext: UIButton!
   
    @IBOutlet var btnPrevious: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let temp = UserDefaults.standard.object(forKey: "TEMPLATE"){
            
            let tem = temp as? String ?? "0"
            selecteIndex = Int(tem) ?? 0
            AgLog.debug("Slected Template is \(selecteIndex)")
            setNewPdf()
            
        }
        else{
            
            setNewPdf()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //  self.setupScrollImages()
        self.setNavItems()

        self.showCustomizeTemplateAlertFirstTIme()
        
        self.webViePdf.contentMode = .scaleToFill
        self.btnPrevious.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = UIInterfaceOrientationMask.all
        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            self.tabBarController?.tabBar.isHidden = true
        } else {
            self.tabBarController?.tabBar.isHidden = false
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func showCustomizeTemplateAlertFirstTIme(){
        
        let alert = UIAlertController(title: "", message: "Please select a default template to use. ", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK_TEXT".localized, style: .default, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Skip", style: .default, handler:{ (UIAlertAction)in
            
            self.showAlertOnSkip()
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertOnSkip(){
        
        Helper.AgAlertView("Template 1 is the default template. This can be changed at any time under the More section.") {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.switchViewControllers()
        }
    }
    
    func setNavItems(){
        
        let cancleBtn = UIBarButtonItem(
            title: "Use Template",
            style: .plain,
            target: self,
            action: #selector(previewPressed(sender:))
        )
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.rightBarButtonItem = cancleBtn
    }
    
    func previewPressed(sender: UIBarButtonItem) {
        self.calltoUpldateTemplateApi()
    }
    
    @IBAction func btnPreClicked(_ sender: Any) {
        // self.moveToPreviousPage()
        if myTempNum == 1{
            
        }
        else{
            myTempNum = myTempNum - 1
            
            if myTempNum == 1{
                self.btnPrevious.isHidden = true
            }
            self.btnNext.isHidden = false
            setNewPdf()
        }
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        // self.moveToNextPage()
        
        if myTempNum == 4{
            
        }
        else{
            myTempNum = myTempNum + 1
            
            if myTempNum == 4{
                self.btnNext.isHidden = true
            }
            self.btnPrevious.isHidden = false
            
            self.setNewPdf()
        }
    }
    
    func setNewPdf(){
        
        let namePdf = "CFormat_\(myTempNum)"
        self.lbltempSelected.text = "Template \(myTempNum)"
        if let pdf = Bundle.main.url(forResource: "\(namePdf)", withExtension: "pdf", subdirectory: nil, localization: nil)  {
            let req = NSURLRequest(url: pdf)
            
            webViePdf.loadRequest(req as URLRequest)
        }
        
        if selecteIndex == myTempNum {
            viewContainer.layer.borderWidth = 5
            viewContainer.layer.borderColor = UIColor(red: 0.5, green: 0.84, blue: 0.14, alpha: 1.0).cgColor
            viewContainer.layer.masksToBounds=true
        }
        else{
            viewContainer.layer.borderWidth = 0
        }
    }

    @IBAction func btnPcloseClicked(_ sender: Any) {
        
        //  self.scrollView.removeFromSuperview()
        self.viewContainer.isHidden = true
        self.btnPClose.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func btnUpdateTemplateClicked(_ sender: Any) {
        
    }
    
    func calltoUpldateTemplateApi() {
        
        let seleteTemplate = "\(myTempNum)"
        
        if Reachability.isConnectedToNetwork() == true {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let strURL = kBaseURL + "updatetemplate"
            
            let dictParams:  [String: String] = [
                "user_id" : UserDetails.loginUserId,
                "template_id" : seleteTemplate
            ]
            
            Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
                
                response.logShow()
                
                if response.isSuccess {
                    
                    UserDefaults.standard.setValue(seleteTemplate, forKey: "TEMPLATE")
                    self.successTemplateUpdate()
                }
                else{
                    Helper.AgAlertView( response.message)
                }
                
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
        else {
            UserDefaults.standard.setValue(seleteTemplate, forKey: "TEMPLATE")
            self.successTemplateUpdate()
        }
    }
 
    func successTemplateUpdate(){
        Helper.AgAlertView("Template updated successfully.") {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.switchViewControllers()
            
        }
    }
}

