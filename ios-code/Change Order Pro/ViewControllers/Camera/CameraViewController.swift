//
//  ViewController.swift
//  camera
//
//  Created by Natalia Terlecka on 10/10/14.
//  Copyright (c) 2014 Imaginary Cloud. All rights reserved.
//

import UIKit
import CameraManager

protocol CameraViewControllerDelegate {
    func cameraViewController(_ picker: CameraViewController?, didFinishPickingMedia image: UIImage)
    func cameraViewControllerDidCancel(_ picker: CameraViewController, isClose: UIView?)
}

class CameraViewController: UIViewController {
    
    // MARK: - Constants
    let cameraManager = CameraManager()
    
    static func getInstant() -> CameraViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
    }
    
    // MARK: - @IBOutlets
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var flashModeImageView: UIImageView!
    @IBOutlet weak var cameraTypeImageView: UIImageView!
    @IBOutlet weak var closeViewImageView: UIImageView!
    
    @IBOutlet weak var gellaryImageView: UIImageView!
    
    @IBOutlet weak var cameraView: UIView!
    
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var vCamera: UIView!
    
    var sourceView: UIView?
    var delegate: CameraViewControllerDelegate?
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()

        cameraManager.shouldFlipFrontCameraImage = false
        cameraManager.shouldRespondToOrientationChanges = true
        cameraManager.showAccessPermissionPopupAutomatically = false
        cameraManager.writeFilesToPhoneLibrary = false
        
        navigationController?.navigationBar.isHidden = true
        
        self.cameraManager.shouldUseLocationServices = false

        let currentCameraState = cameraManager.currentCameraStatus()
        
        if currentCameraState == .notDetermined {
            self.askForCameraPermissions()
        } else if currentCameraState == .ready {
            addCameraToView()
        }

        flashModeImageView.image = UIImage(named: "flash_off")
        if cameraManager.hasFlash {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(changeFlashMode))
            flashModeImageView.addGestureRecognizer(tapGesture)
        }
        
        cameraTypeImageView.image = UIImage(named: "switch_camera")
        let cameraTypeGesture = UITapGestureRecognizer(target: self, action: #selector(changeCameraDevice))
        cameraTypeImageView.addGestureRecognizer(cameraTypeGesture)
        
        closeViewImageView.tapped { _ in
            self.delegate?.cameraViewControllerDidCancel(self, isClose: nil)
        }
        
        gellaryImageView.tapped { _ in
            self.delegate?.cameraViewControllerDidCancel(self, isClose: self.sourceView)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraManager.stopCaptureSession()
    }

    // MARK: - ViewController
    fileprivate func addCameraToView() {
        
        cameraManager.addPreviewLayerToView(cameraView, newCameraOutputMode: CameraOutputMode.stillImage)
        cameraManager.showErrorBlock = { [weak self] (erTitle: String, erMessage: String) -> Void in
        
            let alertController = UIAlertController(title: erTitle, message: erMessage, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alertAction) -> Void in  }))
            
            self?.present(alertController, animated: true, completion: nil)
        }
    }

    // MARK: - @IBActions

    func changeFlashMode() {
        
        switch cameraManager.changeFlashMode() {
        case .off:
            flashModeImageView.image = UIImage(named: "flash_off")
        case .on:
            flashModeImageView.image = UIImage(named: "flash_on")
        case .auto:
            flashModeImageView.image = UIImage(named: "flash_auto")
        }
    }
    
    @IBAction func recordButtonTapped(_ sender: UIButton) {
        
        sender.isEnabled = false
        if cameraManager.cameraOutputMode == .stillImage {
            cameraManager.capturePictureWithCompletion({ (image, error) -> Void in
                if let i = image {
                    self.delegate?.cameraViewController(self, didFinishPickingMedia: i)
                }
                else{
                    self.delegate?.cameraViewControllerDidCancel(self, isClose: self.sourceView)
                    self.cameraManager.showErrorBlock("Error occurred", "Cannot save picture.")
                }
            })
        }
    }
    
    func changeCameraDevice() {
        cameraManager.cameraDevice = cameraManager.cameraDevice == CameraDevice.front ? CameraDevice.back : CameraDevice.front
    }
    
    func askForCameraPermissions() {
        self.cameraManager.askUserForCameraPermission({ permissionGranted in
            if permissionGranted {
                self.addCameraToView()
            }
        })
    }
}
