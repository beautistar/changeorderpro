//
//  TabsViewController.swift
//  Mealvision
//
//  Created by MobiSharnam on 03/01/17.
//  Copyright © 2017 Mobisharnam. All rights reserved.
//

import UIKit

class TabsViewController: UITabBarController,UITabBarControllerDelegate {

    var setSelectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.navigationController?.isNavigationBarHidden = false
        self.delegate = self
        tabBar.tintColor = UIColor.white
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
  
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    
//        if let naviCurrant = viewController as? UINavigationController {
//            if naviCurrant.viewControllers.first is SettingsController {
//
//            }
//            else{
//                if CheckUserDetails.isDeactiveAllPermision {
//                    selectedIndex = 3
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    let payment = storyboard.instantiateViewController(withIdentifier: "ViewAllPlansViewController") as! ViewAllPlansViewController
//                    let navi = UINavigationController(rootViewController: payment)
//                    self.present(navi, animated: true, completion: nil)
//                    return false
//                }
//            }
//        }
        
        return true
    }    
    
    public func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
        
        if let naviCurrant = viewController as? UINavigationController {
        
            if let vc = naviCurrant.viewControllers.first {
                AgLog.debug("TabsViewController index(\(selectedIndex) viewController\(String(describing: vc.self))")
            }
            else{
                AgLog.debug("TabsViewController index(\(selectedIndex)")
            }
            
            if tabBarController.selectedIndex == 0 {
                naviCurrant.popToRootViewController(animated: false)
            }
            else if tabBarController.selectedIndex == 1 { }
            else if tabBarController.selectedIndex == 2 {
                naviCurrant.popToRootViewController(animated: false)
            }
            else if tabBarController.selectedIndex == 3 {
                naviCurrant.popToRootViewController(animated: false)
            }
        }
    }
}
