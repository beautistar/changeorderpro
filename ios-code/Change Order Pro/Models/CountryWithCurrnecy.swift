//
//  CountryWithCurrnecy.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 20/08/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit

struct Currnecy {
    static var current: String {
        get { return UserDefaults.standard.string(forKey: "currentCurrnecy") ?? "$" }
        set { UserDefaults.standard.set(newValue, forKey: "currentCurrnecy") }
    }
}

struct CountryWithCurrnecy{
    var country: String = ""
    var curnecy: String = ""
    var name: String = ""
    
    init(country: String, curnecy: String, name: String) {
        self.country = country
        self.curnecy = curnecy
        self.name = name
    }
}

extension CountryWithCurrnecy {
    static func getAllAvailable() -> [CountryWithCurrnecy]{
        return [
            CountryWithCurrnecy(country: "USD", curnecy: "$", name: "United States"),
            CountryWithCurrnecy(country: "CAD", curnecy: "$", name: "Canada"),
            CountryWithCurrnecy(country: "CNY", curnecy: "¥", name: "China"),
            CountryWithCurrnecy(country: "GBP", curnecy: "£", name: "United Kingdom"),
            CountryWithCurrnecy(country: "INR", curnecy: "₹", name: "India"),
            CountryWithCurrnecy(country: "JPY", curnecy: "¥", name: "Japan"),
            CountryWithCurrnecy(country: "RUB", curnecy: "₽", name: "Russia")
        ]
    }
}
