//
//  UserDetails.swift
//  Change Order Pro
//
//  Created by Gaurav Gudaliya R on 11/07/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit
import SwiftMessages
import Alamofire

struct ImageFlag {
    static let selected = "Selected"
    static let notSelected = "NotSelected"
}

class UserDetails: NSObject {

    static var loginUserId: String {
        get { return UserDefaults.standard.string(forKey: "AppDelegateloginUserId") ?? "" }
        set { UserDefaults.standard.set(newValue, forKey: "AppDelegateloginUserId") }
    }
    static var loginTimeUDID: String {
        get {
            if let udid = UserDefaults.standard.string(forKey: "loginTimeUDID"), udid.isNotNull {
                return udid
            }
            let genrateUdid = UniquiUdid.App.UDID
            UserDefaults.standard.set(genrateUdid, forKey: "loginTimeUDID")
            return genrateUdid
        }
    }
    
    static var isPurchaseReceptSended: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "isPurchaseReceptSended")
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "isPurchaseReceptSended")
        }
    }
    
    static var isPurchaseProduct: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "isPurchaseProduct")
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "isPurchaseProduct")
        }
    }
    
    static var isUserLogin: Bool {
        if let classA = UserDefaults.standard.data(forKey: "USERDETAIL") {
            if (NSKeyedUnarchiver.unarchiveObject(with: classA) as? NSDictionary) != nil {
                return true
            }
        }
        return false
    }
    
    var company_name: String {
        return UserDetails.getCompnayDetailsDict()?.string(forKey: "company_name") ?? ""
    }
    
    static func logoURL() -> String{
        var tempLogoUrl: String = UserDefaults.standard.object(forKey: "LOGO_URL") as? String ?? ""
        
        if let resultCompnay = UserDetails.getCompnayDetailsDict() {
            if let logoUrl = resultCompnay.object(forKey: "logo_url") as? String, logoUrl.isNotNull {
                tempLogoUrl = logoUrl
            }
        }
        
        return tempLogoUrl
    }
    
    static func getCompnayDetailsDict() -> NSDictionary? {
        if let cDetail = UserDefaults.standard.data(forKey: "COMPNAYDETAIL") {
            return NSKeyedUnarchiver.unarchiveObject(with: cDetail) as? NSDictionary
        }
        return nil
    }
    
    static func logoutApiCalled() {
        
        let strURL = kBaseURL + "logout"
        let dictParams:  [String: String] = [
            "user_id" : UserDetails.loginUserId,
            "udid": UserDetails.loginTimeUDID
        ]
        
        if let v = UIApplication.shared.keyWindow {
            MBProgressHUD.showAdded(to: v, animated: true)
        }
        
        Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
            
            switch response.result {
            case .success(_ ):
                if response.isSuccess {
                    UserDetails.clearUserData()
                }
                else{
                    Helper.AgAlertView(response.message)
                }
                break
                
            case .failure(let error):
                 Helper.AgAlertView(error.localizedDescription)
                break
            }
            
            if let v = UIApplication.shared.keyWindow {
                MBProgressHUD.hide(for: v, animated: true)
            }
        }
    }
    
    static func clearUserData() {
        SwiftMessages.hideAll()
        
        if let domain = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            AgLog.debug("Clear ValuesCount :- \(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)")
        }

//        clearAllFilesFromTempDirectory()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.switchBack()
    }
}

func clearAllFilesFromTempDirectory(){
    
    let fileManager = FileManager.default
    let myDocuments = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
    let diskCacheStorageBaseUrl = myDocuments
    guard let filePaths = try? fileManager.contentsOfDirectory(at: diskCacheStorageBaseUrl, includingPropertiesForKeys: nil, options: []) else { return }
    for filePath in filePaths {
        do {
            try fileManager.removeItem(at: filePath)
        }
        catch{
            AgLog.debug(error)
        }
    }
}


