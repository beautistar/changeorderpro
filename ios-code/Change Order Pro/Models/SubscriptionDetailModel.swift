//
//  SubscriptionDetailModel.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 26/07/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit

class SubscriptionDetailModel: NSObject {
    var product_id: String = ""
    var active_plan: Int = 0
    var expires_date_ms: String = ""
    var is_active_sub: Bool = false
    var active_user: Int = 0
    
    override init() {
        super.init()
    }
    
    init(with dictionary: [String: AnyObject]) {
        product_id = dictionary["product_id"] as? String ?? ""
        active_plan = Int(dictionary["active_plan"] as? String ?? "") ?? dictionary["active_plan"] as? Int ?? 0
        expires_date_ms = dictionary["expires_date_ms"] as? String ?? ""
        is_active_sub = (dictionary["is_active_sub"] as? String ?? "0") == "1"
        active_user = Int(dictionary["active_user"] as? String ?? "") ?? dictionary["active_user"] as? Int ?? 0
    }
}
