//
//  PaymentOptionsModel.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 26/07/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit

class PaymentOptionsModel: NSObject {
    var title: String = ""
    var descrip: String = ""
    var billedType: String = ""
    var btnName: String = ""
    var id: String = ""
    var subscriptionId = ""
    
    init(title: String, descrip: String, id: String, billType: String, btnName: String, subscriptionId: String) {
        self.title = title
        self.descrip = descrip
        self.id = id
        self.billedType = billType
        self.btnName = btnName
        self.subscriptionId = subscriptionId
    }
    
    class func get() -> [PaymentOptionsModel] {
        return [
            PaymentOptionsModel(title: "OPTION 1 (1 USER)", descrip: "Full access to one(1) mobile device at a time.", id: "5.00", billType: "($59.99 Billed Annually)", btnName: "$5.00/Month", subscriptionId: "com.changeorderpro.changeorderpro1"),
            
            PaymentOptionsModel(title: "OPTION 2 (3 USERS)", descrip: "Full access to three(3) mobile device at a time under one(1) master account.", id: "12.50", billType: "($149.99 Billed Annually)", btnName: "$12.50/Month", subscriptionId: "com.changeorderpro.changeorderpro3"),
            
            PaymentOptionsModel(title: "OPTION 3 (6 USERS)", descrip: "Full access to six(6) mobile device at a time under one(1) master account.", id: "20.00", billType: "($239.99 Billed Annually)", btnName: "$20.00/Month", subscriptionId: "com.changeorderpro.changeorderpro6"),
            
            PaymentOptionsModel(title: "OPTION 4 (10 USERS)", descrip: "Full access to ten(10) mobile device at a time under one(1) master account.", id: "30.00", billType: "($359.99 Billed Annually)", btnName: "$30.00/Month", subscriptionId: "com.changeorderpro.changeorderpro10")
        ]
    }
}
