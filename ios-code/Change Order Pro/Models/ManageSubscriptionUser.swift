//
//  ManageSubscriptionUser.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 26/07/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit

enum ManageSubscriptionAcceptType: String {
    case ApprovedDeny = "0"
    case Delete = "1"
}

class ManageSubscriptionUser: NSObject {

    var email: String = ""
    var id: String = ""
    var last_name: String = ""
    var first_name: String = ""
    var status: ManageSubscriptionAcceptType = .Delete
    
    init(with response: [String: AnyObject]) {
        email = response["email"] as? String ?? ""
        id = response["id"] as? String ?? ""
        last_name = response["last_name"] as? String ?? ""
        first_name = response["first_name"] as? String ?? ""
//        status = ManageSubscriptionAcceptType(rawValue: response["status"] as? String ?? "") ?? .ApprovedDeny
    }
}
