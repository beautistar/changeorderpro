//
//  CompanyDetailsModel.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 07/08/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire

class CompanyDetailsModel: NSObject {
    
    var id: String = ""
    var user_id: String = ""
    var company_name: String = ""
    var address: String = ""
    var address1: String = ""
    var country: String = ""
    var phone: String = ""
    var email: String = ""
    var city: String = ""
    var state: String = ""
    var zip: String = ""
    var website: String = ""
    var currency: String = ""
    var logo_url: String = ""
    var created_at: String = ""
    var updated_at: String = ""
    
    init(dict response: NSDictionary, isUpdate: Bool = true) {
        super.init()
        
        id = response.string(forKey: "id")
        user_id = response.string(forKey: "user_id")
        company_name = response.string(forKey: "company_name")
        address = response.string(forKey: "address")
        address1 = response.string(forKey: "address1")
        country = response.string(forKey: "country")
        phone = response.string(forKey: "phone")
        email = response.string(forKey: "email")
        city = response.string(forKey: "city")
        state = response.string(forKey: "state")
        zip = response.string(forKey: "zip")
        website = response.string(forKey: "website")
        currency = response.string(forKey: "currency")
        logo_url = response.string(forKey: "logo_url")
        created_at = response.string(forKey: "created_at")
        updated_at = response.string(forKey: "updated_at")
        
        if isUpdate {
            self.update(response: response)
        }
    }
    
    static func isCompanyDetails() -> Bool {
        if let mo = CompanyDetailsModel.getDetails() {
            if mo.company_name.isNotNull, mo.address.isNotNull, mo.phone.isNotNull, mo.email.isNotNull, mo.id.isNotNull, mo.country.isNotNull {
                return true
            }
        }
        
        return false
    }
    
    func downloadLogo(withLogoComplation: ((UIImage) -> ())? = nil) {
        if logo_url.count > 0 {
            
            let path1 = kBaseURL + "public/" + logo_url
            UserDefaults.standard.setValue(path1, forKey: "LOGO_URL")
            
            Alamofire.request(path1).responseImage { response in
                
                if let image = response.result.value {
                    AgLog.debug("logo downloaded: 1\(image)")
                    let cLogoName = "off_logo_\(self.id).jpg"
                    withLogoComplation?(image)
                    DataController.sharedInstance.saveCompnayLogoImageToDocumentDirFolder(withImage: image, imageName: cLogoName)
                }
            }
        }
    }
    
    func update(response: NSDictionary) {
        let encripted = NSKeyedArchiver.archivedData(withRootObject: response)
        UserDefaults.standard.set(encripted, forKey: "COMPNAYDETAIL")
    }
    
    static func getDetails() -> CompanyDetailsModel? {
        if let cDetail = UserDefaults.standard.data(forKey: "COMPNAYDETAIL") {
            if let dict = NSKeyedUnarchiver.unarchiveObject(with: cDetail) as? NSDictionary {
                return CompanyDetailsModel(dict: dict)
            }
        }
        return nil
    }
}
