//
//  CheckUserDetails.swift
//  Change Order Pro
//
//  Created by Wholly-iOS on 03/08/18.
//  Copyright © 2018 Mobisharnam. All rights reserved.
//

import UIKit
import Alamofire
import FirebaseMessaging
import SwiftMessages

var view: MessageView = MessageView.viewFromNib(layout: .statusLine)

class CheckUserDetails: NSObject {
    
    static var shared = CheckUserDetails()
    
    var is_logout: Bool = false
    var is_force_to_logout: Bool = false
    var is_active_sub: Bool = false
    var is_trial: Bool = true
    var trial_days: Int = 14
    var message: String = ""
    
    static var isFullAccess: Bool {
        if CheckUserDetails.shared.is_active_sub {
            return true
        }
        else if (CheckUserDetails.shared.is_trial && CheckUserDetails.shared.trial_days > 0){
            return true
        }
        return false
    }
    
    override init() {
        super.init()
    }
    
    init(with response: NSDictionary, message: String) {
        super.init()
        
        is_logout = response.bool(forKey: "is_logout")
        is_active_sub = response.bool(forKey: "is_active_sub")
        is_force_to_logout = response.bool(forKey: "is_force_to_logout")
        is_trial = response.bool(forKey: "is_trial")
        trial_days = response.integer(forKey: "trial_days")
        self.message = message
        
        if Environment.isDeveloperMode {
            is_trial = false
            is_active_sub = true
        }
        
        if self.is_trial && !self.is_active_sub {
            view.configureContent(title: "", body: message, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: nil , buttonTapHandler: { _ in SwiftMessages.hide() })
            view.configureTheme(.error, iconStyle: .light)
            view.bodyLabel?.font = UIFont.systemFont(ofSize: 12)
            
            var config = SwiftMessages.defaultConfig
            config.presentationStyle = .top
            config.duration = .forever
            config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
            SwiftMessages.show(config: config, view: view)
        }
        else if !self.is_active_sub {
            view.configureContent(title: "", body: message, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: nil , buttonTapHandler: { _ in SwiftMessages.hide() })
            view.configureTheme(.error, iconStyle: .light)
            view.bodyLabel?.font = UIFont.systemFont(ofSize: 12)
            
            var config = SwiftMessages.defaultConfig
            config.presentationStyle = .top
            config.duration = .forever
            config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
            SwiftMessages.show(config: config, view: view)
        }
        
        if !UserDetails.isUserLogin {
           SwiftMessages.hideAll()
        }
        
        self.storeInUserDefault()
    }
    
    func storeInUserDefault() {
        UserDefaults.standard.set(is_logout, forKey: "is_logout")
        UserDefaults.standard.set(is_active_sub, forKey: "is_active_sub")
        UserDefaults.standard.set(is_trial, forKey: "is_trial")
        UserDefaults.standard.set(trial_days, forKey: "trial_days")
        UserDefaults.standard.set(message, forKey: "CheckUserDetailsMessage")
    }
    
    func getInUserDefault() {
        is_logout = UserDefaults.standard.bool(forKey: "is_logout")
        is_active_sub = UserDefaults.standard.bool(forKey: "is_active_sub")
        is_trial = UserDefaults.standard.bool(forKey: "is_trial")
        trial_days = UserDefaults.standard.integer(forKey: "trial_days")
        message = UserDefaults.standard.string(forKey: "CheckUserDetailsMessage") ?? ""
    }
}

extension CheckUserDetails {

    static func checkUser(with time: Int = 1, completion: @escaping (() -> ())) {
        
        let strURL = kBaseURL + "checkuser"
        let dictParams:  [String: String] = [
            "user_id" : UserDetails.loginUserId,
            "udid": UserDetails.loginTimeUDID
        ]
        CheckUserDetails.shared.getInUserDefault()
        
        Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default).responseJSON { response in
            
            response.logShow()
            
            switch response.result {
            case .success(let json ):
                if response.isSuccess {
                    if let dict = (json as? NSDictionary)?.object(forKey: "result") as? NSDictionary {
                        let model = CheckUserDetails(with: dict, message: response.message)
                        CheckUserDetails.shared = model
                        CheckUserDetails.shared.storeInUserDefault()
                        if CheckUserDetails.shared.is_force_to_logout || CheckUserDetails.shared.is_logout {
                            Helper.AgAlertView(title: "", response.message, okayHandler: {
                                UserDetails.clearUserData()
                                clearAllFilesFromTempDirectory()
                            })
                        }
                    }
                }
                else{
                    CheckUserDetails.shared.getInUserDefault()
                    Helper.AgAlertView(response.message)
                }
                break
                
            case .failure(let error):
                CheckUserDetails.shared.getInUserDefault()
                if error._code == 4 {
                    return
                }
                
                if time != 4 {
                    checkUser(with: time + 1, completion: completion)
                }
                else{
                    CheckUserDetails.shared.getInUserDefault()
                }
                break
            }
            
            completion()
        }
    }
    
    static func logoutOtherDevices(time: Int = 1, message: String) {
        
        let strURL = kBaseURL + "logouttoanother"
        let dictParams:  [String: String] = [
            "user_id" : UserDetails.loginUserId,
            "device_token": Messaging.messaging().fcmToken ?? "",
            "device_name": UIDevice.current.name,
            "udid": UserDetails.loginTimeUDID
        ]
        
        Alamofire.request(strURL, method: .post, parameters: dictParams, encoding: URLEncoding.default)
            .responseJSON { response in
            
            response.logShow()
            
            switch response.result {
            case .success:
                if !response.isSuccess {
                    Helper.AgAlertView(response.message)
                }
                CheckUserDetails.checkUser { (userCheckModel) in }
                break
                
            case .failure(let error):

                if error._code == 4 {
                    AGAlertBuilder(withAlert: "", message: message)
                        .addCancelAction(title: "Logout", handler: { _ in
                            UserDetails.clearUserData()
                        })
                        .show()
                    return
                }
                
                if time != 4 {
                    logoutOtherDevices(time: time + 1, message: message)
                }
                else{
                    Helper.AgAlertView(title: "", error.localizedDescription, okayHandler: {
                        Helper.AgAlertView(response.message, okayHandler: {
                            logoutOtherDevices(message: message)
                        })
                    })
                }
                break
            }
        }
    }
}
